#!/bin/bash
set -e
source helpers.sh

SCRIPTS_DIR=`pwd`

if [[ $CODEBUILD_BUILD_ARN == "" ]]
then
  echo "This script is not meant to be used outside code build"
  exit -1
fi
PREFIX=$1
#ACCOUNT=$1
REGION="us-east-1"

if [[ $ACCOUNT == "" ]]
then
  arn=(`echo $CODEBUILD_BUILD_ARN | tr ":" " "`)
  ACCOUNT=${arn[4]}
  ENV=`echo ${arn[5]} | awk -F '_' '{print $NF}'`
fi

aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com

echo "starting Layers"
FUNCTIONS="../layers/*/Dockerfile"
for d in $FUNCTIONS
do
  echo "Processing $d"
  F=`echo $d |sed 's@../layers/@@g' |sed 's@/Dockerfile@@' | sed 's@apis/@@g' |sed 's@functions/@@g' | sed 's@/@@g'`
  echo $F
  if [[ $F != $PREFIX* ]]
  then
    continue
  fi
  VERSION=`grep VERSION ../layers/${F}/Dockerfile |awk -F "=" '{print $NF}' | head -n 1`
  cd ${d%/*}
  if [[ $ENV == 'dev' ]]
  then
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
  else
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
    #Disable version checking for now
#    if [[ `aws ecr list-images --repository-name layers/${F} |grep imageTag |grep ${VERSION}` == "" ]]
#    then
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}:${VERSION}
#      # Also update latest version
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/layers/${F}
#    fi
  fi
  cd $SCRIPTS_DIR
done

echo "starting mongo_ops"
FUNCTIONS="../mongo_ops//*/*/Dockerfile"
for d in $FUNCTIONS
do
  echo "Processing $d"
  F=`echo $d |sed 's@../mongo_ops/@@g' |sed 's@/Dockerfile@@' | sed 's@apis/@@g' |sed 's@functions/@@g' | sed 's@/@@g'`
  if [[ $F != $PREFIX* ]]
  then
    continue
  fi
  VERSION=`grep dkr ../mongo_ops/*/${F}/serverless.yml |awk -F ":" '{print $NF}' | head -n 1`
  cd ${d%/*}
  if [[ $ENV == 'dev' ]]
  then
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
  else
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    # Disable version checking for now
#    if [[ `aws ecr list-images --repository-name ${F} |grep imageTag |grep ${VERSION}` == "" ]]
#    then
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
#      # Also update latest version
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}
#    fi
  fi
  cd $SCRIPTS_DIR
done


echo "starting user_management"
FUNCTIONS="../user_management//*/*/Dockerfile"
for d in $FUNCTIONS
do
  echo "Processing $d"
  F=`echo $d |sed 's@../user_management/@@g' |sed 's@/Dockerfile@@' | sed 's@apis/@@g' |sed 's@functions/@@g' | sed 's@/@@g'`
  if [[ $F != $PREFIX* ]]
  then
    continue
  fi
  VERSION=`grep dkr ../user_management/*/${F}/serverless.yml |awk -F ":" '{print $NF}' | head -n 1`
  cd ${d%/*}
  if [[ $ENV == 'dev' ]]
  then
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
  else
    retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
    # Disable version checking for now
#    if [[ `aws ecr list-images --repository-name ${F} |grep imageTag |grep ${VERSION}` == "" ]]
#    then
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}:${VERSION}
#      # Also update latest version
#      retry docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}
#      retry docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${F}
#    fi
  fi
  cd $SCRIPTS_DIR
done

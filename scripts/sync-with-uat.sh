#!/bin/bash

EXCLUDE=(execute_rule long_polling result_set_detail rule_set_executor run_scheduler execute_external_rule external_rule_processor)

while IFS=  read -r -d $'\0'; do
    P=`echo $REPLY | sed 's@/serverless.yml@@g'`
    F=${P##*/}
    F=`echo $F |grep -v mongo | grep -v user_man`
    if [[ $F == "" ]]
    then
      continue
    fi
    F=`echo $F | awk '{print tolower($0)}' | sed 's/-/_/g'`

    if [[ ! " ${EXCLUDE[*]} " =~ " ${F} " ]]
    then
      A=`find /opt/libretto/ -name $F`
      if [[ $A == "" ]]
      then
        echo "---------"
        echo "Could not find $F"
        echo "---------"
        continue
      fi
      cp -v $P/*{.py,*.js} $A/
    fi

done < <(find ./*/ -name "serverless.yml" -print0)



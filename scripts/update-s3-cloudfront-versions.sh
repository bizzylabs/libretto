#!/bin/bash

arr=( $(aws codebuild list-projects |grep -I INFRA | awk -F '_' '{print $2}') )
stage=`aws codebuild list-projects |grep -I INFRA | awk -F '_' '{print $NF}' |sed 's/"//g' |sed 's/,//g' | head -n 1`

line='"dev" = { '
for i in "${arr[@]}"
do
   L=`aws lambda list-versions-by-function --function-name ${i}_s3_cloudfront |grep '"Version":' | awk '{print $NF}' |grep -v 'LATEST' | tail -n 1 | sed 's/"//g' |sed 's/,//'`
   line=$line"\"${i}\" = \"${L}\", "
done
line=${line::-2}
line=$line' }, #Version of s3 for each customer'

sed -i "/#Version of s3 for each customer in $stage/c\    $line" ../terraform/modules/cloudfront/main.tf

#!/bin/bash
set -e

#echo "AWS_REGION=$AWS_REGION" > .env
if [[ $CODEBUILD_BUILD_ARN == "" ]]
then
  echo "This script is not meant to be used outside code build"
  exit -1
fi

arn=(`echo $CODEBUILD_BUILD_ARN | tr ":" " "`)
echo "ACCOUNT_ID=${arn[4]}" > .env

buildName=(`echo ${arn[5]} | tr "_" " "`)
customer=`echo ${buildName[1]}|sed 's@project/@@g'`

if [[ $(basename `pwd`) == "libretto" ]]
then
  CIDR=`aws ec2 describe-vpcs --filters Name=tag:client,Values=${customer}|grep Cidr |awk '{print $NF}' | head -n 1 |sed 's/"//g' |sed 's/,//g'`
  if [[ $CIDR == "" ]]
  then
    NEXT=$(expr `aws ec2 describe-vpcs |grep '              "CidrBlock":' |grep '10' | grep '/16' |awk '{print $NF}' | head -n 1 |sed 's/"//g' |sed 's/,//g' | awk -F '[.]' '{print $2}'|sort -n |tail -n 1` + 1)
    CIDR="10.$NEXT.0.0/16"
  fi
  echo "CLIENT_CIDRBLOCK=$CIDR" >> .env
fi

STAGE="${buildName[-1]}"
echo "CLIENT_NAME=$customer" >> .env
echo "STAGE=${STAGE}" >> .env

if [[ $REG_AWS_ACCOUNT_ID == "" ]]
then
  REG_AWS_ACCOUNT_ID=${arn[4]}
fi

echo "REG_AWS_ACCOUNT_ID=${REG_AWS_ACCOUNT_ID}" >> .env
echo "DD_API_KEY=${DD_KEY}" >> .env

SG=`aws ec2 describe-security-groups --filters Name=group-name,Values=allow_mongo_egress Name=tag:client,Values=$customer |grep GroupId |awk '{print $NF}' |sed 's/"//g' |sed 's/,//g'`
echo "SG1=${SG}" >> .env


DISTS=($(aws cloudfront list-distributions | jq -r '.DistributionList.Items[].Id'))
for i in "${DISTS[@]}"
do
  output=`aws cloudfront get-distribution --id $i | jq . | grep DomainName |grep "$customer-bizzy-$STAGE" ||true`

  if [[ $output != "" ]]
  then
    DIST_ID=$i
    break
  fi
done

echo "DIST_ID=${DIST_ID}" >> .env

APIGATEWAY_ROLE=`aws iam list-roles | jq '.Roles[].Arn'|grep apigw-cw | sed  's/"//g'`
echo "APIGATEWAY_ROLE=${APIGATEWAY_ROLE}" >> .env

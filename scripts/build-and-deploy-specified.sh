#!/bin/bash
set -e

SCRIPTS_DIR=`pwd`

if [[ $CODEBUILD_BUILD_ARN == "" ]]
then
  echo "This script is not meant to be used outside code build"
  exit -1
fi
#PREFIX=$1
#ACCOUNT=$1
REGION="us-east-1"

arn=(`echo $CODEBUILD_BUILD_ARN | tr ":" " "`)
ENV=`echo ${arn[5]} | awk -F '_' '{print $NF}'`

if [[ $ACCOUNT == "" ]]
then
  ACCOUNT=${arn[4]}
fi

aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com

#FUNCTION='add_rules,admin_set_user_password'

echo "-----"
echo ${FUNCTIONS}
echo "-----"

IFS=', ' read -r -a array <<< "${FUNCTIONS}"
for f in ${array[@]}
do
  dir=`find ../ -type d -name "$f"`
  cd $dir
  VERSION=`grep dkr serverless.yml |awk -F ":" '{print $NF}' | head -n 1`

  docker build ./ --build-arg ACCOUNT=${ACCOUNT} -t ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${f}:${VERSION}
  docker push ${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com/${f}:${VERSION}

  funcName=`head -n 1 serverless.yml|sed 's/://g'`
  echo $funcName
  cd ../
  ${SCRIPTS_DIR}/generate-env.sh
  cat .env
  pwd
  cat serverless.yml
  sls deploy function --function $funcName --stage $STAGE
#  sls deploy function --function `grep "CLIENT_NAME" .env | awk -F = '{print $NF}'`_$f --stage $STAGE
  cd $SCRIPTS_DIR
done

#!/bin/bash
set -e

array=()
while IFS=  read -r -d $'\0'; do
    F=`echo $REPLY |sed 's@../mongo_ops/@@g' |sed 's@/serverless.yml@@' |sed 's@apis/@@g' | sed 's@functions/@@g' |sed 's@/@@g' | sed 's@serverless.yml@@g'`
    echo $F
    array+=("$F")
done < <(find ../mongo_ops/ -name "serverless.yml" -print0)

while IFS=  read -r -d $'\0'; do
    F=`echo $REPLY |sed 's@../user_management/@@g' |sed 's@/serverless.yml@@'| sed 's@apis/@@g' | sed 's@functions/@@g'| sed 's@/@@g' | sed 's@serverless.yml@@g'`

    array+=("$F")
done < <(find ../user_management/ -name "serverless.yml" -print0)


IFS=$'\n' sorted=($(sort <<<"${array[*]}"))
unset IFS

FUNCTIONS=`echo $(printf "\"%s\", " "${sorted[@]}")`
FUNCTIONS=`echo "[" ${FUNCTIONS::${#FUNCTIONS}-1} "]"`
echo $FUNCTIONS


sed -i "\@functions = \[@c\ \ \ \ \ functions = $FUNCTIONS" ../terraform/modules/ecr/main.tf

array2=(`ls ../layers/`)
IFS=$'\n' sorted2=($(sort -u <<<"${array2[*]}"))
unset IFS

LAYERS=`echo $(printf "\"%s\", " "${sorted2[@]}")`
LAYERS=`echo "[" ${LAYERS::${#LAYERS}-1} "]"`
echo $LAYERS


sed -i "\@layers = \[@c\ \ \ \ \ layers = $LAYERS" ../terraform/modules/ecr/main.tf

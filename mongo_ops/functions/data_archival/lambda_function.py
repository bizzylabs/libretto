import datetime
import json
import os
from datetime import datetime, timedelta
import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

# Number of runIds which should be kept in result data apart from runIds which were run in last 24hrs.
ACTIVE_RUN_DATA = 1
ARCHIVE_FLAG = 'IsArchived'
# Max number of runIds which should be archived during one lambda invocation
MAX_ARCHIVE_IN_SINGLE_INVOCATION = 5


def run_id_to_dateStr(run_id):
    dt_object = datetime.fromtimestamp(run_id / 1000)
    return dt_object.strftime("%d/%b/%Y, %H:%M:%S")


def archive_run_data(scheduled_history):
    run_id = scheduled_history["RunId"]
    print(f'Archiving run_id:{run_id} which is {run_id_to_dateStr(run_id)}')
    db.ResultSetDetail.aggregate([{"$match": {"RunId": {"$eq": run_id}}}, {"$merge": "ResultSetDetailArchive"}],
                                 allowDiskUse=True)
    print(f'RunId: {run_id} moved to archive collection')
    db.ResultSetDetail.remove({"RunId": {"$eq": run_id}})
    print(f'RunId: {run_id} deleted from ResultSetDetail collection')

    # Marking IsArchived flag to True for run_id
    mark_as_archived(scheduled_history)


def mark_as_archived(scheduled_history):
    run_id = scheduled_history["RunId"]
    if run_id:
        db.SchedulerHistory.update(
            {"RunId": run_id},
            {
                "$set": {
                    ARCHIVE_FLAG: True,
                    "UpdatedOn": datetime.now()
                }
            }
        )
        print(f'RunId: {run_id} archive flag is set to True')


def lambda_handler(event, context):
    unique_loan_package_id = db.SchedulerHistory.distinct("LoanPackage")
    print(f'Unique loan_package_ids in DB {unique_loan_package_id}')
    previous_day = (datetime.now() - timedelta(days=1))
    one_day_old_run_id = int(previous_day.timestamp() * 1000)

    for each_loan_id in unique_loan_package_id:
        # Filter unarchived scheduler history with RunIds less than (now-1day)
        scheduled_history_list = list(db.SchedulerHistory.find({"$and": [{"LoanPackage": ObjectId(each_loan_id)},
                                                                         {ARCHIVE_FLAG: {"$ne": True}},
                                                                         {"RunId": {"$lte": one_day_old_run_id}}]}))
        # sort by runIDs in DESC order
        scheduled_history_list.sort(key=lambda x: x["RunId"], reverse=True)
        archive_candidate = []
        active_run_ids = []
        non_active_run_ids = []

        for history in scheduled_history_list:
            if len(active_run_ids) >= ACTIVE_RUN_DATA:
                archive_candidate.append(history)
                continue
            if history.get('IsActive', -1) == 1:
                active_run_ids.append(history)
            else:
                non_active_run_ids.append(history)
        archive_count = 0
        for i in range(len(archive_candidate)):
            print(f'processing {i + 1} document out of {len(archive_candidate)}')
            if i >= MAX_ARCHIVE_IN_SINGLE_INVOCATION:
                print(f"Invoking again to avoid timeout after archiving {archive_count} runs")
                lambda_invoke = boto3.client("lambda", region_name="us-east-1")
                payload = {
                }
                lambda_invoke.invoke(
                    FunctionName=os.environ["DATA_ARCHIVE_FUNC_NAME"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                break
            else:
                archive_run_data(archive_candidate[i])
                archive_count += 1

        return {
            "archived_runs": archive_count,
            "is_complete": len(archive_candidate) <= MAX_ARCHIVE_IN_SINGLE_INVOCATION
        }

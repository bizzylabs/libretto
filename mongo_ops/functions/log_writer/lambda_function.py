import json
import os
from datetime import datetime

import boto3
from pymongo import MongoClient


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def lambda_handler(event, context):
    msg_body = json.loads(event["Records"][0]["body"])
    msg_body['timestamp'] = datetime.fromisoformat(msg_body.get('timestamp', str(datetime.now())))
    database = get_initialized_db()
    database.BussinessAuditLogs.insert_one(msg_body)
    username = msg_body["username"]
    request_type = msg_body["action"]
    if request_type in ["timeout", "logout", "timeout/logout"]:
        database.LockRules.remove({"LockedBy": username})
    if request_type == "RuleDeleted":
        database.RuleHistory.insert(msg_body)
    return {"result": "activity logged", "status": 200}

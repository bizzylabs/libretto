import json
import os

import boto3

from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
sns_client = boto3.client('sns', region_name="us-east-1")


def getSchedulerName(schedulerId):
    scheduler = db.SchedulerConfig.find_one({"_id": ObjectId(schedulerId)})
    return scheduler["Name"]


def lambda_handler(event, context):
    print("Received event: " + json.dumps(event))

    try:
        response = {}
        print(event)
        response["StateMachineName"] = event["detail"]["stateMachineArn"].split(":")[-1]
        event_input = json.loads(event["detail"]["input"])
        response["SchedulerId"] = event_input["SchedulerId"]
        response["LoanPackageId"] = event_input["LoanPackageId"]
        response["LoanPackageNam"] = event_input["LoanPackageName"]
        print(event_input["SchedulerId"])
        response["SchedulerName"] = getSchedulerName(event_input["SchedulerId"])
        db.SchedulerConfig.update({"_id": ObjectId(event_input["SchedulerId"])},
                                  {"$set": {"LastRun": datetime.now().strftime("%Y-%m-%d, %H:%M:%S")}})

        if "output" in event["detail"]:
            event_output = json.loads(event["detail"].get("output", None))
        else:
            return {"status": 500, "body": "Missing parameter value for output in detail"}

        if 'isError' in event_output.keys() and event_output["isError"] == True:
            if event_output["cause"] == "Invalid EndDate":
                event["detail"]["status"] = "EXPIRED"
                event["detail"]["output"] = "Scheduler inactivated"
            else:
                event["detail"]["status"] = "FAILED"
                event["detail"]["output"] = event_output["cause"]
            # Update status of current run in scheduler - for UI
            db.SchedulerConfig.update({"_id": ObjectId(event_input["SchedulerId"])},
                                      {"$set": {"CurrentStatus": "Failed"}})
        else:
            event["detail"]["output"] = json.loads(event["detail"]["output"])

        if event["detail"]["status"] == "SUCCEEDED":
            # Update status of current run in scheduler - for UI
            db.SchedulerConfig.update({"_id": ObjectId(event_input["SchedulerId"])},
                                      {"$set": {"CurrentStatus": "Update in progress - UI refresh"}})
        elif event["detail"]["status"] == "FAILED":
            # Update status of current run in scheduler - for UI
            db.SchedulerConfig.update({"_id": ObjectId(event_input["SchedulerId"])},
                                      {"$set": {"CurrentStatus": "Failed"}})

        response["Status"] = event["detail"]["status"]
        response["ResultDetail"] = event["detail"]["output"]
        client_details = list(db.ApplicationDetails.find({}))

        if "Client" in client_details[0].keys():
            response["Client"] = client_details[0]["Client"]
        if "Environment" in client_details[0].keys():
            response["Environment"] = client_details[0]["Environment"]

        response = sns_client.publish(
            TargetArn=os.environ["SNS_TOPIC_ARN"],
            Message=json.dumps({'default': json.dumps(response).replace('",', '",\n')}),
            MessageStructure='json'
        )
        return True

    except Exception as e:
        print(e)
        return {"error": str(e)}

import datetime
import json
import os
import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient
import traceback
import time
from datadog_lambda.metric import lambda_metric

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.client('s3')
lambda_client = boto3.client("lambda")


def data_formatter(projection, data):
    non_displayable_field = [key for key in projection.keys() if not data.get(key, None)]
    projection.update(data)
    for item in non_displayable_field:
        projection[item] = None
    return projection


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def get_data(file_name):
    try:
        obj = s3.get_object(Bucket=os.environ["BRE_BUCKET"], Key=file_name)
        csv_string = obj['Body'].read().decode("utf-8")
        data = eval(csv_string)

    except Exception as e:
        data = []
        traceback.print_exc()

    return data


def lambda_handler(event, context):
    first_start_time = round(time.time() * 1000)
    run_id = event["run_id"]
    rule_set_id = event["rule_set_id"]
    rule = event["rule"]
    collection = event["collection"]
    loan_file_name = event["loan_file"]
    user_filter = event["user_filter"]
    offset = event["offset"]
    limit = event["limit"]
    display_field = event["display_fields"]
    user_filter_exception = user_filter.copy()
    business_rule_id = rule["RuleNameId"]
    last_counter = event["last_counter"]
    exception_count = event["exception_count"]
    loan_package_id = event["loan_package_id"]
    try:
        display_fields = eval(display_field)
        temp = display_fields.copy()
    except Exception as e:
        print(e)
        traceback.print_exc()
        display_fields = {}
        temp = {}

    try:
        exception_status = True
        system_note = []
        note_detail = None
        now = datetime.datetime.now()
        print("reading file starts")
        loans = get_data(loan_file_name)[offset:limit]
        print("file read ends")
        print(event)
        print("Starting Message push")
        rule_set = get_rule_set(rule_set_id)
        lambda_metric(
            metric_name='bizzy.bre.rule.loans',
            value=len(loans),
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'rule_id:' + str(rule["_id"]), 'ruletype:Internal']
        )
        for loan_data in loans:
            loan = data_formatter(temp, loan_data)
            filter_info = {fil: loan.get(fil, None) for fil in user_filter_exception.keys()}
            common = set(display_fields).intersection(set(user_filter))
            if common:
                [user_filter.pop(item) for item in common if item in loan]

            [loan.pop(item) for item in user_filter.keys() if item in loan]
            result_set_detail = {
                "RunId": run_id,
                "RuleSetId": ObjectId(rule_set_id),
                "RuleId": ObjectId(rule["_id"]),
                "RuleNameId": business_rule_id,
                "RuleVersion": rule["Version"],
                "ExceptionStatus": exception_status,
                "Exception": rule["Name"],
                "LoanNumber": loan["LoanNumber"],
                "LoanInfo": loan,
                "InExistence": now,
                "CreatedOn": now,
                "UpdatedOn": datetime.datetime(2200, 1, 1, 0, 0, 0),
                "SystemNote": system_note,
                "FilterInfo": filter_info,
                "Severity": rule["Severity"],
                "Priority": rule["Priority"],
                "NoteDetail": note_detail,
                "Ignore": False
            }
            try:
                start_time = round(time.time() * 1000)
                db.ResultSetDetail.insert(result_set_detail)
                lambda_metric(
                    metric_name='bizzy.bre.db.insert',
                    value=round(time.time() * 1000) - start_time,
                    timestamp=int(time.time()),  # optional, must be within last 20 mins
                    tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                          'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'collection:ResultSetDetail',
                          'ruletype:Internal']
                )
            except Exception as e:
                print(e)
                traceback.print_exc()
                pass

        print("Message push ends")

        if last_counter == 1:
            if exception_count > 0:
                off_set = 0
                li_mit = 2000
                val = int(exception_count / 2000) + 1
                for item in range(val):
                    rule["_id"] = str(rule["_id"])
                    payload = {
                        "loan_file": loan_file_name,
                        "rule": rule,
                        "rule_id": str(rule["_id"]),
                        "rule_set_id": str(rule_set_id),
                        "run_id": run_id,
                        "collection": collection,
                        "offset": off_set,
                        "limit": li_mit,
                        "loan_package_id": str(loan_package_id)
                    }
                    lambda_client.invoke(
                        FunctionName=os.environ["EXECUTE_RULE_PREV_DATA_FUNC"],
                        InvocationType='Event',
                        Payload=json.dumps(payload)
                    )
                    off_set += 2000
                    li_mit += 2000

            rule["_id"] = str(rule["_id"])
            payload = {
                "loan_file": loan_file_name,
                "rule": rule,
                "rule_id": str(rule["_id"]),
                "rule_set_id": str(rule_set_id),
                "run_id": run_id,
                "collection": collection,
                "loan_package_id": str(loan_package_id)
            }
            lambda_client.invoke(
                FunctionName=os.environ["BRE_CHECK_CURED_FUNC"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
        lambda_metric(
            metric_name='bizzy.bre.rule.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'ruletype:Internal']
        )

    except Exception as e:
        print("++++++++++++++", e, "++++++++++++++++")
        traceback.print_exc()
        start_time = round(time.time() * 1000)
        db.ResultSetDetail.update(
            {"RuleId": ObjectId(rule["_id"]), "RuleSetId": ObjectId(rule_set_id), "RunId": run_id},
            {"$set": {"ExceptionStatus": None}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'collection:ResultSetDetail',
                  'ruletype:Internal']
        )
        start_time = round(time.time() * 1000)
        db.RuleSummary.update({"RuleId": ObjectId(rule["_id"]), "RuleSetId": ObjectId(rule_set_id), "RunId": run_id}, {
            "$set": {"ExecutionCompleted": False, "ErrorMessage": "Error during writing exceptions"}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:Internal']
        )
    return True

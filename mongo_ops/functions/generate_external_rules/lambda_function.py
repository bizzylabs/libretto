import json
import os  
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3 
import io 
import pandas as pd
import paramiko

file_name = os.environ["FILE_NAME"]
secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]

def export_file_to_sftp():
    try:
        secret = boto3.client("secretsmanager")
        secret_response = secret.get_secret_value(SecretId=os.environ["SFTP_SECRET_ID"])
        secret_dict = json.loads(secret_response["SecretString"])
        host = secret_dict["host"]
        port = int(secret_dict["port"])
        username = secret_dict["username"]
        password = secret_dict["password"]
        sftpLocation = secret_dict["sftpLocation"]
        transport = paramiko.Transport((host, port))
        transport.connect(username = username, password = password)
        sftp = paramiko.SFTPClient.from_transport(transport)
        path=file_name
        localpath = "/tmp/" + file_name
        sftp.put(localpath, path)
    except Exception as e:
        print(str(e))
    finally:
        sftp.close()
        transport.close()
    return {"status": 200, "body": "file uploaded to SFTP"}
            
def getExceldata(data, file_name):
    try:
        df = pd.DataFrame(data)
        with io.BytesIO() as output:
          with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer,index=False)
          data = output.getvalue()
        s3 = boto3.resource('s3')
        s3.Bucket(BUCKET_NAME).put_object(Key=file_name, Body=data)
    except Exception as e:
        print(str(e))
    return {"status": 200, "body": "file uploaded to S3"}
    
def do_restore(bucket_name, file_name):
    try:
        os.chdir("./")
        os.chdir("/tmp")
        
        s3 = boto3.client('s3')
        with open(file_name, 'wb') as data:
            s3.download_fileobj(BUCKET_NAME, file_name, data)
    except Exception as e:
        print(str(e))
    return {"status": 200, "body": "file moved from S3 to tmp folder"}  
    
def lambda_handler(event, context):
    try:
        null = None
        false = False
        true = True

        if event["body"].get("scheduler_id", None):
            scheduler_id = event["body"].get("scheduler_id",None)
        else:
            return {"status": 500, "body": "Missing parameter value for scheduler_id"}

        scheduler_data =  db.SchedulerHistory.find({"SchedulerConfigId": ObjectId(scheduler_id)}).sort([("_id",-1)]).limit(1)
        for data in scheduler_data:
            list_ruleset_ids = data["RuleSetIds"]
        list_all_external_rules = [["RuleSetName", "RuleName"]]
        for ruleset_id in list_ruleset_ids:
            ruleset_data = db.RuleSetMaster.find({"_id": ruleset_id})
            rule_set_name = ""
            for data in ruleset_data:
                    list_rule_ids = data["RuleIds"]
                    rule_set_name = data["RuleSetName"]
            for rule in list_rule_ids:
                rule_name = ""
                rule_data = db.Rule.find({"_id": rule["RuleId"]})
                for data in rule_data:
                    if data['ExternalSource'] and data['ExternalSource'] == True: 
                        rule_name = data["Name"]
                        list_all_external_rules.append([rule_set_name, rule_name])
        response = getExceldata(list_all_external_rules, file_name)
        print(response)
        print(do_restore(BUCKET_NAME, file_name))
        print(export_file_to_sftp())
    except Exception as e:
        print(str(e))
    
    return {
        'statusCode': 200,
        'body': json.dumps('Exported the list of external rules to SFTP location')
    }

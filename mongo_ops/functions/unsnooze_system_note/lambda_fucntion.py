import json
from pymongo import MongoClient
import boto3
from bson.objectid import ObjectId
from datetime import datetime, timedelta
from collections import OrderedDict
import os

comp_timestamp = datetime.now().replace(hour=0, minute=0, second=0)
time_stamp = datetime.now()

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    # TODO implement
    note_details = db.ResultSetDetail.find({"SystemNote": {"$in": ["Snooze"]}})
    for note_item in note_details:
        if isinstance(note_item, dict):
            s = datetime.strptime(str(note_item["NoteDetail"]["CreatedOn"]), '%Y-%m-%d %H:%M:%S')
            snooze_date = s + timedelta(int(note_item["NoteDetail"]["Days"]))
            print(snooze_date, comp_timestamp)
            if snooze_date < comp_timestamp:
                system_notes = set(note_item.get("SystemNote", []))
                system_notes.remove("Snooze")
                note_detail = None
                db.ResultSetDetail.update(
                    {
                        "_id": note_item["_id"]
                    },
                    {
                        "$set": {
                            "SystemNote": list(system_notes),
                            "NoteDetail": note_detail
                        }
                    }
                )
                note_obj = {
                    "RulesetId": note_item["RuleSetId"],
                    "RuleId": note_item["RuleId"],
                    "LoanNumber": note_item["LoanNumber"],
                    "NoteType": "SystemNote",
                    "Note": {
                        "Note": "Snooze ended",
                        "NoteDetail": note_detail,
                        "Action": "Delete",
                        "RunId": note_item["RunId"],
                        "createdon": time_stamp,
                        "Username": "System"
                    }
                }
                note = db.Notes.insert_one(note_obj)
    return {
        'statusCode': 200,
        'body': json.dumps('All possble items have been unsnoozed')
    }

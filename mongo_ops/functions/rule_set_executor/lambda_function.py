import json
import os
from datetime import datetime
import boto3
from bson.objectid import ObjectId
import traceback
import time
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])

db = client[secret_dict["db_name"]]

null = None
false = False
true = True


def filter_loan_data(condition, view_name, scheduler_id, loan_data_prime_key):
    pipeline = [
        {"$match": eval(condition)},
        {"$out": view_name}
    ]
    view = db["LoanData_" + scheduler_id].aggregate(pipeline, allowDiskUse=True)
    db[view_name].create_index([(loan_data_prime_key, 1)])
    return view


def drop_filtered_loan_data(view_name):
    db[view_name].drop()
    return True


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def result_set_summary(rule_set_id, run_id, rule_ids_count, exception_count, new_exception_count, no_of_loans,
                       loans_with_exception, cleared_loans_count, status, run_type, err_msg=None):
    try:
        current_date_time = datetime.now()
        result_summary = dict()
        result_summary["RunId"] = run_id
        result_summary["RuleSetId"] = rule_set_id
        result_summary["NoOfRules"] = rule_ids_count
        result_summary["TotalNoOfExceptions"] = exception_count
        result_summary["NoOfNewException"] = new_exception_count
        result_summary["NoOfLoans"] = no_of_loans
        result_summary["NoOfLoansWithException"] = loans_with_exception
        result_summary["ClearedException"] = cleared_loans_count
        result_summary["ExecutionCompleted"] = status
        result_summary["RunType"] = run_type
        result_summary["ErrorMessage"] = err_msg
        result_summary["CreatedOn"] = current_date_time
        result_summary["UpdatedOn"] = current_date_time
        rule_set = get_rule_set(rule_set_id)
        lambda_metric(
            metric_name='bizzy.bre.ruleset.rules',
            value=rule_ids_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.exceptions',
            value=exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.loans',
            value=no_of_loans,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        start_time = round(time.time() * 1000)
        details = db.ResultSetSummary.insert(result_summary)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        return details
    except Exception as err:
        print(err)
        traceback.print_exc()


def get_rule(rule_id):
    rule = db.Rule.find_one({"_id": ObjectId(rule_id)}, {"Name": 1,
                                                         "Version": 1, "IsActive": 1, "IsPublished": 1,
                                                         "DisplayFields": 1,
                                                         "Severity": 1, "Priority": 1, "Criteria": 1,
                                                         "RuleLogicFields": 1, "ExternalSource": 1,
                                                         "RuleNameId": 1})
    return rule


def execute_rule_set(event, context):
    first_start_time = round(time.time() * 1000)
    if event.get("Records", None):
        msg_body = json.loads(event["Records"][0]["body"])
    else:
        msg_body = event
    print(msg_body, "=====")
    run_id = msg_body.get("run_id", None)
    rule_set_id = msg_body.get("rule_set_id", None)
    run_type = msg_body.get("run_type", None)
    last_counter = msg_body.get("last_counter", None)
    scheduler_id = msg_body.get("scheduler_id", None)
    loan_package_id = msg_body.get("loan_package_id", None)
    loan_data_prime_key = msg_body.get("loan_data_prime_key", None)
    loan_data_columns = msg_body.get("loan_data_columns"),
    user_filter = msg_body.get("user_filter", {})
    bucket_path = msg_body.get("bucket_path")

    try:
        if None in [run_id, rule_set_id, run_type, last_counter, scheduler_id]:
            # drop_filtered_loan_data(view_name)
            print("Missing required fields!")
            return {
                "error": "Missing required fields!"
            }

        # get rule_set info
        rule_set = get_rule_set(rule_set_id)
        rule_set_id = ObjectId(rule_set_id)

        rule_set_criteria = eval(rule_set["LoanDataFilterExpression"])
        if rule_set_criteria:
            view_name = "LoanDataView_" + str(rule_set_id) + "_" + scheduler_id
            print("filtering loan data!!! for--", view_name)
            filter_loan_data(rule_set["LoanDataFilterExpression"], view_name, scheduler_id, loan_data_prime_key)
        else:
            print("====LoanData as view=====")
            print(rule_set_id)
            view_name = "LoanData_" + scheduler_id

        child_rule_sets = rule_set["ChildRuleSetIds"]
        if len(child_rule_sets) > 0:
            for child_rule_set in child_rule_sets:
                # recursive call of execute_rule_set()
                execute_rule_set(child_rule_set, run_id)
        try:
            rule_ids = rule_set["RuleIds"]
            rule_ids_list = []
            external_rules_list = []
            for rule_id in rule_ids:
                rule = get_rule(rule_id["RuleId"])
                if rule["IsPublished"] == 1 and rule_id["IsActive"] == 1 and rule["ExternalSource"] == False and (
                        (not rule_id.get("StartDate", None)) or rule_id[
                    "StartDate"].date() <= datetime.now().date()) and (
                        (not rule_id.get("EndDate", None)) or rule_id["EndDate"].date() >= datetime.now().date()):
                    rule_ids_list.append(rule)
                elif rule["IsPublished"] == 1 and rule_id["IsActive"] == 1 and rule["ExternalSource"] == True and (
                        (not rule_id.get("StartDate", None)) or rule_id[
                    "StartDate"].date() <= datetime.now().date()) and (
                        (not rule_id.get("EndDate", None)) or rule_id["EndDate"].date() >= datetime.now().date()):
                    external_rules_list.append(rule)
            print("rule_ids======== ", rule_ids_list)
            print("external_ids==========", external_rules_list)
            lambda_metric(
                metric_name='bizzy.bre.ruleset.internalrules',
                value=rule_ids_list,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                      'run_id:' + str(run_id), 'collection:RuleSummary', 'loan_package_id:' + str(loan_package_id),
                      'scheduler_id:' + str(scheduler_id)]
            )
            lambda_metric(
                metric_name='bizzy.bre.ruleset.externalrules',
                value=external_rules_list,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                      'run_id:' + str(run_id), 'collection:RuleSummary', 'loan_package_id:' + str(loan_package_id),
                      'scheduler_id:' + str(scheduler_id)]
            )
        except Exception as e:
            print(e)
            traceback.print_exc()

        no_of_loans = db[view_name].count()
        # check if filtered loan data is not empty
        if no_of_loans <= 0:
            msg = "No Loan data to execute rules."
            result_set_summary(rule_set_id, run_id, len(rule_ids_list), 0, 0, 0, 0, 0, False, run_type, str(msg))
            # drop_filtered_loan_data(view_name)
            if last_counter == 1:
                print("Last counter!!! calling to activate!! no loan data!!")
                lambda_invoke = boto3.client("lambda", region_name="us-east-1")
                payload = {
                    "run_id": run_id,
                    "scheduler_id": scheduler_id,
                    "loan_package_id": loan_package_id,
                    "counter": 1
                }
                lambda_invoke.invoke(
                    FunctionName=os.environ["ACT_EXE_FUNC_NAME"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                # wait_to_activate_execution(scheduler_id, run_id)
            return {
                "msg": msg
            }

        if len(rule_ids_list) > 0 or len(external_rules_list) > 0:
            sqs = boto3.client('sqs')
            if len(rule_ids_list) > 0:
                for rule in rule_ids_list:
                    rule["_id"] = str(rule["_id"])
                    payload = {
                        "run_id": run_id,
                        "scheduler_id": scheduler_id,
                        "loan_package_id": loan_package_id,
                        "rule_set_id": str(rule_set_id),
                        "rule_id": str(rule["_id"]),
                        "collection": view_name,
                        "rule": rule,
                        "loan_data_columns": msg_body.get("loan_data_columns"),
                        "user_filter": user_filter
                    }
                    response = sqs.send_message(
                        QueueUrl=os.environ["RULE_QUEUE_URL"],
                        MessageBody=json.dumps(payload),
                        DelaySeconds=0,
                        MessageGroupId=str(rule["_id"]) + "_" + scheduler_id
                    )
            if len(external_rules_list) > 0:
                for rule in external_rules_list:
                    rule["_id"] = str(rule["_id"])
                    payload = {
                        "run_id": run_id,
                        "scheduler_id": scheduler_id,
                        "loan_package_id": loan_package_id,
                        "rule_set_id": str(rule_set_id),
                        "rule_id": str(rule["_id"]),
                        "file_name": rule_set["RuleSetName"] + "_" + rule["Name"] + ".txt",
                        "collection": view_name,
                        "rule": rule,
                        "user_filter": user_filter,
                        "bucket_path": bucket_path
                    }
                    response = sqs.send_message(
                        QueueUrl=os.environ["EXTERNAL_RULE_QUEUE_URL"],
                        MessageBody=json.dumps(payload),
                        DelaySeconds=0,
                        MessageGroupId=str(rule["_id"]) + "_" + scheduler_id
                    )

            payload = {
                "run_id": run_id,
                "rule_set_id": str(rule_set_id),
                "run_type": run_type,
                "collection": view_name
            }
            response = sqs.send_message(
                QueueUrl=os.environ["POLLING_QUEUE_URL"],
                MessageBody=json.dumps(payload),
                DelaySeconds=0,
                MessageGroupId=str(rule_set_id) + "_" + scheduler_id
            )
        else:
            print("++++No Rules++++++++")
            result_set_summary(rule_set_id, run_id, 0, 0, 0, 0, 0, 0, True, run_type)
            # drop_filtered_loan_data(view_name)

        if last_counter == 1:
            print("Last counter!!! calling to activate!!")
            lambda_invoke = boto3.client("lambda", region_name="us-east-1")
            payload = {
                "run_id": run_id,
                "scheduler_id": scheduler_id,
                "counter": 1
            }
            lambda_invoke.invoke(
                FunctionName=os.environ["ACT_EXE_FUNC_NAME"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
            # wait_to_activate_execution(scheduler_id, run_id)
        lambda_metric(
            metric_name='bizzy.bre.ruleset.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'loan_package_id:' + str(loan_package_id),
                  'scheduler_id:' + str(scheduler_id)]
        )
    except Exception as err:
        result_set_summary(rule_set_id, run_id, 0, 0, 0, 0, 0, 0, False, run_type, str(err))
        # drop_filtered_loan_data(view_name)
        if last_counter == 1:
            print("Last counter!!! calling to activate!! in Exception!!")
            lambda_invoke = boto3.client("lambda", region_name="us-east-1")
            payload = {
                "run_id": run_id,
                "scheduler_id": scheduler_id,
                "counter": 1
            }
            lambda_invoke.invoke(
                FunctionName=os.environ["ACT_EXE_FUNC_NAME"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'loan_package_id:' + str(loan_package_id),
                  'scheduler_id:' + str(scheduler_id)]
        )

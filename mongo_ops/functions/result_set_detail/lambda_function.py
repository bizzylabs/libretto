import json
import os
from datetime import datetime
import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

null = None
false = False
true = True

sqs = boto3.client("sqs", region_name="us-east-1")


def access_filter_info(loan_number, view_name, run_id):
    loan_package_id = db.SchedulerHistory.find_one({"RunId": run_id})["LoanPackage"]
    loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
    filter_data = db.UserAccessFilters.find_one({})["Filters"]
    projection = {item: 1 for item in filter_data[loan_package_name]}
    projection["_id"] = 0
    data = db[view_name].find_one({"LoanNumber": loan_number}, projection)

    return data


def get_loan_info(loan_number, display_fields, view_name):
    condition = {"LoanNumber": loan_number}
    projection = eval(display_fields)
    projection["_id"] = 0
    if len(projection.keys()) > 0:
        projection["LoanNumber"] = 1
    loan = db[view_name].find_one(condition, projection)
    projection.update(loan)
    lis = []
    for key in projection.keys():
        if key not in loan:
            lis.append(key)
    for item in lis:
        del projection[item]
    return projection


def result_set_detail(event, context):
    try:
        count = 0
        del_count = 0
        for data in event["Records"]:
            msg_body = json.loads(data["body"])
            reciept_handle = data["receiptHandle"]
            run_id = msg_body.get("run_id", None)
            rule_set_id = msg_body.get("rule_set_id", None)
            rule_id = msg_body.get("rule_id", None)
            rule_version = msg_body.get("rule_version", None)
            exception_status = msg_body.get("exception_status", None)
            exception = msg_body.get("exception", None)
            loan_number = msg_body.get("loan_number", None)
            collection = msg_body.get("collection", None)
            display_fields = msg_body.get("display_fields", None)
            in_existence = msg_body.get("in_existence", None)
            system_note = msg_body.get("system_note", [])
            note_detail = msg_body.get("note_detail", None)
            severity = msg_body.get("severity")
            priority = msg_body.get("priority")
            update_flag = msg_body.get("update_flag", False)
            ignore = msg_body.get("Ignore", False)
            business_rule_id = msg_body.get("business_rule_id", "")
            if not system_note:
                system_note = []
            if update_flag:
                if note_detail:
                    if note_detail.get("CreatedOn", None):
                        try:
                            note_detail["CreatedOn"] = datetime.strptime(note_detail["CreatedOn"],
                                                                         "%m/%d/%Y %H:%M:%S:%f")
                        except Exception as e:
                            note_detail["CreatedOn"] = datetime.strptime(note_detail["CreatedOn"], "%m/%d/%Y %H:%M:%S")

                db.ResultSetDetail.update_many(
                    {"RuleSetId": ObjectId(rule_set_id), "RuleId": ObjectId(rule_id), "RunId": run_id,
                     "LoanNumber": loan_number},
                    {"$set": {"InExistence": datetime.strptime(in_existence, "%m/%d/%Y, %H:%M:%S:%f"),
                              "SystemNote": system_note,
                              "NoteDetail": note_detail,
                              "Ignore": ignore}})
                count += 1
                try:
                    del_msg = sqs.delete_message(
                        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"], ReceiptHandle=reciept_handle)
                    del_count += 1
                except Exception as e:
                    print(e, "exception")
                    pass

                continue

            current_date_time = datetime.now()
            result_set = dict()
            result_set["RunId"] = run_id
            result_set["RuleSetId"] = ObjectId(rule_set_id)
            result_set["RuleId"] = ObjectId(rule_id)
            result_set["RuleNameId"] = business_rule_id
            result_set["RuleVersion"] = rule_version
            result_set["ExceptionStatus"] = exception_status
            result_set["Exception"] = exception
            result_set["LoanNumber"] = loan_number
            if loan_number:
                result_set["LoanInfo"] = get_loan_info(loan_number, display_fields, collection)
            if not system_note:
                system_note = []
            result_set["InExistence"] = datetime.strptime(in_existence, "%m/%d/%Y, %H:%M:%S:%f")
            result_set["CreatedOn"] = current_date_time
            if exception_status:
                result_set["UpdatedOn"] = datetime(2200, 1, 1, 0, 0, 0)
            else:
                result_set["UpdatedOn"] = current_date_time
            result_set["SystemNote"] = system_note
            if loan_number:
                result_set["FilterInfo"] = access_filter_info(loan_number, collection, run_id)
            result_set["Severity"] = severity
            result_set["Priority"] = priority
            result_set["Ignore"] = ignore
            if note_detail:
                try:
                    note_detail["CreatedOn"] = datetime.strptime(note_detail["CreatedOn"], "%m/%d/%Y %H:%M:%S:%f")
                except Exception as e:
                    note_detail["CreatedOn"] = datetime.strptime(note_detail["CreatedOn"], "%m/%d/%Y %H:%M:%S")
            result_set["NoteDetail"] = note_detail
            try:
                details = db.ResultSetDetail.insert(result_set)
                count += 1
            except Exception as e:
                pass
            try:
                del_msg = sqs.delete_message(
                    QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"], ReceiptHandle=reciept_handle)
                del_count += 1
            except Exception as e:
                print(e, "exception")
                pass
        print(count, "Messages  Inserted")

        print(del_count, "Messages  Deleted")

        return {"status": 200}

    except Exception as err:
        print(err)
        return {"error": str(err)}

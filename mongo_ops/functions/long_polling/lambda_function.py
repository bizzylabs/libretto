import json
import os
import time
from datetime import datetime
import boto3
from bson.objectid import ObjectId
import traceback
import time
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])

db = client[secret_dict["db_name"]]
null = None
false = False
true = True


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def result_set_summary(rule_set_id, run_id, rule_ids_count, exception_count, new_exception_count, no_of_loans,
                       loans_with_exception, cleared_loans_count, status, run_type, err_msg=None, severity=None):
    try:
        current_date_time = datetime.now()
        result_summary = dict()
        result_summary["RunId"] = run_id
        result_summary["RuleSetId"] = rule_set_id
        result_summary["NoOfRules"] = rule_ids_count
        result_summary["TotalNoOfExceptions"] = exception_count
        result_summary["NoOfNewException"] = new_exception_count
        result_summary["NoOfLoans"] = no_of_loans
        result_summary["NoOfLoansWithException"] = loans_with_exception
        result_summary["ClearedException"] = cleared_loans_count
        result_summary["ExecutionCompleted"] = status
        result_summary["RunType"] = run_type
        result_summary["ErrorMessage"] = err_msg
        result_summary["Severity"] = severity
        result_summary["CreatedOn"] = current_date_time
        result_summary["UpdatedOn"] = current_date_time
        rule_set = get_rule_set(rule_set_id)

        lambda_metric(
            metric_name='bizzy.bre.ruleset.rules',
            value=rule_ids_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.exceptions',
            value=exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.loans',
            value=no_of_loans,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.ruleset.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        start_time = round(time.time() * 1000)
        details = db.ResultSetSummary.insert(result_summary)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'collection:ResultSetSummary']
        )
        return details
    except Exception as err:
        print(err)
        traceback.print_exc()


def drop_filtered_loan_data(view_name):
    db[view_name].drop()
    return True


def get_severity(rule_set_id, run_id):
    severity = {
        "TotalException": {"Critical": 0, "High": 0, "Medium": 0, "Low": 0, "Informational": 0},
        "NewException": {"Critical": 0, "High": 0, "Medium": 0, "Low": 0, "Informational": 0},
        "ClearedException": {"Critical": 0, "High": 0, "Medium": 0, "Low": 0, "Informational": 0}
    }
    pipeline = [
        {
            "$match": {"RuleSetId": rule_set_id, "RunId": run_id, "ExecutionCompleted": True}
        },
        {
            "$lookup": {
                "from": "Rule",
                "let": {"summary_rule_id": "$RuleId"},
                "pipeline": [
                    {"$match":
                        {"$expr":
                            {"$and":
                                [
                                    {"$eq": ["$_id", "$$summary_rule_id"]}
                                ]
                            }
                        }
                    },
                    {"$project": {"Severity": 1, "_id": 0}}
                ],
                "as": "severity"
            }
        },
        {
            "$replaceRoot": {"newRoot": {"$mergeObjects": ["$$ROOT", {"$arrayElemAt": ["$severity", 0]}]}}
        },
        {"$project": {"severity": 0}}
    ]
    exceptions = db.RuleSummary.aggregate(pipeline, allowDiskUse=True)
    for exception in exceptions:
        print(exception)
        severity["TotalException"][exception["Severity"]] += exception["Exception"]
        severity["NewException"][exception["Severity"]] += exception["NewException"]
        severity["ClearedException"][exception["Severity"]] += exception["ClearedException"]
    return severity


# @datadog_lambda_wrapper
def long_polling(event, context):
    start_time = round(time.time() * 1000)
    if event.get("Records", None):
        msg_body = json.loads(event["Records"][0]["body"])
    else:
        msg_body = event
    print("msg_body=", msg_body)
    rule_set_id = msg_body.get("rule_set_id", None)
    run_id = msg_body.get("run_id", None)
    run_type = msg_body.get("run_type", None)
    collection = msg_body.get("collection", None)
    counter = msg_body.get("counter", None)
    if counter == None:
        counter = 1
    rule_ids = []
    print("rule_set_id==", rule_set_id)
    try:
        # rule_set_detail
        rule_set = get_rule_set(rule_set_id)
        rule_set_id = ObjectId(rule_set_id)

        # rule_ids

        rule_ids = [rule_id["RuleId"] for rule_id in rule_set["RuleIds"] if rule_id["IsActive"] == 1 and (
                (not rule_id.get("StartDate", None)) or rule_id["StartDate"].date() <= datetime.now().date()) and (
                            (not rule_id.get("EndDate", None)) or rule_id["EndDate"].date() >= datetime.now().date())]
        # wait interval == 10
        sleep_time = 10
        total_time = 10
        time.sleep(sleep_time)
        condition = {"RunId": run_id, "RuleSetId": rule_set_id}
        while sleep_time >= 10:
            if total_time > 850:
                if counter == 10:
                    # drop_filtered_loan_data(collection)
                    # update result set summary
                    result_set_summary(rule_set_id, run_id, len(rule_ids), 0, 0, 0, 0, 0, False, run_type,
                                       "breaking before timeout")
                    print("breaking before timeout!")
                    break
                print("==Invoking again!==")
                lambda_invoke = boto3.client("lambda", region_name="us-east-1")
                payload = {
                    "rule_set_id": str(rule_set_id),
                    "run_id": run_id,
                    "run_type": run_type,
                    "collection": collection,
                    "counter": counter + 1
                }
                lambda_invoke.invoke(
                    FunctionName=os.environ["LONG_POLL_FUNC_NAME"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                break
            executed_rules = db.RuleSummary.find(condition)
            if len(rule_ids) == executed_rules.count():
                print("count matched, updating rule set summary!")
                pipeline = [
                    {"$match": {"$and": [{"RuleSetId": rule_set_id}, {"RunId": run_id},
                                         {"Severity": {"$ne": "Informational"}}]}},
                    {"$group": {
                        "_id": "$RuleSetId",
                        "total_exception": {"$sum": "$Exception"},
                        "total_new_exception": {"$sum": "$NewException"},
                        "total_cleared_exception": {"$sum": "$ClearedException"}
                    }},
                    {"$sort": {"RunId": -1}},
                    {"$limit": 1}
                ]
                severity = get_severity(rule_set_id, run_id)
                print("here")
                detail = list(db.RuleSummary.aggregate(pipeline, allowDiskUse=True))
                total_exception = detail[0]["total_exception"]
                new_exception = detail[0]["total_new_exception"]
                no_of_loans = db[collection].count()
                cleared_exception = detail[0]["total_cleared_exception"]
                unique_loans_with_exception = db.ResultSetDetail.distinct("LoanNumber",
                                                                          {"RunId": run_id, "RuleSetId": rule_set_id,
                                                                           "ExceptionStatus": True})
                print("Total distinct loans=", unique_loans_with_exception)

                print("updating result set summary")
                result_set_summary(rule_set_id, run_id, len(rule_ids), total_exception, new_exception, no_of_loans,
                                   unique_loans_with_exception, cleared_exception, True, run_type, None, severity)

                # drop collection view
                # drop_filtered_loan_data(collection)
                break
            else:
                print("Going to sleep again :( ")
                sleep_time = 10
                total_time = total_time + sleep_time
                time.sleep(sleep_time)
        lambda_metric(
            metric_name='bizzy.bre.ruleset.execution',
            value=(round(time.time() * 1000) - start_time) + total_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'run_id:' + str(run_id), 'counter:' + str(counter)]
        )
    except Exception as err:
        print(err)
        traceback.print_exc()
        result_set_summary(rule_set_id, run_id, len(rule_ids), 0, 0, 0, 0, 0, False, run_type, str(err))

import json
import os
import time
from datetime import datetime
from io import StringIO
import boto3
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
import traceback
import time
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])

db = client[secret_dict["db_name"]]

null = None
false = False
true = True
prev_loans = dict()
sqs = boto3.client('sqs')
s3 = boto3.client('s3', region_name="us-east-1")
lambda_client = boto3.client("lambda")


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def previous_data_lambda_invoker(total_time, rule, shape, rule_id, rule_set_id, run_id, collection, bucket, file_name,
                                 exceptions_count, invoke_counter, loan_package_id):
    sleep_time = 20
    while sleep_time >= 20:
        db_exceptions_details = db.ResultSetDetail.find({"RunId": run_id, "RuleSetId": ObjectId(rule_set_id),
                                                         "RuleId": ObjectId(rule_id)}).count()
        print(db_exceptions_details, "exceptions inside  database")
        print(exceptions_count, "expected exceptions")
        if db_exceptions_details >= exceptions_count:
            off_set = 0
            li_mit = 1000
            val = int(shape / 1000) + 1
            for item in range(val):
                rule["_id"] = str(rule["_id"])
                payload = {
                    "rule": rule,
                    "rule_id": str(rule_id),
                    "rule_set_id": str(rule_set_id),
                    "run_id": run_id,
                    "collection": collection,
                    "offset": off_set,
                    "limit": li_mit,
                    "bucket": bucket,
                    "file_name": file_name,
                    "loan_package_id": str(loan_package_id)
                }
                lambda_client.invoke(
                    FunctionName=os.environ["EXECUTE_RULE_PREV_DATA_FUNC"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                off_set += 1000
                li_mit += 1000

            rule["_id"] = str(rule["_id"])
            payload = {
                "rule": rule,
                "rule_id": str(rule_id),
                "rule_set_id": str(rule_set_id),
                "run_id": run_id,
                "collection": collection,
                "bucket": bucket,
                "file_name": file_name,
                "loan_package_id": str(loan_package_id)
            }
            lambda_client.invoke(
                FunctionName=os.environ["BRE_CHECK_CURED_FUNC"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
            break
        else:
            print("sleeping to invoke prev data execution lambda !!!!!!")
            time.sleep(sleep_time)
            total_time = total_time + sleep_time
            if total_time > 800:
                lambda_invoke = boto3.client("lambda", region_name="us-east-1")
                payload = {
                    "rule": rule,
                    "shape": shape,
                    "rule_id": str(rule_id),
                    "rule_set_id": str(rule_set_id),
                    "run_id": run_id,
                    "collection": collection,
                    "bucket": bucket,
                    "file_name": file_name,
                    "exceptions_count": exceptions_count,
                    "invoke_counter": invoke_counter + 1,
                    "loan_package_id": str(loan_package_id)
                }
                lambda_invoke.invoke(
                    FunctionName=os.environ["EXTERNAL_RULE_PROC_FUNC_NAME"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                break
    return True


def preprocess_dataframe(file_df, offset, limit):
    file_df = file_df.iloc[offset:limit, :]
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    dataframe = file_df.applymap(trim_strings)
    del trim_strings
    del file_df
    col = list(dataframe.columns)
    for column in col:
        if 'date' in column.lower() or column in ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                                                  "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT"]:
            try:
                dataframe[column] = pd.to_datetime(dataframe[column])
            except Exception as e:
                print(e)
                print(column, '++++++++++++++++++++++')
    for column in col:
        if 'date' in column.lower() or column in ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                                                  "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT"]:
            dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
    dataframe = dataframe.replace(r'', np.NaN, regex=True)
    for column in dataframe.columns:
        if dataframe[column].dtype == 'O':
            dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
        elif dataframe[column].dtype == 'int64':
            dataframe[column] = dataframe[column].astype(int).where(dataframe[column].notnull(), None)
        elif dataframe[column].dtype == 'float64':
            dataframe[column] = dataframe[column].astype(float).where(dataframe[column].notnull(), None)
    records = dataframe.to_dict("records")
    # delete the  dataframe
    del dataframe
    # this will return array of objects
    return records


def rule_summary(run_id, rule_set_id, rule_id, severity, exception_count, new_exception_count, cleared_loans_count,
                 error_msg=None):
    """
    :param run_id: id of current scheduled run
    :param rule_set_id: id of Rule Set
    :param rule_id: id of Rule that is executed
    :param exception_count: no of exception found for Rule
    :param new_exception_count: new exception for Rule
    :param cleared_loans_count: total cleared loans
    :param error_msg: error msg if error occurred
    :return: will return insertion detail
    """
    try:
        priority = 2
        priority_dict = {"Critical": 0,
                         "High": 1,
                         "Medium": 2,
                         "Low": 3,
                         "Informational": 4}
        if severity:
            for item in priority_dict.keys():
                if severity == item:
                    priority = priority_dict[item]
        summary = dict()
        summary["RunId"] = run_id
        summary["RuleSetId"] = ObjectId(rule_set_id)
        summary["RuleId"] = ObjectId(rule_id)
        summary["Severity"] = severity
        summary["Priority"] = priority
        summary["Exception"] = exception_count
        summary["NewException"] = new_exception_count
        summary["ClearedException"] = cleared_loans_count
        summary["ExecutionCompleted"] = True
        if error_msg is not None:
            summary["ExecutionCompleted"] = False
            summary["ErrorMessage"] = error_msg
        rule_set = get_rule_set(rule_set_id)
        lambda_metric(
            metric_name='bizzy.bre.rule.exceptions',
            value=exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        start_time = round(time.time() * 1000)
        details = db.RuleSummary.insert(summary)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        return details
    except Exception as err:
        print(err)
        traceback.print_exc()


def put_in_result_set_queue(run_id, rule_set_id, rule_id, version, status, name, loan_number, collection,
                            display_fields, in_existence, sys_note=[], note_detail=None, severity="Medium", priority=2,
                            ignore=False, business_rule_id=""):
    payload = {
        "run_id": run_id,
        "rule_set_id": str(rule_set_id),
        "rule_id": str(rule_id),
        "rule_version": version,
        "exception_status": status,
        "exception": name,
        "loan_number": loan_number,
        "collection": collection,
        "display_fields": display_fields,
        "in_existence": in_existence.strftime("%m/%d/%Y, %H:%M:%S:%f"),
        "system_note": sys_note,
        "note_detail": note_detail,
        "severity": severity,
        "priority": priority,
        "Ignore": ignore,
        "business_rule_id": business_rule_id
    }
    response = sqs.send_message(
        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"],
        MessageBody=json.dumps(payload),
        DelaySeconds=0,
        # MessageGroupId=str(loan_number) + str(rule_id)
    )
    # print("===after==", datetime.now())


def execute_external_rule(rule_set_id, run_id, rule_id, collection, scheduler_id, rule, user_filter, file_name,
                          bucket, offset, limit, last_counter, display_fields, rule_set, loan_package_id):
    """
    :param display_fields:
    :param last_counter:
    :param limit:
    :param offset:
    :param bucket: sftp inbound path
    :param user_filter: user access filter
    :param rule: all information of the whole document
    :param scheduler_id: current scheduler id
    :param file_name: external rule file_name whose data has t be fetched from the bucket path
    :param rule_set_id: id of Rule Set.
    :param run_id: id of current scheduled run.
    :param rule_id: id of Rule that is going to execute over loans.
    :param collection: collection on which Rule's criteria will apply.¬
    :return: this function will not return anything, this will be
    invoked asynchronously.
    """
    start = time.time()
    new_exception = 0
    exception_count = 0
    cleared_loans_count = 0
    business_rule_id = rule["RuleNameId"]
    try:
        display_fields = eval(display_fields)
    except Exception as e:
        traceback.print_exc()
        display_fields = {}
    try:
        exception_status = True
        system_note = []
        note_detail = None
        now = datetime.now()

        try:
            obj = s3.get_object(Bucket=bucket, Key=file_name)
            csv_string = obj['Body'].read().decode("latin1")
            file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='')
            shape = file_df.shape
            # unique_exceptions_count = len(file_df["LoanNumber"].unique())
            del csv_string
        except Exception as e:
            # if file not present or noo data in it.
            print("error_msg:==", e, "==RuleId:==", rule["_id"], )
            rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], exception_count, new_exception,
                         cleared_loans_count, e)
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [],
                                    None,
                                    rule["Severity"], rule["Priority"], False, business_rule_id)
            return True
        temp_display_fields = eval(rule["DisplayFields"])
        temp_display_fields.update(user_filter)
        records = preprocess_dataframe(file_df, offset, limit)

        # prepare data and insert in database.

        for loan in records:
            # print(loan)
            sub_loan_id = loan.get('SubloanId', None)
            try:
                data = db[collection].find_one({"LoanNumber": loan.get("LoanNumber")}, temp_display_fields)
            except Exception as e:
                data = None
            filter_info = {}
            # check if the specified loan number is present in the system
            if data:
                temp_loan_data = loan.copy()
                loan.update(data)
                loan.update(temp_loan_data)
                filter_info = {fil: loan.get(fil, None) for fil in user_filter.keys()}
                common = set(display_fields).intersection(set(user_filter))
                if common:
                    [user_filter.pop(item) for item in common if item in loan]
                [loan.pop(item) for item in user_filter.keys() if item in loan]
                loan.pop('SubloanId', None)
                if "_id" in loan.keys():
                    del loan["_id"]
            result_set_detail = {
                "RunId": run_id,
                "RuleSetId": ObjectId(rule_set_id),
                "RuleId": ObjectId(rule["_id"]),
                "RuleNameId": business_rule_id,
                "RuleVersion": rule["Version"],
                "ExceptionStatus": exception_status,
                "Exception": rule["Name"],
                "SubLoanId": sub_loan_id,
                "LoanNumber": loan["LoanNumber"],
                "LoanInfo": loan,
                "InExistence": now,
                "CreatedOn": now,
                "UpdatedOn": datetime(2200, 1, 1, 0, 0, 0),
                "SystemNote": system_note,
                "FilterInfo": filter_info,
                "Severity": rule["Severity"],
                "Priority": rule["Priority"],
                "NoteDetail": note_detail,
                "Ignore": False
            }
            try:
                start_time = round(time.time() * 1000)
                db.ResultSetDetail.insert(result_set_detail)
                lambda_metric(
                    metric_name='bizzy.bre.db.insert',
                    value=round(time.time() * 1000) - start_time,
                    timestamp=int(time.time()),  # optional, must be within last 20 mins
                    tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                          'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'collection:ResultSetDetail',
                          'ruletype:External']
                )
                # print("y")
            except Exception as e:
                print(e)
                traceback.print_exc()
                pass
        #
        if last_counter == 1:
            end = time.time()
            total_time_till_now = start - end
            previous_data_lambda_invoker(total_time_till_now, rule, shape[0], rule_id, rule_set_id, run_id, collection,
                                         bucket, file_name, shape[0], 0, loan_package_id)

            rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], shape[0], new_exception,
                         cleared_loans_count)
        return True

    except Exception as err:
        print("Exception in external rule===", err)
        rule_summary(run_id, rule_set_id, rule_id, rule["Severity"], 0, 0, 0, str(err))
        put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [], None,
                                rule["Severity"], rule["Priority"], False, business_rule_id)


def external_rule_handler(event, context):
    first_start_time = round(time.time() * 1000)
    if event.get("Records", None):
        msg_body = json.loads(event["Records"][0]["body"])
    else:
        msg_body = event
    print("msg_body=", msg_body)
    scheduler_id = msg_body.get("scheduler_id", None)
    run_id = msg_body.get("run_id", None)
    rule_set_id = msg_body.get("rule_set_id", None)
    rule_id = msg_body.get("rule_id", None)
    collection = msg_body.get("collection", None)
    rule = msg_body.get("rule")
    user_filter = msg_body.get("user_filter", {})
    file_name = msg_body.get("file_name")
    bucket_path = msg_body.get("bucket")
    offset = msg_body.get("offset")
    limit = msg_body.get("limit")
    last_counter = msg_body.get("last_counter")
    display_fields = msg_body.get("display_fields")
    exceptions_count = msg_body.get("exceptions_count")
    invoke_counter = msg_body.get("invoke_counter", None)
    loan_package_id = msg_body.get("loan_package_id")
    shape = msg_body.get("shape")
    rule_set = get_rule_set(rule_set_id)
    try:
        if None in [run_id, rule_set_id, collection, rule_id]:
            print("Missing required fields!")
            return {
                "error": "Missing required fields!"
            }
        print("rule_id====", rule_id)
        if invoke_counter:
            print(invoke_counter, "invoke_counter")
            if invoke_counter == 4:
                print("Unable to restore previous data")
                return True
            else:
                print("inside invoker")
                previous_data_lambda_invoker(10, rule, shape, rule_id, rule_set_id, run_id,
                                             collection,
                                             bucket_path, file_name, exceptions_count, invoke_counter, loan_package_id)
                rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], shape, 0,
                             0)

                return True
        else:
            status = execute_external_rule(ObjectId(rule_set_id), run_id, ObjectId(rule_id), collection, scheduler_id,
                                           rule, user_filter, file_name, bucket_path, offset, limit, last_counter,
                                           display_fields, rule_set, loan_package_id)
            print("Running status", status)
        lambda_metric(
            metric_name='bizzy.bre.rule.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'ruletype:External']
        )
        return {"message": "Execution Completed"}

    except Exception as e:
        print(e)
        traceback.print_exc()
        return {"error": str(e)}

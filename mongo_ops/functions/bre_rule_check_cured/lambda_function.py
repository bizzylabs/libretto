import json
import boto3
import datetime
from datetime import timedelta
from pymongo import MongoClient
from bson.objectid import ObjectId
import os
import pandas as pd
from io import StringIO
import traceback
import time
from datadog_lambda.metric import lambda_metric

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.client('s3')
sqs = boto3.client('sqs')


def load_sftp_file(bucket, file_name):
    obj = s3.get_object(Bucket=bucket, Key=file_name)
    csv_string = obj['Body'].read().decode("latin1")
    file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='', usecols=["LoanNumber"])
    loan_numbers = file_df["LoanNumber"].tolist()

    return loan_numbers


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def loans_caught_in_last_run(rule_set_id, run_id, rule_id, business_rule_id, loan_package_id):
    """
    :param rule_set_id: id of Rule Set
    :param run_id: id of current scheduled run
    :param rule_id: id of Rule that is going to execute over loans.
    :return: loans caught in previous execution of Rule for given Ruleset
    This function will fetch last run stats of rule for given ruleset from ResultSetDetail collection.
    This stat will be used to check cleared loans and new loans.
    """
    scheduler_run_ids = db.SchedulerHistory.find({"LoanPackage": ObjectId(loan_package_id)}).sort(
        [("RunId", -1)]).limit(5)

    run_ids = [item["RunId"] for item in scheduler_run_ids if item]
    pipeline = [
        {"$match": {"$and": [{"RuleSetId": rule_set_id},
                             {"RunId": {"$in": run_ids}},
                             {"RunId": {"$ne": run_id}},
                             {"RuleNameId": business_rule_id}
                             ]}},
        {"$group": {"_id": "$RunId", "data": {
            "$push": {"LoanNumber": "$LoanNumber", "ExceptionStatus": "$ExceptionStatus", "InExistence": "$InExistence",
                      "SystemNote": "$SystemNote", "NoteDetail": "$NoteDetail"}}}},
        {"$sort": {"_id": -1}},
        {"$limit": 1}
    ]
    prev_detail = db.ResultSetDetail.aggregate(pipeline)
    return prev_detail


def update_rule_summary(run_id, rule_set_id, rule_id, new_exception_count, cleared_loans_count):
    try:
        data = db.RuleSummary.find_one(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleId": ObjectId(rule_id)})
        new = data.get("NewException", 0)
        cleared = data.get("ClearedException", 0)

        new_exception_count = new + new_exception_count
        cleared_loans_count = cleared + new_exception_count
        print(cleared_loans_count, new_exception_count)
        rule_set = get_rule_set(rule_set_id)
        start_time = round(time.time() * 1000)
        details = db.RuleSummary.update(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleId": ObjectId(rule_id)},
            {"$set": {"NewException": new_exception_count, "ClearedException": cleared_loans_count}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id)]
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id)]
        )
    except Exception as err:
        print(err)
        traceback.print_exc()


def check_valid_snooze(kwargs):
    no_of_days = kwargs.get("Days", None)
    created_on = kwargs.get("CreatedOn", None)

    if no_of_days and created_on and created_on.date() + timedelta(days=no_of_days) > datetime.datetime.now().date():
        return True
    return False


def put_in_result_set_queue(run_id, rule_set_id, rule_id, version, status, name, loan_number, collection,
                            display_fields, in_existence, sys_note=[], note_detail=None, severity="Medium", priority=2,
                            update_flag=False, ignore=False, business_rule_id=""):
    payload = {
        "run_id": run_id,
        "rule_set_id": str(rule_set_id),
        "rule_id": str(rule_id),
        "rule_version": version,
        "exception_status": status,
        "exception": name,
        "loan_number": loan_number,
        "collection": collection,
        "display_fields": display_fields,
        "in_existence": in_existence.strftime("%m/%d/%Y, %H:%M:%S:%f"),
        "system_note": sys_note,
        "note_detail": note_detail,
        "severity": severity,
        "priority": priority,
        "update_flag": update_flag,
        "Ignore": ignore,
        "business_rule_id": business_rule_id

    }
    response = sqs.send_message(
        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"],
        MessageBody=json.dumps(payload),
        DelaySeconds=0
    )


def check_prev_status(loan_number, prev_loans):
    exception_status = prev_loans.get(loan_number, None)
    if exception_status is not None and exception_status[0]:
        return exception_status
    else:
        return False


def get_data(file_name):
    try:
        obj = s3.get_object(Bucket=os.environ["BRE_BUCKET"], Key=file_name)
        csv_string = obj['Body'].read().decode("utf-8")
        data = eval(csv_string)

    except Exception as e:
        data = []
        traceback.print_exc()
        # if data_type == "loan_data":
        #     csv_string = "[]"
        # else:
        #     csv_string = "{}"

    # return eval(csv_string)
    return data


def lambda_handler(event, context):
    first_start_time = round(time.time() * 1000)
    new_exception = 0
    cleared_loans_count = 0
    print(event)
    loan_file_name = event.get("loan_file")
    rule = event.get("rule")
    rule_id = ObjectId(event["rule_id"])
    rule_set_id = ObjectId(event["rule_set_id"])
    run_id = event["run_id"]
    collection = event["collection"]
    bucket = event.get("bucket", None)
    file_name = event.get("file_name", None)
    business_rule_id = rule["RuleNameId"]
    loan_package_id = event.get("loan_package_id")
    rule_set = get_rule_set(rule_set_id)
    try:
        if bucket and file_name:
            loan_numbers = load_sftp_file(bucket, file_name)
        else:
            loan_exceptions = get_data(loan_file_name)
            loan_numbers = [item["LoanNumber"] for item in loan_exceptions]

        loans_cursor = loans_caught_in_last_run(rule_set_id, run_id, rule_id, business_rule_id, loan_package_id)
        print("======= Preparing prev data ======")

        prev_loans = {loan["LoanNumber"]: [loan["ExceptionStatus"], loan.get("InExistence", None),
                                           loan.get("SystemNote", []), loan.get("NoteDetail", None)] for pre_loans in
                      loans_cursor for loan in
                      pre_loans["data"]}
        print("======= Preparing prev data ======")

        # cured check
        print("========checking cured one========")
        loans_without_exception = []
        loans = db[collection].find(
            {"LoanNumber": {"$nin": loan_numbers}},
            {"LoanNumber": 1, "_id": 0}
        )
        for loan in loans:
            loans_without_exception.append(loan["LoanNumber"])
        if len(prev_loans) > 0:
            exception_status = False
            for loan_number in loans_without_exception:
                prev_status = check_prev_status(loan_number, prev_loans)
                if prev_status:
                    cleared_loans_count = cleared_loans_count + 1
                    if prev_status[1] != None:
                        in_existence = prev_status[1]
                    else:
                        in_existence = datetime.datetime.now()
                    put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                            exception_status, rule["Name"], loan_number,
                                            collection, rule["DisplayFields"], in_existence, [], None,
                                            rule["Severity"], rule["Priority"], False, False, business_rule_id)
        print("========checking cured done========")
        # loans_without_exception
        update_rule_summary(run_id, rule_set_id, rule["_id"], new_exception, cleared_loans_count)

        lambda_metric(
            metric_name='bizzy.bre.rule.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id)]
        )
        return True
    except Exception as e:
        print(e)
        traceback.print_exc()
        pass
        return True

import boto3
import json
from pymongo import MongoClient
import os
from bson.objectid import ObjectId
import time
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def create_index(collection_name, index_tuple):
    result = db[collection_name].create_index(index_tuple)
    return result

# @datadog_lambda_wrapper
def lambda_handler(event, context):
    collection_details = event.get("collection_details", [])
    scheduler_id = event.get("scheduler_id")
    loan_package_id = event.get("loan_package_id")
    drop_collection_list = list()
    pipeline = []
    for item in collection_details:
        primary_key = "LoanNumber"

        if item["IsMain"] == True:
            #Ignore supplementary file as it has been already been merged in main file
            if item.get('IsExtra', None) is True:
                continue
            primary_key = item["MatchKey"]
            print(primary_key)

        else:
            if item.get("FileSubType", None):
                collection = db.SupportingFileType.find_one({"_id": ObjectId(item["FileType"])})["FileType"]
                sub_type = db.SupportingFileSubType.find_one({"_id": ObjectId(item["FileSubType"])})["Name"]
                collection_name = scheduler_id + "_" + collection + "_" + sub_type
                drop_collection_list.append(collection_name)
                foriegn_key = item["MatchKey"]
                lookup_dict = {
                    '$lookup': {
                        'from': collection_name,
                        'let': {'loan_number': "$" + primary_key},
                        'pipeline': [
                            {'$match':
                                {'$expr':
                                    {'$and':
                                        [
                                            {'$eq': ["$" + foriegn_key, "$$loan_number"]}
                                        ]
                                    }
                                }
                            },
                            {'$project': {'_id': 0, foriegn_key: 0}}
                        ],
                        'as': collection + "." + sub_type
                    }
                }
                pipeline.append(lookup_dict)
            else:
                collection = db.SupportingFileType.find_one({"_id": ObjectId(item["FileType"])})["FileType"]
                collection_name = scheduler_id + "_" + collection
                drop_collection_list.append(collection_name)
                foriegn_key = item["MatchKey"]
                lookup_dict = {
                    '$lookup': {
                        'from': collection_name,
                        'let': {'loan_number': "$" + primary_key},
                        'pipeline': [
                            {'$match':
                                {'$expr':
                                    {'$and':
                                        [
                                            {'$eq': ["$" + foriegn_key, "$$loan_number"]}
                                        ]
                                    }
                                }
                            },
                            {'$project': {'_id': 0, foriegn_key: 0}}
                        ],
                        'as': collection
                    }
                }
                pipeline.append(lookup_dict)
    final_collection = "LoanData_" + scheduler_id
    pipeline.append({"$out": final_collection})
    print("pipeline: ", pipeline)

    agg_collection = scheduler_id + "_Loan"
    drop_collection_list.append(agg_collection)

    rule_testing = db.RuleTestingLoanData.find_one({"LoanPackageId": ObjectId(loan_package_id)})
    if not rule_testing:
        start_time = round(time.time() * 1000)
        db.RuleTestingLoanData.insert({
            "LoanPackageId": ObjectId(loan_package_id),
            "LoanDataCollectionName": final_collection,
            "IsActive": 0
        })
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=['loan_package_id:'+ loan_package_id, 'final_collection:'+final_collection, 'collection:RuleTestingLoanData']
            )
    else:
        start_time = round(time.time() * 1000)
        db.RuleTestingLoanData.update(
            {"LoanPackageId": ObjectId(loan_package_id)}, {"$set": {"IsActive": 0}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=['loan_package_id:'+ loan_package_id, 'final_collection:'+final_collection, 'collection:RuleTestingLoanData']
            )
    db[final_collection].remove({})
    print("Start aggregation pipeline")
    db[agg_collection].aggregate(pipeline, allowDiskUse=True)
    print("Finish aggregation pipeline")
    create_index(final_collection, [(primary_key, 1)])
    rule_testing_final = db.RuleTestingLoanData.update({"LoanPackageId": ObjectId(loan_package_id)}, {
        "$set": {"LoanDataCollectionName": final_collection, "IsActive": 1}})
    for col_name in drop_collection_list:
        db[col_name].drop({})

    return primary_key



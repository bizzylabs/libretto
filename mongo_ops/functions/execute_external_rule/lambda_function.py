import json
import os
from datetime import datetime
from io import StringIO
from pymongo import MongoClient
import traceback
import time
from datadog_lambda.metric import lambda_metric

import boto3
import pandas as pd
from bson.objectid import ObjectId

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

null = None
false = False
true = True
prev_loans = dict()
sqs = boto3.client('sqs', region_name="us-east-1")
s3 = boto3.client('s3', region_name="us-east-1")
lambda_client = boto3.client("lambda", region_name="us-east-1")


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def rule_summary(run_id, rule_set_id, rule_id, severity, exception_count, new_exception_count, cleared_loans_count,
                 error_msg=None):
    """
    :param severity:
    :param run_id: id of current scheduled run
    :param rule_set_id: id of Rule Set
    :param rule_id: id of Rule that is executed
    :param exception_count: no of exception found for Rule
    :param new_exception_count: new exception for Rule
    :param cleared_loans_count: total cleared loans
    :param error_msg: error msg if error occurred
    :return: will return insertion detail
    """
    try:
        priority = 2
        priority_dict = {"Critical": 0,
                         "High": 1,
                         "Medium": 2,
                         "Low": 3,
                         "Informational": 4}
        if severity:
            for item in priority_dict.keys():
                if severity == item:
                    priority = priority_dict[item]
        summary = dict()
        summary["RunId"] = run_id
        summary["RuleSetId"] = ObjectId(rule_set_id)
        summary["RuleId"] = ObjectId(rule_id)
        summary["Severity"] = severity
        summary["Priority"] = priority
        summary["Exception"] = exception_count
        summary["NewException"] = new_exception_count
        summary["ClearedException"] = cleared_loans_count
        summary["ExecutionCompleted"] = True
        if error_msg is not None:
            summary["ExecutionCompleted"] = False
            summary["ErrorMessage"] = error_msg
        rule_set = get_rule_set(rule_set_id)
        lambda_metric(
            metric_name='bizzy.bre.rule.exceptions',
            value=exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        start_time = round(time.time() * 1000)
        details = db.RuleSummary.insert(summary)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:External']
        )
        return details
    except Exception as err:
        print(err)
        traceback.print_exc()


def put_in_result_set_queue(run_id, rule_set_id, rule_id, version, status, name, loan_number, collection,
                            display_fields, in_existence, sys_note=[], note_detail=None, severity="Medium", priority=2,
                            ignore=False, business_rule_id=""):
    payload = {
        "run_id": run_id,
        "rule_set_id": str(rule_set_id),
        "rule_id": str(rule_id),
        "rule_version": version,
        "exception_status": status,
        "exception": name,
        "loan_number": loan_number,
        "collection": collection,
        "display_fields": display_fields,
        "in_existence": in_existence.strftime("%m/%d/%Y, %H:%M:%S:%f"),
        "system_note": sys_note,
        "note_detail": note_detail,
        "severity": severity,
        "priority": priority,
        "Ignore": ignore,
        "business_rule_id": business_rule_id
    }
    response = sqs.send_message(
        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"],
        MessageBody=json.dumps(payload),
        DelaySeconds=0,
        # MessageGroupId=str(loan_number) + str(rule_id)
    )
    # print("===after==", datetime.now())


def execute_external_rule(rule_set_id, run_id, rule_id, collection, scheduler_id, rule, user_filter, file_name,
                          bucket_path, loan_package_id):
    """
    :param bucket_path: sftp inbound path
    :param user_filter: user access filter
    :param rule: all information of the whole document
    :param scheduler_id: current scheduler id
    :param file_name: external rule file_name whose data has t be fetched from the bucket path
    :param rule_set_id: id of Rule Set.
    :param run_id: id of current scheduled run.
    :param rule_id: id of Rule that is going to execute over loans.
    :param collection: collection on which Rule's criteria will apply.¬
    :return: this function will not return anything, this will be
    invoked asynchronously.
    """
    new_exception = 0
    exception_count = 0
    cleared_loans_count = 0
    business_rule_id = rule["RuleNameId"]

    try:
        source_bucket = bucket_path.split("/")
        if len(source_bucket) > 1:
            bucket = source_bucket[0]
            file_name = "/".join(source_bucket[1:]) + "/" + file_name
        # if file not present.
        try:
            obj = s3.get_object(Bucket=bucket, Key=file_name)
        except Exception as e:
            # if file not present or noo data in it.
            print("error_msg:==", e, "==RuleId:==", rule["_id"], )
            rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], exception_count, new_exception,
                         cleared_loans_count, "File Nt Present")
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [],
                                    None,
                                    rule["Severity"], rule["Priority"], False, business_rule_id)
            return True
        # if no data present in file.
        try:
            csv_string = obj['Body'].read().decode("latin1")
            file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='')
            if file_df.shape[0] == 0:
                raise Exception("No data Present")
        except Exception as e:
            print(e)
            rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], exception_count, new_exception,
                         cleared_loans_count)
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [],
                                    None,
                                    rule["Severity"], rule["Priority"], False, business_rule_id)
            return True
        offset = 0
        limit = 1000
        val = int(file_df.shape[0] / 1000) + 1
        print(val)

        for item in range(val):
            last_counter = 0
            if item == val - 1:
                last_counter = 1
            rule["_id"] = str(rule["_id"])
            payload = {
                "rule": rule,
                "rule_id": str(rule_id),
                "rule_set_id": str(rule_set_id),
                "run_id": run_id,
                "collection": collection,
                "bucket": bucket,
                "file_name": file_name,
                "user_filter": user_filter,
                "offset": offset,
                "limit": limit,
                "last_counter": last_counter,
                "display_fields": rule["DisplayFields"],
                "loan_package_id": str(loan_package_id)
            }
            lambda_client.invoke(
                FunctionName=os.environ["EXTERNAL_RULE_PROC_FUNC_NAME"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
            offset += 1000
            limit += 1000
    except Exception as err:
        print("Exception in external rule===", err)
        rule_summary(run_id, rule_set_id, rule_id, rule["Severity"], 0, 0, 0, str(err))
        put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [], None,
                                rule["Severity"], rule["Priority"], False, business_rule_id)


def execute_rule_handler(event, context):
    first_start_time = round(time.time() * 1000)
    if event.get("Records", None):
        msg_body = json.loads(event["Records"][0]["body"])
    else:
        msg_body = event
    print("msg_body=", msg_body)
    loan_package_id = msg_body.get("loan_package_id", None)
    scheduler_id = msg_body.get("scheduler_id", None)
    run_id = msg_body.get("run_id", None)
    rule_set_id = msg_body.get("rule_set_id", None)
    rule_id = msg_body.get("rule_id", None)
    collection = msg_body.get("collection", None)
    rule = msg_body.get("rule")
    user_filter = msg_body.get("user_filter", {})
    file_name = msg_body.get("file_name")
    bucket_path = msg_body.get("bucket_path")
    rule_set = get_rule_set(rule_set_id)
    try:
        if None in [run_id, rule_set_id, collection, rule_id]:
            print("Missing required fields!")
            return {
                "error": "Missing required fields!"
            }
        print("rule_id====", rule_id)
        status = execute_external_rule(ObjectId(rule_set_id), run_id, ObjectId(rule_id), collection, scheduler_id, rule,
                                       user_filter, file_name, bucket_path, loan_package_id)
        print("Running status", status)
        lambda_metric(
            metric_name='bizzy.bre.rule.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'loan_package_id:' + str(loan_package_id),
                  'scheduler_id:' + str(scheduler_id), 'ruletype:External']
        )
        return {"message": "Execution Completed"}

    except Exception as e:
        print(e)
        traceback.print_exc()
        return {"error": str(e)}

import json
import os

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        client = db.ApplicationDetails.find_one({})['Client']
        event_bridge = boto3.client("events", region_name="us-east-1")
        scheduler_id = event.get("scheduler_id", None)

        rule_name = f"Scheduler_{scheduler_id}_{client}"
        print(" ..rule name:: *{}* ".format(rule_name))

        scheduler_dict = dict()
        scheduler_dict["SchedulerId"] = str(scheduler_id)
        scheduler_dict['Name'] = rule_name
        print('scheduler_dict: .. ', scheduler_dict)

        event_bridge.put_rule(Name=rule_name, ScheduleExpression='rate(45 minutes)', State='ENABLED')

        rslt = event_bridge.put_targets(Rule=rule_name,
                                        Targets=[
                                            {
                                                "Arn": os.environ["STATE_MACHINE_STATUS_ARN"],
                                                "Input": json.dumps(scheduler_dict),
                                                'Id': 'MyTargetID',
                                            }]
                                        )
        print('rslt. ', rslt)
        return {
            "statusCode": 200,
            "message": "Scheduler triggered event successfully."
        }
    except Exception as e:
        return {"statusCode": 500, "error": str(e)}


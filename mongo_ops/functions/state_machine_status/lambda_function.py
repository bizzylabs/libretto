
import json
import os

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    print('event: .. . ..  ', event)
    event_bridge = boto3.client("events", region_name="us-east-1")
    rule_name = event['Name']
    scheduler_id = event['SchedulerId']

    try:
        current_status = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)}, {"CurrentStatus": 1, "Name": 1})[
            "CurrentStatus"]
        print('current_status ...', current_status)
        if not current_status.lower().strip() in ['completed', 'failed']:
            data = db.SchedulerConfig.update_one({"_id": ObjectId(scheduler_id)}, {"$set": {"CurrentStatus": 'Failed'}})
            print(data.raw_result)
        ## removing triggers
        event_bridge.remove_targets(Rule=rule_name, Ids=["MyTargetID"], Force=True)
        event_bridge.delete_rule(Name=rule_name, Force=True)
    except Exception as e:
        return {"statusCode": 500, "error": str(e)}

    return {
        'statusCode': 200,
        'body': json.dumps('Scheduler triggered event successfully.')
    }

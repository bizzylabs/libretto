import json
import os
from datetime import datetime
import boto3
from bson.objectid import ObjectId
import traceback
import time
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

null = None
false = False
true = True
prev_loans = dict()
sqs = boto3.client('sqs')
s3 = boto3.resource('s3')
lambda_client = boto3.client("lambda")


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def rule_summary(run_id, rule_set_id, rule_id, severity, exception_count, new_exception_count, cleared_loans_count,
                 error_msg=None):
    """
    :param run_id: id of current scheduled run
    :param rule_set_id: id of Rule Set
    :param rule_id: id of Rule that is executed
    :param exception_count: no of exception found for Rule
    :param new_exception_count: new exception for Rule
    :param cleared_loans_count: total cleared loans
    :param error_msg: error msg if error occurred
    :return: will return insertion detail
    """
    try:
        priority = 2
        priority_dict = {"Critical": 0,
                         "High": 1,
                         "Medium": 2,
                         "Low": 3,
                         "Informational": 4}
        if severity:
            for item in priority_dict.keys():
                if severity == item:
                    priority = priority_dict[item]
        summary = dict()
        summary["RunId"] = run_id
        summary["RuleSetId"] = ObjectId(rule_set_id)
        summary["RuleId"] = ObjectId(rule_id)
        summary["Severity"] = severity
        summary["Priority"] = priority
        summary["Exception"] = exception_count
        summary["NewException"] = new_exception_count
        summary["ClearedException"] = cleared_loans_count
        summary["ExecutionCompleted"] = True
        if error_msg is not None:
            summary["ExecutionCompleted"] = False
            summary["ErrorMessage"] = error_msg
        rule_set = get_rule_set(rule_set_id)
        lambda_metric(
            metric_name='bizzy.bre.rule.exceptions',
            value=exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:Internal']
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.newexception',
            value=new_exception_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id)]
        )
        lambda_metric(
            metric_name='bizzy.bre.rule.clearedexception',
            value=cleared_loans_count,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['severity:' + severity, 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id)]
        )
        start_time = round(time.time() * 1000)
        details = db.RuleSummary.insert(summary)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary', 'ruletype:Internal']
        )
        return details
    except Exception as err:
        print(err)
        traceback.print_exc()


def get_exceptions(criteria, display_fields, collection, user_filter):
    """
    :param user_filter:
    :param criteria: Rule criteria that will be apply on loans
    :param display_fields: fields/columns that needed after execution
    :param collection: collection on which criteria will be applied.
    :return: returns caught exceptions.
    """
    try:
        condition = eval(criteria)
        projection = eval(display_fields)
        projection.update(user_filter)

        projection["_id"] = 0
        if len(projection.keys()) > 0:
            projection["LoanNumber"] = 1
        # using filtered loan data here
        loans = db[collection].find(condition, projection)
        count = loans.count()
        if count > 0:
            return list(loans)
        else:
            return None
    except Exception as err:
        print(err)
        traceback.print_exc()


def put_in_result_set_queue(run_id, rule_set_id, rule_id, version, status, name, loan_number, collection,
                            display_fields, in_existence, sys_note=[], note_detail=None, severity="Medium", priority=2,
                            ignore=False, business_rule_id=""):
    payload = {
        "run_id": run_id,
        "rule_set_id": str(rule_set_id),
        "rule_id": str(rule_id),
        "rule_version": version,
        "exception_status": status,
        "exception": name,
        "loan_number": loan_number,
        "collection": collection,
        "display_fields": display_fields,
        "in_existence": in_existence.strftime("%m/%d/%Y, %H:%M:%S:%f"),
        "system_note": sys_note,
        "note_detail": note_detail,
        "severity": severity,
        "priority": priority,
        "Ignore": ignore,
        "business_rule_id": business_rule_id
    }
    response = sqs.send_message(
        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"],
        MessageBody=json.dumps(payload),
        DelaySeconds=0,
        # MessageGroupId=str(loan_number) + str(rule_id)
    )
    # print("===after==", datetime.now())


def execute_rule(rule_set_id, run_id, rule_id, collection, scheduler_id, loan_data_fields, rule, user_filter, loan_package_id):
    """
    :param rule_set_id: id of Rule Set.
    :param run_id: id of current scheduled run.
    :param rule_id: id of Rule that is going to execute over loans.
    :param collection: collection on which Rule's criteria will apply.¬
    :return: this function will not return anything, this will be
    invoked asynchronously.
    """
    new_exception = 0
    exception_count = 0
    cleared_loans_count = 0
    business_rule_id = rule["RuleNameId"]
    try:
        rule["_id"] = ObjectId(rule["_id"])
        missing_fields = [RuleLogicField for RuleLogicField in rule["RuleLogicFields"] if
                          RuleLogicField not in loan_data_fields]
        if len(missing_fields) > 0:
            error_msg = "Fields are missing in Loan Data: " + str(missing_fields)
            print("error_msg:==", error_msg, "==RuleId:==", rule["_id"], "==Fields:==", rule["RuleLogicFields"])
            rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], exception_count, new_exception,
                         cleared_loans_count, error_msg)
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [],
                                    None,
                                    rule["Severity"], rule["Priority"], False, business_rule_id)
            return True

        try:
            print("=========== Running Query ============")
            exception = get_exceptions(rule["Criteria"], rule["DisplayFields"], collection,
                                       user_filter)
            if exception is not None:
                exception_count = len(exception)
            print("============Query Ends ===============")
        except Exception as e:
            print("==============Errr in query==================")
            traceback.print_exc()
            raise e

        loans_with_exception = []
        # if exception found, save loans info with exception_status true
        if exception is not None:
            exception_status = True
            print("========Exception Loop=========")
            loans_with_exception = exception
            system_note = []
            note_detail = None
            in_existence = datetime.now()
        loan_file_name = "loan_data" + "_" + str(rule_set_id) + "_" + str(rule_id) + "_" + str(run_id) + ".txt"
        s3object = s3.Object(os.environ["BRE_BUCKET"], loan_file_name)
        s3object.put(
            Body=(bytes(str(loans_with_exception).encode('utf-8')))
        )
        if exception_count > 0:
            print(exception_count)
            offset = 0
            limit = 1000
            val = int(exception_count / 1000) + 1
            for item in range(val):
                last_counter = 0
                if item == val - 1:
                    last_counter = 1
                rule["_id"] = str(rule["_id"])
                total_loans = loans_with_exception[offset:limit]
                if len(total_loans) > 0:
                    payload = {
                        "run_id": run_id,
                        "rule_set_id": str(rule_set_id),
                        "rule": rule,
                        "collection": collection,
                        "loan_file": loan_file_name,
                        "offset": offset,
                        "limit": limit,
                        "user_filter": user_filter,
                        "display_fields": rule["DisplayFields"],
                        "last_counter": last_counter,
                        "exception_count": exception_count,
                        "loan_package_id": str(loan_package_id)
                    }

                    lambda_client.invoke(
                        FunctionName=os.environ["BRE_INSERT_DATA_FUNC"],
                        InvocationType='Event',
                        Payload=json.dumps(payload)
                    )
                offset += 1000
                limit += 1000
        print("========After Exception Loop=========")

        rule_summary(run_id, rule_set_id, rule["_id"], rule["Severity"], exception_count, new_exception,
                     cleared_loans_count)
        if exception_count == 0:
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [],
                                    None,
                                    rule["Severity"], rule["Priority"], False, business_rule_id)
        return True
    except Exception as err:
        print("Exception in execute_rule===", err)
        rule_summary(run_id, rule_set_id, rule_id, rule["Severity"], 0, 0, 0, str(err))
        put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                True, rule["Name"], None, collection, rule["DisplayFields"], datetime.now(), [], None,
                                rule["Severity"], rule["Priority"], False, business_rule_id)


def execute_rule_handler(event, context):
    try:
        first_start_time = round(time.time() * 1000)
        if event.get("Records", None):
            msg_body = json.loads(event["Records"][0]["body"])
        else:
            msg_body = event
        print("msg_body=", msg_body)
        loan_package_id = msg_body.get("loan_package_id", None)
        scheduler_id = msg_body.get("scheduler_id", None)
        run_id = msg_body.get("run_id", None)
        rule_set_id = msg_body.get("rule_set_id", None)
        rule_id = msg_body.get("rule_id", None)
        collection = msg_body.get("collection", None)
        loan_data_columns = msg_body.get("loan_data_columns")
        rule = msg_body.get("rule")
        user_filter = msg_body.get("user_filter", {})
        rule_set = get_rule_set(rule_set_id)
        try:
            if None in [run_id, rule_set_id, collection, rule_id]:
                print("Missing required fields!")
                return {
                    "error": "Missing required fields!"
                }
            print("rule_id====", rule_id)
            status = execute_rule(ObjectId(rule_set_id), run_id, ObjectId(rule_id), collection, scheduler_id,
                                  loan_data_columns, rule, user_filter, loan_package_id)
            print("Running status", status)
            lambda_metric(
                metric_name='bizzy.bre.rule.execution',
                value=round(time.time() * 1000) - first_start_time,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                      'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id), 'loan_package_id:' + str(loan_package_id),
                      'scheduler_id:' + str(scheduler_id), 'ruletype:Internal']
            )
            return {"message": "Execution Completed"}

        except Exception as e:
            print(e)
            traceback.print_exc()
            return {"error": str(e)}

    except Exception as err:
        return {'statusCode': 500, "error": "execute rule failed"}

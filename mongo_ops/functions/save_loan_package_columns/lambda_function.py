import json
import re
import io
import pandas as pd
from pymongo import MongoClient
import boto3
from bson.objectid import ObjectId
from datetime import datetime
import os
import time
from datadog_lambda.metric import lambda_metric

secret = boto3.client("secretsmanager", region_name='us-east-1')
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] +
    "/" + secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.client("s3")


def camelcase(s):
    val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', s).strip()  # adding space after camelcase
    val1 = re.search("\d{1,}", val)  # search for number after lower case
    if val1:
        final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
    else:
        final_key = val
    return final_key


def get_columns_for_file(bucket, filename):
    fileObj = s3.get_object(Bucket=bucket, Key=filename)
    df = pd.read_csv(io.StringIO(fileObj["Body"].read().decode('utf-8')), sep='\t', lineterminator='\n')
    columns = list(df.columns)
    columns = [{"Name": column, "Display Name": camelcase(column), "CreatedOn": datetime.today(),
                "UpdatedOn": datetime.today(), "Width": int(str(df[column].astype(str).str.len().max()))} for
               column in columns]

    lambda_metric(
        metric_name='bizzy.bre.loanpackagefile.columns',
        value=len(columns),
        timestamp=int(time.time()),  # optional, must be within last 20 mins
        tags=["filename:" + filename]
    )
    return columns


def supporting_file(file_subtype, this_file_subtype):
    name = ""
    for file in file_subtype:
        if str(file['_id']) == str(this_file_subtype):
            name = file['Name']
    return name


def get_rule(rule_id):
    rule = db.Rule.find_one({"_id": ObjectId(rule_id)}, {"Name": 1, "IsPublished": 1, "ExternalSource": 1})
    return rule


def lambda_handler(event, context):
    loan_package_id = ObjectId(event.get("loanPackageId"))
    s3_path = ""
    for config in db.ApplicationDetails.find({}):
        s3_path = config.get("BucketPaths").get("SFTPInboundPath")
    bucket = s3_path.split("/")[0]
    path = "".join(s3_path.split("/")[1:]) + "/"

    package_detail = list(db.LoanPackageDetail.aggregate([
        {
            "$match": {"LoanPackageId": loan_package_id}
        },

        {
            "$lookup":
                {
                    "from": "SupportingFileType",
                    "localField": "FileType",
                    "foreignField": "_id",
                    "as": "FileType"
                }
        }

    ]))

    data = list(db.LoanPackageColumns.find({"LoanPackageId": loan_package_id}))
    file_subtype = list(db.SupportingFileSubType.find({}))

    if not data:
        data = {"LoanPackageId": loan_package_id,
                "Columns": {
                    "Loan": [],
                    "Escrow": [],
                    "Task": {},
                    "ArmChanges": [], "Tempo": []
                }
                }
        for package_file in package_detail:

            if package_file["IsMain"]:
                data["Columns"]["Loan"] = get_columns_for_file(bucket, path + package_file["FileName"])
            else:
                if package_file.get("FileSubType", None) is not None:
                    this_file_subtype = package_file.get("FileSubType")
                    if supporting_file(file_subtype, this_file_subtype) == 'Active':
                        data["Columns"]['Task']['Active'] = get_columns_for_file(bucket,
                                                                                 path + package_file["FileName"])
                    elif supporting_file(file_subtype, this_file_subtype) == 'Archive':
                        data["Columns"]['Task']['Archive'] = get_columns_for_file(bucket,
                                                                                  path + package_file["FileName"])
                else:
                    data["Columns"][package_file["FileType"][0]["FileType"]] = get_columns_for_file(bucket,
                                                                                                    path + package_file[
                                                                                                        "FileName"])
    else:
        print("Already existing loan_package_id. ")
        data = data[0]
        old_data = {"Loan": {},
                    "Escrow": {},
                    "Task": {'Active': {}, 'Archive': {}},
                    "ArmChanges": {}, "Tempo": {}}

        for filetype in data["Columns"]:
            if filetype == 'Task':
                for filesubtype in data["Columns"][filetype]:
                    for column in data["Columns"][filetype][filesubtype]:
                        old_data[filetype][filesubtype][column["Name"]] = column
            else:
                for column in data["Columns"][filetype]:
                    old_data[filetype][column["Name"]] = column

        for package_file in package_detail:

            if package_file["IsMain"]:
                latest_columns = get_columns_for_file(bucket, path + package_file["FileName"])
                for new_column in latest_columns:
                    if new_column["Name"] not in old_data["Loan"]:
                        data["Columns"]["Loan"].append(new_column)
                    else:
                        for column in data["Columns"]["Loan"]:
                            if column["Name"] == new_column["Name"] and column["Width"] != new_column["Width"]:
                                column["UpdatedOn"] = new_column["UpdatedOn"]
                                column["Width"] = new_column["Width"]
                                column["Display Name"] = new_column["Display Name"]

            else:
                latest_columns = get_columns_for_file(bucket, path + package_file["FileName"])
                if package_file.get("FileSubType", None) is not None:
                    this_file_subtype = package_file.get("FileSubType")
                    filesubtype_name = supporting_file(file_subtype, this_file_subtype)
                    for new_column in latest_columns:
                        if new_column['Name'] not in old_data['Task'][filesubtype_name]:
                            data["Columns"]['Task'][filesubtype_name].append(new_column)
                        else:
                            for column in data["Columns"]['Task'][filesubtype_name]:
                                if column["Name"] == new_column["Name"] and column["Width"] != new_column["Width"]:
                                    column["UpdatedOn"] = new_column["UpdatedOn"]
                                    column["Width"] = new_column["Width"]
                                    column["Display Name"] = new_column["Display Name"]



                else:
                    for new_column in latest_columns:
                        if new_column["Name"] not in old_data[package_file["FileType"][0]["FileType"]]:
                            data["Columns"][package_file["FileType"][0]["FileType"]].append(new_column)
                        else:
                            for column in data["Columns"][package_file["FileType"][0]["FileType"]]:
                                if column["Name"] == new_column["Name"] and column["Width"] != new_column["Width"]:
                                    column["UpdatedOn"] = new_column["UpdatedOn"]
                                    column["Width"] = new_column["Width"]
                                    column["Display Name"] = new_column["Display Name"]
    if "_id" in data:
        start_time = round(time.time() * 1000)
        db.LoanPackageColumns.update({'_id': data["_id"]}, {"$set": data})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=["loan_package_id:" + str(loan_package_id), 'collection:LoanPackageColumns']
        )
    else:
        start_time = round(time.time() * 1000)
        db.LoanPackageColumns.insert_one(data)
        lambda_metric(
            metric_name='bizzy.bre.db.insert',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=["loan_package_id:" + str(loan_package_id), 'collection:LoanPackageColumns']
        )
    lambda_invoke = boto3.client("lambda", region_name="us-east-1")
    external_rule_file_names = []
    rule_sets = db.RuleSetMaster.find({"RuleSetName": {"$ne": "Draft Rules"}})
    for rule_set in rule_sets:
        if len(rule_set["RuleIds"]) > 0:
            for rule_id in rule_set["RuleIds"]:
                rule = get_rule(rule_id["RuleId"])
                if rule["IsPublished"] == 1 and rule["ExternalSource"]:
                    file_name = f'{rule_set["RuleSetName"]}_{rule["Name"]}.txt'
                    external_rule_file_names.append(file_name)
    payload = {
        "external_files": external_rule_file_names,
        "total_files": len(external_rule_file_names),
        "loan_package_id": str(loan_package_id)
    }
    lambda_invoke.invoke(
        FunctionName=os.environ["POPULATE_EXTERNAL_FILE_COLUMNS_FUNC_NAME"],
        InvocationType='Event',
        Payload=json.dumps(payload)
    )
    return {"result": "success"}

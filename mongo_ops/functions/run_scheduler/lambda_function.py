import json
import os
from datetime import datetime
import boto3
from bson.objectid import ObjectId
import traceback
import time
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])

db = client[secret_dict["db_name"]]
null = None
false = False
true = True


def get_loan_data_fields(scheduler_id):
    """
    :return: Columns of LoanData collection
    Get the columns of Loan Data and Escrow Line,
    to check in advance whether the columns used in query is in collection
    or not. If columns do not exists and has been used in query, Rule will
    not be executed.
    """
    loan_data_columns = db.BRELoanDataColumns.find_one({})
    columns = []
    if loan_data_columns is not None:
        columns.extend(loan_data_columns["LoanDataFields"])
        columns.extend(loan_data_columns.get(f'{scheduler_id}_EscrowDataFields', []))
        columns.extend(loan_data_columns.get(f'{scheduler_id}_Task_ActiveDataFields', []))
        columns.extend(loan_data_columns.get(f'{scheduler_id}_Task_ArchiveDataFields', []))
        columns.extend(loan_data_columns.get(f'{scheduler_id}_ArmChangesDataFields', []))
        columns.extend(loan_data_columns.get(f'{scheduler_id}_TempoDataFields', []))
    lambda_metric(
        metric_name='bizzy.bre.loandatafields',
        value=len(columns),
        timestamp=int(time.time()),  # optional, must be within last 20 mins
        tags=['scheduler_id:' + str(scheduler_id), 'collection:BRELoanDataColumns']
    )
    return columns


def get_timestamp():
    now = datetime.now()
    return int(datetime.timestamp(now) * 1000)  # in milli_seconds


# save scheduler detail
def save_scheduler_history(scheduler_id, scheduler_name, rule_set_ids, loan_package_id):
    data = dict()
    now = datetime.now()
    timestamp = get_timestamp()
    run_id = timestamp
    data["LoanPackage"] = ObjectId(loan_package_id)
    data["SchedulerConfigId"] = scheduler_id
    data["SchedulerConfigName"] = scheduler_name
    data["RuleSetIds"] = rule_set_ids
    data["IsActive"] = 0
    data["RunId"] = run_id
    data["CreatedOn"] = now
    data["UpdatedOn"] = now
    start_time = round(time.time() * 1000)
    db.SchedulerHistory.insert(data)
    lambda_metric(
        metric_name='bizzy.bre.db.insert',
        value=round(time.time() * 1000) - start_time,
        timestamp=int(time.time()),  # optional, must be within last 20 mins
        tags=['scheduler_id:' + str(scheduler_id), 'run_id:' + str(run_id), 'collection:SchedulerHistory']
    )
    return run_id


# @datadog_lambda_wrapper
def start_executor(event, context):
    try:
        sqs = boto3.client('sqs')
        scheduler_id = event.get("scheduler_id", None)
        loan_package_id = event.get("loan_package_id", None)
        loan_data_prime_key = event.get("loan_data_prime_key", None)
        loan_package_name = event.get("loan_package_name", None)
        scheduler_config = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)})

        # fetch all user access filters needed
        filter_data = db.UserAccessFilters.find_one({})["Filters"]
        user_filter = {item: 1 for item in filter_data[loan_package_name]}

        scheduler_id = scheduler_config["_id"]  # scheduler_config_id
        scheduler_name = scheduler_config["Name"]  # scheduler_config_id
        rule_set_ids = scheduler_config["RuleSetIds"]
        config = db.ApplicationDetails.find_one({})
        bucket_path = config["BucketPaths"]["SFTPInboundPath"]
        # create RunId
        run_id = save_scheduler_history(scheduler_id, scheduler_name, rule_set_ids, loan_package_id)

        # get all loan data columns needed
        bre_loan_data_columns = get_loan_data_fields(scheduler_id)

        start_time = round(time.time() * 1000)
        # Update status of current run in scheduler - for UI 
        result = db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                           {"$set": {"CurrentStatus": "Update in progress - Rule engine"}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['scheduler_id:' + str(scheduler_id), 'run_id:' + str(run_id), 'collection:SchedulerConfig']
        )
        print(result)
        # execute rule set
        for rule_set_id in rule_set_ids:
            last_counter = 0
            if rule_set_id == rule_set_ids[-1]:
                last_counter = 1
            payload = {
                "rule_set_id": str(rule_set_id),
                "run_id": run_id,
                "run_type": "scheduler",
                "last_counter": last_counter,
                "scheduler_id": str(scheduler_id),
                "loan_package_id": loan_package_id,
                "loan_data_prime_key": loan_data_prime_key,
                "loan_data_columns": bre_loan_data_columns,
                "user_filter": user_filter,
                "bucket_path": bucket_path

            }
            response = sqs.send_message(
                QueueUrl=os.environ["RULESET_QUEUE_URL"],
                MessageBody=json.dumps(payload),
                DelaySeconds=0,
                MessageGroupId=str(rule_set_id) + "_" + str(scheduler_id)
            )
        lambda_metric(
            metric_name="bizzy.bre.rulesets",
            value=len(rule_set_ids),
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['scheduler_id:' + str(scheduler_id), 'run_id:' + str(run_id)]
        )
        return {
            "code": 200,
            "message": "Execution successful"
        }
    except Exception as err:
        print(str(err))
        traceback.print_exc()
        start_time = round(time.time() * 1000)
        # Update status of current run in scheduler - for UI
        db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                  {"$set": {"CurrentStatus": "Failed"}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['scheduler_id:' + str(scheduler_id), 'run_id:' + str(run_id), 'collection:SchedulerConfig']
        )
        return {
            "code": 200,
            "message": err
        }

import json
import boto3
import time
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime
import os
import traceback
import time
from datadog_lambda.metric import lambda_metric

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
lambda_invoke = boto3.client("lambda", region_name="us-east-1")
env_name = os.environ["CLIENT_ENV"]


def lambda_handler(event, context):
    scheduler_id = event.get("scheduler_id", None)
    run_id = event.get("run_id", None)
    counter = event.get("counter", None)
    try:
        db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                  {"$set": {"LastRun": datetime.now().strftime("%Y-%m-%d, %H:%M:%S")}})
        if counter == 8:
            # Update status of current run in scheduler - for UI
            start_time = round(time.time() * 1000)
            db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                      {"$set": {"CurrentStatus": "Failed"}})

            lambda_metric(
                metric_name='bizzy.bre.db.update',
                value=round(time.time() * 1000) - start_time,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=["run_id:" + str(run_id), "scheduler_id:" + str(scheduler_id), 'collection:SchedulerConfig']
            )
            lambda_metric(
                metric_name='bizzy.bre.execution',
                value=round(time.time() * 1000),
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=["scheduler_id:" + str(scheduler_id), "run_id:" + str(run_id), "bre_status:Stop",
                      "date_time:" + datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
            )
            payload = {
                "subject": env_name + " UI not updated",
                "message": env_name + " UI is not populated after today's BRE execution"
            }
            lambda_invoke.invoke(
                FunctionName='sns_message_publisher',
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
        else:
            # Update status of current run in scheduler - for UI
            start_time = round(time.time() * 1000)
            db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                      {"$set": {"CurrentStatus": "Completed"}})
            lambda_metric(
                metric_name='bizzy.bre.db.update',
                value=round(time.time() * 1000) - start_time,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=["run_id:" + str(run_id), "scheduler_id:" + str(scheduler_id), 'collection:SchedulerConfig']
            )
            scheduler_config = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)})
            rule_set_ids = scheduler_config["RuleSetId"]
            failed_rule_sets = {}
            for rule_set_id in rule_set_ids:
                ruleset_name = rule_set_id['label']
                result_set_details = db.ResultSetSummary.find_one(
                    {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id['value'])})
                if result_set_details:
                    if result_set_details["ExecutionCompleted"] == False or result_set_details["ErrorMessage"] != None:
                        failed_rule_sets[ruleset_name] = {}
                        failed_rule_sets[ruleset_name]["ErrorMessage"] = result_set_details["ErrorMessage"]
                        rules_summary = db.RuleSummary.find(
                            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id['value']), "ExecutionCompleted": False})
                        failed_rules = {}
                        for rule in rules_summary:
                            rule_details = db.Rule.find_one({"_id": rule["RuleId"]})
                            if rule_details:
                                failed_rules[rule_details["Name"]] = rule["ErrorMessage"]
                        failed_rule_sets[ruleset_name]["FailedRules"] = failed_rules
                else:
                    # for debugging
                    print(run_id)
                    print(rule_set_id)
                    print(result_set_details)
            # check on the failed rule sets len and have another message. 
            print(failed_rule_sets)
            if len(failed_rule_sets) > 0:
                print(failed_rule_sets)
                lambda_metric(
                    metric_name='bizzy.bre.execution',
                    value=round(time.time() * 1000),
                    timestamp=int(time.time()),  # optional, must be within last 20 mins
                    tags=["scheduler_id:" + str(scheduler_id), "run_id:" + str(run_id), "bre_status:Stop",
                          "date_time:" + datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
                )
                payload = {
                    "subject": env_name + " UI updated",
                    "message": env_name + " UI is populated. But some rulesets failed. Enclosed the ruleset and the rules with the error messages: " + str(
                        failed_rule_sets)
                }
                lambda_invoke.invoke(
                    FunctionName='sns_message_publisher',
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
            elif len(failed_rule_sets) == 0:
                # Publish SNS message 
                lambda_metric(
                    metric_name='bizzy.bre.execution',
                    value=round(time.time() * 1000),
                    timestamp=int(time.time()),  # optional, must be within last 20 mins
                    tags=["scheduler_id:" + str(scheduler_id), "run_id:" + str(run_id), "bre_status:Stop",
                          "date_time:" + datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
                )
                payload = {
                    "subject": env_name + " UI updated",
                    "message": env_name + " UI is populated successfully after today's BRE execution"
                }
                lambda_invoke.invoke(
                    FunctionName='sns_message_publisher',
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
        return True
    except Exception as err:
        print(err)
        traceback.print_exc()
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

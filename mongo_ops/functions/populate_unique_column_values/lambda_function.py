import json
from io import StringIO
import os
import boto3
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient
import time
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper


secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
date_column_list = ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                    "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT", "Note",
                    "CertificateOfReasonableValue", "ClosingDisclosure", "DeedOfTrustMortgage",
                    "EscrowDisclosure", "EscrowWaiver", "Guarantee", "GuarantyAgreement",
                    "HomeOwnersDeclaration", "HUD", "LossMitPackages", "Modifications",
                    "TaxInformation", "45DayLetterRequestedDate",
                    "CCLetterRequestedDate", "HelloWelcomeLetterRequestedDate",
                    "PrivacyNoticeLetterRequestedDate",
                    "ProgramLetterRequestedDate",
                    "TPPLetterRequestedDate",
                    "LMPackageReceivedLetterRequestedDate",
                    "LMPackageRequestLetterRequestedDate"
                    ]

# @datadog_lambda_wrapper
def lambda_handler(event, context):
    loan_package_id = ObjectId(event.get("loanPackageId"))

    pkg_files = list(db.LoanPackageDetail.find({"LoanPackageId": loan_package_id, "IsMain": True}))
    # ensure that supplementary files are processed in the last
    pkg_files.sort(key=lambda x: False if x.get("IsExtra") is None else x.get("IsExtra"))
    print("Package files after sorting: " + str(pkg_files))
    # if two column are present in both main and extra, then in left join we ignore the col in extra file.
    # this list will help us in avoiding updating the unique value count for such columns
    already_seen_columns = []
    for loan_pkj_file in pkg_files:
        if loan_pkj_file:
            file_name = loan_pkj_file["FileName"]
        else:
            return {"Error": "Not found", "status": 404}
        isExtraFile = loan_pkj_file.get("IsExtra") is not None and loan_pkj_file.get("IsExtra") == True
        print(f"Processing File {file_name} with extraFlag as {isExtraFile}")
        sftp_bucket_path = db.ApplicationDetails.find_one({})["BucketPaths"]["SFTPInboundPath"]
        if sftp_bucket_path:
            file_name = sftp_bucket_path.split("/")[1] + "/" + file_name
            bucket = sftp_bucket_path.split("/")[0]
            print(file_name, bucket)
        else:
            return {"Error": "bucket_path not found in event", "status": 404}

        s3 = boto3.client('s3', region_name="us-east-1")
        obj = s3.get_object(Bucket=bucket, Key=file_name)
        csv_string = obj['Body'].read().decode("latin1")
        file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='')
        trim_strings = lambda x: x.strip() if isinstance(x, str) else x
        dataframe = file_df.applymap(trim_strings)
        del csv_string
        del trim_strings
        del file_df
        col = list(dataframe.columns)
        for column in col:
            if 'date' in column.lower() or column in date_column_list:
                try:
                    dataframe[column] = pd.to_datetime(dataframe[column])
                except Exception as e:
                    print(e)
                    print(column, '++++++++++++++++++++++')
        print("done")
        for column in col:
            if 'date' in column.lower() or column in date_column_list:
                dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
        print("done")
        dataframe = dataframe.replace(r'', np.NaN, regex=True)
        for column in dataframe.columns:
            if dataframe[column].dtype == 'O':
                dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
            elif dataframe[column].dtype == 'int64':
                dataframe[column] = dataframe[column].astype(int).where(dataframe[column].notnull(), None)
            elif dataframe[column].dtype == 'float64':
                dataframe[column] = dataframe[column].astype(float).where(dataframe[column].notnull(), None)
        new_df = dataframe.copy()
        del dataframe
        try:
            new_df[['MersMin']] = new_df[['MersMin']].astype(str)
        except Exception as e:
            pass

        final = []
        for current_file_column in new_df.columns:
            # check if the column already present in previous main files.
            if current_file_column in already_seen_columns:
                print(f"Found a duplicate column {current_file_column} in filename: {file_name}")
                continue
            else:
                already_seen_columns.append(current_file_column)
            column_info = dict()
            unique_count = len(new_df[current_file_column].unique())
            column_info["Name"] = current_file_column
            column_info["Count"] = unique_count
            print(f"Found unique value {unique_count} in column:{current_file_column}")
            final.append(column_info)

        loan_pkg_columns = db.LoanPackageColumns.find_one({"LoanPackageId": loan_package_id})["Columns"]["Loan"]

        lambda_metric(
            metric_name='bizzy.bre.loanpackagefile.columns',
            value= len(list(loan_pkg_columns)),
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["loan_package_id:"+str(loan_package_id), "file_name:"+file_name]
            )

        for loan_value in loan_pkg_columns:
            for new_item in final:
                if loan_value["Name"] == new_item["Name"]:
                    loan_value.update({"Count": new_item["Count"]})
                    break
        # update LoanPackageColumns Loan
        start_time = round(time.time() * 1000)
        update_query = db.LoanPackageColumns.update(
            {
                "LoanPackageId": loan_package_id
            },
            {
                "$set": {"Columns.Loan": loan_pkg_columns
                         }
            }
        )

        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["loan_package_id:"+str(loan_package_id), 'collection:LoanPackageColumns']
            )

    return {
        'statusCode': 200,
        'body': "LoanPackageColumns columns have been updated."
    }

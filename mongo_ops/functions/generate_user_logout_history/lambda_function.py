import csv
import datetime
import io
import json
import os

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]


def get_csv_data(data):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post)
    keys = data[0].keys()
    csv_out.writerow(keys)
    for row in data:
        csv_out.writerow(row.values())
    print("Done")
    return data_to_post.getvalue()


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            s3 = boto3.client("s3")
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data)
            s3 = boto3.resource('s3')
            object_acl = s3.ObjectAcl(bucket_name, file_name)
            response = object_acl.put(ACL='public-read')
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def extract_date_details(date):
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime


def lambda_handler(event, context):
    try:
        null = None
        false = False
        true = True

        current_time = datetime.datetime.now() + datetime.timedelta(
            hours=24)  # To add the logs during current day as well
        yesterdays_date = (datetime.datetime.now() - datetime.timedelta(hours=24))

        # Validate and send 500 return with error message as shown above inside save_data
        if event.get("from_date", yesterdays_date):
            gmt_from_date = event.get("from_date", yesterdays_date)
        else:
            return {"status": 500, "body": "Missing parameter value for yesterday_date"}

        if event.get("to_date", current_time):
            gmt_to_date = event.get("to_date", current_time)
        else:
            return {"status": 500, "body": "Missing parameter value for yesterday_date"}

        from_date = extract_date_details(gmt_from_date)
        to_date = extract_date_details(gmt_to_date)

        login_pipe = [{"$match": {"$expr": {
            "$and": [{"$eq": ["$action", "login"]}, {"$gte": ["$timestamp", from_date]},
                     {"$lte": ["$timestamp", to_date]}]}}}, {"$project": {"_id": 0}}]
        login_logs = db.BussinessAuditLogs.aggregate(login_pipe)

        login_count = 0
        user_logout_logger = []
        # Iterate each user's login log and check for logout within the interval else append a log to logout logger list
        for user_login_log in login_logs:
            login_count += 1

            # Get 24 hour interval from the login of the user
            username = user_login_log["username"]
            from_datetime = user_login_log["timestamp"]
            to_datetime = user_login_log["timestamp"] + datetime.timedelta(hours=24)
            pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$username", username]}, {"$gt": ["$timestamp", from_datetime]},
                         {"$lte": ["$timestamp", to_datetime]}]}}}]
            user_interval_logs = db.BussinessAuditLogs.aggregate(pipe)

            # LOGIC:
            # min_login = if the interval has another login - get the diff with current_login -> min value will be the first login after current login; track min_login timestamp
            # min_logout = if the interval has a logout - get the diff with current_login  -> min value will be the first logout after current login
            # min_if the interval has a timeout - get the diff with current_login -> min value will be the first timestamp after current login
            # if this min_logout or min_timeout is greater than min_login -> meaning there is no logout or timeout between two logins. If so, then
            #           if the min_login is not within 15 minutes from the current_login then create a logout with 15 minutes from current_login.
            #           if the min_login is within 15 minutes from the current_login the create a logout with 1 minutes before the  min_login timestamp

            current_login_time = user_login_log["timestamp"]
            min_login_diff = datetime.timedelta(hours=24)
            min_logout_diff = datetime.timedelta(hours=24)
            min_timeout_diff = datetime.timedelta(hours=24)
            min_login_time = current_login_time + datetime.timedelta(seconds=2)
            for user_log in user_interval_logs:
                user_log_diff = user_log["timestamp"] - current_login_time
                if user_log["action"] == "login" and user_log_diff < min_login_diff:
                    min_login_diff = user_log_diff
                    min_login_time = user_log["timestamp"]
                elif user_log["action"] == "logout" and user_log_diff < min_logout_diff:
                    min_logout_diff = user_log_diff
                elif user_log["action"] == "timeout" and user_log_diff < min_timeout_diff:
                    min_timeout_diff = user_log_diff

            new_logout_log = {}

            # if this min_logout or min_timeout is greater than min_login -> meaning there is no logout or timeout between two logins. If so, then
            if min_logout_diff > min_login_diff and min_timeout_diff > min_login_diff and min_login_time > current_login_time + datetime.timedelta(
                    seconds=2):
                # if the min_login is within 15 minutes from the current_login the create a logout with 1 minutes before the  min_login timestamp
                if min_login_diff <= datetime.timedelta(minutes=15):
                    new_logout_log = user_login_log
                    new_logout_log["action"] = "logout"
                    if min_login_diff <= datetime.timedelta(minutes=1):
                        new_logout_log["timestamp"] = min_login_time - datetime.timedelta(
                            seconds=1)  # logout 1 second before next login
                    else:
                        new_logout_log["timestamp"] = min_login_time - datetime.timedelta(
                            minutes=1)  # logout 1 minute before next login
                    user_logout_logger.append(new_logout_log)
                # if the min_login is not within 15 minutes from the current_login then create a logout with 15 minutes from current_login.
                elif min_login_diff > datetime.timedelta(minutes=15):
                    new_logout_log = user_login_log
                    new_logout_log["action"] = "logout"
                    new_logout_log["timestamp"] = current_login_time + datetime.timedelta(
                        minutes=15)  # logout 15 minutes after the current login
                    user_logout_logger.append(new_logout_log)

        # write logout_logger into the DB
        for log in user_logout_logger:
            db.BussinessAuditLogs.insert(log)

        # Internal use only
        # filename = "Bizzy_usage_report.csv"
        # csv_data = get_csv_data(user_logout_logger)
        # response = save_data(BUCKET_NAME, csv_data, filename)

        return True

    except Exception as e:
        print({'statusCode': 500, "Error": "generate user logout history failed"})

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

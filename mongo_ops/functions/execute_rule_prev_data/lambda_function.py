import datetime
import json
import os
from datetime import timedelta
from io import StringIO
import boto3
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient
import traceback
import time
from datadog_lambda.metric import lambda_metric

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.client('s3')
sqs = boto3.client('sqs')


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": rule_set_id})
    return rule_sets


def load_sftp_file(bucket, file_name, offset, limit):
    obj = s3.get_object(Bucket=bucket, Key=file_name)
    csv_string = obj['Body'].read().decode("latin1")
    file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='', usecols=["LoanNumber"])
    file_df = file_df.iloc[offset:limit, :]
    loan_numbers = file_df["LoanNumber"].tolist()

    return loan_numbers


def loans_caught_in_last_run(rule_set_id, run_id, rule_id, loans, business_rule_id, loan_package_id):
    """
    :param loans:
    :param business_rule_id:
    :param rule_set_id: id of Rule Set
    :param run_id: id of current scheduled run
    :param rule_id: id of Rule that is going to execute over loans.
    :return: loans caught in previous execution of Rule for given Ruleset
    This function will fetch last run stats of rule for given ruleset from ResultSetDetail collection.
    This stat will be used to check cleared loans and new loans.
    """
    scheduler_run_ids = db.SchedulerHistory.find({"LoanPackage": ObjectId(loan_package_id)}).sort(
        [("RunId", -1)]).limit(5)

    run_ids = [item["RunId"] for item in scheduler_run_ids if item]
    pipeline = [
        {"$match": {"$and": [{"RunId": {"$in": run_ids}},
                             {"RuleSetId": rule_set_id},
                             {"RunId": {"$ne": run_id}},
                             {"RuleNameId": business_rule_id},
                             {"LoanNumber": {"$in": loans}}]}},
        {"$group": {"_id": "$RunId", "data": {
            "$push": {"LoanNumber": "$LoanNumber", "ExceptionStatus": "$ExceptionStatus", "InExistence": "$InExistence",
                      "SystemNote": "$SystemNote", "NoteDetail": "$NoteDetail"}}}},
        {"$sort": {"_id": -1}},
        {"$limit": 1}
    ]
    rule_set = get_rule_set(rule_set_id)
    start_time = round(time.time() * 1000)
    prev_detail = db.ResultSetDetail.aggregate(pipeline)
    lambda_metric(
        metric_name='bizzy.bre.db.aggregate',
        value=round(time.time() * 1000) - start_time,
        timestamp=int(time.time()),  # optional, must be within last 20 mins
        tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"], 'rule_id:' + str(rule_id),
              'run_id:' + str(run_id), 'collection:ResultSetDetail']
    )
    return prev_detail


def update_rule_summary(run_id, rule_set_id, rule_id, new_exception_count, cleared_loans_count):
    try:
        data = db.RuleSummary.find_one(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleId": ObjectId(rule_id)})
        new = data.get("NewException", 0)
        cleared = data.get("ClearedException", 0)

        new_exception_count = new + new_exception_count
        cleared_loans_count = cleared + new_exception_count
        print(cleared_loans_count, new_exception_count)
        rule_set = get_rule_set(rule_set_id)
        start_time = round(time.time() * 1000)
        details = db.RuleSummary.update(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleId": ObjectId(rule_id)},
            {"$set": {"NewException": new_exception_count, "ClearedException": cleared_loans_count}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule_id), 'run_id:' + str(run_id), 'collection:RuleSummary']
        )
    except Exception as err:
        print(err)
        traceback.print_exc()


def check_valid_snooze(kwargs):
    no_of_days = kwargs.get("Days", None)
    created_on = kwargs.get("CreatedOn", None)
    try:
        if no_of_days and created_on and created_on.date() + timedelta(
                days=no_of_days) > datetime.datetime.now().date():
            return True
        return False
    except Exception as e:
        traceback.print_exc()
        return False


def put_in_result_set_queue(run_id, rule_set_id, rule_id, version, status, name, loan_number, collection,
                            display_fields, in_existence, sys_note=[], note_detail=None, severity="Medium", priority=2,
                            update_flag=False, ignore=False, business_rule_id=""):
    if not sys_note:
        sys_note = []
    payload = {
        "run_id": run_id,
        "rule_set_id": str(rule_set_id),
        "rule_id": str(rule_id),
        "rule_version": version,
        "exception_status": status,
        "exception": name,
        "loan_number": loan_number,
        "collection": collection,
        "display_fields": display_fields,
        "in_existence": in_existence.strftime("%m/%d/%Y, %H:%M:%S:%f"),
        "system_note": sys_note,
        "note_detail": note_detail,
        "severity": severity,
        "priority": priority,
        "update_flag": update_flag,
        "Ignore": ignore,
        "business_rule_id": business_rule_id
    }
    response = sqs.send_message(
        QueueUrl=os.environ["RESULTSET_DET_QUEUE_URL"],
        MessageBody=json.dumps(payload),
        DelaySeconds=0
    )


def check_prev_status(loan_number, prev_loans):
    exception_status = prev_loans.get(loan_number, None)
    if exception_status is not None and exception_status[0]:
        return exception_status
    else:
        return False


def get_data(file_name):
    try:
        obj = s3.get_object(Bucket=os.environ["BRE_BUCKET"], Key=file_name)
        csv_string = obj['Body'].read().decode("utf-8")
        data = eval(csv_string)

    except Exception as e:
        traceback.print_exc()
        data = []
    return data


def lambda_handler(event, context):
    first_start_time = round(time.time() * 1000)
    new_exception = 0
    cleared_loans_count = 0
    print(event)
    loan_file_name = event.get("loan_file")
    rule = event.get("rule")
    rule_id = ObjectId(event["rule_id"])
    rule_set_id = ObjectId(event["rule_set_id"])
    run_id = event["run_id"]
    collection = event["collection"]
    offset = event["offset"]
    limit = event["limit"]
    bucket = event.get("bucket", None)
    file_name = event.get("file_name", None)
    business_rule_id = rule["RuleNameId"]
    loan_package_id = event.get("loan_package_id")
    rule_set = get_rule_set(rule_set_id)
    try:
        if bucket and file_name:
            loan_numbers = load_sftp_file(bucket, file_name, offset, limit)
        else:
            loan_exceptions = get_data(loan_file_name)[offset:limit]
            loan_numbers = [item["LoanNumber"] for item in loan_exceptions]

        loans_cursor = loans_caught_in_last_run(rule_set_id, run_id, rule_id, loan_numbers, business_rule_id,
                                                loan_package_id)
        print("======= Preparing prev data ======")

        prev_loans = {loan["LoanNumber"]: [loan["ExceptionStatus"], loan.get("InExistence", None),
                                           loan.get("SystemNote", []), loan.get("NoteDetail", None)] for pre_loans in
                      loans_cursor for loan in
                      pre_loans["data"]}
        print("======= Preparing prev data ======")

        print("========Exception Loop=========")
        exception_status = True
        for loan_data in loan_numbers:
            ignore = False
            # check if exception is new
            system_note = []
            note_detail = None
            prev_status = check_prev_status(loan_data, prev_loans)
            if not prev_status:
                new_exception = new_exception + 1
                in_existence = datetime.datetime.now()
            else:
                if prev_status[2] and "Ignore" in prev_status[2]:
                    ignore = True
                if prev_status[2] and "Snooze" in prev_status[2] and prev_status[3] and check_valid_snooze(
                        prev_status[3]):
                    system_note = prev_status[2]
                    if type(prev_status[3]) == dict:
                        note_detail = prev_status[3].copy()
                    else:
                        note_detail = prev_status[3]
                if "Snooze" in prev_status[2] and prev_status[3] and not check_valid_snooze(prev_status[3]):
                    prev_status[2].remove("Snooze")
                    system_note = prev_status[2]
                    note_detail = None
                if prev_status[2] and "Complete" in prev_status[2]:
                    prev_status[2].remove("Complete")
                    system_note = prev_status[2]
                    if type(prev_status[3]) == dict:
                        note_detail = prev_status[3].copy()
                    else:
                        note_detail = prev_status[3]
                else:
                    system_note = prev_status[2]
                    if type(prev_status[3]) == dict:
                        note_detail = prev_status[3].copy()
                    else:
                        note_detail = prev_status[3]
                if prev_status[1] != None:
                    in_existence = prev_status[1]
                else:
                    in_existence = datetime.datetime.now()
            if note_detail:
                if note_detail.get("CreatedOn", None):
                    note_detail["CreatedOn"] = note_detail["CreatedOn"].strftime("%m/%d/%Y %H:%M:%S:%f")
            put_in_result_set_queue(run_id, rule_set_id, rule["_id"], rule["Version"],
                                    exception_status, rule["Name"], loan_data,
                                    collection, rule["DisplayFields"], in_existence, system_note, note_detail,
                                    rule["Severity"], rule["Priority"], True, ignore, business_rule_id)

        print("========After Exception Loop=========")
        lambda_metric(
            metric_name='bizzy.bre.rule.execution',
            value=round(time.time() * 1000) - first_start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=['rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                  'rule_id:' + str(rule["_id"]), 'run_id:' + str(run_id)]
        )
        return True
    except Exception as e:
        print(e)
        traceback.print_exc()
        return True

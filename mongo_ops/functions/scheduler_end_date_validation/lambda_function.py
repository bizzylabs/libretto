import json
from pymongo import MongoClient
import boto3
from datetime import datetime
from bson.objectid import ObjectId
import os
import traceback
import time
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper


secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

# @datadog_lambda_wrapper
def end_date_validation(event, context):
    try:
        scheduler_id = event.get("scheduler_id")
        lambda_metric(
            metric_name='bizzy.bre.execution',
            value= round(time.time() * 1000),
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["scheduler_id:"+str(scheduler_id), 'bre_status:Start', "date_time:" + datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
            )
        scheduler_config = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)})
        end_date = scheduler_config["EndDate"]
        datetimeobj = datetime.strptime(end_date, '%Y-%m-%dT%H:%M:%S.%fZ').replace(hour=0, minute=0, second=0,
                                                                                   microsecond=0)
        current_date = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        if (datetimeobj >= current_date):
            print(datetimeobj, "scheduled end date")
            print(current_date, "current_date")
            # Update status of current run in scheduler - for UI
            print(scheduler_id)
            start_time = round(time.time() * 1000)
            db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                      {"$set": {"CurrentStatus": "Update in progress  - Data loading"}})
            lambda_metric(
                metric_name='bizzy.bre.db.update',
                value= round(time.time() * 1000) - start_time,
                timestamp=int(time.time()), # optional, must be within last 20 mins
                tags=["scheduler_id:"+str(scheduler_id), 'collection:SchedulerConfig']
            )
            return True

        else:
            # print(scheduler_id)
            start_time = round(time.time() * 1000)
            db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                      {"$set": {"Active": 0, "CurrentStatus": "Completed", "IsActive": [{"label": "Deactive", "value": 0}]}})
            lambda_metric(
                metric_name='bizzy.bre.db.update',
                value= round(time.time() * 1000) - start_time,
                timestamp=int(time.time()), # optional, must be within last 20 mins
                tags=["scheduler_id:"+str(scheduler_id), 'collection:SchedulerConfig']
            )
            client = db.ApplicationDetails.find_one({})['Client']
            rule_name = scheduler_config["Name"].replace(" ", "")
            rule_name = rule_name + "_" + client
            event_bridge = boto3.client("events", region_name="us-east-1")
            response = event_bridge.disable_rule(Name=rule_name)
            return False

    except Exception as e:
        print(e)
        traceback.print_exc()
        start_time = round(time.time() * 1000)
        # Update status of current run in scheduler - for UI
        db.SchedulerConfig.update({"_id": ObjectId(scheduler_id)},
                                      {"$set": {"CurrentStatus": "Failed"}})
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["scheduler_id:"+str(scheduler_id), 'collection:SchedulerConfig']
            )
        return {"success": False, "error": True}

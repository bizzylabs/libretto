import json
import os
from io import StringIO
import boto3
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient
import time
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper


secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

date_column_list = ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                    "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT", "Note",
                    "CertificateOfReasonableValue", "ClosingDisclosure", "DeedOfTrustMortgage",
                    "EscrowDisclosure", "EscrowWaiver", "Guarantee", "GuarantyAgreement",
                    "HomeOwnersDeclaration", "HUD", "LossMitPackages", "Modifications",
                    "TaxInformation", "45DayLetterRequestedDate",
                    "CCLetterRequestedDate", "HelloWelcomeLetterRequestedDate",
                    "PrivacyNoticeLetterRequestedDate",
                    "ProgramLetterRequestedDate",
                    "TPPLetterRequestedDate",
                    "LMPackageReceivedLetterRequestedDate",
                    "LMPackageRequestLetterRequestedDate"
                    ]


def create_index(collection_name, index_tuple):
    result = db[collection_name].create_index(index_tuple)
    return result

# @datadog_lambda_wrapper
def lambda_handler(event, context):
    loan_package_id = event.get("loan_package_id")
    package_files = event.get("package_files")
    scheduler_id = event.get("scheduler_id")
    continue_execution = event['iterator'].get("Continue")
    file_dict_index = event['iterator'].get('Index')

    if len(package_files) == 0:
        return {"Continue": False, "Index": file_dict_index + 1}

    start_time = round(time.time() * 1000)
    file_dict = package_files[file_dict_index]
    if file_dict['IsMain'] == True:
        if file_dict.get('IsExtra', None) is True:
            collection = scheduler_id + '_SuppLoan'
        else:
            collection = scheduler_id + '_Loan'
        file_name = file_dict["FileName"]
        ind = file_dict["MatchKey"]
        display_field = "LoanDataFields"
    else:
        collection = scheduler_id + "_" + db.SupportingFileType.find_one({"_id": ObjectId(file_dict["FileType"])})[
            "FileType"]
        if file_dict.get("FileSubType", None):
            collection = collection + "_" + \
                         db.SupportingFileSubType.find_one({"_id": ObjectId(file_dict["FileSubType"])})["Name"]
        file_name = file_dict["FileName"]
        ind = file_dict["MatchKey"]
        display_field = collection + "DataFields"

    if event.get("bucket_path", None):
        source_bucket = event.get("bucket_path").split("/")
        if len(source_bucket) > 1:
            bucket = source_bucket[0]
            file_name = "/".join(source_bucket[1:]) + "/" + file_name

    s3 = boto3.client('s3', region_name="us-east-1")
    obj = s3.get_object(Bucket=bucket, Key=file_name)
    csv_string = obj['Body'].read().decode("latin1")
    file_df = pd.read_csv(StringIO(csv_string), sep='\t', encoding='latin1', na_values='',
                          dtype={"FhaVaPmiCaseNumberAlpha": str})
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    dataframe = file_df.applymap(trim_strings)
    del csv_string
    del trim_strings
    del file_df
    col = list(dataframe.columns)
    for column in col:
        if 'date' in column.lower() or column in date_column_list:
            try:
                dataframe[column] = pd.to_datetime(dataframe[column])
            except Exception as e:
                print(e)
                print(column, '++++++++++++++++++++++')
    print("done")
    for column in col:
        if 'date' in column.lower() or column in date_column_list:
            dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
    print("done")
    dataframe = dataframe.replace(r'', np.NaN, regex=True)
    for column in dataframe.columns:
        if dataframe[column].dtype == 'O':
            dataframe[column] = dataframe[column].astype(object).where(dataframe[column].notnull(), None)
        elif dataframe[column].dtype == 'int64':
            dataframe[column] = dataframe[column].astype(int).where(dataframe[column].notnull(), None)
        elif dataframe[column].dtype == 'float64':
            dataframe[column] = dataframe[column].astype(float).where(dataframe[column].notnull(), None)
    new_df = dataframe.copy()
    del dataframe
    column = list(new_df.columns)
    try:
        new_df[['MersMin']] = new_df[['MersMin']].astype(str)
    except Exception as e:
        pass
    try:
        new_df[['FhaVaPmiCaseNumberAlpha']] = new_df[['FhaVaPmiCaseNumberAlpha']].astype(str)
        new_df['FhaVaPmiCaseNumberAlpha'].replace({'None': None}, inplace=True)
    except Exception as e:
        pass

    offset = 0
    limit = 10000
    val = int(new_df.shape[0] / 10000) + 1
    print(val)

    db[collection].remove({})
    print(collection, "______")
    for i in range(val):
        df_json = new_df.iloc[offset:limit, :].to_dict("records")
        count = len(df_json)
        print(count)
        value = int(count / 1000) + 1
        off_set = 0
        lim_it = 1000
        for i in range(value):
            data = df_json[off_set:lim_it]
            if len(data) > 0:
                db[collection].insert_many(data)
                off_set = off_set + 1000
                lim_it = lim_it + 1000
        offset = offset + 10000
        limit = limit + 10000
    create_index(collection, [(ind, 1)])

    loan_data_columns = db.LoanDataColumns.find_one({})
    bre_loan_data_columns = db.BRELoanDataColumns.find_one({})
    # column.remove(ind)

    if file_dict.get('IsExtra', False) is False:
        db.BRELoanDataColumns.update({"_id": bre_loan_data_columns["_id"]}, {"$set": {display_field: column}})
    elif file_dict.get('IsExtra', False) is True and file_dict['IsMain'] == True:
        print("Processing supplementary main file")
        main_match_key = "LoanNumber"
        for file in package_files:
            if file['IsMain'] == True and file_dict.get('IsExtra', False) is False:
                print("found main file" + file)
                main_match_key = file['MatchKey']
                break
        join_main_with_supplementary_collection(main_collection=scheduler_id + '_Loan',
                                                supp_collection=collection,
                                                main_key=main_match_key,
                                                supp_key=ind,
                                                supp_columns=column)
        # In case of extra files, we need to append any additional columns to main file
        # Extra files should always be processed after main files
        bre_loan_column = bre_loan_data_columns.get(display_field, [])
        # Log columns which are common in supplementary and main file
        for field in column:
            if field in bre_loan_column:
                print("Supplementary main file has duplicate column " + field + " which is present in main file also")
        bre_loan_column.extend(column)
        db.BRELoanDataColumns.update({"_id": bre_loan_data_columns["_id"]},
                                     {"$set": {display_field: list(set(bre_loan_column))}})

    loan_column = loan_data_columns.get(display_field, [])
    loan_column.extend(column)
    db.LoanDataColumns.update({"_id": loan_data_columns["_id"]}, {"$set": {display_field: list(set(loan_column))}})

    lambda_metric(
        metric_name='bizzy.bre.populatedata',
        value= round(time.time() * 1000) - start_time,
        timestamp=int(time.time()), # optional, must be within last 20 mins
        tags=["scheduler_id:"+str(scheduler_id), "file_name:"+str(file_name)]
        )
    if file_dict_index == len(package_files) - 1:
        lambda_invoke = boto3.client("lambda", region_name="us-east-1")
        payload = {
            "loanPackageId": loan_package_id,
        }

        lambda_invoke.invoke(
            FunctionName=os.environ["LOAN_PACK_FUNC_NAME"],
            InvocationType='Event',
            Payload=json.dumps(payload)
        )

    file_dict_index += 1
    continue_execution = file_dict_index < len(package_files)

    return {"Continue": continue_execution, "Index": file_dict_index}


def join_main_with_supplementary_collection(main_collection,
                                            supp_collection,
                                            main_key="LoanNumber",
                                            supp_key="LoanNumber",
                                            supp_columns=[]):
    pipeline = [
        {
            '$lookup': {
                'from': supp_collection,
                'localField': main_key,
                'foreignField': supp_key,
                'as': 'suppCollection'
            }
        },
        {
            '$replaceRoot': {
                'newRoot': {
                    '$mergeObjects': [
                        {'$arrayElemAt': ["$suppCollection", 0]},
                        "$$ROOT"
                    ]
                }
            }
        },
        {
            '$project': {"suppCollection": 0}
        },
        {
            "$out": main_collection
        }
    ]
    print("pipeline: ", pipeline)
    print("Doing left join with the main loan collection")
    start_time = round(time.time() * 1000)
    db[main_collection].aggregate(pipeline, allowDiskUse=True)
    lambda_metric(
        metric_name='bizzy.bre.db.join',
        value= round(time.time() * 1000) - start_time,
        timestamp=int(time.time()), # optional, must be within last 20 mins
        tags=["main_collection:"+str(main_collection), "supp_collection:"+str(supp_collection)]
        )
    print("Removing missing supplementary columns in main file collection")
    # for supp_file_col in supp_columns:
    #     db[main_collection].update(
    #         {supp_file_col: {"$exists": False}},
    #         {"$set": {supp_file_col: None}},
    #         upsert=False,
    #         multi=True
    #     )
    # Removing temporary supplementary file from collection
    db[supp_collection].drop({})

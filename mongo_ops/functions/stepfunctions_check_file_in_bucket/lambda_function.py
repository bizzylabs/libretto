import boto3
import traceback
# from datadog_lambda.wrapper import datadog_lambda_wrapper

s3_client = boto3.client("s3")

# @datadog_lambda_wrapper
def stepfunctions_check_file_in_bucket(event, context):
    bucket_name = event.get("bucket_path", None)
    package_files = event.get("package_files", None)

    splited_path = bucket_name.split("/")
    bucket = splited_path[0]
    prefix = "/".join(splited_path[1:])

    output = False
    try:
        Contents = s3_client.list_objects(Bucket=bucket, Prefix=prefix)
        obj_keys = [content["Key"] for content in Contents["Contents"]]
        output = all([True if prefix + "/" + file["FileName"] in obj_keys else False for file in package_files])

    except Exception as e:
        print("Exception", e)
        traceback.print_exc()
        output = False

    return output
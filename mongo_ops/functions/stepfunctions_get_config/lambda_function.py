import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
import time
import os
from ddtrace import tracer
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper


secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

@tracer.wrap()
# @datadog_lambda_wrapper
def stepfunctions_get_config(event, context):
    # TODO implement
    loan_package_id = event.get("loan_package_id")
    output = dict()
    cofig = db.ApplicationDetails.find_one({})
    package_details = db.LoanPackageDetail.find({"LoanPackageId": ObjectId(loan_package_id)})

    package_files = []
    for package_detail in package_details:
        package_dict = dict()
        package_dict["FileType"] = str(package_detail["FileType"])
        package_dict["FileName"] = package_detail["FileName"]
        package_dict["MatchKey"] = package_detail["MatchKey"]
        package_dict["IsMain"] = package_detail["IsMain"]
        if package_detail.get("FileSubType", None):
            package_dict["FileSubType"] = str(package_detail["FileSubType"])
        package_dict["LoanPackageId"] = str(package_detail["LoanPackageId"])
        if package_detail.get("IsExtra", None):
            package_dict["IsExtra"] = package_detail["IsExtra"]
        package_files.append(package_dict)

    primary_files = list(filter(lambda x: not x.get("IsExtra"), package_files))
    extra_files = list(filter(lambda x: x.get("IsExtra", None) == True, package_files))
    # ensure that primary files are processed before extra files.
    # old collections in populate_data_from_files will be dropped for primary files only
    ordered_pkg_files = primary_files
    ordered_pkg_files.extend(extra_files)

    if len(ordered_pkg_files) != len(package_files):
        raise Exception("length of package files should be equal to ordered package files")

    output["BucketPaths"] = cofig["BucketPaths"]
    output["Client"] = cofig["Client"]
    output["Environment"] = cofig["Environment"]
    output["Files"] = ordered_pkg_files
    lambda_metric(
        metric_name='bizzy.bre.getconfig.filescount',
        value= len(ordered_pkg_files),
        timestamp=int(time.time()), # optional, must be within last 20 mins
        tags=["loan_package_id:"+str(loan_package_id)]
    )

    return output

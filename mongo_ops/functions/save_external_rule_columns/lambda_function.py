import json
import re
import io
import pandas as pd
from pymongo import MongoClient
import boto3
from bson import ObjectId
from datetime import datetime
import os

secret = boto3.client("secretsmanager", region_name='us-east-1')
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] +
    "/" + secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.client("s3")


def data_updater(old_data, new_data):
    old_data_names = [item["Name"] for item in old_data]
    for item in new_data:
        if item["Name"] in old_data_names:
            for index, value in enumerate(old_data):
                if value["Name"] == item["Name"]:
                    old_data[index]["Width"] = item["Width"]
        else:
            old_data.append(item)
    return old_data


def camelcase(s):
    val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', s).strip()  # adding space after camelcase
    val1 = re.search("\d{1,}", val)  # search for number after lower case
    if val1:
        final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
    else:
        final_key = val
    return final_key


def get_columns_for_file(bucket, filename):
    res = None
    try:
        file_obj = s3.get_object(Bucket=bucket, Key=filename)
    except Exception as e:
        print(e)
        return [], e
    df = pd.read_csv(io.StringIO(file_obj["Body"].read().decode('latin1')), sep='\t', encoding='latin1', na_values='')
    if df.shape[0] == 0:
        print("no data present")
        return [], "No data present"
    columns = list(df.columns)
    columns = [
        {"Name": column, "Display Name": camelcase(column), "CreatedOn": datetime.today(),
         "UpdatedOn": datetime.today(), "Width": int(str(df[column].astype(str).str.len().max()))}
        for
        column in columns]
    return columns, res


def lambda_handler(event, context):
    external_files = event["external_files"]
    total_files = event["total_files"]
    loan_package_id = event["loan_package_id"]
    print(event)
    s3_path = ""
    for config in db.ApplicationDetails.find({}):
        s3_path = config.get("BucketPaths").get("SFTPInboundPath")
    bucket = s3_path.split("/")[0]
    path = "".join(s3_path.split("/")[1:]) + "/"

    # processing only 5 files at a time
    for file in external_files[:5]:
        data, res = get_columns_for_file(bucket, path + file)
        if res:
            continue
        old_data = db.LoanPackageColumns.find_one({"LoanPackageId": ObjectId(loan_package_id)})["Columns"]["Loan"]
        columns = data_updater(old_data, data)
        db.LoanPackageColumns.update({"LoanPackageId": ObjectId(loan_package_id)},
                                     {"$set": {"Columns.Loan": columns}})
    lambda_invoke = boto3.client("lambda", region_name="us-east-1")
    # invoking lambda again for next set of files.
    if len(external_files[5:]) > 0:
        payload = {
            "external_files": external_files[5:],
            "total_files": len(external_files[5:]),
            "loan_package_id": str(loan_package_id)
        }
        lambda_invoke.invoke(
            FunctionName=os.environ["POPULATE_EXTERNAL_FILE_COLUMNS_FUNC_NAME"],
            InvocationType='Event',
            Payload=json.dumps(payload)
        )
    else:
        print("last counter")
        payload = {
            "loanPackageId": str(loan_package_id)
        }
        lambda_invoke.invoke(
            FunctionName=os.environ["POPULATE_COLUMN_FUNC_NAME"],
            InvocationType='Event',
            Payload=json.dumps(payload)
        )

    return {"result": "success"}

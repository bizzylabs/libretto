import json
import boto3
import time
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime
import os
import traceback
import time
from datadog_lambda.metric import lambda_metric
# from datadog_lambda.wrapper import datadog_lambda_wrapper


secret = boto3.client("secretsmanager") 
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
lambda_invoke = boto3.client("lambda", region_name="us-east-1")

def activate_execution(run_id):
    try:
        start_time = round(time.time() * 1000)
        db.SchedulerHistory.update(
            {"RunId": run_id},
            {
                "$set": {
                    "IsActive": 1,
                    "UpdatedOn": datetime.now()
                }
            }
        )
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["run_id:"+str(run_id), 'collection:SchedulerHistory']
            )
    except Exception as err:
        print(err)
        traceback.print_exc()

def drop_ruleset_view(rule_set_ids):
    for rule_set_id in rule_set_ids:
        view_name = "LoanDataView_" + str(rule_set_id)
        db[view_name].drop()

# @datadog_lambda_wrapper
def lambda_handler(event, context):
    scheduler_id = event.get("scheduler_id", None)
    run_id = event.get("run_id", None)
    counter = event.get("counter", None)
    print("scheduler_id, run_id", scheduler_id, run_id)
    print("Wating to activate execution!!")
    scheduler_config = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)})
    rule_set_ids = scheduler_config["RuleSetId"]
    if counter == 12:
        start_time = round(time.time() * 1000)
        db.SchedulerHistory.update(
            {"RunId": run_id},
            {
                "$set": {
                    "IsActive": 2,
                    "UpdatedOn": datetime.now()
                }
            }
        )
        lambda_metric(
            metric_name='bizzy.bre.db.update',
            value= round(time.time() * 1000) - start_time,
            timestamp=int(time.time()), # optional, must be within last 20 mins
            tags=["run_id:"+str(run_id), "scheduler_id:"+str(scheduler_id), 'collection:SchedulerHistory']
            )
        print("Calling SNS function!!!!!!")
        # drop_ruleset_view(rule_set_ids)
        # SNS : BSI UI not updated
        payload = {
            "run_id": run_id,
            "scheduler_id": scheduler_id,
            "counter": counter
        }
        lambda_invoke.invoke(
            FunctionName=os.environ["ACT_EXE_STATUS_FUNC_NAME"],
            InvocationType='Event',
            Payload=json.dumps(payload)
        )
        return True 
    sleep_time = 10
    total_time = 10
    while sleep_time >= 10:
        summaries_count = db.ResultSetSummary.distinct("RuleSetId", {"RunId": run_id})
        print(summaries_count)
        if len(rule_set_ids) == len(summaries_count):
            lambda_metric(
                metric_name="bizzy.bre.rulesets",
                value= len(rule_set_ids),
                timestamp=int(time.time()), # optional, must be within last 20 mins
                tags=["run_id:"+str(run_id), "scheduler_id:"+str(scheduler_id)]
                )
            activate_execution(run_id)
            drop_ruleset_view(rule_set_ids)
            print("activated execution!!!!!!")
            # SNS : BSI UI updated with Rulesets 
            print("Calling SNS function!!!!!!")
            payload = {
                "run_id": run_id,
                "scheduler_id": scheduler_id,
                "counter": counter
            }
            lambda_invoke.invoke(
                FunctionName=os.environ["ACT_EXE_STATUS_FUNC_NAME"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
            break
        else:
            print("sleeping to activate execution!!!!!!")
            time.sleep(sleep_time)
            total_time = total_time + sleep_time
            if total_time > 850:
                payload = {
                    "run_id": run_id,
                    "scheduler_id": scheduler_id,
                    "counter": counter + 1
                }
                lambda_invoke.invoke(
                    FunctionName=os.environ["ACT_EXE_FUNC_NAME"],
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                break
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

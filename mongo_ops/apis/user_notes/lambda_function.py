import json
import os
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        user_name = event["context"].get("username", None)
        time_stamp = datetime.now()
        ruleset = event["body"].get("RulesetId", "")
        rule = event["body"].get("RuleId", None)
        loan_number = event["body"].get("LoanNumber", "")
        run_id = event["body"].get("RunId", "")
        note = event["body"].get("Note", {})
        request_type = event["body"].get("Type", "GET")
        note_type = event["body"].get("NoteType", "")
        loan_package_id = event["body"].get("LoanPackageId", None)
        note["createdon"] = time_stamp
        note["Username"] = user_name
        note_reason = event["body"].get("NoteReason", "")
        # note["NoteType"] = "UserNote"
        business_rule_id = ""
        if loan_number:
            loan_number = int(loan_number)
        if rule:
            business_rule_id = db.Rule.find_one({"_id": ObjectId(rule)}).get("RuleNameId", "")
        if request_type == "POST":
            if not ruleset and not rule and not loan_number and not run_id:
                return {
                    'statusCode': 400,
                    'body': json.dumps('Missing fields.')
                }
            loan_package_id = db.SchedulerHistory.find_one({"RunId": run_id}).get("LoanPackage")
            note_obj = {
                "RulesetId": ObjectId(ruleset),
                "RuleId": ObjectId(rule),
                "RuleNameId": business_rule_id,
                "LoanNumber": loan_number,
                "LoanPackageId": loan_package_id,
                "NoteType": "UserNote",
                "Reason": ""
            }
            if note:
                note_obj["Note"] = note
            if note_reason:
                note_obj["Reason"] = note_reason
            db.Notes.insert_one(note_obj)

            return {
                'statusCode': 200,
                'body': json.dumps('Note has been added successfully.')
            }
        else:
            print(note_type)
            run_ids = []
            if loan_package_id:
                run_ids = db.SchedulerHistory.ditinct("RunId", {"LoanPackage": ObjectId(loan_package_id)})
            all_notes = []
            if ruleset and rule and loan_number and run_id:
                if note_type == "SN":
                    data = list(db.Notes.find(
                        {"$and": [{"RulesetId": ObjectId(ruleset), "RuleId": ObjectId(rule), "Note.RunId": run_id,
                                   "LoanNumber": loan_number}, {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                                                        {"Note.RunId": {"$in": run_ids}}]}]},
                        {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1, "_id": False}))
                else:
                    data = list(
                        db.Notes.find(
                            {"$and": [
                                {"RulesetId": ObjectId(ruleset), "LoanNumber": loan_number, "RuleId": ObjectId(rule),
                                 "Note.RunId": run_id, "NoteType": "UserNote"},
                                {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                         {"Note.RunId": {"$in": run_ids}}]}]},
                            {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1, "_id": False}))
            elif ruleset and rule and loan_number and not run_id:
                if note_type == "SN":
                    data = list(db.Notes.find({"$and": [{"RulesetId": ObjectId(ruleset), "LoanNumber": loan_number},
                                                        {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                                                 {"Note.RunId": {"$in": run_ids}}]}]},
                                              {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1,
                                               "_id": False}))
                else:
                    data = list(
                        db.Notes.find(
                            {"$and": [
                                {"RulesetId": ObjectId(ruleset), "LoanNumber": loan_number, "NoteType": "UserNote"},
                                {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                         {"Note.RunId": {"$in": run_ids}}]}]},
                            {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1, "_id": False}))
            elif loan_number and not ruleset and not rule:
                if note_type == "SN":
                    print("coming")
                    data = list(
                        db.Notes.find({"$and": [{"LoanNumber": loan_number},
                                                {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                                         {"Note.RunId": {"$in": run_ids}}]}]},
                                      {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1, "_id": False}))
                else:
                    data = list(db.Notes.find({"$and": [{"LoanNumber": loan_number, "NoteType": "UserNote"},
                                                        {"$or": [{"LoanPackageId": ObjectId(loan_package_id)},
                                                                 {"Note.RunId": {"$in": run_ids}}]}]},
                                              {"Note": 1, "RuleId": 1, "NoteType": 1, "Reason": 1, "Key": 1,
                                               "_id": False}))
            else:
                return {
                    'statusCode': 200,
                    'body': json.dumps('please provide ruleset, rule and loan_number or only loan_number.')
                }
            for item in data:
                name = list(db.Rule.find({"_id": item["RuleId"]}, {"_id": False, "Name": 1}))
                if name:
                    rule_name = name[0]["Name"]
                else:
                    rule_name = ""
                key = item.get("Key", "")
                if key:
                    item["Note"]["key"] = key
                noteexist = item["Note"].get("Note", "")
                is_user_note_added_during_ignore_snooze = False
                if key in ["Ignore", "Snooze"] and item["NoteType"] == "UserNote":
                    is_user_note_added_during_ignore_snooze = True

                if "Snoozed for" in noteexist:
                    NoteDetail = item["Note"].get("NoteDetail", "")
                    if NoteDetail:
                        item["Note"]["NoteDetail"]["CreatedOn"] = str(item["Note"]["NoteDetail"]["CreatedOn"])
                        note_value = item["Note"]["NoteDetail"].get("notesValue", "")
                        if len(note_value.strip()) == 0 and is_user_note_added_during_ignore_snooze:
                            print("Skipping UserNote as notesValue is empty")
                            continue
                        if note_value:
                            item["Note"]["notesValue"] = note_value
                if item["NoteType"] == "UserNote" and "Ignore" in noteexist:
                    note_value = item["Note"]["NoteDetail"].get("notesValue", "")
                    if len(note_value.strip()) == 0 and is_user_note_added_during_ignore_snooze:
                        print("Skipping UserNote as notesValue is empty")
                        continue
                    if note_value:
                        item["Note"]["notesValue"] = note_value
                if item["NoteType"] == "UserNote" and "review" in noteexist:
                    NoteDetail = item["Note"].get("NoteDetail", "")
                    if NoteDetail:
                        item["Note"]["Note"] = item["Note"]["NoteDetail"]
                item["Note"]["sort_item"] = item["Note"]["createdon"]
                date_time = item["Note"]["createdon"].strftime("%H:%M, %d %b %Y")
                item["Note"]["createdon"] = date_time
                item["Note"]["rule_name"] = rule_name
                item["Note"]["note_type"] = item["NoteType"]
                reasonexist = item.get("Reason", "")
                if reasonexist:
                    item["Note"]["reason"] = item["Reason"]

                all_notes.append(item["Note"])
            data = sorted(all_notes, key=lambda i: (i['sort_item']), reverse=True)
            for obj in data:
                del obj["sort_item"]
            return data
    except Exception as err:
        return {'statusCode': 500, "error": "user notes failed"}

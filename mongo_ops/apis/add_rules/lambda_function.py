import datetime
import json
import os
import uuid
from base64 import b64decode
from urllib.parse import urlparse

import boto3
from bson.objectid import ObjectId
from log_queue import push_log
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def uri_validator(x):
    try:
        result = urlparse(x)
        if result.scheme in ["https", "http", "www"] or "www" in result.netloc.title():
            return True
    except Exception as e:
        return False


def get_date_time(date_string):
    """
    :str date_string: date_string should be in Month/Date/Year(10/20/2018) Format.

    :return: datetime object.
    """
    if isinstance(date_string, str):
        date_time = date_string.strip() + " 00:00:00"
        date_obj = datetime.datetime.strptime(date_time, "%m/%d/%Y %H:%M:%S")
        return date_obj
    else:
        return None


def add_rule(event, context):
    try:
        start_date = event["body"].get("start_date", None)
        end_date = event["body"].get("end_date", None)
        user_name = event["context"].get("username", None)
        rule_name = event["body"].get("rule_name", None)
        description = event["body"].get("description", None)
        rule_type = event["body"].get("rule_type", None)
        rule_sub_type = event["body"].get("rule_sub_type", None)
        compliance_type = event["body"].get("compliance_type", None)
        rule_set_ids = event["body"].get("departments", [])
        criteria = event["body"].get("criteria", None)
        readable_criteria = event["body"].get("readable_criteria", None)
        display_fields = event["body"].get("display_fields", None)
        masking_fields = event["body"].get("masking_fields", None)
        is_published = int(event["body"].get("is_published", 0))
        created_from_ui = int(event["body"].get("created_from_ui", 1))
        json_criteria = event["body"].get("json_criteria", {})
        rule_logic_fields = event["body"].get("rule_logic_fields", [])
        loan_package = event["body"].get("loan_package_id", [{'label': 'No Label', 'value': ""}])
        severity = event["body"].get("severity", "Medium")
        source = event["body"].get("source", "")
        copy_rule_id = event["body"].get("copy_rule_id", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        username = event["context"].get("username", None)
        email = event["context"].get("email", None)
        log = {"action": "RuleCopied", "username": username, "email": email, "ip": ip,
               "browser": browser, "timestamp": str(datetime.datetime.now()),
               "CopiedRuleId": copy_rule_id, "CopiedRuleNameId": "",
               "CopiedRuleName": "", "RuleId": "", "RuleNameId": "", "RuleName": ""}
        hot_link_url = event["body"].get("HotLink", "")
        external_source = event["body"].get("ExternalSource", False)

        # convert string date into datetime obj.
        start_date = get_date_time(start_date)
        end_date = get_date_time(end_date)

        if len(rule_logic_fields) > 0:
            rule_logic_fields = [item for item in rule_logic_fields if "()" not in item]

        if len(rule_set_ids) > 0:
            rule_set_ids = list(set(rule_set_ids))

        is_ui_editable = True
        # Retrun true if hot link is valid
        if hot_link_url:
            res = uri_validator(hot_link_url)
            if not res:
                return {
                    "status": 500,
                    "Error": "Hot link is not a valid one"
                }
        priority = 2
        priority_dict = {"Critical": 0,
                         "High": 1,
                         "Medium": 2,
                         "Low": 3,
                         "Informational": 4}
        for item in priority_dict.keys():
            if severity == item:
                priority = priority_dict[item]
        if json_criteria and external_source == False:
            try:
                validatejson = json.loads(json.dumps(json_criteria))
            except:
                return {
                    "error": "json_criteria is not json serialize. Give a valid json"
                }

        # Todo: check all required fields
        # if not rule_set_ids:
        #     rule_set_ids = ["5eea0d1c21c6ca8dd44abb63"]

        if is_published == 1 and external_source == False and None in [rule_name, criteria, rule_type, rule_sub_type,
                                                                       compliance_type,
                                                                       display_fields, is_published,
                                                                       rule_logic_fields] and len(rule_set_ids) <= 0:
            return {
                "error": "Missing Required fields"
            }
        else:
            # Todo: if refs exists
            rule_set_ids = [ObjectId(rule_set_id) for rule_set_id in rule_set_ids]
            if rule_type != "":
                rule_type = ObjectId(rule_type)
            if rule_sub_type != "":
                rule_sub_type = ObjectId(rule_sub_type)
            if compliance_type != "":
                compliance_type = ObjectId(compliance_type)

            criteria = b64decode(criteria).decode('utf-8')
            print(criteria, "+++++++++++++")
            if copy_rule_id:
                temp_rule = db.Rule.find_one({"_id": ObjectId(copy_rule_id)})
                unique_rule_id = temp_rule.get("RuleNameId", "")
                log["CopiedRuleNameId"] = unique_rule_id
                log["CopiedRuleName"] = temp_rule.get("Name", "")
                if temp_rule["isUiEditable"] == False:
                    criteria = temp_rule["Criteria"]
                    is_ui_editable = temp_rule["isUiEditable"]
                    try:
                        json_criteria = temp_rule["JsonCriteria"]
                    except Exception as e:
                        pass
            rule = {
                "AuthorName": user_name,
                "RuleType": rule_type,
                "RuleSubType": rule_sub_type,
                "ComplianceType": compliance_type,
                "MaskingFieldsCriteria": masking_fields,
                "CoreLibFlag": 0,
                "IsEditable": 1,
                "isUiEditable": is_ui_editable,
                "Version": 1,
                "IsActive": 1,
                "DeleteStatus": 0,
                "Departments": rule_set_ids,
                "Name": rule_name,
                "Description": description,
                "ReadableCriteria": readable_criteria,
                "Criteria": criteria,
                "JsonCriteria": json_criteria,
                "DisplayFields": str(display_fields),
                "MaskingFields": str(masking_fields),
                "IsPublished": is_published,
                "CreatedFromUI": created_from_ui,
                "CreatedOn": datetime.datetime.now(),
                "UpdatedOn": datetime.datetime.now(),
                "RuleLogicFields": rule_logic_fields,
                "LoanPackage": loan_package,
                "Severity": severity,
                "Priority": priority,
                "Source": source,
                "CreatedBy": user_name,
                "UpdatedBy": '',
                "Slug": rule_name.lower(),
                "HotLink": hot_link_url,
                "ExternalSource": external_source,
                "StartDate": start_date,
                "EndDate": end_date
            }

            # check if adding rule from bizzy
            app_detail = db.ApplicationDetails.find_one({})
            unique_id = uuid.uuid4().hex
            hex_string = app_detail['Client'].lower().encode("utf-8").hex()
            time_string = hex(int(datetime.datetime.timestamp(datetime.datetime.now()) * 1000))
            final_string = f"{time_string}{unique_id}{hex_string}"
            rule["RuleNameId"] = final_string
            if app_detail['Client'].lower() == "bizzy":
                rule["CoreLibFlag"] = 1
                rule["CreatedBy"] = "Bizzy"
                rule["BizzyDepartment"] = rule_set_ids

                # insert into rule
            rule_id = db.Rule.insert_one(rule).inserted_id
            # update RuleSetMaster if published
            if is_published == 1:
                returned_ruleset_id = str(rule_set_ids[0])
                for rule_set_id in rule_set_ids:
                    db.RuleSetMaster.update(
                        {"_id": rule_set_id},
                        {"$push":
                             {"RuleIds":
                                  {"RuleId": rule_id, "Version": 1, "IsActive": 1,
                                   "EndDate": end_date, "StartDate": start_date}  # Todo: replace with version
                              }
                         }
                    )
                if copy_rule_id:
                    log["RuleId"] = str(rule_id)
                    log["RuleNameId"] = final_string
                    log["RuleName"] = rule_name
                    push_log.push(log)
            else:
                # db.Rule.update({'_id': rule_id}, {"$set":{'ParentId': rule_id}})
                db.RuleSetMaster.update(
                    {"RuleSetName": "Draft Rules"},
                    {"$push":
                         {"RuleIds":
                              {"ParentId": "", "ChildId": rule_id}  # Todo: replace with version
                          }
                     }
                )
                returned_ruleset_id = str(
                    list(db.RuleSetMaster.find({"RuleSetName": "Draft Rules"}, {"_id": 1}))[0].get("_id"))
        client.close()
        return {
            "status": 200,
            "returned_ruleset_id": returned_ruleset_id,
            "message": "Inserted successfully"
        }
    except Exception as err:
        return {'statusCode': 500,"error": "add rules failed"}

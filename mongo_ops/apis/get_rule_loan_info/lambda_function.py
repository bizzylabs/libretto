import json
import os
import re
from collections import OrderedDict
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

date_column_list = ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                    "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT", "Note",
                    "CertificateOfReasonableValue", "ClosingDisclosure", "DeedOfTrustMortgage",
                    "EscrowDisclosure", "EscrowWaiver", "Guarantee", "GuarantyAgreement",
                    "HomeOwnersDeclaration", "HUD", "LossMitPackages", "Modifications",
                    "TaxInformation", "45DayLetterRequestedDate",
                    "CCLetterRequestedDate", "HelloWelcomeLetterRequestedDate",
                    "PrivacyNoticeLetterRequestedDate",
                    "ProgramLetterRequestedDate",
                    "TPPLetterRequestedDate",
                    "LMPackageReceivedLetterRequestedDate",
                    "LMPackageRequestLetterRequestedDate"
                    ]


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


database = get_initialized_db()


def formatter(value):
    return "{:,}".format(value)


def loan_centric(loan_number, rule_id, run_id, rule_set_id, filter_condition, user_name):
    data = database.ResultSetDetail.aggregate([
        {"$match": {"$and": [{"RunId": run_id, "ExceptionStatus": True, "LoanNumber": loan_number},
                             filter_condition,
                             {"$or": [{"RuleId": {"$ne": ObjectId(rule_id)}},
                                      {"RuleSetId": {"$ne": ObjectId(rule_set_id)}}]}]}},
        {"$lookup":
            {
                "from": "RuleSetMaster",
                "let": {"rule_set_id": "$RuleSetId"},
                "pipeline": [
                    {"$match": {"$and": [{"$expr": {"$eq": ["$_id", "$$rule_set_id"]}},
                                         {"$expr": {"$in": [user_name, "$Users"]}}]}},
                    {"$project": {"_id": 1}}
                ],
                "as": "RuleSet"
            }
        }])
    count = 0
    for item in data:
        if len(item["RuleSet"]) > 0:
            count += 1
    return count


def get_search_fields(search, field):
    search = search.rstrip("/")
    criteria = {}
    if search and field:
        if search.lower() == "null":
            criteria = {"$expr": {"$eq": [f"$LoanInfo.{field}", None]}}

        elif "date" in field.lower():
            m = search.split("/")
            # For complete date format.
            if len(m) == 3:
                dtformat = "%m/%d/%Y"
                criteria = {"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}}, f"{search}"]}}
            # For month, date and year only if one number is given. eg: 11
            elif len(m) == 1:
                if len(m[0]) <= 2:
                    mnth_format = "%m"
                    date_format = "%d"
                    criteria = {"$or": [{"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{mnth_format}", "date": f"$LoanInfo.{field}"}},
                                f"{search}"]}},
                        {"$expr": {"$eq": [{"$dateToString": {"format": f"{date_format}",
                                                              "date": f"$LoanInfo.{field}"}},
                                           f"{search}"]}}]}
                if len(m[0]) == 4:
                    dtformat = "%Y"
                    criteria = {"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}},
                                f"{search}"]}}
            # For month with year or month with date if 2 numbers are given. eg: 11/06 or 11/2021
            elif len(m) == 2:
                year_flag = False
                for i in m:
                    if len(i) == 4:
                        year_flag = True
                        break
                if year_flag:
                    # date with year or month with year
                    if int(m[0]) <= 12:
                        dtformat = "%m/%Y"
                    else:
                        print("coming here")
                        dtformat = "%d/%Y"
                else:
                    dtformat = "%m/%d"
                criteria = {"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}}, f"{search}"]}}
            else:
                return {"searchMsg": "Provide either full date along with month and year or only month or only year."}
            print(criteria)
        elif search.lower() == "null":
            print("coming here")
            # criteria = {f"$LoanInfo.{field}" : None}
            criteria = {"$expr": {"$eq": [f"$LoanInfo.{field}", None]}}
        else:
            criteria = {"$expr":
                {
                    "$regexMatch": {
                        "input": {"$toString": f"$converted"},
                        "regex": f"{search}",
                        "options": "i"
                    }
                }
            }
    return criteria


def get_rule_loan_info(event, context):
    try:
        user_name = event["context"]["username"]
        event = event["body"]
        data_list = []
        open_with_filter_loans = []
        open_exception = False
        filter_count = {}
        rule_id = event.get("RuleId", None)
        rule_set_id = event.get("RuleSetId", None)
        search_word = event.get("searchWord", None)
        search_item = event.get("searchItem", None)
        limit = event.get("limit", 10)
        offset = event.get("offset", 0)
        loan_package_id = event.get("loan_package_id")
        note_filter = event.get("note_filter", [])
        loan_sort_key = event.get("loan_sort_key", None)
        sort_key = event.get("sort_by", "LoanNumber")
        date_column_list = ["TSK_CMPLTN_DT_DB089", "TSK_FOLLOW_UP_DT", "TSK_SCHD_DT",
                            "TSK_FOLLOW_UP_DT_ARCH", "TSK_SCHD_DT_ARCH", "TSK_CMPLTN_DT", "Note",
                            "CertificateOfReasonableValue", "ClosingDisclosure", "DeedOfTrustMortgage",
                            "EscrowDisclosure", "EscrowWaiver", "Guarantee", "GuarantyAgreement",
                            "HomeOwnersDeclaration", "HUD", "LossMitPackages", "Modifications",
                            "TaxInformation", "45DayLetterRequestedDate",
                            "CCLetterRequestedDate", "HelloWelcomeLetterRequestedDate",
                            "PrivacyNoticeLetterRequestedDate",
                            "ProgramLetterRequestedDate",
                            "TPPLetterRequestedDate",
                            "LMPackageReceivedLetterRequestedDate",
                            "LMPackageRequestLetterRequestedDate"
                            ]

        if search_item and search_word and "date" in search_item.lower() and search_word.lower() != "null":
            only_digit = search_word.replace("/", "")
            for item in only_digit:
                if not item.isdigit():
                    return {
                        "statusCode": 200,
                        "result": [],
                        "total_loan": 0,
                        "filter_count": filter_count
                    }
        if search_item == "" and search_word != "":
            return {
                "statusCode": 200,
                "result": [],
                "total_loan": 0,
                "filter_count": filter_count
            }

        if sort_key == "CreatedOn":
            sort_key = "InExistence"
        else:
            sort_key = f"LoanInfo.{sort_key}"

        if loan_sort_key:
            sort_key = f"LoanInfo.{loan_sort_key}"

            # print(sort_key)
        filter_key = event.get("filter_key", "all")
        asc_key = -1 if event.get("asc", "N") == "N" else 1

        if sort_key == "InExistence" and asc_key == 1:
            asc_key = -1
        elif sort_key == "InExistence" and asc_key == -1:
            asc_key = 1
        else:
            pass

        if not loan_package_id:
            return {"error": "Loan package id missing"}

        filters = event.get("filters", None)
        condition_filters = event.get("condition_filters", None)

        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters[key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters[key]}
        else:

            loan_package_name = database.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = database.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters["Filters"][loan_package_name][key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters["Filters"][loan_package_name][key]}

        run_id = event.get("run_id")
        match_criteria = get_search_fields(search_word, search_item)
        if not search_item:
            search_item = "LoanNumber"
        if rule_id is None or rule_set_id is None or run_id is None:
            return {"error": "Required Fields are  Missing"}
        filter_count = {
            "Complete": 0,
            "Review": 0,
            "Snooze": 0,
            "Ignore": 0,
            "Open Exception": 0
        }

        filters = ["Complete", "Review", "Snooze", "Ignore"]
        for item in filters:
            data_count = database.ResultSetDetail.find(
                {"$and": [filter_condition,
                          {"RuleId": ObjectId(rule_id), "LoanNumber": {"$ne": None}, "RuleSetId": ObjectId(rule_set_id),
                           "ExceptionStatus": True,
                           "RunId": run_id,
                           "SystemNote": {"$in": [item]}}]}).count()
            filter_count[item] = formatter(data_count)
        open_count = database.ResultSetDetail.find(
            {"$and": [filter_condition,
                      {"RuleId": ObjectId(rule_id), "LoanNumber": {"$ne": None}, "RuleSetId": ObjectId(rule_set_id),
                       "RunId": run_id,
                       "ExceptionStatus": True, "SystemNote": {"$nin": filters}}]}).count()
        filter_count["Open Exception"] = formatter(open_count)
        # print(filter_count)
        if note_filter:
            if "Open Exceptions" in note_filter:
                open_exception = True
                note_filter.remove("Open Exceptions")
        else:
            print("dont be here")
            return {
                "statusCode": 200,
                "result": [],
                "total_loan": 0,
                "filter_count": filter_count
            }
        loans = []
        total_loan = 0
        if filter_key == "NewExceptions":
            if note_filter and not open_exception:
                loans = database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$in": note_filter}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    },
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}])

                count = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$in": note_filter}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    },
                    {"$count": "total"}
                ]))
                if len(count) > 0:
                    total_loan = count[0]["total"]


            elif open_exception and note_filter:
                filter_loans = database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$in": note_filter}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    },
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}])
                non_filter_loans = database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$nin": filters}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    },
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}])
                for item in filter_loans:
                    open_with_filter_loans.append(item)
                for nfitem in non_filter_loans:
                    open_with_filter_loans.append(nfitem)
                loans = open_with_filter_loans
                filter_loans_count = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$in": note_filter}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    }, {"$count": "total"}]))
                non_filter_loans_count = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$nin": filters}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    }, {"$count": "total"}]))
                if len(filter_loans_count) > 0:
                    total_loan = filter_loans_count[0]["total"]
                if len(non_filter_loans_count) > 0:
                    total_loan = total_loan + non_filter_loans_count[0]["total"]
            else:
                loans = database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$nin": filters}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    },
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}])

                count = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [{"RuleId": ObjectId(rule_id)},
                                         {"RuleSetId": ObjectId(rule_set_id)},
                                         {"RunId": run_id},
                                         {"LoanNumber": {"$ne": None}},
                                         filter_condition,
                                         {"SystemNote": {"$nin": filters}},
                                         {"ExceptionStatus": True},
                                         {"$expr": {
                                             "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                                     {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}]
                                }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {
                        "$project": {"_id": 1, "RunId": 1, "RuleSetId": 1, "RuleId": 1, "RuleVersion": 1,
                                     "ExceptionStatus": 1,
                                     "Exception": 1, "LoanNumber": 1, "LoanInfo": 1, "InExistence": 1, "UpdatedOn": 1,
                                     "SystemNote": 1}
                    }, {"$count": "total"}]))

                if len(count) > 0:
                    total_loan = count[0]["total"]
        else:

            if note_filter and not open_exception:
                loans = database.ResultSetDetail.aggregate([{"$match": {
                    "$and": [{"RuleId": ObjectId(rule_id)}, filter_condition, {"RuleSetId": ObjectId(rule_set_id)},
                             {"RunId": run_id}, {"LoanNumber": {"$ne": None}},
                             {"ExceptionStatus": True}, {"SystemNote": {"$in": note_filter}}]}},
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$sort": {sort_key: asc_key}}, {"$skip": offset},
                    {"$limit": limit}])
                total_loan = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [filter_condition,
                                         {"RuleId": ObjectId(rule_id), "LoanNumber": {"$ne": None},
                                          "RuleSetId": ObjectId(rule_set_id),
                                          "RunId": run_id,
                                          "ExceptionStatus": True, "SystemNote": {"$in": note_filter}}]}},
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$count": "total_loan"}
                ]))
                if total_loan and len(total_loan) > 0:
                    total_loan = total_loan[0].get("total_loan")

            elif open_exception and note_filter:
                null = None
                note_filter.append([])
                loans = database.ResultSetDetail.aggregate([{"$match": {
                    "$and": [{"RuleId": ObjectId(rule_id)}, filter_condition, {"RuleSetId": ObjectId(rule_set_id)},
                             {"RunId": run_id},
                             {"LoanNumber": {"$ne": None}},
                             {"ExceptionStatus": True}, {"SystemNote": {"$in": note_filter}}]}},
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$sort": {sort_key: asc_key}}, {"$skip": offset},
                    {"$limit": limit}])
                total_loan = list(database.ResultSetDetail.aggregate([
                    {"$match": {"$and": [filter_condition,
                              {"RuleId": ObjectId(rule_id), "LoanNumber": {"$ne": None}, "RuleSetId": ObjectId(rule_set_id),
                               "RunId": run_id,
                               "ExceptionStatus": True, "SystemNote": {"$in": note_filter}}]}},
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$count": "total_loan"}
                ]))
                if total_loan and len(total_loan) > 0:
                    total_loan = total_loan[0].get("total_loan")

            else:
                loans = database.ResultSetDetail.aggregate([{"$match": {
                    "$and": [{"RuleId": ObjectId(rule_id)}, filter_condition, {"RuleSetId": ObjectId(rule_set_id)},
                             {"RunId": run_id}, {"LoanNumber": {"$ne": None}},
                             {"ExceptionStatus": True}, {"SystemNote": {"$nin": filters}}]}},
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$sort": {sort_key: asc_key}}, {"$skip": offset},
                    {"$limit": limit}])
                total_loan = list(database.ResultSetDetail.aggregate([
                    {"$match":
                    {"$and": [filter_condition,
                              {"RuleId": ObjectId(rule_id), "RuleSetId": ObjectId(rule_set_id), "LoanNumber": {"$ne": None},
                               "RunId": run_id,
                               "ExceptionStatus": True, "SystemNote": {"$nin": filters}}]
                     }
                     },
                    {"$addFields": {"converted": {
                        "$switch": {"branches": [
                            {"case": {"$eq": [{"$type": f"$LoanInfo.{search_item}"}, "double"]},
                             "then": {"$toInt": f"$LoanInfo.{search_item}"}
                             },
                        ],
                            "default": {"$toString": f"$LoanInfo.{search_item}"}}
                    },
                    }},
                    {"$match": match_criteria},
                    {"$count": "total_loan"}
                ]))
                if total_loan and len(total_loan) > 0:
                    total_loan = total_loan[0].get("total_loan")
        try:
            hot_link_flag = False
            hotlink_obj = database.GlobalHotlink.find_one({}, {"GlobalHotlink": 1, "IsActive": 1, "_id": 0})
        except:
            hotlink_obj = {'IsActive': 0, 'GlobalHotlink': ''}

        if hotlink_obj["IsActive"] == 1:
            hot_link_flag = True
            rule_hot_link = database.Rule.find_one({"_id": ObjectId(rule_id)}, {"_id": 0, "HotLink": 1})
            print(rule_hot_link)
            if rule_hot_link and rule_hot_link.get("HotLink", ""):
                url = rule_hot_link["HotLink"]
            else:
                url = hotlink_obj["GlobalHotlink"]

        for loan in loans:
            loan_detail = OrderedDict()
            if hot_link_flag:
                # print(loan["LoanInfo"]["LoanNumber"])
                loan_detail["HotLink"] = url + str(loan["LoanInfo"]["LoanNumber"])
            for key, value in loan["LoanInfo"].items():
                if key == "_id":
                    value = str(value)
                if "date" in key.lower() or key in date_column_list:
                    # print(key)
                    try:
                        if type(value) != str and value:
                            value = str(value.strftime('%m/%d/%Y'))
                    except Exception as e:
                        print(e)
                        value = str(value)
                val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', key).strip()  # adding space after camelcase
                val1 = re.search("\d{1,}", val)  # search for number after lower case
                if val1:
                    final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                else:
                    final_key = val
                loan_detail[final_key] = value
            if loan.get("InExistence", None) is None:
                loan_detail["CreatedOn"] = datetime.now().strftime('%m/%d/%Y')
            else:
                loan_detail["CreatedOn"] = loan.get("InExistence").strftime('%m/%d/%Y')
            noteflag = loan.get("SystemNote", "")
            if not noteflag:
                loan_detail["SystemNote"] = []
            else:
                loan_detail["SystemNote"] = loan["SystemNote"]

            loan_detail["TotalNoException"] = loan_centric(loan_detail["Loan Number"], rule_id, run_id, rule_set_id,
                                                           filter_condition, user_name)
            note_count = database.Notes.find({"NoteType": "UserNote", "LoanNumber": loan_detail["Loan Number"]}).count()
            if note_count > 0:
                loan_detail["NotesFlag"] = 1
            else:
                loan_detail["NotesFlag"] = 0
            loan_detail.move_to_end('Loan Number', False)
            data_list.append(loan_detail)

        if total_loan and type(total_loan) == int and  total_loan <= 0 or total_loan and type(total_loan) == list and len(total_loan) <= 0:
            return {
                "statusCode": 200,
                "result": [],
                "total_loan": 0,
                "filter_count": filter_count
            }
        return {
            'statusCode': 200,
            'result': data_list,
            'total_loan': total_loan,
            'filter_count': filter_count
        }
    except Exception as err:
        return {'statusCode': 500, "error": "get rule loan info failed"}


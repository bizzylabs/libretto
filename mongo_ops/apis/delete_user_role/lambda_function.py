import datetime
import json
import os

import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        role_name = event["body"].get("RoleName", None)
        if not role_name:
            return {"Error": "Role name is required", "statusCode": 500}
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        cdt = datetime.datetime.now()
        log_document = {}
        try:
            response = cognito.delete_group(
                UserPoolId=user_pool_id,
                GroupName=role_name
            )
            db.UserRolePermissions.remove({"RoleName": role_name})

        except Exception as err:
            return {
                'statusCode': 500,
                'body': {"Error": str(err)}
            }
        try:
            logdin_username = event["context"]["username"]
        except:
            logdin_username = ""
        log_document["Ip"] = ip
        log_document["browser"] = browser
        log_document["username"] = logdin_username
        log_document["timestamp"] = cdt
        log_document["action"] = "Deleted"
        log_document["resource"] = role_name
        log_document["log_source"] = "Roles"
        insert_log = db.BussinessAuditLogs.insert_one(log_document)

        user_records = db.Users.find({"roles": role_name})
        for data in user_records:
            username = data['username']
            data['roles'].remove(role_name)
            post = {
                'role': data['roles'],
                'UpdatedOn': cdt
            }
            users_update_query = db.Users.update_one({'username': username}, {"$set": post}, upsert=False)
            # print(users_update_query.raw_result)


        return {
            'statusCode': 200,
            'body': {"Success": "Role deleted successfully."}
        }
    except Exception as err:
        return {'statusCode': 500, "Error": "delete user rule failed"}

import json
from pymongo import MongoClient
import boto3
import os

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        loan_package = db.LoanPackage.aggregate([{"$sort": {"Name": 1}}])

        packages = []

        for item in loan_package:
            package = dict()
            package["_id"] = str(item["_id"])
            package["Name"] = item["Name"]
            packages.append(package)

        return {"result": packages}
    except Exception as err:
        return {'statusCode': 500, "Error": "loan package details failed"}

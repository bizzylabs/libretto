import csv
import datetime
import io
import json
import os
from string import punctuation

import boto3
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]
s3 = boto3.client("s3")


def remove_special_char(filename):
    special_char_list = list(punctuation)
    special_char_list.extend(['_ ', ' '])
    for char in special_char_list:
        filename = filename.replace(char, "_")
    return filename


def get_csv_data(data):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post, dialect='excel')
    keys = data[0].keys()
    csv_out.writerow(keys)
    for row in data:
        csv_out.writerow(row.values())
    return data_to_post.getvalue()


def getExceldata(data, file_name):
    df = pd.DataFrame(data)
    with io.BytesIO() as output:
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)
        data = output.getvalue()

    s3 = boto3.resource('s3')
    s3.Bucket(BUCKET_NAME).put_object(Key=file_name, Body=data)
    return {"status": 200, "body": "file uploaded."}


def create_presigned_url(bucket_name, object_name, expiration=300):
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None
    return response


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            content_type = "text/csv;charset=utf-8"
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data, ContentType=content_type)
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def export_summary_data(event, context):
    try:
        user_name = event["context"]["username"]
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        event = event["body"]
        d = str(datetime.datetime.now().timestamp()).split('.')[0]
        # filename = "summary_report{}.csv".format(d)
        output = []
        rule_ids = []
        rule_detail = dict()
        rulesetname = ""
        rule_set_id = event.get("ruleSetId", None)
        ruleIds = event.get("ruleIds", [])
        data_filter = event.get("filter", "all")
        run_id = event["run_id"]
        count = 0
        loan_package_id = event.get("loan_package_id")
        filters = event.get("filters", None)
        condition_filters = event.get("condition_filters", None)
        exception_type = event.get("exception_type", None)
        if exception_type == "open" :
            exception_filter = {"$eq": ["$SystemNote", [] ]}
        else:
            exception_filter = {}

        rule_count_condition = [exception_filter,{"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}]

        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)

                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)

        else:
            loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = db.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)
        if not rule_set_id:
            return {"Error Message": "RuleSetId is required"}

        if len(ruleIds) == 1:
            rulename = str(list(db.Rule.find({"_id": ObjectId(ruleIds[0])}, {"Name": 1}))[0].get("Name"))
            cdt = datetime.datetime.now()
            currentstmp = str(cdt.timestamp()).split('.')[0]
            filename = "{}_{}".format(rulename, currentstmp)
            for ids in ruleIds:
                rule_ids.append(ObjectId(ids))
        else:
            try:
                rule_id_list = db.RuleSummary.find({"RuleSetId": ObjectId(rule_set_id), "RunId": run_id},
                                                   {"RuleId": 1, "_id": 0})
            except Exception as err:
                return {"Error Message": str(err)}

            if len(ruleIds) > 0:
                for ids in ruleIds:
                    rule_ids.append(ObjectId(ids))
            else:
                rule_ids = [rule_id["RuleId"] for rule_id in rule_id_list]
            rulesetname = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})["RuleSetName"]
            cdt = datetime.datetime.now()
            currentstmp = str(cdt.timestamp()).split('.')[0]
            filename = "{}_{}_{}".format(user_name,rulesetname,currentstmp)

        filename = remove_special_char(filename) + ".xlsx"

        rule_query = [
            {"$match": {"$and": [{
                "RunId": run_id,
                "RuleSetId": ObjectId(rule_set_id),
                "ExceptionStatus": True
            }
            ]}},
            {"$group": {
                "_id": {"rule_set_id": "$RuleSetId",
                        "rule_id": "$RuleId"
                        },
                "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                "priority": {"$first": "$Severity"},
                "sort_priority": {"$first": "$Priority"},
                "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}}
            }},
            {"$group": {
                "_id": "$_id.rule_set_id",
                "Rules": {
                    "$push": {
                        "RuleId": "$_id.rule_id",
                        "exceptions": "$rule_count",
                        "new": "$new_count",
                        "severity": "$priority",
                        "priority": "$sort_priority"
                    },
                }
            }},
            {"$unwind": "$Rules"},
            {"$lookup":
                {
                    "from": "Rule",
                    "let": {"rule_id": "$Rules.RuleId"},
                    "pipeline": [
                        {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                    ],
                    "as": "RuleData"
                }
            },
            {"$lookup":
                {
                    "from": "RuleType",
                    "localField": "RuleData.RuleType",
                    "foreignField": "_id",
                    "as": "RuleType"
                }
            },
            {"$lookup":
                {
                    "from": "RuleSubType",
                    "localField": "RuleData.RuleSubType",
                    "foreignField": "_id",
                    "as": "RuleSubType"
                }
            },
            {"$lookup":
                {
                    "from": "ComplianceType",
                    "localField": "RuleData.ComplianceType",
                    "foreignField": "_id",
                    "as": "ComplianceType"
                }
            }
        ]
        for rule in db.ResultSetDetail.aggregate(rule_query):
            rule_detail = dict()
            try:
                rule_data = rule["RuleData"][0]
            except Exception as e:
                rule_data = db.Rule.find_one({"_id": ObjectId(rule["Rules"]["RuleId"])})
            rule_detail["Business Rule"] = rule_data["Name"]
            rule_detail["Rule SubType"] = ""
            rule_detail["Rule Type"] = ""
            rule_detail["Compliance Type"] = ""
            if len(rule["RuleType"]) > 0:
                rule_detail["Rule Type"] = rule["RuleType"][0]["Name"]
            if len(rule["RuleSubType"]) > 0:
                rule_detail["Rule SubType"] = rule["RuleSubType"][0]["Name"]
            if len(rule["ComplianceType"]) > 0:
                rule_detail["Compliance Type"] = rule["ComplianceType"][0]["Name"]
            rule_detail["Priority"] = rule["Rules"]["severity"]
            rule_detail["Exception"] = rule["Rules"]["exceptions"]
            rule_detail["New"] = rule["Rules"]["new"]
            if rule_detail["Exception"] == 0:
                count += 1
            if data_filter == "ZeroExceptions":
                if rule_detail["Exception"] == 0:
                    output.append(rule_detail)
            elif data_filter == "WithExceptions":
                if rule_detail["Exception"] != 0:
                    output.append(rule_detail)
            elif data_filter == "NewExceptions":
                if rule_detail["NewException"] != 0:
                    output.append(rule_detail)
            else:
                output.append(rule_detail)

        response = getExceldata(output, filename)
        client.close()
        if response.get("status") == 200:
            file_link = create_presigned_url(BUCKET_NAME, filename)
        else:
            file_link = ""
        response['file_path'] = file_link
        log_document = {}
        log_document["Ip"] = ip
        log_document["browser"] = browser
        log_document["Username"] = user_name
        log_document["CreatedOn"] = cdt
        log_document["RunId"] = run_id
        log_document["action"] = "Export"
        log_document["log_from"] = "Summary"
        log_document["file_name"] = filename
        if rulesetname:
            log_document["RuleSetName"] = rulesetname
        insert_log = db.BussinessAuditLogs.insert_one(log_document)
        client.close()
        return response
    except Exception as err:
        return {'statusCode': 500, "Error": "export exception summary failed"}


import json
import os
import boto3
from bson import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        Priority = event["body"].get("Priority", [])
        ruleset = event["body"].get("Ruleset", [])
        username = event["body"].get("Username", [])
        action = event["body"].get("Action", [])
        rule = event["body"].get("Rule", [])
        notes = event["body"].get("Notes", [])
        servicing = event["body"].get("Servicing System", [])
        is_active = event["body"].get("IsActive", 1)
        request_type = event["body"].get("Type", "GET")
        notesconfig_id = event["body"].get("notes_config_id")

        if request_type == "GET":
            resp = db.NotesConfig.find_one()
            print(resp)
            resp["notesconfig_id"] = str(resp["_id"])
            del resp["_id"]
            return {'statusCode': 200, "body": resp}
        if request_type == "POST":
            dict = {"Priority": Priority,
                    "Ruleset": ruleset,
                    "Username": username,
                    "Action": action,
                    "Business Rule": rule,
                    "Notes Category": notes,
                    "Active": is_active,
                    "Servicing System": servicing}
            resp = db.NotesConfig.update_one({"_id": ObjectId(notesconfig_id)}, {"$set": dict})
            print(resp)
            if resp["modifiedCount"] == 1:
                return {'statusCode': 200, "body": "Successfully updated"}
            else:
                return {'statusCode': 500, "body": "Not Updated"}
    except Exception as err:
        return {'statusCode': 500, "error": "notes configuration failed"}
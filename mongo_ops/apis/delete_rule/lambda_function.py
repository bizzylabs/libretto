import json
import os
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from log_queue import push_log
from pymongo import MongoClient

secret_id = os.environ['SECRET_ID']


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=secret_id)
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def delete_rule(event, context):
    try:
        role = event["context"].get("role", "")
        rule_set_ids = event['body'].get("RuleSetIds")
        rule_id = event['body'].get("RuleId")
        rule_set_ids_log = event['body'].get("RuleSetIds")
        rule_id_log = event['body'].get("RuleId")
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        username = event["context"].get("username", None)
        email = event["context"].get("email", None)

        database = get_initialized_db()
        rule_log = database.Rule.find_one({"_id": ObjectId(rule_id_log)})

        log = {"action": "RuleDeleted", "username": username, "email": email, "ip": ip,
               "browser": browser, "timestamp": str(datetime.now()), "RuleId": rule_id_log,
               "RuleSetId": rule_set_ids_log, "RuleNameId": rule_log.get("RuleNameId", ""),
               "Version": rule_log.get("Version", 0), "RuleName": rule_log.get("Name", "")}

        if len(rule_set_ids) == 0:
            rule = list(database.Rule.find({'_id': ObjectId(rule_id)}))
            if rule:
                rule_set_ids = [ObjectId(id) for id in rule[0]['Departments']]

        else:
            rule_set_ids = [ObjectId(id) for id in rule_set_ids]

        if ObjectId("5f1af96897d568eae6cca2cf") in rule_set_ids:
            rule_set_data = list(database.RuleSetMaster.find({'_id': ObjectId("5f1af96897d568eae6cca2cf")}))
            if rule_set_data:
                for mapping in rule_set_data[0]['RuleIds']:
                    if mapping['ChildId'] == ObjectId(rule_id):
                        rule_set_data[0]['RuleIds'].remove(mapping)
                database.RuleSetMaster.update({'_id': ObjectId("5f1af96897d568eae6cca2cf")},
                                              {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})
                # push log to database
                push_log.push(log)
            return {"result": "Rule deleted."}

        # update in ruleSetMaster
        try:
            for ruleSetId in rule_set_ids:
                rule_set_data = list(database.RuleSetMaster.find({'_id': ruleSetId}))
                if rule_set_data:
                    for ruleId in rule_set_data[0]['RuleIds']:
                        if ruleId['RuleId'] == ObjectId(rule_id):
                            rule_set_data[0]['RuleIds'].remove(ruleId)
                    database.RuleSetMaster.update({'_id': ObjectId(ruleSetId)},
                                                  {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})
        except:
            return {"error": "Rule mapping from ruleset could not be deleted."}

        # update in rule table
        try:
            database.Rule.update({'_id': ObjectId(rule_id)}, {'$pull': {'Departments': {'$in': rule_set_ids}}})
            rule = list(database.Rule.find({'_id': ObjectId(rule_id)}))
            if rule:
                if len(rule[0]['Departments']) == 0:
                    database.Rule.update({'_id': ObjectId(rule_id)}, {"$set": {'IsActive': 0}})
        except:
            return {"error": "Department from rule could not be deleted."}

        # push logs to database
        push_log.push(log)
        return {"result": "Rule deleted."}
    except Exception as e:
        return {"statusCode": 500, "error": "Delete rule failed."}

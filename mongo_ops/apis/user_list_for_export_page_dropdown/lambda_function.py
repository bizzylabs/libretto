import json
import boto3
from pymongo import MongoClient
import datetime
import os

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def extract_date_details(date):
    """
    str date: "gmtFromTime": "Mon, 31 Jan 2022 18:30:00 GMT",
    str date: "gmtToTime": "Tue, 01 Feb 2022 18:29:00 GMT"
    :return: datetime object.
    """
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime.replace(hour=0, minute=0, second=0, microsecond=0)


def lambda_handler(event, context):
    """Fetch data with distinct.
        str start_date: string in month/date/year (12/22/2020) format.
        str end_date: string in month/date/year (12/24/2020) format.
    """
    try:
        bussiness_user_obj = None
        start_date = event["body"].get("gmtFromTime", None)
        end_date = event["body"].get("gmtToTime", None)

        if not start_date and not end_date:
            bussiness_user_obj = db.BussinessAuditLogs.aggregate(
                [{"$group": {"_id": None, "uniqueValues": {"$addToSet": "$username"}}}])

        if not bussiness_user_obj:
            if start_date:
                gmt_from_date = extract_date_details(start_date)
            else:
                return {"status": 500, "body": "Missing parameter value for gmtFromTime"}

            if end_date:
                gmt_to_date = extract_date_details(end_date)
            else:
                return {"status": 500, "body": "Missing parameter value for gmtToTime"}

            bussiness_user_obj = db.BussinessAuditLogs.aggregate([{"$match": {"$and": [
                {'timestamp': {'$ne': None,
                               "$gte": gmt_from_date,
                               "$lte": gmt_to_date}}]
            }},
                {"$group": {"_id": None,
                            "uniqueValues": {"$addToSet": "$username"}}},
                {"$project": {"_id": 0}}
            ])

        bauditlog_user_objs = [data['uniqueValues'] for data in bussiness_user_obj][0]

        # removing "" because we are capturing logs for Migration having blank username.
        final_username_list = list(set(bauditlog_user_objs))
        users = []

        if "" in final_username_list:
            final_username_list.remove("")
        if None in final_username_list:
            final_username_list.remove(None)
        for user in final_username_list:
            if user:
                if ".com" in user or ".tech" in user:
                    user = user.split("@")[0]
                if user not in users:
                    users.append(user)

        return {
            'statusCode': 200,
            'users': sorted(users)
        }
    except Exception as err:
        return {'statusCode': 500, "error": "user list for export page drop down failed:"}

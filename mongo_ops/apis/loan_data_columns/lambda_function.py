import json
import os

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        data = list(db.LoanDataColumns.find({}))
        LoanDataFields = []
        if len(data) < 1:
            return {"error": "Data not exists"}
        if data[0].get("LoanDataFields", None):
            fields = data[0].get("LoanDataFields")
            fields.sort()
            LoanFields = [{"value": key, "path": ""} for key in fields]
            LoanFields.append({"value": "ServicerHandlesInsurance", "path": ""})
            LoanFields.append({"value": "ServicerHandlesTaxes", "path": ""})
            LoanFields.append({"value": "ShortYearStatementDt", "path": ""})
            LoanFields.append({"value": "ProofOfClaimFiledDate", "path": ""})
            LoanFields.append({"value": "MortgageInsurancePercentageCovered", "path": ""})
            LoanFields.append({"value": "ArmChangeStatus", "path": ""})
            LoanFields.append({"value": "ArmChangeType", "path": ""})
            LoanFields.append({"value": "ArmChangeCounter", "path": ""})
            LoanFields.append({"value": "ArmChangeRate", "path": ""})
            LoanFields.append({"value": "ArmChangeAmount", "path": ""})
            LoanFields.append({"value": "AffectedBorrowerStartDate", "path": ""})
            LoanFields.append({"value": "LoanTypeDescription", "path": ""})
            LoanFields.append({"value": "ForbearanceNonPayStartDate", "path": ""})
            LoanFields.append({"value": "ForbearanceNonPayEndDate", "path": ""})
            LoanFields.append({"value": "ForbearanceRepayStartDate", "path": ""})
            LoanFields.append({"value": "ForbearanceRepayEndDate", "path": ""})
            LoanFields.append({"value": "ReinstatementFlag", "path": ""})
            LoanFields.append({"value": "LoanAssumptionFlag", "path": ""})
            LoanFields.append({"value": "PreforeclosureSaleFlag", "path": ""})
            LoanFields.append({"value": "ListingAgreementFlag", "path": ""})
            LoanFields.append({"value": "PartialClaimFlag", "path": ""})
            LoanFields.append({"value": "PayoffFlag", "path": ""})
            LoanFields.append({"value": "ChargeOffFlag", "path": ""})
            LoanFields.append({"value": "OriginalAmount", "path": ""})
            LoanFields.append({"value": "UserField111", "path": ""})
        if data[0].get("EscrowDataFields", None):
            LoanFields.extend([{"value": key, "path": "Escrow"} for key in data[0].get("EscrowDataFields")])
        if data[0].get("TaskFields", None):
            LoanFields.extend([{"value": key, "path": "Task"} for key in data[0].get("TaskFields")])

        client.close()
        return {"result": LoanFields}
    except Exception as err:
        return {'statusCode': 500, "error": "loan data columns failed"}

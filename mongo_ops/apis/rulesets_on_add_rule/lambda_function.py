import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
from collections import OrderedDict
import os

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def get_ruleset_data(event, context):
    output = []
    try:
        user_name = event["context"]["username"]
        rule_sets = db.RuleSetMaster.find({})
        for rule_set in rule_sets:
            if rule_set["RuleSetName"] != "Draft Rules":
                rule_set_detail = OrderedDict()
                rule_set_detail["_id"] = str(rule_set.get("_id"))
                rule_set_detail["RuleSetName"] = rule_set.get("RuleSetName")
                rule_count = len(rule_set.get("RuleIds"))
                rule_set_detail["rulecount"] = rule_count
                rule_set_detail["ShowStatusFlag"] = 0
                if user_name in rule_set["Users"]:
                    rule_set_detail["ShowStatusFlag"] = 1
                    rule_set_detail["RuleSetVersion"] = rule_set.get("RuleSetVersion")

                output.append(rule_set_detail)
        output = sorted(output, key=lambda x: x['RuleSetName'])
        output = sorted(output, key=lambda i: i['ShowStatusFlag'], reverse=True)
        client.close()
        return {"result": sorted(output, key=lambda x: x['RuleSetName'] != 'Draft Rules', reverse=True)}


    except Exception as err:
        return {'statusCode': 500, "Error": "rule sets on add rule failed"}

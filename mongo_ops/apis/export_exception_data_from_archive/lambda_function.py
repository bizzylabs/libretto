import json
import os
import datetime
import boto3
from pymongo import MongoClient
import pandas as pd
from string import punctuation
import io
from bson import ObjectId
import xlsxwriter


secret = boto3.client("secretsmanager", region_name='us-east-1')
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

BUCKET_NAME = secret_dict["export_bucket"]


def create_presigned_url(bucket_name, object_name, expiration=300):
    s3 = boto3.client("s3")
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except Exception as e:
        return None
    return response


def export_data_to_excel(data, filename):
    df = pd.DataFrame(data)
    with io.BytesIO() as output:
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)
        data = output.getvalue()

    s3 = boto3.resource('s3')
    s3.Bucket(BUCKET_NAME).put_object(Key=filename, Body=data)

    file_link = create_presigned_url(BUCKET_NAME, filename)
    if file_link is None:
        file_link = ""

    if file_link:
        return {"status": 200, "body": "file uploaded.", "file_path": file_link}
    else:
        return {"status": 400, "body": "file not uploaded."}


def extract_date_details(date):
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime


def get_run_ids(loan_package_id):
    loan_data = db.SchedulerHistory.find({"LoanPackage": ObjectId(loan_package_id)})
    run_ids = []
    for loan in loan_data:
        run_ids.append(loan['RunId'])
    return run_ids


def lambda_handler(event, context):
    try:
        user_name = event["context"]["username"]
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)

        loan_number = event["body"].get("loan_number")
        loan_type = event["body"].get("loan_type")
        start_time = extract_date_details(event["body"].get("gmtFromTime"))
        end_time = extract_date_details(event["body"].get("gmtToTime"))
        loan_package_id = event['body'].get("loan_package_id")
        file_name = event['body'].get("file_name")

        result_array = []
        final_response = []
        response_data_dict = {}

        run_ids = get_run_ids(loan_package_id)

        result_set_detail_collection = db['ResultSetDetailArchive']

        query = [{"$match": {"LoanNumber": loan_number, "RunId": {"$in": run_ids},
                             "InExistence": {"$lte": end_time}, "UpdatedOn": {"$gte": start_time}
                             }
                  },
                 {
                     "$group": {
                         "_id": {"rule_set_id": "$RuleSetId",
                                 "rule_id": "$RuleId",
                                 "exception_status": "$ExceptionStatus",
                                 "in_existence": "$InExistence"
                                 },
                         "rule_priority": {"$first": "$Severity"},
                         "rule_version": {"$first": "$RuleVersion"},
                         "identified_on": {"$first": "$InExistence"},
                         "resolved_on": {"$first": "$UpdatedOn"},
                         "exception_status": {"$first": "$ExceptionStatus"},
                         "run_id": {"$first": "$RunId"}
                     }
                 },
                 {"$addFields": {"identifiedOn": {"$toDate":
                                                      {"$dateToString": {"format": "%Y-%m-%d",
                                                                         "date": "$identified_on"}}}}},
                 {"$addFields": {"resolvedOn": {"$toDate":
                                                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$resolved_on"}}}}},
                 {"$lookup":
                     {
                         "from": "Rule",
                         "let": {"rule_id": "$_id.rule_id"},
                         "pipeline": [
                             {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                         ],
                         "as": "RuleData"
                     }
                 }, {"$unwind": "$RuleData"},
                 {"$lookup":
                     {
                         "from": "RuleSetMaster",
                         "let": {"rule_set_id": "$_id.rule_set_id"},
                         "pipeline": [
                             {"$match": {"$expr": {"$eq": ["$_id", "$$rule_set_id"]}}}
                         ],
                         "as": "RuleSetMasterData"
                     }
                 }, {"$unwind": "$RuleSetMasterData"},
                 {"$project": {"_id": 1, "rule_priority": 1, "rule_version": 1, "run_id": 1,
                               "identified_on": 1, "resolved_on": 1, "exception_status": 1,
                               "RuleSetMasterData": 1, "RuleData": 1,
                               "identifiedOn": 1, "resolvedOn": 1,
                               "days_to_resolve": {"$ceil":
                                                       {"$divide": [{"$subtract": ["$resolvedOn", "$identifiedOn"]},
                                                                    86400000]}}}},
                 ]

        if loan_type == "audit":
            query[0]['$match'].update({"Severity": "Critical"})

        response_data = result_set_detail_collection.aggregate(query)
        for record in response_data:
            key = str(record['_id']['rule_set_id']) + str(record['_id']['rule_id']) + record['_id']['in_existence'].strftime("%m/%d/%Y %H:%M")
            if key not in response_data_dict:
                response_data_dict[key] = [record]
            else:
                response_data_dict[key].append(record)

        for key, value in response_data_dict.items():
            if len(value) == 1:
                if value[0]['exception_status']:
                    final_response.append(value[0])
            if len(value) == 2:
                for record in value:
                    if not record['exception_status']:
                        final_response.append(record)

        for x in final_response:
            result_dict = {
                "Rule Set": x['RuleSetMasterData']['RuleSetName'],
                "Rule Name": x['RuleData']['Name'],
                "Version": x['rule_version'],
                "Priority": x['rule_priority'],
                "Identified On": x['identified_on'].strftime("%m/%d/%Y"),
                "Resolved On": x['resolved_on'].strftime("%m/%d/%Y"),
                "Days To Resolve": x['days_to_resolve'] + 1,
            }
            if x['exception_status']:
                result_dict['Resolved On'] = "-"
                result_dict['Days To Resolve'] = "-"
            result_array.append(result_dict)

        cdt = datetime.datetime.now()
        response = export_data_to_excel(result_array, file_name)

        if response['status'] == 200:
            log_document = {"Ip": ip, "browser": browser, "Username": user_name, "CreatedOn": cdt,
                            "action": "Export", "log_from": "Loan_History", "file_name": file_name,
                            "criteria": {
                                "LoanNumber": loan_number,
                                "LoanType": loan_type,
                                "StartDate": start_time,
                                "EndDate": end_time
                            }}

            db.BussinessAuditLogs.insert_one(log_document)

        return response
    except Exception as err:
        return {'statusCode': 500, "error": "exception data from archive polling failed"}
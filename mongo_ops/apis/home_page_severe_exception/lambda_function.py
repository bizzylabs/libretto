
import json
import os
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

severity_data = dict()


def get_total_exceptions_count(data):
    top5_exceptions = get_ruleset_and_rule_name(data)
    top5_exceptions.sort(key=lambda item: item.get("TotalExceptions"), reverse=True)
    for exception in top5_exceptions:
        severity = exception.get("Severity", None)
        if severity and exception["TotalExceptions"] != 0 and len(severity_data[severity]["Rules"]) < 3:
            severity_data[severity]["Rules"].append(exception)


def get_ruleset_and_rule_name(data_list):
    out = []
    for dict_data in data_list:
        temp_dict = dict()
        rule_set_id = dict_data["RuleSetId"]
        rule_id = dict_data["Rule"]
        ruleset = db.RuleSetMaster.find_one({"_id": rule_set_id}, {"RuleSetName": 1, "_id": False})
        rule = db.Rule.find_one({"_id": rule_id}, {"Name": 1, "Severity": 1, "_id": False})
        if rule:
            temp_dict["RuleSetName"] = ruleset.get("RuleSetName")
            temp_dict["RuleName"] = rule.get("Name")
            temp_dict["Severity"] = dict_data["severity"]
            temp_dict["RuleSetId"] = str(rule_set_id)
            temp_dict["RuleId"] = str(rule_id)
            temp_dict["RunId"] = dict_data["RunId"]
            temp_dict["TotalExceptions"] = dict_data["exceptions"]
            out.append(temp_dict)

    return out


def latest_available(rule_set_id, run_id, created_date, loan_package):
    try:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S.%f')
    except:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S')
    run_ids = [item["RunId"] for item in db.SchedulerHistory.find(
        {"LoanPackage": ObjectId(loan_package), "RunId": {"$gt": run_id}, "IsActive": 1}) if item]
    result = db.ResultSetSummary.find_one({"$and": [{"RunId": {"$in": run_ids}},
                                                    {"RuleSetId": ObjectId(rule_set_id), "RunId": {"$gt": run_id},
                                                     "CreatedOn": {"$gte": created_on}}]})
    if result:
        val = db.SchedulerHistory.find_one({"RunId": result["RunId"], "LoanPackage": ObjectId(loan_package)},
                                           {"_id": 0, "IsActive": 1})
        if val and val["IsActive"] == 1:
            return True
    return False


def lambda_handler(event, context):
    try:
        user_name = event["context"]["username"]
        loan_package_id = event["body"].get("loan_package_id", None)
        if not loan_package_id:
            return {"error": "loan package id is missing"}
        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        exception_type = event["body"].get("exception_type", None)
        if exception_type == "open":
            exception_filter = {"$eq": ["$SystemNote", []]}
        else:
            exception_filter = {}

        severity_type = ["Critical", "High", "Medium", "Low", "Informational"]
        for severity in severity_type:
            severity_data[severity] = {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0,
                                       "Rules": []}

        rule_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$ExceptionStatus", True]},
                                {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$ExceptionStatus", True]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}, {"$eq": ["$Ignore", False]}]
        cleared_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                   {"$eq": ["$ExceptionStatus", False]}, {"$eq": ["$Ignore", False]}]
        high_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$ExceptionStatus", True]},
                                {"$eq": ["$Severity", "High"]}, {"$eq": ["$Ignore", False]}]
        low_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$ExceptionStatus", True]},
                               {"$eq": ["$Severity", "Low"]}, {"$eq": ["$Ignore", False]}]
        medium_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {"$eq": ["$ExceptionStatus", True]},
                                  {"$eq": ["$Severity", "Medium"]}, {"$eq": ["$Ignore", False]}]
        critical_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                    {"$eq": ["$ExceptionStatus", True]},
                                    {"$eq": ["$Severity", "Critical"]}, {"$eq": ["$Ignore", False]}]
        informational_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                         {"$eq": ["$ExceptionStatus", True]},
                                         {"$eq": ["$Severity", "Informational"]}, {"$eq": ["$Ignore", False]}]
        new_high_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                                    {"$eq": ["$Severity", "High"]},
                                    {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        new_low_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                                   {"$eq": ["$Severity", "Low"]},
                                   {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]

        new_medium_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                                      {"$eq": ["$Severity", "Medium"]},
                                      {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        new_critical_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                                        {"$eq": ["$Severity", "Critical"]},
                                        {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        new_informational_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]}, {
            "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                                             {"$eq": ["$Severity", "Informational"]},
                                             {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        cleared_high_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                        {"$eq": ["$ExceptionStatus", False]},
                                        {"$eq": ["$Severity", "High"]}, {"$eq": ["$Ignore", False]}]
        cleared_low_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                       {"$eq": ["$ExceptionStatus", False]},
                                       {"$eq": ["$Severity", "Low"]}, {"$eq": ["$Ignore", False]}]
        cleared_medium_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                          {"$eq": ["$ExceptionStatus", False]},
                                          {"$eq": ["$Severity", "Medium"]}, {"$eq": ["$Ignore", False]}]
        cleared_critical_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                            {"$eq": ["$ExceptionStatus", False]},
                                            {"$eq": ["$Severity", "Critical"]}, {"$eq": ["$Ignore", False]}]
        cleared_informational_count_condition = [exception_filter, {"$ne": ["$LoanNumber", None]},
                                                 {"$eq": ["$ExceptionStatus", False]},
                                                 {"$eq": ["$Severity", "Informational"]}, {"$eq": ["$Ignore", False]}]

        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)
                    informational_count_condition.append(cond)
                    new_high_count_condition.append(cond)
                    new_low_count_condition.append(cond)
                    new_medium_count_condition.append(cond)
                    new_critical_count_condition.append(cond)
                    new_informational_count_condition.append(cond)
                    cleared_high_count_condition.append(cond)
                    cleared_low_count_condition.append(cond)
                    cleared_medium_count_condition.append(cond)
                    cleared_critical_count_condition.append(cond)
                    cleared_informational_count_condition.append(cond)

        else:
            try:
                loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
                filters = db.UserFilters.find_one({"UserName": user_name})
            except Exception as err:
                return {"error": "Database opeartional error", "status": 500}
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)
                    informational_count_condition.append(cond)
                    new_high_count_condition.append(cond)
                    new_low_count_condition.append(cond)
                    new_medium_count_condition.append(cond)
                    new_critical_count_condition.append(cond)
                    new_informational_count_condition.append(cond)
                    cleared_high_count_condition.append(cond)
                    cleared_low_count_condition.append(cond)
                    cleared_medium_count_condition.append(cond)
                    cleared_critical_count_condition.append(cond)
                    cleared_informational_count_condition.append(cond)

        rule_set_ids = list(db.RuleSetMaster.aggregate([
            {"$match": {"Users": {"$in": [user_name]}}},
            {"$group": {"_id": "", "ids": {"$push": "$_id"}}},
            {"$project": {"_id": 0}}
        ]))
        if len(rule_set_ids) > 0:
            rule_set_ids = rule_set_ids[0].get("ids")
        else:
            return {"SeverExceptions": {
                "Critical": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0,
                             "Rules": []},
                "High": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
                "Medium": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
                "Low": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
                "Informational": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0,
                                  "Rules": []},
            }}
        scheduler_config_detail = db.SchedulerConfig.find({"LoanPackageId": ObjectId(loan_package_id)})
        run_ids = []
        all_rules = []
        all_rulesets = []
        for scheduler_config in scheduler_config_detail:
            output = []
            scheduler_config_id = scheduler_config.get("_id")
            # scheduler_history = list(db.SchedulerHistory.find({"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
            #                                                    "SchedulerConfigId": ObjectId(
            #                                                        scheduler_config_id)}).sort(
            #     [("RunId", -1)]).limit(1))
            scheduler_history = list(db.SchedulerHistory.aggregate(
                [{"$match": {"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
                             "SchedulerConfigId": ObjectId(scheduler_config_id)}},
                 {"$sort": {"RunId": -1}},
                 {"$limit": 1},
                 ]))

            if len(scheduler_history) > 0:
                scheduler_history = scheduler_history[0]
                run_id = scheduler_history.get("RunId")
                run_ids.append(run_id)
                pipe = [
                    {"$match": {"$and": [{"RunId": run_id, "RuleSetId": {"$in": rule_set_ids}}]}},
                    {"$group": {
                        "_id": {"rule_set_id": "$RuleSetId",
                                "rule_id": "$RuleId",
                                },
                        "priority": {"$first": "$Severity"},
                        "run_id": {"$first": "$RunId"},
                        "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                        "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}},
                        "cleared_count": {"$sum": {"$cond": [{"$and": cleared_count_condition}, 1, 0]}},
                        "High_count": {"$sum": {"$cond": [{"$and": high_count_condition}, 1, 0]}},
                        "Low_count": {"$sum": {"$cond": [{"$and": low_count_condition}, 1, 0]}},
                        "Medium_count": {"$sum": {"$cond": [{"$and": medium_count_condition}, 1, 0]}},
                        "Critical_count": {"$sum": {"$cond": [{"$and": critical_count_condition}, 1, 0]}},
                        "Informational_count": {"$sum": {"$cond": [{"$and": informational_count_condition}, 1, 0]}},

                        "High_count_new": {"$sum": {"$cond": [{"$and": new_high_count_condition}, 1, 0]}},
                        "Low_count_new": {"$sum": {"$cond": [{"$and": new_low_count_condition}, 1, 0]}},
                        "Medium_count_new": {"$sum": {"$cond": [{"$and": new_medium_count_condition}, 1, 0]}},
                        "Critical_count_new": {"$sum": {"$cond": [{"$and": new_critical_count_condition}, 1, 0]}},
                        "Informational_count_new": {
                            "$sum": {"$cond": [{"$and": new_informational_count_condition}, 1, 0]}},

                        "High_count_cleared": {"$sum": {"$cond": [{"$and": cleared_high_count_condition}, 1, 0]}},
                        "Low_count_cleared": {"$sum": {"$cond": [{"$and": cleared_low_count_condition}, 1, 0]}},
                        "Medium_count_cleared": {"$sum": {"$cond": [{"$and": cleared_medium_count_condition}, 1, 0]}},
                        "Critical_count_cleared": {
                            "$sum": {"$cond": [{"$and": cleared_critical_count_condition}, 1, 0]}},
                        "Informational_count_cleared": {
                            "$sum": {"$cond": [{"$and": cleared_informational_count_condition}, 1, 0]}},
                    }},
                    {"$group": {
                        "_id": "$_id.rule_set_id",
                        "Rules": {
                            "$push": {
                                "RuleSetId": "$_id.rule_set_id",
                                "Rule": "$_id.rule_id",
                                "RunId": "$run_id",
                                "exceptions": "$rule_count",
                                "new": "$new_count",
                                "severity": "$priority"
                            },
                        },
                        "total_exception": {"$sum": "$rule_count"},
                        "new_exceptions": {"$sum": "$new_count"},
                        "cleared_exceptions": {"$sum": "$cleared_count"},

                        "high_severity": {"$sum": "$High_count"},
                        "low_severity": {"$sum": "$Low_count"},
                        "medium_severity": {"$sum": "$Medium_count"},
                        "critical_severity": {"$sum": "$Critical_count"},
                        "informational_severity": {"$sum": "$Informational_count"},

                        "high_severity_new": {"$sum": "$High_count_new"},
                        "low_severity_new": {"$sum": "$Low_count_new"},
                        "medium_severity_new": {"$sum": "$Medium_count_new"},
                        "critical_severity_new": {"$sum": "$Critical_count_new"},
                        "informational_severity_new": {"$sum": "$Informational_count_new"},

                        "high_severity_cleared": {"$sum": "$High_count_cleared"},
                        "low_severity_cleared": {"$sum": "$Low_count_cleared"},
                        "medium_severity_cleared": {"$sum": "$Medium_count_cleared"},
                        "critical_severity_cleared": {"$sum": "$Critical_count_cleared"},
                        "informational_severity_cleared": {"$sum": "$Informational_count_cleared"},
                    }},
                    {"$sort": {"total_exception": -1}}
                ]
                for rule_sets_data in db.ResultSetDetail.aggregate(pipe):
                    if rule_sets_data["_id"] not in all_rulesets:
                        rule_set_detail = dict()
                        rule_set_detail["_id"] = str(rule_sets_data.get("_id"))
                        rule_set_detail["RunId"] = run_id
                        exceptions = db.ResultSetSummary.find_one(
                            {"RuleSetId": ObjectId(rule_sets_data["_id"]), "RunId": run_id})
                        if exceptions:
                            created_on_temp = exceptions.get("CreatedOn")
                        if not latest_available(rule_set_detail["_id"], rule_set_detail["RunId"], created_on_temp,
                                                loan_package_id):
                            total_loans_count = list(db.ResultSetSummary.aggregate([
                                {"$match": {"RunId": run_id}},
                                {"$group": {
                                    "_id": "$RuleSetId",
                                    "total_loans": {"$max": "$NoOfLoans"}
                                }},
                                {"$limit": 1}
                            ]))
                            if len(total_loans_count) > 0:
                                total_loan = total_loans_count[0]["total_loans"]
                            else:
                                total_loan = 0
                            severity_data["Critical"]["TotalException"] += rule_sets_data["critical_severity"]
                            severity_data["Critical"]["NewException"] += rule_sets_data["critical_severity_new"]
                            severity_data["Critical"]["ClearedException"] += rule_sets_data["critical_severity_cleared"]
                            severity_data["Critical"]["TotalLoan"] = total_loan
                            severity_data["High"]["TotalException"] += rule_sets_data["high_severity"]
                            severity_data["High"]["NewException"] += rule_sets_data["high_severity_new"]
                            severity_data["High"]["ClearedException"] += rule_sets_data["high_severity_cleared"]
                            severity_data["High"]["TotalLoan"] = total_loan
                            severity_data["Medium"]["TotalException"] += rule_sets_data["medium_severity"]
                            severity_data["Medium"]["NewException"] += rule_sets_data["medium_severity_new"]
                            severity_data["Medium"]["ClearedException"] += rule_sets_data["medium_severity_cleared"]
                            severity_data["Medium"]["TotalLoan"] = total_loan
                            severity_data["Low"]["TotalException"] += rule_sets_data["low_severity"]
                            severity_data["Low"]["NewException"] += rule_sets_data["low_severity_new"]
                            severity_data["Low"]["ClearedException"] += rule_sets_data["low_severity_cleared"]
                            severity_data["Low"]["TotalLoan"] = total_loan
                            severity_data["Informational"]["TotalException"] += rule_sets_data["informational_severity"]
                            severity_data["Informational"]["NewException"] += rule_sets_data[
                                "informational_severity_new"]
                            severity_data["Informational"]["ClearedException"] += rule_sets_data[
                                "informational_severity_cleared"]
                            severity_data["Informational"]["TotalLoan"] = total_loan
                            all_rules.extend(rule_sets_data["Rules"])
        get_total_exceptions_count(all_rules)

        return {"SeverExceptions": severity_data}
    except Exception as e:
        return {"SeverExceptions": {
            "Critical": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0,
                         "Rules": []},
            "High": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
            "Medium": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
            "Low": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0, "Rules": []},
            "Informational": {"TotalException": 0, "NewException": 0, "ClearedException": 0, "TotalLoan": 0,
                              "Rules": []},
        }}


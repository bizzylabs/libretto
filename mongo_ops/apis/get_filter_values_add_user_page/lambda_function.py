import datetime
import json
import os

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        data = db.AccessFilterValues.find_one({})["Values"]
        for item in data.keys():
            for key in data[item].keys():
                if "date" in key.lower():
                    lis = [str(value.date()) if isinstance(value, datetime.datetime) else value for value in
                           data[item][key]]
                    data[item][key] = lis
        return {
            'status': 200,
            'result': data
        }
    except Exception as e:
        return {"status": 500, "error": "Unknown error"}

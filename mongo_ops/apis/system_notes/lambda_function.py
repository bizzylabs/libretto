import json
import os
import time
from datetime import datetime
from io import BytesIO

import boto3
import pandas as pd
from bson.objectid import ObjectId
from datadog_lambda.metric import lambda_metric
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
bucket_name = secret_dict["bulk_action_bucket"]
s3 = boto3.client("s3", region_name="us-east-1")


def getExceldata(data, file_name, keys):
    df = pd.DataFrame(data, columns=keys)
    with BytesIO() as output:
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)
        data = output.getvalue()

    s3 = boto3.resource('s3', region_name="us-east-1")
    s3.Bucket(bucket_name).put_object(Key=file_name, Body=data)
    return {"status": 200, "body": "file uploaded."}


def create_pre_signed_url(bucket, object_name, expiration=7200):
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except Exception as e:
        print(e)
        return None
    return response


def get_rule_set(rule_set_id):
    rule_sets = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
    return rule_sets


def data_saver_and_url_creator(data_not_in_db, data_already_actioned, current_time, bucket, user_name):
    file_name = f"{user_name}_{current_time}.xlsx"
    keys = ["LoanNumber", "Status"]
    try:
        data_list = [{"LoanNumber": data, "Status": "Not Valid"} for data in data_not_in_db]
        if len(data_already_actioned) > 0:
            already_actioned_list = [{"LoanNumber": data, "Status": "Already Actioned"} for data in
                                     data_already_actioned]
            data_list.extend(already_actioned_list)
        response = getExceldata(data_list, file_name, keys)
        if response.get("status") == 200:
            file_link = create_pre_signed_url(bucket, file_name)
        else:
            file_link = ""

        return file_link
    except Exception as e:
        print("inside data saver", e)
        return ""


def data_status_checker(loan_numbers, run_id, rule_set_id, business_rule_id, action, note_type):
    if action == "Delete":
        result_detail = list(db.ResultSetDetail.find(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
             "LoanNumber": {"$in": loan_numbers}, "SystemNote": {"$in": [note_type]}},
            {"SystemNote": 1, "NoteDetail": 1, "LoanNumber": 1}
        ))
    else:
        result_detail = list(db.ResultSetDetail.find(
            {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
             "LoanNumber": {"$in": loan_numbers}},
            {"SystemNote": 1, "NoteDetail": 1, "LoanNumber": 1}
        ))

    if len(result_detail) > 0:
        if action == "Add":
            data_in_db = [item["LoanNumber"] for item in result_detail if item.get("LoanNumber")]
            loan_numbers_not_in_db = get_uncommon_element(loan_numbers, data_in_db)
            already_action_loan_number = [element for element in loan_numbers if element not in loan_numbers_not_in_db]

            return loan_numbers_not_in_db, already_action_loan_number, []
        else:
            valid_loan_number_for_delete = [item["LoanNumber"] for item in result_detail if item.get("LoanNumber")]
            loan_numbers_not_in_db = get_uncommon_element(loan_numbers, valid_loan_number_for_delete)
            return loan_numbers_not_in_db, [], valid_loan_number_for_delete

    else:
        return loan_numbers, [], []


def data_inserter(key, rule_set_id, rule_id, business_rule_id, loan_numbers, notekey, action, run_id, time_stamp,
                  user_name, note_reason, note_detail, rule_set):
    system_notes_list = []
    user_note_list = []
    loan_package_id = db.SchedulerHistory.find_one({"RunId": run_id}).get("LoanPackage")
    for loan_number in loan_numbers:
        note_obj = {
            "Key": key,
            "RulesetId": ObjectId(rule_set_id),
            "RuleId": ObjectId(rule_id),
            "RuleNameId": business_rule_id,
            "LoanNumber": loan_number,
            "LoanPackageId": loan_package_id,
            "NoteType": "SystemNote",
            "Note": {
                "Note": notekey,
                "NoteDetail": None,
                "Action": action,
                "RunId": run_id,
                "createdon": time_stamp,
                "Username": user_name
            }
        }
        if note_reason:
            note_obj["Reason"] = note_reason
        system_notes_list.append(note_obj)
        tags = ['loan_number:' + str(loan_number), 'action:' + action, 'notekey:' + notekey, 'user_name:' + user_name,
                "note_detail:None", 'rule_id:' + str(rule_id),
                'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                'run_id:' + str(run_id), 'note_type:' + key]
        if note_reason:
            tags.append("note_reason:" + note_reason)
        lambda_metric(
            metric_name='bizzy.user.workflow.systemnotes',
            value=1,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=tags
        )
        if note_detail:
            user_note_obj = {
                "Key": key,
                "RulesetId": ObjectId(rule_set_id),
                "RuleId": ObjectId(rule_id),
                "RuleNameId": business_rule_id,
                "LoanNumber": loan_number,
                "LoanPackageId": loan_package_id,
                "NoteType": "UserNote",
                "Note": {
                    "Note": notekey,
                    "NoteDetail": note_detail,
                    "Action": action,
                    "RunId": run_id,
                    "createdon": time_stamp,
                    "Username": user_name
                }
            }
            if key == "Review":
                if note_reason:
                    user_note_obj["Reason"] = note_reason
            user_note_list.append(user_note_obj)
            tags = ['loan_number:' + str(loan_number), 'action:' + action, 'notekey:' + notekey,
                    'user_name:' + user_name, "note_detail:" + str(note_detail),
                    'rule_id:' + str(rule_id), 'rule_set_id:' + str(rule_set_id),
                    'rule_set_name:' + rule_set["RuleSetName"], 'run_id:' + str(run_id), 'note_type:' + key]
            if note_reason:
                tags.append("note_reason:" + note_reason)
            lambda_metric(
                metric_name='bizzy.user.workflow.usernotes',
                value=1,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=tags
            )
    if len(user_note_list) > 0:
        db.Notes.insert(user_note_list)
    if len(system_notes_list) > 0:
        db.Notes.insert(system_notes_list)

    return True


def get_uncommon_element(lis1, lis2):
    return list(set(lis1) - set(lis2))


def lambda_handler(event, context):
    try:
        run_id = event["body"].get("RunId", None)
        rule_set_id = event["body"].get("RulesetId", None)
        rule_id = event["body"].get("RuleId", None)
        loan_numbers = event["body"].get("LoanNumber", [])
        note_type = event["body"].get("SystemNote", None)
        note_detail = event["body"].get("NoteDetail", None)
        action = event["body"].get("Action", None)
        user_name = event["context"].get("username", None)
        note_reason = event["body"].get("NoteReason", None)
        consent = event["body"].get("Consent", False)
        bulk_action = event["body"].get("Bulk", False)
        time_stamp = datetime.now()
        business_rule_id = db.Rule.find_one({"_id": ObjectId(rule_id)}).get("RuleNameId", "")
        cdt = datetime.now()
        current_time = str(cdt.timestamp()).split('.')[0]
        final_loan_numbers_list = []
        # temp_loan_numbers_list = re.findall(r'\d+', loan_number)
        # loan_numbers = list(map(int, temp_loan_numbers_list))
        if type(loan_numbers) != list:
            return {
                'statusCode': 500,
                'body': json.dumps('Invalid Data')
            }

        elif len(loan_numbers) > 1500:
            return {
                'statusCode': 500,
                'body': json.dumps('Data limit exceeded')
            }

        try:
            rule_set = get_rule_set(rule_set_id)

            if action == "Add":
                result_detail = list(db.ResultSetDetail.find(
                    {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                     "LoanNumber": {"$in": loan_numbers}, "SystemNote": {"$nin": [note_type]}},
                    {"SystemNote": 1, "NoteDetail": 1, "LoanNumber": 1}
                ))
            else:
                result_detail = list(db.ResultSetDetail.find(
                    {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                     "LoanNumber": {"$in": loan_numbers}, "SystemNote": {"$in": [note_type]}},
                    {"SystemNote": 1, "NoteDetail": 1, "LoanNumber": 1}
                ))

            count = len(result_detail)
            if count > 0:
                db_loan_numbers_list = [item["LoanNumber"] for item in result_detail if item.get("LoanNumber")]
                loan_numbers_rejected = get_uncommon_element(loan_numbers, db_loan_numbers_list)
                loan_numbers_not_in_db, loans_already_actioned, valid_loan_for_delete = data_status_checker(
                    loan_numbers_rejected, run_id,
                    rule_set_id, business_rule_id, action, note_type)
                print("loan_numbers_not_in_db ----- ", loan_numbers_not_in_db)
                print("loans_already_actioned ------ ", loans_already_actioned)
                final_loan_numbers_list = [element for element in loan_numbers if element not in loan_numbers_rejected]
                final_loan_numbers_list_size = len(final_loan_numbers_list)
                final_loan_numbers_list = list(set(final_loan_numbers_list))
                if len(valid_loan_for_delete) > 0:
                    final_loan_numbers_list.extend(valid_loan_for_delete)
                if not consent and len(loan_numbers_rejected) > 0:
                    file_path = data_saver_and_url_creator(loan_numbers_not_in_db, loans_already_actioned, current_time,
                                                           bucket_name,
                                                           user_name)
                    return {
                        "Failed": len(loan_numbers_rejected),
                        "Pass": final_loan_numbers_list_size,
                        "FilePath": file_path,
                        "Message": None,
                        "Confirmation": True
                    }

            if count == 0:
                loan_numbers_not_in_db, loans_already_actioned, valid_loan_for_delete = data_status_checker(loan_numbers,
                                                                                                            run_id,
                                                                                                            rule_set_id,
                                                                                                            business_rule_id,
                                                                                                            action,
                                                                                                            note_type)
                loan_numbers_rejected = [loan for loan in loan_numbers if loan not in valid_loan_for_delete]

                if len(valid_loan_for_delete) == 0:
                    file_path = data_saver_and_url_creator(loan_numbers_not_in_db, loans_already_actioned, current_time,
                                                           bucket_name,
                                                           user_name)
                    return {
                        "statusCode": 500,
                        "Failed": len(loan_numbers),
                        "Pass": 0,
                        "FilePath": file_path,
                        "Message": None
                    }
                if not consent and len(loan_numbers_rejected) > 0 and len(valid_loan_for_delete) > 0:
                    file_path = data_saver_and_url_creator(loan_numbers_not_in_db, loans_already_actioned, current_time,
                                                           bucket_name,
                                                           user_name)
                    print("im here in this.")
                    return {
                        "Failed": len(loan_numbers_rejected),
                        "Pass": len(valid_loan_for_delete),
                        "FilePath": file_path,
                        "Message": None,
                        "Confirmation": True
                    }
                if len(valid_loan_for_delete) > 0:
                    final_loan_numbers_list = valid_loan_for_delete

            try:
                system_note = set(result_detail.get("SystemNote", []))
            except Exception as e:
                system_note = set()

            if action == "Add":
                if note_type == "Snooze":
                    note_detail["CreatedOn"] = datetime.now()
                    notekey = "Snoozed for {} days".format(note_detail["Days"])
                    resp_msg = "Snoozed for {} days successfully.".format(note_detail["Days"])
                if note_type == "Complete":
                    resp_msg = "Marked as complete successfully."
                    notekey = "Marked as complete"
                if note_type == "Review":
                    resp_msg = "Marked for review successfully."
                    notekey = "Marked for review"
                if note_type == "Ignore":
                    resp_msg = "Marked as Ignore successfully."
                    notekey = "Ignored"
                system_note.add(note_type)

            if action == "Delete":
                if note_type == "Snooze":
                    notekey = "Snooze ended"
                    resp_msg = "Snooze ended successfully."
                if note_type == "Complete":
                    notekey = "Unmarked as complete"
                    resp_msg = "Unmarked as complete successfully."
                if note_type == "Review":
                    notekey = "Review cancelled"
                    resp_msg = "Review cancelled successfully."
                if note_type == "Ignore":
                    notekey = "Restored"
                    resp_msg = "Restored successfully."
                if note_type in ["Snooze", "Ignore"]:
                    note_detail = None
            if bulk_action:
                notekey = notekey + " via bulk action."
                resp_msg = resp_msg.replace(".", "")
                resp_msg = resp_msg + " via bulk action."

            if note_type in ["Snooze", "Ignore"]:
                if action == "Add":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},

                        {"$push": {"SystemNote": note_type},
                         "$set": {
                             "NoteDetail": note_detail
                         }
                         }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['action:add', 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                              'run_id:' + str(run_id), 'note_type:' + str(note_type)]
                    )

                if action == "Delete":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},
                        {"$pull": {"SystemNote": note_type}
                         }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['action:delete', 'rule_set_id:' + str(rule_set_id),
                              'rule_set_name:' + rule_set["RuleSetName"], 'run_id:' + str(run_id),
                              'note_type:' + str(note_type)]
                    )
            else:
                if action == "Add":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},
                        {
                            "$push": {
                                "SystemNote": note_type
                            }
                        }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['action:add', 'rule_set_id:' + str(rule_set_id), 'rule_set_name:' + rule_set["RuleSetName"],
                              'run_id:' + str(run_id), 'note_type:' + str(note_type)]
                    )
                if action == "Delete":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},
                        {
                            "$pull": {
                                "SystemNote": note_type
                            }
                        }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['action:delete', 'rule_set_id:' + str(rule_set_id),
                              'rule_set_name:' + rule_set["RuleSetName"], 'run_id:' + str(run_id),
                              'note_type:' + str(note_type)]
                    )

            if note_type == "Review":
                data_inserter("Review", rule_set_id, rule_id, business_rule_id, final_loan_numbers_list, notekey, action,
                              run_id, time_stamp, user_name, note_reason, note_detail, rule_set)
            elif note_type == "Complete":
                data_inserter("Complete", rule_set_id, rule_id, business_rule_id, final_loan_numbers_list, notekey, action,
                              run_id, time_stamp, user_name, note_reason, note_detail, rule_set)
            elif note_type == "Snooze":
                data_inserter("Snooze", rule_set_id, rule_id, business_rule_id, final_loan_numbers_list, notekey, action,
                              run_id, time_stamp, user_name, note_reason, note_detail, rule_set)
            elif note_type == "Ignore":
                data_inserter("Ignore", rule_set_id, rule_id, business_rule_id, final_loan_numbers_list, notekey, action,
                              run_id, time_stamp, user_name, note_reason, note_detail, rule_set)
            else:
                return {"ErrorMessage": "Kindly provide valid note type"}
            # adding mapping for ignore in result set detail to get results on Home page
            if note_type == "Ignore":
                if action == "Add":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},
                        {
                            "$set": {
                                "Ignore": True
                            }
                        }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['notekey:ignored', 'rule_set_id:' + str(rule_set_id),
                              'rule_set_name:' + rule_set["RuleSetName"], 'run_id:' + str(run_id),
                              'note_type:' + str(note_type)]
                    )
                if action == "Delete":
                    update_result = db.ResultSetDetail.update_many(
                        {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id), "RuleNameId": business_rule_id,
                         "LoanNumber": {"$in": final_loan_numbers_list}},
                        {
                            "$set": {
                                "Ignore": False
                            }
                        }
                    )
                    final_loan_numbers_list_size = update_result.modified_count
                    lambda_metric(
                        metric_name='bizzy.user.workflow.systemnotes',
                        value=len(final_loan_numbers_list),
                        timestamp=int(time.time()),  # optional, must be within last 20 mins
                        tags=['action:delete', 'rule_set_id:' + str(rule_set_id),
                              'rule_set_name:' + rule_set["RuleSetName"], 'run_id:' + str(run_id),
                              'note_type:' + str(note_type)]
                    )

            response_final = {
                "statusCode": 200,
                "Failed": len(loan_numbers_rejected),
                "Pass": final_loan_numbers_list_size,
                "URL": "",
                "Message": None,
                "Confirmation": False
            }
            if response_final["Failed"] == 0:
                response_final["Message"] = "{}".format(resp_msg)
            else:
                response_final["Message"] = None
                file_path = data_saver_and_url_creator(loan_numbers_not_in_db, loans_already_actioned, current_time,
                                                       bucket_name,
                                                       user_name)
                response_final["URL"] = file_path
            return response_final
        except Exception as err:
            print(err)
            return {
                'statusCode': 500,
                'body': 'Something went wrong while performing {} action.'.format(action)
            }
    except Exception as err:
        return {'statusCode': 500, "error": "system notes failed"}
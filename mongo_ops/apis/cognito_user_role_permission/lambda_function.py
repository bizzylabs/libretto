import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
import os
import datetime

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        GroupName = event["body"].get("RoleName", None)
        ReviewResults = event["body"].get("ReviewResults", {})
        RuleLibrary = event["body"].get("RuleLibrary", False)
        Scheduler = event["body"].get("Scheduler", False)
        UserManagement = event["body"].get("UserManagement", False)
        LoanHistory = event["body"].get("LoanHistory", {})
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        if not GroupName:
            return {"Error": "Role Name is required", "statusCode": 500}
        cdt = datetime.datetime.now()
        log_document = {}
        response = cognito.create_group(
            GroupName=GroupName,
            UserPoolId=user_pool_id
        )
        resp = {
            "RoleName": GroupName,
            "ReviewResults": {
                "RuleSetNames": ReviewResults.get("RuleSetNames", []),
                "View": ReviewResults.get("View", True),
                "Export": ReviewResults.get("Export", True),
                "Create/Edit": ReviewResults.get("Create/Edit", True),
                "GrantAccessFalg": ReviewResults.get("GrantAccessFalg", False)
            },
            "RuleLibrary": RuleLibrary,
            "Scheduler": Scheduler,
            "UserManagement": UserManagement,
            "LoanHistory": {
                "View": LoanHistory.get("View", True),
                "Export": LoanHistory.get("Export", True)
            }
        }
        roleobj = db.UserRolePermissions.insert_one(resp)
        logdin_username = ""
        if "username" in event["context"]:
            logdin_username = event["context"]["username"]
        log_document["ip"] = ip
        log_document["browser"] = browser
        log_document["username"] = logdin_username
        log_document["timestamp"] = cdt
        log_document["action"] = "Created"
        log_document["resource"] = GroupName
        log_document["log_source"] = "Roles"
        insert_log = db.BussinessAuditLogs.insert_one(log_document)

        return {
            'statusCode': 200,
            'body': json.dumps('Document updated')
        }
    except Exception as err:
        return {'statusCode': 500, "Error": "delete role failed"}


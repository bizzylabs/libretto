import json
import os
from base64 import b64encode

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def scheduler(event, context):
    try:
        scheduler_status = db.SchedulerConfig.find({"CurrentStatus": {"$nin": ["Completed", "", "Failed"]}})
        if scheduler_status is None or len(list(scheduler_status)) == 0:
            result = {
                "label": "scheduler",
                "value": ""
            }
        else:
            result = {
                "label": "scheduler",
                "value": "scheduler"
            }

        encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
        return {"result": encoded_string}

    except Exception as err:
        return {'statusCode': 500, "error": "schedular status failed"}
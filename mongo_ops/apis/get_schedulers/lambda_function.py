import json
import os
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


# minimum_required_group = 'SUPER-ADMIN'


def get_scheduler(event, context):
    try:
        role = event["context"].get("role", "")
        print("role", role)
        schedulerId = event["body"].get("id")
        database = get_initialized_db()
        schedulers = None
        if schedulerId is None:
            schedulers = list(database.SchedulerConfig.find({}))
        else:
            schedulers = list(database.SchedulerConfig.find({'_id': ObjectId(schedulerId)}))
        result = []
        for scheduler in schedulers:
            scheduler["_id"] = str(scheduler["_id"])
            scheduler["LoanPackageId"] = str(scheduler["LoanPackageId"])
            try:
                scheduler["CreatedOn"] = scheduler["CreatedOn"].isoformat()
                scheduler["UpdatedOn"] = scheduler["UpdatedOn"].isoformat()
                scheduler["RuleSetIds"] = [str(rule) for rule in scheduler.get("RuleSetIds")]
            except:
                pass

            try:
                for ruleset in scheduler["RuleSetId"]:
                    ruleset["value"] = str(ruleset["value"])

                for package in scheduler["LoanPackage"]:
                    package["value"] = str(package["value"])
            except:
                pass

            scheduler["Status"] = ""
            if len(str(scheduler["CurrentStatus"])) > 0:
                scheduler["Status"] = str(scheduler["CurrentStatus"])
            # else:
            #     gmt_time = scheduler["GmtTime"].split(":")
            #     time = datetime.now().replace(hour=int(gmt_time[0]))
            #     time = time.replace(minute=int(gmt_time[1]))
            #     dataa = db.SchedulerHistory.find({"SchedulerConfigId": ObjectId(scheduler["_id"]),
            #                                       "LoanPackage": ObjectId(scheduler["LoanPackageId"])}).sort(
            #         [("RunId", -1)]).limit(1)
            #     data = list(dataa)
            #     if scheduler["Active"] == 1:

            #         if datetime.now() >= time:
            #             scheduler["Status"] = "In Progress"
            #             if data and data[0]["IsActive"] == 1:
            #                 print("hereeeeeee")
            #                 scheduler["Status"] = "Completed"
            #             if data and data[0]["IsActive"] == 2:
            #                 scheduler["Status"] = "Failed"
            #             if not data:
            #                 scheduler["Status"] = ""
            #         if datetime.now() < time:
            #             if data and data[0]["IsActive"] == 1:
            #                 print("hereeeeeee")
            #                 scheduler["Status"] = "Completed"
            #             elif data and data[0]["IsActive"] == 2:
            #                 scheduler["Status"] = "Failed"
            #     else:
            #         if data and data[0]["IsActive"] == 1:
            #             scheduler["Status"] = "Completed"
            #         elif data and data[0]["IsActive"] == 2:
            #             scheduler["Status"] = "Failed"

            result.append(scheduler)

        return {"schedulers": result}

    except Exception as err:
        return {'statusCode': 500, "error": "get schedulers failed"}


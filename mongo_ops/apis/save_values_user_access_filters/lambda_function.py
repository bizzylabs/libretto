import json
import os
import boto3
from bson import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def update_func(user_filters, access_filters, condition):
    if condition == "Filters":
        item = []
    elif condition == "ConditionsFilter":
        item = {
            "Include": []
        }
    elif condition == "OptionsFilter":
        item = [
            {
                "label": "Include",
                "value": "Include"
            }
        ]
    update_flag = False
    for filter_item in user_filters:
        user_filterid = filter_item["_id"]
        original_filter_value = access_filters["Filters"]
        user_filters_values = filter_item[condition]
        # print(original_filter_value,user_filters_values)
        for user_item, user_value in user_filters_values.items():
            new_filter_list = []
            old_filter_list = []
            old_filter_list = list(user_value.keys())
            new_filter_list = original_filter_value[user_item]
            new_uncommon_list = [item for item in new_filter_list if item not in old_filter_list]
            old_uncommon_list = [item for item in old_filter_list if item not in new_filter_list]
            print(old_uncommon_list, new_uncommon_list)
            if old_uncommon_list and new_uncommon_list:
                print("delete old items")
                update_flag = True
                for old_item in old_uncommon_list:
                    if old_item in user_value:
                        del user_value[old_item]
                # insert items
                for new_item in new_uncommon_list:
                    user_value[new_item] = item
            else:
                update_flag = True
                for old_item in old_uncommon_list:
                    if old_item in user_value:
                        del user_value[old_item]
                if len(new_uncommon_list) > 0:
                    for new_item in new_uncommon_list:
                        user_value[new_item] = item

        if update_flag:
            db.UserFilters.update({"_id": user_filterid}, {"$set": {condition: user_filters_values}})
    return True


def lambda_handler(event, context):
    try:
        user_access_filter_data = db.UserAccessFilters.find_one({})["Filters"]
        final_data = dict()
        for key in user_access_filter_data.keys():
            final_data[key] = {}
            loan_package_id = db.LoanPackage.find_one({"Name": key})["_id"]
            collection_name_obj = db.RuleTestingLoanData.find_one({"LoanPackageId": ObjectId(loan_package_id)})
            if collection_name_obj:
                collection_name = collection_name_obj["LoanDataCollectionName"]
            if len(user_access_filter_data[key]) > 0:
                for value in user_access_filter_data[key]:
                    final_data[key][value] = db[collection_name].distinct(value)
        access_filter_value = db.AccessFilterValues.find_one({})
        if access_filter_value:
            db.AccessFilterValues.update({"_id": access_filter_value["_id"]}, {"$set": {"Values": final_data}})
        else:
            db.AccessFilterValues.insert({"Values": final_data})

        user_filters = list(db.UserFilters.find({}))
        access_filters = db.UserAccessFilters.find_one({})

        update_filters = update_func(user_filters, access_filters, "Filters")
        update_condition_filter = update_func(user_filters, access_filters, "ConditionsFilter")
        update_options_filter = update_func(user_filters, access_filters, "OptionsFilter")

        return {"status": 200, "message": "Data saved successfully."}

    except Exception as err:
        return {'statusCode': 500, "Error": "save values user access filters failed"}


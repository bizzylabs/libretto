import json
import os
from collections import OrderedDict
from datetime import datetime
from itertools import islice

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

severity_data = dict()


def latest_available(rule_set_id, run_id, created_date, loan_package):
    try:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S.%f')
    except:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S')
    run_ids = [item["RunId"] for item in db.SchedulerHistory.find(
        {"LoanPackage": ObjectId(loan_package), "RunId": {"$gt": run_id}, "IsActive": 1}) if item]
    result = db.ResultSetSummary.find_one({"$and": [{"RunId": {"$in": run_ids}},
                                                    {"RuleSetId": ObjectId(rule_set_id), "RunId": {"$gt": run_id},
                                                     "CreatedOn": {"$gte": created_on}}]})
    if result:
        val = db.SchedulerHistory.find_one({"RunId": result["RunId"], "LoanPackage": ObjectId(loan_package)},
                                           {"_id": 0, "IsActive": 1})
        if val and val["IsActive"] == 1:
            return True
    return False


pagecount = None


def lambda_handler(event, context):
    try:
        user_name = event["context"]["username"]

        loan_package_id = event["body"].get("loan_package_id")

        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        exception_type = event["body"].get("exception_type", None)
        if exception_type == "open":
            exception_filter = {"$eq": ["$SystemNote", []]}
        else:
            exception_filter = {}

        rule_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                                {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                               {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$ExceptionStatus", True]}, {
                                   "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                           {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                               {"$eq": ["$Ignore", False]}]
        cleared_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                                   {"$ne": ["$LoanNumber", None]},
                                   {"$eq": ["$ExceptionStatus", False]}, {"$eq": ["$Ignore", False]}]
        high_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$Severity", "High"]}, {"$eq": ["$Ignore", False]}]
        low_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$Severity", "Low"]}, {"$eq": ["$Ignore", False]}]
        medium_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                                  {"$eq": ["$Severity", "Medium"]}, {"$eq": ["$Ignore", False]}]
        critical_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]},
                                    {"$ne": ["$LoanNumber", None]},
                                    {"$eq": ["$Severity", "Critical"]}, {"$eq": ["$Ignore", False]}]
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)

        else:
            loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = db.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)

        final_output = OrderedDict()
        exception_overview = dict()
        exception_overview["TotalException"] = 0
        exception_overview["TotalLoan"] = 0
        exception_overview["NewExceptions"] = 0
        exception_overview["ExceptionCleared"] = 0

        rule_set_ids = list(db.RuleSetMaster.aggregate([
            {"$match": {"Users": {"$in": [user_name]}}},
            {"$group": {"_id": "", "ids": {"$push": "$_id"}}},
            {"$project": {"_id": 0}}
        ]))
        if len(rule_set_ids) > 0:
            rule_set_ids = rule_set_ids[0].get("ids")
        else:
            return {"AllResultSets": {"Page-1": {}},
                    "array_of_records": [],
                    "execution_completed": {},
                    "record_count": 0,
                    "run_type_docs": {}}
        scheduler_config_detail = db.SchedulerConfig.find({"LoanPackageId": ObjectId(loan_package_id)})
        run_ids = []
        all_rulesets = []
        for scheduler_config in scheduler_config_detail:
            output = []
            scheduler_config_id = scheduler_config.get("_id")
            # scheduler_history = list(db.SchedulerHistory.find({"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
            #                                                    "SchedulerConfigId": ObjectId(
            #                                                        scheduler_config_id)}).sort(
            #     [("RunId", -1)]).limit(1))

            scheduler_history = list(db.SchedulerHistory.aggregate([{"$match":
                {"IsActive": 1, "LoanPackage": ObjectId(loan_package_id), "SchedulerConfigId": ObjectId(scheduler_config_id)}},
                {"$sort": {"RunId": -1}},
                {"$limit": 1},
                ]))


            if len(scheduler_history) > 0:
                scheduler_history = scheduler_history[0]
                run_id = scheduler_history.get("RunId")
                run_ids.append(run_id)
                pipe = [
                    {"$match": {"$and": [{
                        "RunId": run_id,
                        "RuleSetId": {"$in": rule_set_ids},
                    }]}},
                    {"$group": {
                        "_id": {"rule_set_id": "$RuleSetId",
                                "rule_id": "$RuleId"
                                },
                        "rule_count": {"$sum": {"$cond": [
                            {"$and": rule_count_condition}, 1,
                            0]}},
                        "priority": {"$first": "$Severity"},
                        "new_count": {"$sum": {"$cond": [
                            {"$and": new_count_condition}, 1, 0]}},
                        "cleared_count": {"$sum": {"$cond": [
                            {"$and": cleared_count_condition}, 1,
                            0]}},
                        "High_count": {"$sum": {
                            "$cond": [{"$and": high_count_condition}, 1,
                                      0]}},
                        "Low_count": {"$sum": {
                            "$cond": [{"$and": low_count_condition}, 1,
                                      0]}},
                        "Medium_count": {"$sum": {
                            "$cond": [{"$and": medium_count_condition}, 1,
                                      0]}},
                        "Critical_count": {"$sum": {
                            "$cond": [{"$and": critical_count_condition},
                                      1, 0]}},

                    }},
                    {"$group": {
                        "_id": "$_id.rule_set_id",
                        "Rules": {
                            "$push": {
                                "Rule": "$_id.rule_id",
                                "exceptions": "$rule_count",
                                "new": "$new_count",
                                "severity": "$priority"
                            },
                        },
                        "total_exception": {"$sum": "$rule_count"},
                        "new_exceptions": {"$sum": "$new_count"},
                        "cleared_exceptions": {"$sum": "$cleared_count"},
                        "high_severity": {"$sum": "$High_count"},
                        "low_severity": {"$sum": "$Low_count"},
                        "medium_severity": {"$sum": "$Medium_count"},
                        "critical_severity": {"$sum": "$Critical_count"},
                    }},
                    {"$sort": {"total_exception": -1}}
                ]
                for rule_sets_data in db.ResultSetDetail.aggregate(pipe):
                    # print(rule_sets_data)
                    if rule_sets_data["_id"] not in all_rulesets:
                        rule_set_detail = dict()
                        rule_set_detail["_id"] = str(rule_sets_data.get("_id"))
                        rule_set_master = db.RuleSetMaster.find_one({"_id": ObjectId(rule_sets_data["_id"])})
                        rule_set_detail["RuleSetName"] = rule_set_master.get("RuleSetName")
                        rule_set_detail["RuleSetVersion"] = rule_set_master.get("RuleSetVersion")
                        rule_set_detail["RunDate"] = datetime.fromtimestamp(run_id / 1000).strftime("%d %b, %Y")
                        rule_set_detail["TotalNoOfExceptions"] = rule_sets_data["total_exception"]
                        rule_set_detail["NoOfNewException"] = rule_sets_data["new_exceptions"]
                        rule_set_detail["UpdatedOn"] = ""
                        rule_set_detail["RunId"] = run_id
                        rule_set_detail["RunType"] = ""
                        rule_set_detail["ClearedException"] = rule_sets_data["cleared_exceptions"]
                        rule_set_detail["ExecutionCompleted"] = ""
                        rule_set_detail["ErrorMessage"] = ""

                        exceptions = db.ResultSetSummary.find_one(
                            {"RuleSetId": ObjectId(rule_sets_data["_id"]), "RunId": run_id})
                        if exceptions:
                            rule_set_detail["UpdatedOn"] = exceptions.get("UpdatedOn").isoformat()
                            created_on_temp = exceptions.get("CreatedOn")
                            rule_set_detail["RunType"] = exceptions.get("RunType")
                            rule_set_detail["ExecutionCompleted"] = exceptions.get("ExecutionCompleted")
                            rule_set_detail["ErrorMessage"] = exceptions.get("ErrorMessage")
                        rule_set_detail["StartDate"] = ""
                        rule_set_detail["EndDate"] = ""
                        rule_set_detail["StartTime"] = ""
                        rule_set_detail["TimeZone"] = ""
                        rule_set_detail["Frequency"] = []
                        rule_set_detail["RunsEvery"] = ""
                        schedulers = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_config_id),
                                                                  "$expr": {
                                                                      "$in": [rule_sets_data["_id"], "$RuleSetIds"]}})
                        if schedulers:
                            rule_set_detail["StartDate"] = schedulers.get("StartDate")
                            rule_set_detail["EndDate"] = schedulers.get("EndDate")
                            rule_set_detail["StartTime"] = schedulers.get("StartTime")
                            rule_set_detail["TimeZone"] = schedulers.get("TimeZone")
                            rule_set_detail["Frequency"] = schedulers.get("Frequency")
                            rule_set_detail["RunsEvery"] = schedulers.get("RunsEvery")

                        if not latest_available(rule_set_detail["_id"], rule_set_detail["RunId"], created_on_temp,
                                                loan_package_id):
                            rule_set_detail["Severity"] = {
                                "TotalException": {"Critical": rule_sets_data["critical_severity"],
                                                   "High": rule_sets_data["high_severity"],
                                                   "Medium": rule_sets_data["medium_severity"],
                                                   "Low": rule_sets_data["low_severity"]}}
                            all_rulesets.append(rule_sets_data["_id"])
                            output.append(rule_set_detail)

                out = sorted(output, key=lambda i: i['TotalNoOfExceptions'], reverse=True)
                day = datetime.fromtimestamp(run_id / 1000).strftime("%d %b, %Y")
                if final_output.get(day, None):
                    final_output[day] = sorted(final_output[day] + out,
                                               key=lambda i: (i['TotalNoOfExceptions'], i['RunId']), reverse=True)
                else:
                    final_output[day] = out
        try:
            data_keys = [key for key in final_output.keys() if len(final_output[key]) == 0]
            for item in data_keys:
                del final_output[item]
            final_output = OrderedDict(sorted(final_output.items(), key=lambda x: x[1][0]['RunId'], reverse=True))
            client.close()
            alldata = {"AllResultSets": final_output}
        except Exception as e:
            client.close()
            alldata = {"AllResultSets": final_output}
        # print(alldata)
        array_of_records = []
        execution_completed = {}
        run_type_docs = {}
        docs = {}
        for key, value in dict(alldata["AllResultSets"]).items():
            array_of_records.extend(value)
            inner_doc_list = []
            inner_adhoc_list = []
            for innerdoc in value:
                temp_doc = []
                temp_adhoc_doc = []
                flag_value = innerdoc.get("ExecutionCompleted", True)
                run_type_value = innerdoc.get("RunType", "")
                if not flag_value:
                    temp_doc.append(innerdoc)
                    inner_doc_list.extend(temp_doc)
                if run_type_value == "ad_hoc":
                    temp_adhoc_doc.append(innerdoc)
                    inner_adhoc_list.extend(temp_adhoc_doc)

            if inner_doc_list:
                execution_completed[key] = inner_doc_list
            if inner_adhoc_list:
                run_type_docs[key] = inner_adhoc_list
        alldata["array_of_records"] = array_of_records

        keys = list(alldata["AllResultSets"].keys())

        def chunk(it, size):
            it = iter(it)
            return iter(lambda: tuple(islice(it, size)), ())

        iterondata = dict(alldata["AllResultSets"])
        alldata["AllResultSets"] = {}
        alldata["execution_completed"] = {}
        alldata["run_type_docs"] = {}

        def pagination_content(keys, iterondata, pagecount, all_rule_set=True, adhoc=False, record_count=None):
            allresultset = {}
            # print(pagecount, "initail page count")
            count = 0
            temp = []
            if all_rule_set and not record_count:
                record_count = 0
            if not pagecount:
                pagecount = 0
            complete = False
            extra_content = []
            record_content = {}
            # print()
            for ky in keys:
                more_contents = {}
                data = list(chunk(iterondata[ky], 3))
                # print("#### chunk data #####",data)
                if data:
                    for tpdata in data:
                        count += 1
                        if all_rule_set:
                            record_count += len(tpdata)
                        temp.append(list(tpdata))
                        if count == 3:
                            complete = True
                            extra_content.extend([item for item in [list(item) for item in data] if item not in temp])
                            if extra_content:
                                more_contents[ky] = [item for lis in extra_content for item in lis]
                                # print("********",len(extra_content),"************" ,more_contents)
                            break
                    if count <= 3:
                        allresultset[ky] = temp
                    temp = []
                    if count == 3 and complete:
                        pagecount += 1
                        page = "Page-{}".format(pagecount)
                        # print(page, "++++++++++ page ++++++++++++=")
                        if all_rule_set:
                            alldata["AllResultSets"].update({page: allresultset})
                        elif adhoc:
                            alldata["run_type_docs"].update({page: allresultset})
                        else:
                            alldata["execution_completed"].update({page: allresultset})
                        allresultset = {}
                        count = 0
                        if len(more_contents.keys()) > 0:
                            more_keys = list(more_contents.keys())
                            # print("Calling reucrsive")
                            # print("********",more_contents)
                            iterondata = more_contents
                            keys = more_keys
                            x, y = pagination_content(keys, iterondata, pagecount, all_rule_set=True,
                                                      record_count=record_count)
                            # alldata["AllResultSets"].update({page : allresultset})
                            return x, y
                    elif count <= 3 and ky in keys[-1::]:
                        pagecount += 1
                        page = "Page-{}".format(pagecount)
                        if all_rule_set:
                            alldata["AllResultSets"].update({page: allresultset})
                        elif adhoc:
                            alldata["run_type_docs"].update({page: allresultset})
                        else:
                            alldata["execution_completed"].update({page: allresultset})
            if all_rule_set:
                alldata["record_count"] = record_count
            return alldata, record_content

        alldata, record_content = pagination_content(keys, iterondata, pagecount)
        if execution_completed:
            comp_keys = list(execution_completed.keys())
            alldata, record_content = pagination_content(comp_keys, execution_completed, pagecount, all_rule_set=False)
        if run_type_docs:
            adhoc_keys = list(run_type_docs.keys())
            alldata, record_content = pagination_content(adhoc_keys, run_type_docs, pagecount, all_rule_set=False,
                                                         adhoc=True)
        # if not alldata["AllResultSets"]:
        #     alldata["AllResultSets"] = {"Page-1":{}}
        return alldata
    except Exception as e:
        print(e)
        return {"AllResultSets": {"Page-1": {}},
                "array_of_records": [],
                "execution_completed": {},
                "record_count": 0,
                "run_type_docs": {}}

import datetime
import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
import os

from utils.exceptions import BadRequest, NotFound
from utils.response_decorator import response_mapper, error_mapper
from utils.validator import is_empty_or_valid_url


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" + secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]

db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


@error_mapper
@response_mapper
def lambda_handler(event, context):
    validate_request(event)
    request_method = event["body"].get("method_type", "GET")
    database = get_initialized_db()
    if request_method == 'GET':
        global_hotlink = get_global_hotlink(database)
        if global_hotlink is None:
            raise NotFound("global hotlink is not configured")
        return global_hotlink
    else:
        id = create_or_update_global_hotlink(database, event)
        return {"id": str(id)}


def validate_request(event):
    request_method = event["body"].get("method_type", "GET")  # Default to GET request
    if request_method not in ['GET', 'POST']:
        raise BadRequest("method_type should be either GET or POST")
    if request_method == 'POST':
        global_hotlink = event["body"].get("global_hotlink", None)
        is_active = event["body"].get("is_active", None)

        if is_active not in [1, 0]:
            raise BadRequest("Not a valid is_active status")


def get_global_hotlink(database):
    get_all_data = database.GlobalHotlink.find({})
    documents = list(get_all_data)
    if not documents:
        return None
    else:
        row = documents[0]
        return {
            "id": str(row.get("_id")),
            "is_active": row.get("IsActive", 0),
            "global_hotlink": row.get("GlobalHotlink", None),
            "created_by": row.get("CreatedBy", None),
            "updated_by": row.get("UpdatedBy", None),
            "created_on": row.get("CreatedOn").isoformat(),
            "updated_on": row.get("UpdatedOn").isoformat()
        }


def create_or_update_global_hotlink(database, event):
    try:
        # Check if all fields have right info; if not throw error
        # This method assumes that hotlink & is_active validation is done
        user_name = event["context"].get("username", None)
        global_hotlink = event["body"].get("global_hotlink", None)
        is_active = event["body"].get("is_active", 0)

        document_id = None
        existing_hotlink_info = get_global_hotlink(database)
        if existing_hotlink_info is None:
            result = {
                "IsActive": is_active,
                "GlobalHotlink": global_hotlink,
                "CreatedBy": user_name,
                "UpdatedBy": user_name,
                "CreatedOn": datetime.datetime.now(),
                "UpdatedOn": datetime.datetime.now()
            }
            document_id = database.GlobalHotlink.insert_one(result).inserted_id
        else:
            # Update global link info
            result = {
                "IsActive": is_active,
                "GlobalHotlink": global_hotlink,
                "UpdatedBy": user_name,
                "UpdatedOn": datetime.datetime.now()
            }
            document_id = ObjectId(existing_hotlink_info["id"])
            database.GlobalHotlink.update({"_id": document_id}, {"$set": result})
        return document_id
    except Exception as err:
        return {'statusCode': 500, "error": "global hotlink failed"}

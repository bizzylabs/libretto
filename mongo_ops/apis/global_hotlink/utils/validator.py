import re

_url_regex = re.compile(
    r'^((http)s?://)|(www\.)'  # http:// or https:// or www.
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


def is_empty_or_valid_url(url):
    '''
    :param url: Input URL from API
    :return: Return True if URL is empty or valid
    '''
    if url is None or not url.strip():
        return True
    else:
        # https://stackoverflow.com/a/7995979
        return _url_regex.search(url) is not None

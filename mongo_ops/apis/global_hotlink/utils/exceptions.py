class BadRequest(Exception):
    pass


class AuthorizationError(Exception):
    pass


class NotFound(Exception):
    pass

from utils.exceptions import BadRequest, AuthorizationError, NotFound


def response_mapper(f):
    def inner(*args, **kwargs):
        result = f(*args, **kwargs)
        return {
            "statusCode": 200,
            "result": result
        }

    return inner


def error_mapper(f):
    def inner(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
            return result
        except BadRequest as e:
            return {
                "statusCode": 400,
                "error": str(e)
            }

        except AuthorizationError as e:
            return {
                "statusCode": 403,
                "error": str(e)
            }
        except NotFound as e:
            return {
                "statusCode": 404,
                "error": str(e)
            }

    return inner

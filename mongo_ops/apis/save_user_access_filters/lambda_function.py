import json
import boto3
from pymongo import MongoClient
from bson.objectid import ObjectId
import os
import datetime

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        filters = event["body"].get("Filters", {})
        request_type = event["body"].get("request_type")
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        cdt = datetime.datetime.now()
        log_document = {}
        if request_type == "GET":
            # filter_value = []
            all_users = list(db.UserFilters.find({}, {"UserName": 1, "_id": 0}))
            # Getting all Loan Packages
            user_filter_data = db.UserAccessFilters.find_one({})
            all_filters = user_filter_data["Filters"].keys() if "Filters" in user_filter_data else []
            main_count_obj = {}
            filter_user_map = {}
            for item in all_filters:
                filter_user_map[item] = {}
                main_count_obj[item] = {}
                for filter_key in user_filter_data["Filters"][item]:
                    query_key = f"Filters.{item}.{filter_key}"
                    data = list(db.UserFilters.find({query_key: {"$ne": []}}))
                    count = len(data)
                    user_lis = []
                    if count > 0:
                        user_lis = [username["UserName"] for username in data]
                    filter_user_map[item][filter_key] = user_lis
                    main_count_obj[item][filter_key] = count
            if user_filter_data:
                filter_value = user_filter_data.get("Filters")
                return {"result": filter_value, "filter_count": main_count_obj, "filter_user_map": filter_user_map,
                        "status": 200}
            else:
                loan_packages = db.LoanPackage.find({})
                temp_dict = {}
                for item in loan_packages:
                    temp_dict[item["Name"]] = []
                return {"result": temp_dict, "filter_count": main_count_obj, "filter_user_map": filter_user_map,
                        "status": 200}
        else:
            if len(filters) == 0:
                return {"Error": "Filters are required", "status": 500}
            user_filter_data = db.UserAccessFilters.find_one({})
            if user_filter_data:
                data = db.UserAccessFilters.update({"_id": ObjectId(user_filter_data["_id"])},
                                                   {"$set": {"Filters": filters}})
            else:
                data = db.UserAccessFilters.insert({"Filters": filters})

            logdin_username = event["context"].get("username", "")
            log_document["ip"] = ip
            log_document["browser"] = browser
            log_document["username"] = logdin_username
            log_document["timestamp"] = cdt
            log_document["action"] = "Updated"
            log_document["resource"] = "UserAccessFilters"
            log_document["log_source"] = "Filters"
            log_document["filters"] = filters
            insert_log = db.BussinessAuditLogs.insert_one(log_document)

            return {"Message": "Saved successfully", "status": 200}

    except Exception as err:
        return {'statusCode': 500, "Error": "save user access filters failed"}

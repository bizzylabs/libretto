import json
from pymongo import MongoClient
import boto3
import os
from bson.objectid import ObjectId


def get_database():
    secret = boto3.client("secretsmanager", region_name="us-east-1")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def lambda_handler(event, context):
    try:
        if "body" in event:
            event = event["body"]

        loan_package_ids = [ObjectId(LP_id) for LP_id in event.get("loanPackageIds")]
        database = get_initialized_db()

        packages = list(database.LoanPackageColumns.find({"LoanPackageId": {"$in": loan_package_ids}}))

        intermediate_result = {}

        for package in packages:
            for filetype in package["Columns"]:
                if filetype == 'Task':
                    for subtype in package["Columns"][filetype]:
                        for column in package["Columns"][filetype][subtype]:
                            data_dic = intermediate_result.get(column["Name"], {"Package": [], "CreatedOn": []})
                            data_dic["Package"].append(str(package["LoanPackageId"]))
                            data_dic["CreatedOn"].append(str(column["CreatedOn"]))
                            data_dic["path"] = filetype + "." + subtype
                            intermediate_result[column["Name"]] = data_dic

                else:
                    for column in package["Columns"][filetype]:
                        data_dic = intermediate_result.get(column["Name"], {"Package": [], "CreatedOn": []})
                        data_dic["Package"].append(str(package["LoanPackageId"]))
                        data_dic["CreatedOn"].append(str(column["CreatedOn"]))
                        if filetype == "Loan":
                            data_dic["path"] = ""
                        else:
                            print(filetype)
                            data_dic["path"] = filetype
                        intermediate_result[column["Name"]] = data_dic

        result = []

        for column_name in intermediate_result:
            result.append(
                {"Name": column_name, "value": column_name, "Package": intermediate_result[column_name]["Package"],
                 "path": intermediate_result[column_name]["path"],
                 "CreatedOn": intermediate_result[column_name]["CreatedOn"]})

        custom_functions = ["BeginningOfMonth()", "EndOfLastMonth()", "BeginningOfLastYear()", "BeginningOfLastMonth()",
                            "ForeclosureRecommendationDate", "EndOfMonth()", "YearOfLastMonth()", "Today()", "OccupancyCode",
                            "PrepaymentPenaltyPlanNumber"]

        for function in custom_functions:
            function_dict = {
                "Name": function,
                "value": function,
                "Package": [
                    "5f8691c66b21c6c0aecfe6c6"
                ],
                "path": "",
                "CreatedOn": [
                    "2020-12-07 07:19:12.247000"
                ]
            }
            result.append(function_dict)

        return {
            'statusCode': 200,
            'result': sorted(result, key=lambda i: i['Name'])
        }

    except Exception as err:
        return {'statusCode': 500, "error": "get loan package fields failed"}
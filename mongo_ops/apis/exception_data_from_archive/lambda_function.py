import json
import os
import datetime
import boto3
from pymongo import MongoClient
from bson import ObjectId

secret = boto3.client("secretsmanager", region_name='us-east-1')
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def extract_date_details(date):
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime


def get_run_ids(loan_package_id):
    loan_data = db.SchedulerHistory.find({"LoanPackage": ObjectId(loan_package_id)})
    run_ids = []
    for loan in loan_data:
        run_ids.append(loan['RunId'])
    return run_ids


def lambda_handler(event, context):
    try:
        loan_number = event["body"].get("loan_number")
        loan_type = event["body"].get("loan_type")
        offset = event["body"].get("offset", 0)
        limit = event["body"].get("limit", 10)
        sort_key = event["body"].get("sort_key", "days_to_resolve")
        asc_key = event["body"].get("asc_key", -1)
        start_time = extract_date_details(event["body"].get("gmtFromTime"))
        end_time = extract_date_details(event["body"].get("gmtToTime"))
        loan_package_id = event['body'].get("loan_package_id")

        run_ids = get_run_ids(loan_package_id)
        result_array = []
        final_response = []
        response_data_dict = {}

        result_set_detail_collection = db['ResultSetDetailArchive']

        if sort_key == "Rule Set":
            sort_key = "RuleSetMasterData.RuleSetName"
        if sort_key == "Rule Name":
            sort_key = "RuleData.Name"
        if sort_key == "Version":
            sort_key = "rule_version"
        if sort_key == "Priority":
            sort_key = "rule_priority"
        if sort_key == "Identified On":
            sort_key = "identified_on"
        if sort_key == "Resolved On":
            sort_key = "resolved_on"
        if sort_key == "Days To Resolve":
            sort_key = "days_to_resolve"

        query = [{"$match": {"LoanNumber": loan_number, "RunId": {"$in": run_ids},
                             "InExistence": {"$lte": end_time}, "UpdatedOn": {"$gte": start_time}
                             }
                  },
                 {
                     "$group": {
                         "_id": {"rule_set_id": "$RuleSetId",
                                 "rule_id": "$RuleId",
                                  "exception_status": "$ExceptionStatus",
                                 "in_existence": "$InExistence"
                                 },
                         "rule_priority": {"$first": "$Severity"},
                         "rule_version": {"$first": "$RuleVersion"},
                         "identified_on": {"$first": "$InExistence"},
                         "resolved_on": {"$first": "$UpdatedOn"},
                         "exception_status": {"$first": "$ExceptionStatus"},
                         "run_id": {"$first": "$RunId"}
                     }
                 },
                 {"$addFields": {"identifiedOn": {"$toDate":
                                                      {"$dateToString": {"format": "%Y-%m-%d", "date": "$identified_on"}}}}},
                 {"$addFields": {"resolvedOn": {"$toDate":
                                                    {"$dateToString": {"format": "%Y-%m-%d", "date": "$resolved_on"}}}}},
                 {"$lookup":
                     {
                         "from": "Rule",
                         "let": {"rule_id": "$_id.rule_id"},
                         "pipeline": [
                             {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                         ],
                         "as": "RuleData"
                     }
                 }, {"$unwind": "$RuleData"},
                 {"$lookup":
                     {
                         "from": "RuleSetMaster",
                         "let": {"rule_set_id": "$_id.rule_set_id"},
                         "pipeline": [
                             {"$match": {"$expr": {"$eq": ["$_id", "$$rule_set_id"]}}}
                         ],
                         "as": "RuleSetMasterData"
                     }
                 }, {"$unwind": "$RuleSetMasterData"},
                 {"$project": {"_id": 1, "rule_priority": 1, "rule_version": 1, "run_id": 1,
                               "identified_on": 1, "resolved_on": 1, "exception_status": 1,
                               "RuleSetMasterData": 1, "RuleData": 1,
                               "identifiedOn": 1, "resolvedOn": 1,
                               "days_to_resolve": {"$ceil":
                                                       {"$divide": [{"$subtract": ["$resolvedOn", "$identifiedOn"]},
                                                                    86400000]}}}},
                 {"$sort": {sort_key: asc_key}},
                 {"$skip": offset},
                 {"$limit": limit}
                 ]

        if loan_type == "audit":
            query[0]['$match'].update({"Severity": "Critical"})

        response_data = result_set_detail_collection.aggregate(query)
        for record in response_data:
            key = str(record['_id']['rule_set_id']) + str(record['_id']['rule_id']) + record['_id']['in_existence'].strftime("%m/%d/%Y %H:%M")
            if key not in response_data_dict:
                response_data_dict[key] = [record]
            else:
                response_data_dict[key].append(record)

        for key, value in response_data_dict.items():
            if len(value) == 1:
                if value[0]['exception_status']:
                    final_response.append(value[0])
            if len(value) == 2:
                for record in value:
                    if not record['exception_status']:
                        final_response.append(record)

        for x in final_response:
            rule_id = x['RuleData']['_id']
            ruleset_id = x['RuleSetMasterData']['_id']
            run_id = x['run_id']
            note_count_data = db.Notes.aggregate([{"$match": {"NoteType": "UserNote", "LoanNumber": loan_number,
                                                              "RuleId": ObjectId(rule_id), "Note.RunId": run_id,
                                                              "RulesetId": ObjectId(ruleset_id)}},
                                                  {"$group": {"_id": "LoanNumber", "count": {"$sum": 1}}}])

            notes_count = 0
            for notes in note_count_data:
                notes_count = notes['count']
            result_dict = {"RuleId": str(rule_id),
                           "RulesetId": str(ruleset_id),
                           "RunId": run_id,
                           "Rule Set": x['RuleSetMasterData']['RuleSetName'],
                           "Rule Name": x['RuleData']['Name'],
                           "Version": x['rule_version'],
                           "Priority": x['rule_priority'],
                           "Identified On": x['identified_on'].strftime("%m/%d/%Y"),
                           "Resolved On": x['resolved_on'].strftime("%m/%d/%Y"),
                           "Days To Resolve": x['days_to_resolve'] + 1,
                           "Notes Icon": 1 if notes_count > 0 else 0
                           }
            if x['exception_status']:
                result_dict['Resolved On'] = "-"
                result_dict['Days To Resolve'] = "-"

            result_array.append(result_dict)

        if result_array:
            return result_array
        else:
            return {"message": "No data found."}

    except Exception as err:
        return {'statusCode': 500, "error": "exception data from archive failed"}
import csv
import datetime
import io
import json
import os

import boto3
from pymongo import MongoClient

lambda_invoke = boto3.client("lambda", region_name="us-east-1")
client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]
BUCKET_NAME = mongo_dict["export_bucket"]


def get_csv_data(data):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post)
    if len(data) > 0:
        keys = data[0].keys()
        csv_out.writerow(keys)
        for row in data:
            csv_out.writerow(row.values())
        print("Done")
        return data_to_post.getvalue()
    else:
        csv_out.writerow("")
        return data_to_post.getvalue()


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            s3 = boto3.client("s3")
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data)
            s3 = boto3.resource('s3')
            object_acl = s3.ObjectAcl(bucket_name, file_name)
            response = object_acl.put(ACL='public-read')
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def extract_date_details(date):
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime


def lambda_handler(event, context):
    try:
        null = None
        false = False
        true = True

        gmt_from_date = event["body"].get("gmtFromTime", None)
        gmt_to_date = event["body"].get("gmtToTime", None)
        usernames = event["body"].get("usernames", [])
        file_name = event["body"].get("file_name", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        cdt = datetime.datetime.now()
        log_document = {}

        from_date_format = extract_date_details(gmt_from_date)
        to_date_format = extract_date_details(gmt_to_date)

        # Get list of all cognito users for their first name and last name
        cognito_active_user_list = cognito.list_users(
            UserPoolId=user_pool_id,
            AttributesToGet=[
                'email',
                'custom:FirstName',
                'custom:LastName'
            ]
        )
        get_users_list = []
        # If user list is empty, get all users from cognito with first name and last name
        if len(usernames) == 0:
            users_first_last_names = {}
            for user in cognito_active_user_list["Users"]:
                get_users_list.append(user["Username"])
                users_first_last_names[user["Username"]] = {}
                users_first_last_names[user["Username"]][user["Attributes"][0]["Name"]] = user["Attributes"][0]["Value"]
                users_first_last_names[user["Username"]][user["Attributes"][1]["Name"]] = user["Attributes"][1]["Value"]
        # If user list has users, compare and get their first name and last name from cognito userlist
        elif len(usernames) > 0:

            users_first_last_names = {}
            get_users_list = usernames

            for user in usernames:
                for cognito_user in cognito_active_user_list["Users"]:
                    if cognito_user["Username"] == user:
                        users_first_last_names[cognito_user["Username"]] = {}
                        users_first_last_names[cognito_user["Username"]][cognito_user["Attributes"][0]["Name"]] = \
                            cognito_user["Attributes"][0]["Value"]
                        users_first_last_names[cognito_user["Username"]][cognito_user["Attributes"][1]["Name"]] = \
                            cognito_user["Attributes"][1]["Value"]
                    else:
                        # deleted users or users not present in cognito will not have firstname or lastname. Assiging placeholder for future validation
                        users_first_last_names[user] = {}
                        users_first_last_names[user]["custom:LastName"] = "Lastname"
                        users_first_last_names[user]["custom:FirstName"] = "Firstname"

        if len(usernames) > 0:
            pipe = [{"$match": {"$expr": {
                "$and": [{"$in": ["$Username", usernames]}, {"$gte": ["$CreatedOn", from_date_format]},
                         {"$lte": ["$CreatedOn", to_date_format]}]}}}]
        else:
            pipe = [{"$match": {"$expr": {
                "$and": [{"$gte": ["$CreatedOn", from_date_format]}, {"$lte": ["$CreatedOn", to_date_format]}]}}}]
        logs = db.BussinessAuditLogs.aggregate(pipe)

        user_export_logs = []

        for log in logs:
            user_details = {}
            print(log)
            if log["action"] == "Export":
                key = log.keys()
                if "Username" in key:
                    user_details["Username"] = log["Username"]
                else:
                    user_details["Username"] = ""
                username = log["Username"]
                if users_first_last_names[username]["custom:FirstName"] and users_first_last_names[username][
                    "custom:FirstName"] != "Firstname":
                    first_name = users_first_last_names[username]["custom:FirstName"]
                else:
                    first_name = ""
                if users_first_last_names[username]["custom:LastName"] and users_first_last_names[username][
                    "custom:LastName"] != "Lastname":
                    last_name = users_first_last_names[username]["custom:LastName"]
                else:
                    last_name = ""
                user_details["FullName"] = first_name + " " + last_name
                if "RuleSetName" in key:
                    user_details["RuleSetName"] = log["RuleSetName"]
                else:
                    user_details["RuleSetName"] = ""
                if "RuleName" in key:
                    user_details["RuleName"] = log["RuleName"]
                else:
                    user_details["RuleName"] = ""
                if "log_from" in key:
                    user_details["log_from"] = log["log_from"]
                else:
                    user_details["log_from"] = ""
                if "file_name" in key:
                    user_details["file_name"] = log["file_name"]
                else:
                    user_details["file_name"] = ""
                if "CreatedOn" in key:
                    user_details["timestamp"] = log["CreatedOn"]
                else:
                    user_details["timestamp"] = ""
                if "Ip" in key:
                    user_details["ip"] = log["Ip"]
                else:
                    user_details["ip"] = ""
                if "browser" in key:
                    user_details["browser"] = log["browser"]
                else:
                    user_details["browser"] = ""
            user_export_logs.append(user_details)

        csv_data = get_csv_data(user_export_logs)
        response = save_data(BUCKET_NAME, csv_data, file_name)
        try:
            logdin_username = event["context"]["username"]
        except Exception as e:
            logdin_username = ""

        log_document["ip"] = ip
        log_document["browser"] = browser
        log_document["username"] = logdin_username
        log_document["timestamp"] = cdt
        log_document["file name"] = file_name
        log_document["action"] = "Reports"
        log_document["log_source"] = "Logs_report"
        db.BussinessAuditLogs.insert_one(log_document)
        return True

    except Exception as e:
        print({'statusCode': 500, "Error": "generate usage export logs failed"})

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

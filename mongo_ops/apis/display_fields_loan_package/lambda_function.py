import json
import os

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        temp_dict = {}
        loan_packages = db.LoanPackage.find({})
        for item in loan_packages:
            val = [item["Name"] for data in db.LoanPackageColumns.find({"LoanPackageId": item["_id"]}) for item in
                   data["Columns"]["Loan"] if item.get("Count", 0) <= 300]
            val.sort()
            temp_dict[item["Name"]] = val
        return {
            "result": temp_dict
        }
    except Exception as err:
        return {'statusCode': 500, "Error": "display fields loan package failed"}


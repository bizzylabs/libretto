import json
import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
from collections import OrderedDict
import os
import re


def get_database():
    secret = boto3.client("secretsmanager", region_name="us-east-1")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


null = None
false = False
true = True

db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def get_search_key_ruleset_rule_count(data_filter, search_word):
    rule_id_list = db.RuleSetMaster.find({})
    rule_ids = []
    output = []
    existing_rule_ids = []
    for outer_item in rule_id_list:
        ruleset_data = OrderedDict()
        rule_ids = [rule_id["RuleId"] for rule_id in outer_item["RuleIds"] if rule_id.get("RuleId", None)]
        if outer_item["RuleSetName"] == "Draft Rules":
            rule_ids = [mapping["ChildId"] for mapping in outer_item.get("RuleIds")]

        rule_query = [

            {"$match": {"_id": {"$in": rule_ids}}},
            {
                "$lookup":
                    {
                        "from": "RuleType",
                        "localField": "RuleType",
                        "foreignField": "_id",
                        "as": "RuleType"
                    }
            },
            {
                "$lookup":
                    {
                        "from": "RuleSubType",
                        "localField": "RuleSubType",
                        "foreignField": "_id",
                        "as": "RuleSubType"
                    }
            },
            {
                "$lookup":
                    {
                        "from": "ComplianceType",
                        "localField": "ComplianceType",
                        "foreignField": "_id",
                        "as": "ComplianceType"
                    }
            },
            {"$match": {"$and": [data_filter, {"$or": get_search_fields(search_word)}]}}
        ]
        rules = list(db.Rule.aggregate(rule_query))
        count = len(rules)
        if count != 0:
            ruleset_data["_id"] = str(outer_item["_id"])
            ruleset_data["RuleSetName"] = outer_item["RuleSetName"]
            ruleset_data["rulecount"] = count
            ruleset_data["ShowStatusFlag"] = 1
            ruleset_data["RuleSetVersion"] = outer_item.get("RuleSetVersion")
            ruleset_data["ChildRuleSetIds"] = [str(rule) for rule in outer_item.get("ChildRuleSetIds")]
            ruleset_data["RuleIds"] = [
                {"RuleId": str(rule["_id"]), "Version": rule["Version"], "IsActive": rule["IsActive"]} for rule in
                rules]
            output.append(ruleset_data)

    output = sorted(output, key=lambda x: x['RuleSetName'])
    final_data = sorted(output, key=lambda x: x['RuleSetName'] != 'Draft Rules', reverse=True)
    return final_data


def get_rule_set_details(rule_set_ids):
    rule_set_details = []
    for rule_set_id in rule_set_ids:
        rule_set = db.RuleSetMaster.find_one({"_id": rule_set_id})
        if rule_set is not None:
            rule_set_details.append({"_id": str(rule_set_id), "name": rule_set["RuleSetName"]})
    return rule_set_details


def get_filter_template(expfilter):
    if expfilter == "ZeroExceptions":
        return {"Exceptions.Exception": {"$eq": 0}}
    elif expfilter == "WithExceptions":
        return {"Exceptions.Exception": {"$gt": 0}}
    elif expfilter == "NewExceptions":
        return {"Exceptions.NewException": {"$gt": 0}}
    else:
        return {}


def get_search_fields(search):
    """ added date search functionality.
    :str search: search word coming from ui search field.
    :return: query object
    """
    fields = ["Name", 'RuleSubType.Name', "RuleType.Name", "ComplianceType.Name", "Description", "Severity", "StartDate"
              , "EndDate"]
    criteria = []
    # special character search
    final_str = search
    try:
        string_check = re.compile('[@_!#$%^&<>+?/\|}{~:]')
        s = string_check.search(search)
        if s:
            sc = s.group()
            final_str = search.replace(sc, "\\{}".format(sc))
    except:
        final_str = search
    print(final_str)
    for field in fields:
        if 'date' in field.lower():
            m = search.split("/")
            # For complete date format.
            # {'$expr': {'$eq': [{'$dateToString': {'format': '%d/%m/%Y', 'date': '$EndDate'}}, '20/10/2024']}}
            if len(m) == 3:
                dt_format = "%m/%d/%Y"
                criteria.append({"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{dt_format}", "date": f"${field}"}}, f"{search}"]}})
            # For month, date and year only if one number is given. eg: 11
            elif len(m) == 1:
                if len(m[0]) <= 2:
                    month_format = "%m"
                    date_format = "%d"
                    criteria.append({"$or": [{"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{month_format}", "date": f"${field}"}}, f"{search}"]}},
                        {"$expr": {"$eq":
                            [{"$dateToString": {"format": f"{date_format}", "date": f"${field}"}}, f"{search}"]}}
                    ]})
                if len(m[0]) == 4:
                    date_format = "%Y"
                    criteria.append({"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{date_format}", "date": f"${field}"}}, f"{search}"]}})
            # For month with year or month with date if 2 numbers are given. eg: 11/06 or 11/2021
            elif len(m) == 2:
                year_flag = False
                for i in m:
                    if len(i) == 4:
                        year_flag = True
                        break
                if year_flag:
                    # date with year or month with year
                    if int(m[0]) <= 12:
                        date_format = "%m/%Y"
                    else:
                        print("coming here")
                        date_format = "%d/%Y"
                else:
                    date_format = "%m/%d"
                criteria.append({"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{date_format}", "date": f"${field}"}}, f"{search}"]}})
            else:
                return {"searchMsg": "Provide either full date along with month and year or only month or only year."}
            print(criteria)

        else:
            criteria.append({field: {"$regex": final_str, "$options": 'i'}})
    return criteria


def date_to_str(data):
    """datetime data: covert data into string format. (%d/%m/%Y)"""
    record = data.strftime('%m/%d/%Y') if isinstance(data, datetime.datetime) else ''
    return record


def get_rule_data(event, context):
    output = []
    final_output = []
    rule_detail = OrderedDict()
    try:
        rule_set_id = event["body"].get("ruleSetId", None)
        rule_id = event["body"].get("ruleId", None)
        search_word = event["body"].get("searchWord", "").strip()
        data_filter = get_filter_template(event.get("filter", "all"))
        offset = event["body"].get("offset", 0)
        limit = event["body"].get("limit", float("inf"))
        sort_key = event["body"].get("sortby", "RuleType")
        asc_key = -1 if event["body"].get("asc", "N") == "N" else 1
        database = get_initialized_db()
        client = db.ApplicationDetails.find_one({})['Client']

        if sort_key == "Exception":
            sort_key = "Exceptions.Exception"
        elif sort_key == "NewException":
            sort_key = "Exceptions.NewException"
        elif sort_key == "RuleType":
            sort_key = "RuleType.Name"
        elif sort_key == "RuleSubType":
            sort_key = "RuleSubType.Name"
        elif sort_key == "ComplianceType":
            sort_key = "ComplianceType.Name"
        elif sort_key == "Severity":
            sort_key = "Priority"
        elif sort_key == "Name":
            sort_key = "Slug"

        rule_id_dict = list(database.RuleSetMaster.aggregate(
            [{"$match": {"_id": ObjectId(rule_set_id)}},
             {"$project": {"ruleids": "$RuleIds.RuleId", "rulesDic": "$RuleIds", "_id": 0, "RuleSetName": 1}}]))
        if rule_id_dict[0].get("RuleSetName") != "Draft Rules":
            rule_status = {}
            for rule in rule_id_dict[0].get("rulesDic"):
                rule_status[rule["RuleId"]] = rule

        if rule_id is not None:
            data = list(database.Rule.find({"_id": ObjectId(rule_id)}))
            for each_row in data:
                rule_detail = dict()
                rule_detail["_id"] = str(each_row["_id"])
                rule_detail['StartDate'] = date_to_str(each_row.get('StartDate', ""))
                rule_detail['EndDate'] = date_to_str(each_row.get('EndDate', ""))

                if rule_id_dict[0].get("RuleSetName") != "Draft Rules":
                    rule_detail["IsActive"] = rule_status[each_row["_id"]]["IsActive"]
                    rule_detail["Version"] = each_row["Version"]
                else:
                    rule_detail["IsActive"] = each_row["IsActive"]
                    rule_detail["Version"] = each_row["Version"]
                if "isUiEditable" in each_row:
                    rule_detail["isUiEditable"] = each_row["isUiEditable"]
                else:
                    rule_detail["isUiEditable"] = True
                rule_detail["Name"] = each_row["Name"]
                rule_detail["Description"] = each_row["Description"]
                rule_detail["RuleType"] = str(each_row["RuleType"])
                rule_detail["RuleSubType"] = str(each_row["RuleSubType"])
                rule_detail["ComplianceType"] = str(each_row["ComplianceType"])
                rule_detail["CoreLibFlag"] = each_row["CoreLibFlag"]
                rule_detail["IsEditable"] = each_row["IsEditable"]
                rule_detail["RuleNameId"] = each_row["RuleNameId"]
                rule_detail["CreatedOn"] = each_row["CreatedOn"].isoformat()
                rule_detail["UpdatedOn"] = each_row["UpdatedOn"].isoformat()
                rule_detail["Departments"] = [str(dept) for dept in each_row["Departments"]]
                rule_detail["Criteria"] = eval(each_row["Criteria"])
                rule_detail["DisplayFields"] = eval(each_row["DisplayFields"])
                rule_detail["Departments_detail"] = get_rule_set_details(each_row["Departments"])
                if rule_detail["CoreLibFlag"] == 1:
                    rule_detail["BizzyDepartment"] = get_rule_set_details(each_row["BizzyDepartment"])
                rule_detail["LoanPackage"] = each_row.get("LoanPackage", [{'label': 'No Label', 'value': ""}])
                rule_detail["Severity"] = each_row.get("Severity", "")
                rule_detail["Source"] = each_row.get("Source", "")
                rule_detail["CreatedBy"] = each_row.get("CreatedBy", "")
                rule_detail["UpdatedBy"] = each_row.get("UpdatedBy", "")
                rule_detail["HotLink"] = each_row.get("HotLink", "")
                rule_detail["ExternalSource"] = each_row.get("ExternalSource", False)

                if "ParentId" in each_row:
                    rule_detail["ParentId"] = str(each_row["ParentId"])
                if "JsonCriteria" not in each_row:
                    rule_detail["JsonCriteria"] = {}
                else:
                    rule_detail["JsonCriteria"] = each_row["JsonCriteria"]
                output.append(rule_detail)

            return {"result": output}

        if rule_set_id is not None:
            rule_ids = rule_id_dict[0].get("ruleids")
            rule_set_name = rule_id_dict[0].get("RuleSetName")
            search_query = {}
            if search_word and search_word != "":
                search_query = {"$or": get_search_fields(search_word)}

            if rule_set_name == "Draft Rules":
                rule_ids = [mapping["ChildId"] for mapping in rule_id_dict[0].get("rulesDic")]
                draft_rule_query = [
                    {"$match": {"_id": {"$in": rule_ids}}},
                    {
                        "$lookup":
                            {
                                "from": "RuleType",
                                "localField": "RuleType",
                                "foreignField": "_id",
                                "as": "RuleType"
                            }
                    },
                    {
                        "$lookup":
                            {
                                "from": "RuleSubType",
                                "localField": "RuleSubType",
                                "foreignField": "_id",
                                "as": "RuleSubType"
                            }
                    },
                    {
                        "$lookup":
                            {
                                "from": "ComplianceType",
                                "localField": "ComplianceType",
                                "foreignField": "_id",
                                "as": "ComplianceType"
                            }
                    },
                    {"$match": {"$and": [data_filter, search_query]}},
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}

                ]
                for rule in database.Rule.aggregate(draft_rule_query):
                    rule_detail = dict()
                    rule_detail["HotLink"] = rule.get("HotLink", "")
                    rule_detail["_id"] = str(rule["_id"])
                    rule_detail['StartDate'] = date_to_str(rule.get('StartDate', ""))
                    rule_detail['EndDate'] = date_to_str(rule.get('EndDate', ""))
                    rule_detail["AuthorName"] = rule["AuthorName"]
                    rule_detail["Name"] = rule["Name"]
                    rule_detail["Description"] = rule["Description"]
                    rule_detail["RuleSubType"] = ""
                    rule_detail["RuleType"] = ""
                    rule_detail["ComplianceType"] = ""
                    if len(rule["RuleType"]) > 0:
                        rule_detail["RuleType"] = rule["RuleType"][0]["Name"]
                    if len(rule["RuleSubType"]) > 0:
                        rule_detail["RuleSubType"] = rule["RuleSubType"][0]["Name"]
                    if len(rule["ComplianceType"]) > 0:
                        rule_detail["ComplianceType"] = rule["ComplianceType"][0]["Name"]
                    rule_detail["Criteria"] = eval(rule["Criteria"])
                    rule_detail["DisplayFields"] = eval(rule["DisplayFields"])
                    rule_detail["MaskingFieldsCriteria"] = rule["MaskingFieldsCriteria"]
                    rule_detail["IsActive"] = rule["IsActive"]
                    rule_detail["CoreLibFlag"] = rule["CoreLibFlag"]
                    rule_detail["IsEditable"] = rule["IsEditable"]
                    if "isUiEditable" in rule:
                        rule_detail["isUiEditable"] = rule["isUiEditable"]
                    rule_detail["IsPublished"] = rule["IsPublished"]
                    rule_detail["RuleNameId"] = rule["RuleNameId"]
                    rule_detail["CreatedOn"] = rule["CreatedOn"].isoformat()
                    rule_detail["UpdatedOn"] = rule["UpdatedOn"].isoformat()
                    rule_detail["Version"] = rule["Version"]
                    rule_detail["ReadableCriteria"] = rule["ReadableCriteria"]
                    rule_detail["Departments"] = [str(dept) for dept in rule["Departments"]]
                    rule_detail["Departments_detail"] = get_rule_set_details(rule["Departments"])
                    if rule_detail["CoreLibFlag"] == 1:
                        rule_detail["BizzyDepartment"] = get_rule_set_details(
                            rule.get("BizzyDepartment", rule["Departments"]))
                    rule_detail["LoanPackage"] = rule.get("LoanPackage", [{'label': 'No Label', 'value': ""}])
                    rule_detail["JsonCriteria"] = rule.get("JsonCriteria", {})
                    rule_detail["Severity"] = rule.get("Severity", "")
                    rule_detail["Source"] = rule.get("Source", "")
                    rule_detail["CreatedBy"] = rule.get("CreatedBy", rule["AuthorName"])
                    rule_detail["UpdatedBy"] = rule.get("UpdatedBy", "")
                    rule_detail["ExternalSource"] = rule.get("ExternalSource", False)
                    if rule['CoreLibFlag'] == 0:
                        rule_detail["ClientRule"] = client + ' ' + 'Rule'
                    if "ParentId" in rule:
                        rule_detail["ParentId"] = str(rule["ParentId"])

                    output.append(rule_detail)
                res = {"result": output, "ZeroExceptionsFlag": 0}
                if search_word and search_word != "":
                    res["searched_rule_sets"] = get_search_key_ruleset_rule_count(data_filter, search_word)
                return res

            rule_query = [

                {"$match": {"_id": {"$in": rule_ids}}},
                {
                    "$lookup":
                        {
                            "from": "RuleType",
                            "localField": "RuleType",
                            "foreignField": "_id",
                            "as": "RuleType"
                        }
                },
                {
                    "$lookup":
                        {
                            "from": "RuleSubType",
                            "localField": "RuleSubType",
                            "foreignField": "_id",
                            "as": "RuleSubType"
                        }
                },
                {
                    "$lookup":
                        {
                            "from": "ComplianceType",
                            "localField": "ComplianceType",
                            "foreignField": "_id",
                            "as": "ComplianceType"
                        }
                },
                {"$match": {"$and": [data_filter, search_query]}},
                {"$sort": {sort_key: asc_key}},
                {"$skip": offset},
                {"$limit": limit}
            ]
            for rule in database.Rule.aggregate(rule_query):
                rule_detail = OrderedDict()
                rule_detail["HotLink"] = rule.get("HotLink", "")
                rule_detail["_id"] = str(rule["_id"])
                rule_detail['StartDate'] = date_to_str(rule.get('StartDate', ""))
                rule_detail['EndDate'] = date_to_str(rule.get('EndDate', ""))
                rule_detail["AuthorName"] = rule["AuthorName"]
                rule_detail["Name"] = rule["Name"]
                if "isUiEditable" in rule:
                    rule_detail["isUiEditable"] = rule["isUiEditable"]
                rule_detail["Description"] = rule["Description"]
                rule_detail["RuleSubType"] = ""
                rule_detail["RuleType"] = ""
                rule_detail["ComplianceType"] = ""
                if len(rule["RuleType"]) > 0:
                    rule_detail["RuleType"] = rule["RuleType"][0]["Name"]
                if len(rule["RuleSubType"]) > 0:
                    rule_detail["RuleSubType"] = rule["RuleSubType"][0]["Name"]
                if len(rule["ComplianceType"]) > 0:
                    rule_detail["ComplianceType"] = rule["ComplianceType"][0]["Name"]
                rule_detail["Criteria"] = eval(rule["Criteria"])
                rule_detail["DisplayFields"] = eval(rule["DisplayFields"])
                rule_detail["MaskingFieldsCriteria"] = rule["MaskingFieldsCriteria"]
                try:
                    rule_detail["IsActive"] = rule_status[rule["_id"]]["IsActive"]
                except Exception as e:
                    rule_detail["IsActive"] = rule["IsActive"]
                rule_detail["Version"] = rule["Version"]
                rule_detail["CoreLibFlag"] = rule["CoreLibFlag"]
                rule_detail["IsEditable"] = rule["IsEditable"]
                rule_detail["RuleNameId"] = rule["RuleNameId"]
                rule_detail["IsPublished"] = rule["IsPublished"]
                rule_detail["CreatedOn"] = rule["CreatedOn"].isoformat()
                rule_detail["UpdatedOn"] = rule["UpdatedOn"].isoformat()
                rule_detail["ReadableCriteria"] = rule["ReadableCriteria"]
                rule_detail["Departments"] = [str(dept) for dept in rule["Departments"]]
                rule_detail["Departments_detail"] = get_rule_set_details(rule["Departments"])
                if rule_detail["CoreLibFlag"] == 1:
                    rule_detail["BizzyDepartment"] = get_rule_set_details(rule["BizzyDepartment"])
                rule_detail["LoanPackage"] = rule.get("LoanPackage", [{'label': 'No Label', 'value': ""}])
                rule_detail["JsonCriteria"] = rule.get("JsonCriteria", {})
                rule_detail["Severity"] = rule.get("Severity", "")
                rule_detail["Source"] = rule.get("Source", "")
                rule_detail["CreatedBy"] = rule.get("CreatedBy", rule["AuthorName"])
                rule_detail["UpdatedBy"] = rule.get("UpdatedBy", "")
                rule_detail["ExternalSource"] = rule.get("ExternalSource", False)

                if rule['CoreLibFlag'] == 0:
                    rule_detail["ClientRule"] = client + ' Rule'
                if "ParentId" in rule:
                    rule_detail["ParentId"] = str(rule["ParentId"])
                rule_detail["Exception"] = 0
                rule_detail["NewException"] = 0

                output.append(rule_detail)
            res = {"result": output, "ZeroExceptionsFlag": 0}
            if search_word and search_word != "":
                res["searched_rule_sets"] = get_search_key_ruleset_rule_count(data_filter, search_word)
            return res
    except Exception as err:
        return {'statusCode': 500, "error": "get rules data failed"}
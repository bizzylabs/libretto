import json
import os

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def get_rule_sub_type(event, context):
    try:
        http_method = event["body"]["method"]
        data = event["body"]
        rule_type = data.get("rule_type", None)
        compliance_type = data.get("compliance_type", None)
        rule_sub_type_name = data.get("sub_type_name", None)
        client_name = db.ApplicationDetails.find_one({}).get("Client")
        if http_method == "GET":
            if not rule_type:
                return {
                    "statusCode": 500,
                    "message": "Invalid Parameter"
                }
            if not compliance_type:
                compliance_type_data = db.ComplianceType.find_one({"Name": ""})
                compliance_type = compliance_type_data["_id"]
            rule_sub_type = db.RuleSubType.find(
                {"ComplianceType": {"$in": [ObjectId(compliance_type)]}, "RuleType": {"$in": [ObjectId(rule_type)]}}).sort(
                [("Name", 1)])
            output = []
            for data in rule_sub_type:
                rule_sub_type_data = dict()
                rule_sub_type_data["_id"] = str(data["_id"])
                rule_sub_type_data["Name"] = data["Name"]
                output.append(rule_sub_type_data)
            res = {
                "result": output,
                "compliance": False
            }
            rule_type_data = db.RuleType.find_one({"_id": ObjectId(rule_type)})
            if client_name.lower() != "bizzy" and rule_type_data["Name"].lower() == "compliance":
                res["compliance"] = True

            return res

        elif http_method == "POST":
            try:
                if not rule_sub_type_name or not rule_type:
                    return {
                        "statusCode": 500,
                        "message": "Data missing."
                    }
                rule_type_data = db.RuleType.find_one({"_id": ObjectId(rule_type)})
                if not rule_type_data:
                    return {
                        "statusCode": 500,
                        "message": "Invalid rule type"
                    }

                if not compliance_type:
                    compliance_type_data = db.ComplianceType.find_one({"Name": ""})
                    compliance_type = compliance_type_data["_id"]

                compliance_type_data = db.ComplianceType.find_one({"_id": ObjectId(compliance_type)})
                if not compliance_type_data:
                    return {
                        "statusCode": 500,
                        "message": "Invalid compliance type"
                    }
                sub_type_check = db.RuleSubType.find_one({"Name": rule_sub_type_name})
                if sub_type_check:
                    comp_type = sub_type_check.get("ComplianceType", [])
                    comp_type.append(ObjectId(compliance_type_data["_id"]))
                    comp_type = list(set(comp_type))
                    rule_typ = sub_type_check.get("RuleType", [])
                    rule_typ.append(ObjectId(rule_type))
                    rule_typ = list(set(rule_typ))

                    db.RuleSubType.update({"Name": rule_sub_type_name},
                                          {"$set": {"ComplianceType": comp_type, "RuleType": rule_typ}})
                    ins_id = db.RuleSubType.find_one({"Name": rule_sub_type_name})["_id"]
                    # inserting log in sub_type_history
                    db.SubTypeHistory.insert_one({"action": "updated", "sub_id": ins_id})
                else:
                    ins_id = db.RuleSubType.insert_one(
                        {"Name": rule_sub_type_name,
                         "ComplianceType": [ObjectId(compliance_type_data["_id"])],
                         "RuleType": [ObjectId(rule_type)]}).inserted_id
                    # inserting log in sub_type_history
                    db.SubTypeHistory.insert_one({"action": "inserted", "sub_id": ins_id})
                return {
                    "statusCode": 200,
                    "message": "Successfully updated data.",
                    "inserted_id": str(ins_id)
                }
            except Exception as e:
                print(e)
                return {
                    "statusCode": 500,
                    "message": " Duplicate sub type."
                }
    except Exception as err:
        return {'statusCode': 500, "error": "get rule subtype failed"}

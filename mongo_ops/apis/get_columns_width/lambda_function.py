import json
import os
import boto3
from bson import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    try:
        data = db.LoanPackageColumns.find({})
        data_dict = {}
        for item in data:
            data_dict[str(item["_id"])] = {}
            for column in item["Columns"]["Loan"]:
                display_name = column["Display Name"] if "Display Name" in column else column['Name']
                width = column["Width"] if "Width" in column else 15
                data_dict[str(item["_id"])][display_name] = width
        return {'statusCode': 200, "body": data_dict}
    except Exception as err:
        print(err)
        return {'statusCode': 500, "error": "get_column_width failed"}

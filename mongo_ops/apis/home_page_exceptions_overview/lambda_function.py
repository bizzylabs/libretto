import json
import os
import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient
from datetime import datetime

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def latest_available(rule_set_id, run_id, created_date, loan_package):
    try:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S.%f')
    except:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S')
    run_ids = [item["RunId"] for item in db.SchedulerHistory.find(
        {"LoanPackage": ObjectId(loan_package), "RunId": {"$gt": run_id}, "IsActive": 1}) if item]
    result = db.ResultSetSummary.find_one({"$and": [{"RunId": {"$in": run_ids}},
                                                    {"RuleSetId": ObjectId(rule_set_id), "RunId": {"$gt": run_id},
                                                     "CreatedOn": {"$gte": created_on}}]})
    if result:
        val = db.SchedulerHistory.find_one({"RunId": result["RunId"], "LoanPackage": ObjectId(loan_package)},
                                           {"_id": 0, "IsActive": 1})
        if val and val["IsActive"] == 1:
            return True
    return False


def lambda_handler(event, context):
    exception_overview = dict()
    exception_overview["TotalException"] = 0
    exception_overview["TotalLoan"] = 0
    exception_overview["NewExceptions"] = 0
    exception_overview["ExceptionCleared"] = 0
    try:
        user_name = event["context"]["username"]
        loan_package_id = event["body"].get("loan_package_id", None)
        if not loan_package_id:
            return {"error": "loan package id is missing"}

        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        exception_type = event["body"].get("exception_type", None)
        if exception_type == "open":
            exception_filter = {"$eq": ["$SystemNote", []]}
        else:
            exception_filter = {}
        loan_filter_condition = [{}]

        rule_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                                {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$ExceptionStatus", True]}, {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                               {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$ExceptionStatus", True]}, {
                                   "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                           {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]},
                               {"$eq": ["$Ignore", False]}]
        cleared_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                                   {"$ne": ["$LoanNumber", None]},
                                   {"$eq": ["$ExceptionStatus", False]}, {"$eq": ["$Ignore", False]}]
        high_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$Severity", "High"]}, {"$eq": ["$Ignore", False]}]
        low_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$Severity", "Low"]}, {"$eq": ["$Ignore", False]}]
        medium_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                                  {"$eq": ["$Severity", "Medium"]}, {"$eq": ["$Ignore", False]}]
        critical_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]},
                                    {"$ne": ["$LoanNumber", None]},
                                    {"$eq": ["$Severity", "Critical"]}, {"$eq": ["$Ignore", False]}]

        if filters and condition_filters:
            try:
                for key in filters:
                    if len(filters[key]) > 0:
                        if "Include" in condition_filters[key].keys():
                            cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                            loan_filter_condition.append({key: {"$in": filters[key]}})
                        else:
                            cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                            loan_filter_condition.append({key: {"$nin": filters[key]}})
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)
                        cleared_count_condition.append(cond)
                        high_count_condition.append(cond)
                        low_count_condition.append(cond)
                        medium_count_condition.append(cond)
                        critical_count_condition.append(cond)
            except Exception as err:
                return {"statusCode": 500, "error in filter condition block": str(err)}
        else:
            try:
                loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
                filters = db.UserFilters.find_one({"UserName": user_name})
                for key in filters["Filters"][loan_package_name]:
                    if len(filters["Filters"][loan_package_name][key]) > 0:
                        if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                            cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                            loan_filter_condition.append({key: {"$in": filters["Filters"][loan_package_name][key]}})
                        else:
                            cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                            loan_filter_condition.append({key: {"$nin": filters["Filters"][loan_package_name][key]}})
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)
                        cleared_count_condition.append(cond)
                        high_count_condition.append(cond)
                        low_count_condition.append(cond)
                        medium_count_condition.append(cond)
                        critical_count_condition.append(cond)
            except Exception as err:
                return {"statusCode": 500, "error": str(err)}

        try:
            rule_set_ids = list(db.RuleSetMaster.aggregate([
                {"$match": {"Users": {"$in": [user_name]}}},
                {"$group": {"_id": "", "ids": {"$push": "$_id"}}},
                {"$project": {"_id": 0}}
            ]))
        except Exception as err:
            return {"statusCode": 500, "error": "RuleSetMaster databae operation error"}
        if len(rule_set_ids) > 0:
            rule_set_ids = rule_set_ids[0].get("ids")
        else:
            return {
                "ExceptionOverview": {"TotalException": 0, "TotalLoan": 0, "NewExceptions": 0, "ExceptionCleared": 0}}
        try:
            scheduler_config_detail = db.SchedulerConfig.find({"LoanPackageId": ObjectId(loan_package_id)})
            collection_name = db.RuleTestingLoanData.find_one({"LoanPackageId": ObjectId(loan_package_id)})[
                "LoanDataCollectionName"]
            # filtered_count = db[collection_name].find({"$and": loan_filter_condition}).count()
            match_criteria = {"$match": {"$and": loan_filter_condition}}
            group_query = {"$group": {"_id": None, "count": {"$sum": 1}}}
            ruleset_db = db[collection_name].aggregate([match_criteria, group_query])
            draft_rule = [data for data in ruleset_db]
            filtered_count = 0
            if draft_rule:
                filtered_count = draft_rule[0].get('count', 0)
        except Exception as err:
            return {"statusCode": 500, "error": "databae operation error occured"}
        run_ids = []
        all_rulesets = []
        try:
            for scheduler_config in scheduler_config_detail:
                output = []
                scheduler_config_id = scheduler_config.get("_id")
                # scheduler_history = list(
                #     db.SchedulerHistory.find({"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
                #                               "SchedulerConfigId": ObjectId(scheduler_config_id)}).sort(
                #         [("RunId", -1)]).limit(1))

                scheduler_history = list(db.SchedulerHistory.aggregate(
                                                [{"$match": {"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
                                                             "SchedulerConfigId": ObjectId(scheduler_config_id)}},
                                                            {"$sort": {"RunId": -1}},
                                                            {"$limit": 1},
                                                ]))

                if len(scheduler_history) > 0:
                    scheduler_history = scheduler_history[0]
                    run_id = scheduler_history.get("RunId")
                    run_ids.append(run_id)
                    pipe = [
                        {"$match": {"$and": [{
                            "RunId": run_id,
                            "RuleSetId": {"$in": rule_set_ids},
                        }]}},
                        {"$group": {
                            "_id": {"rule_set_id": "$RuleSetId",
                                    "rule_id": "$RuleId"
                                    },
                            "rule_count": {"$sum": {"$cond": [
                                {"$and": rule_count_condition}, 1,
                                0]}},
                            "priority": {"$first": "$Severity"},
                            "new_count": {"$sum": {"$cond": [
                                {"$and": new_count_condition}, 1, 0]}},
                            "cleared_count": {"$sum": {"$cond": [
                                {"$and": cleared_count_condition}, 1,
                                0]}},
                            "High_count": {"$sum": {
                                "$cond": [{"$and": high_count_condition}, 1,
                                          0]}},
                            "Low_count": {"$sum": {
                                "$cond": [{"$and": low_count_condition}, 1,
                                          0]}},
                            "Medium_count": {"$sum": {
                                "$cond": [{"$and": medium_count_condition}, 1,
                                          0]}},
                            "Critical_count": {"$sum": {
                                "$cond": [{"$and": critical_count_condition},
                                          1, 0]}},

                        }},
                        {"$group": {
                            "_id": "$_id.rule_set_id",
                            "Rules": {
                                "$push": {
                                    "Rule": "$_id.rule_id",
                                    "exceptions": "$rule_count",
                                    "new": "$new_count",
                                    "severity": "$priority"
                                },
                            },
                            "total_exception": {"$sum": "$rule_count"},
                            "new_exceptions": {"$sum": "$new_count"},
                            "cleared_exceptions": {"$sum": "$cleared_count"},
                            "high_severity": {"$sum": "$High_count"},
                            "low_severity": {"$sum": "$Low_count"},
                            "medium_severity": {"$sum": "$Medium_count"},
                            "critical_severity": {"$sum": "$Critical_count"},
                        }},
                        {"$sort": {"total_exception": -1}}
                    ]
                    for rule_sets_data in db.ResultSetDetail.aggregate(pipe):
                        if rule_sets_data["_id"] not in all_rulesets:
                            rule_set_detail = dict()
                            rule_set_detail["_id"] = str(rule_sets_data.get("_id"))
                            rule_set_detail["RunId"] = run_id

                            exceptions = db.ResultSetSummary.find_one(
                                {"RuleSetId": ObjectId(rule_sets_data["_id"]), "RunId": run_id})
                            if exceptions:
                                created_on_temp = exceptions.get("CreatedOn")
                            if not latest_available(rule_set_detail["_id"], rule_set_detail["RunId"], created_on_temp,
                                                    loan_package_id):
                                exception_overview["TotalException"] += rule_sets_data["total_exception"]
                                exception_overview["NewExceptions"] += rule_sets_data["new_exceptions"]
                                exception_overview["ExceptionCleared"] += rule_sets_data["cleared_exceptions"]
                                exception_overview["TotalLoan"] = filtered_count
                                rule_set_detail["Severity"] = {
                                    "TotalException": {"Critical": rule_sets_data["critical_severity"],
                                                       "High": rule_sets_data["high_severity"],
                                                       "Medium": rule_sets_data["medium_severity"],
                                                       "Low": rule_sets_data["low_severity"]}}
        except Exception as err:
            return {"statusCode": 500, "error in getting scheduler_config_detail": str(err)}

        try:
            client.close()
            return {"ExceptionOverview": exception_overview}
        except Exception as e:
            client.close()
            print(e)
            return {"ExceptionOverview": exception_overview}
    except Exception as e:
        return {"ExceptionOverview": exception_overview}

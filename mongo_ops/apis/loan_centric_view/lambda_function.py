import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
from collections import OrderedDict
import os

null = None
true = True
false = False


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def lambda_handler(event, context):
    try:
        run_id = event["body"].get("RunId", None)
        rule_set_id = event["body"].get("RuleSetId", None)
        rule_id = event["body"].get("RuleId", None)
        loan_number = event["body"].get("LoanNumber", None)
        offset = event["body"].get("OffSet", 0)
        limit = event["body"].get("Limit", float("inf"))
        sort_key = event["body"].get("SortBy", "RuleSetName")
        asc_key = True if event["body"].get("Asc", "N") == "N" else False
        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        loan_package_id = event["body"].get("loan_package_id", None)
        user_name = event["context"]["username"]
        if not loan_package_id:
            return {"error": "loan package id is required"}
        database = get_initialized_db()
        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters[key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters[key]}
        else:
            loan_package_name = database.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = database.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters["Filters"][loan_package_name][key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters["Filters"][loan_package_name][key]}

        if sort_key == "Severity":
            sort_key = "Priority"

        output = []
        rules = database.ResultSetDetail.find({"$and": [
            {"RunId": run_id, "ExceptionStatus": True, "LoanNumber": loan_number},
            filter_condition,
            {"$or": [{"RuleId": {"$ne": ObjectId(rule_id)}}, {"RuleSetId": {"$ne": ObjectId(rule_set_id)}}]}]},
            {"_id": 0, "RuleSetId": 1, "RuleId": 1, "SystemNote": 1})
        data = [item for item in rules]

        for item in data:
            rule_detail = OrderedDict()
            rule_set_master = database.RuleSetMaster.find_one({"_id": item["RuleSetId"]})
            if user_name in rule_set_master["Users"]:
                rule_detail["RuleSetName"] = rule_set_master["RuleSetName"]
                rule_detail["RuleSetId"] = str(item["RuleSetId"])
                rule_detail["SystemNote"] = item.get("SystemNote", "")
                rule = database.Rule.find_one(item["RuleId"])
                rule_detail["RuleId"] = str(rule["_id"])
                rule_detail["Name"] = rule["Name"]
                rule_detail["Severity"] = rule.get("Severity", "")
                rule_detail["Priority"] = rule.get("Priority", "")

                output.append(rule_detail)

        return {"result": sorted(output, key=lambda x: x[sort_key], reverse=asc_key)}
    except Exception as err:
        return {'statusCode': 500, "error": "loan centric view failed"}



import json
import os

import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        event = event["body"]
        output = []
        pagination_token = event.get("offset", None)
        limit = event.get("limit", 20)
        filter_type = event.get("filter_type", "username").lower()
        filter_value = event.get("filter_value", "").lower()

        if filter_type not in ["username", "email"]:
            return {"ErrorMessage": f"Wrong filter value - {filter_type}"}
        filter_parameter = f"{filter_type}^=\"{filter_value}\""

        try:
            response = cognito.list_users(
                PaginationToken=pagination_token,
                UserPoolId=user_pool_id,
                AttributesToGet=[
                    'email_verified',
                    'email',
                    'custom:FirstName',
                    'custom:LastName'
                ],
                Limit=limit,
                Filter=filter_parameter
            )
        except Exception as e:
            print(e)
            response = cognito.list_users(
                UserPoolId=user_pool_id,
                AttributesToGet=[
                    'email_verified',
                    'email',
                    'custom:FirstName',
                    'custom:LastName'
                ],
                Limit=limit,
                Filter=filter_parameter
            )

        try:
            for item in response["Users"]:
                user_detail = dict()
                mfaresponse = cognito.admin_get_user(
                    UserPoolId=user_pool_id,
                    Username=item["Username"]
                )
                if "UserMFASettingList" in mfaresponse:
                    if len(mfaresponse["UserMFASettingList"]) >= 1:
                        user_detail["MfaConfigured"] = True
                    else:
                        user_detail["MfaConfigured"] = False
                else:
                    user_detail["MfaConfigured"] = False
                user_detail["UserName"] = item["Username"]
                for details in item["Attributes"]:
                    data = details["Name"]
                    if data == "email_verified":
                        user_detail["EmailVerified"] = details["Value"]
                    if data == "email":
                        user_detail["Email"] = details["Value"]
                    if data == "custom:FirstName":
                        user_detail["Firstname"] = details["Value"]
                    if data == "custom:LastName":
                        user_detail["Lastname"] = details["Value"]

                user_detail["Enabled"] = item["Enabled"]
                user_detail["UserStatus"] = item["UserStatus"]
                group_result = cognito.admin_list_groups_for_user(Username=item["Username"], UserPoolId=user_pool_id)
                group = group_result.get("Groups", "")
                if group:
                    group = [item["GroupName"] for item in group]
                    user_detail["Groups"] = group
                else:
                    user_detail["Groups"] = []
                last_log_in_time = ""
                last_log_in = db.BussinessAuditLogs.find(
                    {"action": "login", "username": {"$in": [item["Username"], user_detail["Email"]]}}).sort(
                    [("timestamp", -1)]).limit(1)
                if last_log_in.count() != 0:
                    last_log_in_time = str(last_log_in[0]["timestamp"])
                user_detail["LastLogin"] = last_log_in_time
                last_verification_mail_time = ""

                last_verification_mail = db.BussinessAuditLogs.find(
                    {"action": "verificationMail", "username": {"$in": [item["Username"], user_detail["Email"]]}}).sort(
                    [("timestamp", -1)]).limit(1)

                if last_verification_mail.count() != 0:
                    last_verification_mail_time = str(last_verification_mail[0]["timestamp"])

                user_detail["LastVerificationMail"] = last_verification_mail_time
                filters = db.UserFilters.find_one({"UserName": item["Username"]})
                user_detail["AccessFilters"] = {}
                user_detail["ConditionsFilter"] = {}
                user_detail["OptionsFilter"] = {}
                if filters:
                    user_detail["AccessFilters"] = filters["Filters"]
                    user_detail["ConditionsFilter"] = filters["ConditionsFilter"]
                    user_detail["OptionsFilter"] = filters["OptionsFilter"]

                enable_filter = db.UserFilters.find_one({"UserName": item["Username"]})
                if enable_filter:
                    enable_filter_value = enable_filter.get("EnableFilter", False)
                else:
                    enable_filter_value = False
                user_detail["EnableFilter"] = enable_filter_value
                output.append(user_detail)
            data = sorted(output, key=lambda i: (i['UserName']))

        except Exception as e:
            print(e)
            return {"result": list()}

        pagination_token = response.get("PaginationToken", None)

        if len(data) == 0:
            return {
                "statusCode": 400,
                "message": "User not found",
                "result": data,
                "pagination_token": response.get("PaginationToken", None)
            }
        else:
            return {
                "result": data,
                "pagination_token": pagination_token
            }
    except Exception as e:
        mongo_client.close()
        return {'statusCode': 500, "error": "admin list users failed"}


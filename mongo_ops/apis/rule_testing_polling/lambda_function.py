import json
import os
import re
from base64 import b64decode
from io import StringIO

import boto3
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]

null = None
true = True
false = False


def lambda_handler(event, context):
    try:
        criteria = event["body"].get("criteria", "{}")
        display_fields = event["body"].get("display_fields", "{}")
        loan_package_id = event["body"].get("loan_package_id")
        file_name = event["body"].get("filename")
        if not loan_package_id:
            return {"statusCode": 500, "Error": "Loan package id missing"}
        if not file_name:
            return {"statusCode": 500, "Error": "filename field is  missing"}
        if criteria:
            try:
                criteria = eval(b64decode(criteria).decode('utf-8'))
                # criteria = eval(criteria)
                temp = {"LoanNumber": 1}
                temp.update(eval(display_fields))
                display_fields = temp.copy()
            except Exception as err:
                return {"statusCode": 500, "Error": "Query not supported."}
        loan_collection_name = db.RuleTestingLoanData.find_one({"LoanPackageId": ObjectId(loan_package_id), "IsActive": 1})
        if not loan_collection_name:
            return {"statusCode": 500, "Error": "Either data processing is going on or data is not present"}
        try:
            s3 = boto3.client('s3', region_name="us-east-1")
            obj = s3.get_object(Bucket=BUCKET_NAME, Key=file_name)
            try:
                csv_string = obj['Body'].read().decode("latin1")
            except Exception as e:
                return {
                    'statusCode': 200,
                    'result': [],
                    'total_count': 0
                }
            file_df = pd.read_csv(StringIO(csv_string), encoding='latin1', na_values='')
            try:
                colomns = [k for k, v in display_fields.items()]
                sorted_colomns = ["_id", "TotalCount"]
                for word in colomns:
                    val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ',
                                 word).strip()  # adding space after camelcase
                    val1 = re.search("\d{1,}" , val)  # search for number after lower case
                    if val1:
                        final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                    else:
                        final_key = val
                    sorted_colomns.append(final_key)
                final_df = file_df.reindex(columns=sorted_colomns)
            except Exception as e:
                final_df = file_df

            final_df = final_df.fillna("")
            count = int(final_df["TotalCount"][0])
            final_df.drop("TotalCount", axis=1, inplace=True)
            output = final_df.to_dict("records")
        except pd.errors.EmptyDataError as e:
            return {
                'statusCode': 200,
                'result': [],
                'total_count': 0
            }
        except Exception as e:
            return {"statusCode": 503, "Error": "File not present."}

        return {
            'statusCode': 200,
            'result': output,
            'total_count': count
        }
    except Exception as err:
        return {'statusCode': 500, "Error": "rule testing polling failed"}



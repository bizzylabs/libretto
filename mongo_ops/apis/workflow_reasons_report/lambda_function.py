import json
import os
import datetime
import boto3
from pymongo import MongoClient
import pandas as pd
import io
from bson import ObjectId
import xlsxwriter
import time
from datadog_lambda.metric import lambda_metric
from openpyxl import load_workbook


secret = boto3.client("secretsmanager", region_name='us-east-1')
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

BUCKET_NAME = secret_dict["export_bucket"]
DATA_FILE_NAME = "workflow_reason_report_data.xlsx"
TEMPLATE_FILE_NAME = "workflow_template.xlsx"


def create_presigned_url(bucket_name, object_name, expiration=300):
    s3 = boto3.client("s3")
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except Exception as e:
        return None
    return response


def export_data_to_excel(data, filename):
    df = pd.DataFrame(data)
    with io.BytesIO() as output:
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)
        data = output.getvalue()

    s3 = boto3.resource('s3')
    s3.Bucket(BUCKET_NAME).put_object(Key=filename, Body=data)

    file_link = create_presigned_url(BUCKET_NAME, filename)
    if file_link is None:
        file_link = ""

    if file_link:
        return {"status": 200, "body": "file uploaded.", "file_path": file_link}
    else:
        return {"status": 400, "body": "file not uploaded."}


def extract_date_details(date):
    if date:
        date_string = (" ").join(date.split(" ")[1:-1])
        converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
        return converted_datetime
    else:
        return None


def get_rule_set(rule_set_ids):
    rule_set_names = []
    if rule_set_ids:
        for rule_set_id in rule_set_ids:
            rule_set = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)})
            rule_set_names.append(rule_set["RuleSetName"])
    return rule_set_names


def get_run_ids(loan_package_id):
    start_time = round(time.time() * 1000)
    loan_data = db.SchedulerHistory.find({"LoanPackage": ObjectId(loan_package_id)})
    lambda_metric(
        metric_name='bizzy.bre.db.find',
        value=round(time.time() * 1000) - start_time,
        timestamp=int(time.time()),  # optional, must be within last 20 mins
        tags=['loan_package_id:' + str(loan_package_id), 'collection:SchedulerHistory']
    )
    run_ids = []
    for loan in loan_data:
        run_ids.append(loan['RunId'])
    return run_ids


def update_workflow_data(data_file_name, template_file_name):
    df = pd.read_excel(data_file_name, sheet_name="Data")
    book = load_workbook(template_file_name)
    writer = pd.ExcelWriter(template_file_name, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    df.to_excel(writer, sheet_name="Data", index=False)
    writer.close()


def lambda_handler(event, context):
    try:
        loan_numbers = event["body"].get("loan_numbers", None)
        start_time = extract_date_details(event["body"].get("gmtFromTime", None))
        end_time = extract_date_details(event["body"].get("gmtToTime", None))
        loan_package_id = event['body'].get("loan_package_id", None)
        file_name = event['body'].get("file_name", None)
        workflow_action = event['body'].get("workflow_action", None)
        users = event['body'].get("users", None)
        reasons = event['body'].get("reasons", None)
        rule_name_id = event['body'].get("rule_name_id", None)
        rule_set_id = event['body'].get("rule_set_id", None)

        aggregate_tags = []

        if not file_name:
            now = datetime.datetime.now()
            file_name = "workflow_reason_report_" + now.strftime("%d-%m-%Y") + ".xlsx"
        aggregate_tags.append(file_name)

        run_ids = get_run_ids(loan_package_id) if loan_package_id else None

        if loan_package_id:
            aggregate_tags.append('loan_package_id:' + str(loan_package_id))

        query = [
            {"$match": {"SystemNote": {"$ne": []}}},
            {"$lookup":
                {
                    "from": "Rule",
                    "let": {"rule_id": "$RuleId"},
                    "pipeline": [
                        {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                    ],
                    "as": "RuleData"
                }
            }, {"$unwind": "$RuleData"},
            {"$lookup":
                {
                    "from": "RuleSetMaster",
                    "let": {"rule_set_id": "$RuleSetId"},
                    "pipeline": [
                        {"$match": {"$expr": {"$eq": ["$_id", "$$rule_set_id"]}}}
                    ],
                    "as": "RuleSetMasterData"
                }
            }, {"$unwind": "$RuleSetMasterData"},
            {
                "$lookup": {
                    "from": "Notes",
                    "let": {
                        "rule_set_id": "$RuleSetId",
                        "rule_name_id": "$RuleNameId",
                        "loan_number": "$LoanNumber",
                        "key": "$SystemNote",
                        "note_type": "SystemNote",
                        "note_action": "Add"
                    },
                    "pipeline": [{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {"$eq": ["$RulesetId", "$$rule_set_id"]},
                                    {"$eq": ["$RuleNameId", "$$rule_name_id"]},
                                    {"$eq": ["$LoanNumber", "$$loan_number"]},
                                    {"$in": ["$Key", "$$key"]},
                                    {"$eq": ["$NoteType", "$$note_type"]},
                                    {"$eq": ["$Note.Action", "$$note_action"]}
                                ]
                            }
                        }
                    }],
                    "as": "NotesData"
                }
            }, {"$unwind": "$NotesData"},
        ]

        if workflow_action:
            query[0]['$match'].update({"SystemNote": {"$in": workflow_action}})

        if loan_numbers:
            query[0]['$match'].update({"LoanNumber": {"$in": loan_numbers}})

        if run_ids:
            query[0]['$match'].update({"RunId": {"$in": run_ids}})

        if start_time and end_time:
            query[0]['$match'].update({"CreatedOn": {"$gte": start_time, "$lte": end_time}})
            query[5]['$lookup']['let'].update({"start_time": start_time, "end_time": end_time})
            query[5]['$lookup']['pipeline'][0]['$match']['$expr']['$and'].append(
                {"$gte": ["$Note.createdon", "$$start_time"]})
            query[5]['$lookup']['pipeline'][0]['$match']['$expr']['$and'].append(
                {"$lte": ["$Note.createdon", "$$end_time"]})

        if rule_name_id:
            query[0]['$match'].update({"rule_name_id": {"$in": rule_name_id}})
            aggregate_tags.append('rule_name_id:' + str(rule_name_id))

        if rule_set_id:
            query[0]['$match'].update({"rule_name_id": {"$in": rule_set_id}})
            aggregate_tags.append('rule_set_id:' + str(rule_set_id))

        if users:
            query[5]['$lookup']['let'].update({"users": users})
            query[5]['$lookup']['pipeline'][0]['$match']['$expr']['$and'].append({"$in": ["$Note.Username", "$$users"]})

        if reasons:
            query[5]['$lookup']['let'].update({"reasons": reasons})
            query[5]['$lookup']['pipeline'][0]['$match']['$expr']['$and'].append({"$in": ["$Reason", "$$reasons"]})

        rule_set = get_rule_set(rule_set_id)
        aggregate_tags.append('rule_set_names:' + str(rule_set))

        start_time = round(time.time() * 1000)
        data = db.ResultSetDetailArchive.aggregate(query)
        lambda_metric(
            metric_name='bizzy.bre.db.aggregate',
            value=round(time.time() * 1000) - start_time,
            timestamp=int(time.time()),  # optional, must be within last 20 mins
            tags=aggregate_tags
        )

        result_array = []
        for record in data:
            data_dict = {
                "Ruleset Name": record['RuleSetMasterData']['RuleSetName'],
                "Business Rule": record['RuleData']['Name'],
                "Loan Number": record["LoanNumber"],
                "Priority": record['Severity'],
                "Workflow Action": record['NotesData']['Key'],
                "Duration Active (Snooze)": "",
                "Action By": "",
                "Action Time": "",
                "Reason": ""
            }
            if record['NotesData']['Key'] == "Snooze" and "Days" in record["NoteDetail"]:
                data_dict["Duration Active (Snooze)"] = record["NoteDetail"]["Days"]
            notes_section = record['NotesData']
            if notes_section:
                data_dict["Action By"] = notes_section["Note"]["Username"]
                data_dict["Action Time"] = notes_section["Note"]["createdon"].strftime("%d/%m/%Y %H:%M")
                if "Reason" in notes_section:
                    data_dict["Reason"] = notes_section["Reason"]

            result_array.append(data_dict)

        # response = export_data_to_excel(result_array, file_name)

        df = pd.DataFrame(result_array)
        df.to_excel("/tmp/" + DATA_FILE_NAME, sheet_name="Data", index=False)
        s3.Bucket(BUCKET_NAME).download_file(TEMPLATE_FILE_NAME, "/tmp/" + TEMPLATE_FILE_NAME)
        update_workflow_data("/tmp/" + DATA_FILE_NAME, "/tmp/" + TEMPLATE_FILE_NAME)
        s3.Bucket(BUCKET_NAME).upload_file("/tmp/" + TEMPLATE_FILE_NAME, file_name)

        return {"message": "Data will be uploaded to " + BUCKET_NAME + " with file name: " + file_name}
    except Exception as e:
        return {'statusCode': 500, 'error': "workflow reasons report failed"}

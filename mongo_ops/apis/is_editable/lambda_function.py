import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
from datetime import datetime
import os

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]



def in_draft(rule_id):
    in_draft_as_parent = db.RuleSetMaster.find_one({'$and': [
        {"RuleSetName": "Draft Rules"},
        {"RuleIds": {"$elemMatch": {"ParentId": rule_id}}}
    ]})
    if in_draft_as_parent is not None:
        return True

    return False


def is_editable(event, context):
    role = event["context"].get("role", "")
    print(role)
    try:
        user_name = event["context"].get("username", None)
        rule_id = event["body"].get("RuleId", None)
        action = event["body"].get("action", None)

        if rule_id is None:
            return {"result": False, "Message": "Missing required fields"}

        rule_id = ObjectId(rule_id)
        if action == "cancel":
            db.LockRules.delete_one({"RuleId": rule_id, "LockedBy": user_name})
            return {"result": False, "Message": "Transaction cancelled."}

        if in_draft(rule_id):
            return {"result": False, "Message": "This rule is already in Draft Rules"}

        else:
            is_locked = db.LockRules.find_one({"RuleId": rule_id})
            # check if it is a child rule, that was in draft because of "save & exit" on ADD Rule
            # is_child = db.RuleSetMaster.find_one({"Name": "Draft Rules", "RuleIds": {"$elemMatch": {"ParentId": "", "ChildId": rule_id }}})
            is_child = db.RuleSetMaster.find_one(
                {"RuleSetName": "Draft Rules", "RuleIds": {"$elemMatch": {"ParentId": "", "ChildId": rule_id}}}
            )
            if is_child is not None and is_locked is None:
                print("##This was a child Id##")
                db.LockRules.insert_one(
                    {
                        "LockedBy": user_name,
                        "RuleId": rule_id,
                        "CreatedOn": datetime.now(),
                        "UpdatedOn": datetime.now()
                    }
                )
                return {"result": True}


            elif is_locked is None:
                db.LockRules.insert_one(
                    {
                        "LockedBy": user_name,
                        "RuleId": rule_id,
                        "CreatedOn": datetime.now(),
                        "UpdatedOn": datetime.now()
                    }
                )
                return {"result": True}
            else:
                if is_locked["LockedBy"] == user_name:
                    db.LockRules.update({"RuleId": rule_id}, {"$set": {"UpdatedOn": datetime.now()}})
                    return {"result": True}
                # if TTL is expired allow to edit and lock for current user.
                # else:
                #     # calculate time diff between UpdatedOn and current time in minutes
                #     time_diff = (datetime.now() - locked_rule["UpdatedOn"]).total_seconds()/60
                #     # if diff is greater than TTl(i.e. 5 min)
                #     # then update user in lock table and allow new user

                #     if time_diff > 5:
                #         db.LockRules.update(
                #             {"RuleId": rule_id},
                #             {
                #                 "LockedBy": user_name,
                #                 "CreatedOn": datetime.now(),
                #                 "UpdatedOn": datetime.now()
                #             }
                #         )
                #         return {"result":True}
                client.close()
                return {"result": False,
                        "Message": "This Rule is locked and being edited by '" + is_locked["LockedBy"] + "'"}

    except Exception as err:
        print(err)
        client.close()
        return {"result": False, "Message": "Oops! Something went wrong. Please try again later."}


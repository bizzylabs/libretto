import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
import os

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def get_result_set_summary(event, context):
    try:
        event = event['body'] if 'body' in event else event
        RuleSetId = event.get("RuleSetId", None)
        run_id = event.get("run_id")
        if not RuleSetId:
            return {'error': "Rule Set ID not provided."}
        data = db.ResultSetSummary.find({"RuleSetId": ObjectId(RuleSetId), "RunId": run_id},
                                        {"NoOfLoans": 1, "NoOfRules": 1,
                                         "NoOfNewException": 1, "NoOfLoansWithException": 1,
                                         "TotalNoOfExceptions": 1, "_id": False})

    except Exception as e:
        return {'error': "Rule Set ID not present."}
    return {'summary': list(data)}
import datetime
import json
import os
import re

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def scheduler_in_DB(schedulerId, schedulerName):
    return len(list(db.SchedulerConfig.find({"_id": ObjectId(schedulerId), "Name": schedulerName}))) == 1


def contains_special_char(string):
    regex = re.compile('[@_!#$%^&*()<>?/\|}{~:]')
    return regex.search(string) != None


def event_trigger(event, context):
    try:
        role = event["context"].get("role", "")
        client = db.ApplicationDetails.find_one({})['Client']
        state = event['body'].get('state', None)
        event_bridge = boto3.client("events", region_name="us-east-1")
        scheduler_id = event['body'].get("scheduler_id", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        cdt = datetime.datetime.now()
        log_document = {}

        if state and state.lower() == "delete":
            if not scheduler_id:
                return {"statusCode": 403, "error": "Scheduler Id required to delete"}
            rule_name = db.SchedulerConfig.find_one({"_id": ObjectId(scheduler_id)}, {"Name": 1})["Name"]
            res = rule_name
            rule_name = rule_name.replace(" ", "")
            rule_name = rule_name + "_" + client
            try:
                event_bridge.remove_targets(Rule=rule_name, Ids=['MyTargetID'], Force=True)
                event_bridge.delete_rule(Name=rule_name, Force=True)
            except:
                pass
            db.SchedulerConfig.remove({"_id": ObjectId(scheduler_id)})
            try:
                logdin_username = event["context"]["username"]
            except:
                logdin_username = ""
            log_document["ip"] = ip
            log_document["browser"] = browser
            log_document["username"] = logdin_username
            log_document["timestamp"] = cdt
            log_document["action"] = "Deleted"
            log_document["resource"] = res
            log_document["log_source"] = "Scheduler"
            insert_log = db.BussinessAuditLogs.insert_one(log_document)
            return {"statusCode": 200, "message": "Scheduler deleted"}

        if len(event['body'].get("frequency", [])) < 1:
            return {"statusCode": 403, 'error': 'Days of Week Required'}

        if contains_special_char(event['body'].get("name", "")):
            return {"statusCode": 403, 'error': 'Special characters not allowed in scheduler name'}

        for field in ["startTime", "frequency", "loanPackage"]:
            if event['body'].get(field) is None:
                return {"statusCode": 403, "error": f"{field} is required"}

        try:
            rule_name = event['body'].get("name")
            eventRuleSets = event['body'].get("rulesets", [])
            ruleset_ids = []
            for ruleset in eventRuleSets:
                ruleset["value"] = ObjectId(ruleset["value"])
                ruleset_ids.append(ruleset["value"])

            loan_package = event['body'].get("loanPackage")
            for package in loan_package:
                package["value"] = ObjectId(package["value"])
        except Exception as err:
            return {"statusCode": 500, "error": str(err)}

        scheduler = dict()
        scheduler["Name"] = event['body'].get("name", "BSIScheduler")
        scheduler["StartTime"] = event['body'].get("startTime")
        scheduler["EndDate"] = event['body'].get("endDate")
        scheduler["RunsEvery"] = event['body'].get("runsEvery")
        scheduler["Frequency"] = event['body'].get("frequency", [])
        scheduler["RuleSetId"] = eventRuleSets
        scheduler["RuleSetIds"] = ruleset_ids
        scheduler["LoanPackage"] = loan_package
        scheduler["LoanPackageId"] = loan_package[0]["value"]
        scheduler["CreatedOn"] = datetime.datetime.now()
        scheduler["UpdatedOn"] = datetime.datetime.now()
        scheduler["TimeZone"] = event['body'].get("selectedTimeZone")
        scheduler["IsActive"] = event['body'].get("isActive")
        scheduler["CurrentStatus"] = ""

        try:
            datetimeobj = event['body'].get("gmtTime", None)
            modified_date_and_time = datetime.datetime.strptime(datetimeobj, "%a, %d %b %Y %H:%M:%S %Z")
            scheduler["GmtTime"] = f"{modified_date_and_time.hour}:{modified_date_and_time.minute}"

            if event['body'].get("isActive")[0].get("value") == 1:
                state = "ENABLED"
                scheduler["Active"] = 1
            else:
                state = "DISABLED"
                scheduler["Active"] = 0

            scheduler_config = db.SchedulerConfig
            scheduler_config_id = None
            rulesets_updated = None
            frequency_updated = None
            name_updated = None
            runs_updated = None
            active_updated = None

            if event['body'].get("schedulerId") is None:
                scheduler_config_id = scheduler_config.insert_one(scheduler).inserted_id
                action = "Added"
            else:
                if not scheduler_in_DB(event['body'].get("schedulerId"), scheduler["Name"]):
                    return {'error': 'Wrong parameters'}
                scheduler_config_id = ObjectId(event['body'].get("schedulerId"))
                print(scheduler_config_id)
                scheduler_data = db.SchedulerConfig.find({"_id": ObjectId(scheduler_config_id), "Name": scheduler["Name"]})
                for old_data in scheduler_data:
                    if old_data["Name"] != scheduler["Name"] :
                        name_updated = scheduler["Name"]
                    mismatch_count = 0
                    for new_rule in scheduler["RuleSetIds"]:
                        if ObjectId(new_rule) not in old_data["RuleSetIds"]:
                            mismatch_count+=1
                    if mismatch_count > 0 or len(old_data["RuleSetIds"]) != len(scheduler["RuleSetIds"]):
                        rulesets_updated = scheduler["RuleSetIds"]
                    mismatch_count = 0
                    for new_freq in scheduler["Frequency"]:
                        if new_freq not in old_data["Frequency"]:
                            mismatch_count+=1
                    if mismatch_count > 0 or len(old_data["Frequency"]) != len(scheduler["Frequency"]):
                        frequency_updated = scheduler["Frequency"]
                    if old_data["RunsEvery"] != scheduler["RunsEvery"]:
                        runs_updated = scheduler["RunsEvery"]
                    if old_data["IsActive"][0]["value"] != scheduler["IsActive"][0]["value"] and old_data["IsActive"][0]["label"] != scheduler["IsActive"][0]["label"] :
                        active_updated = scheduler["IsActive"]

                    scheduler["CurrentStatus"] = old_data.get("CurrentStatus", "")

                scheduler_config.update({'_id': scheduler_config_id}, {"$set": scheduler})
                action = "Updated"

            if not scheduler_config_id:
                return {"error": "Scheduler Not configured"}

            freq = ",".join(event['body'].get("gmtSelectedWeekDay", []))
            cron = f"cron({modified_date_and_time.minute} {modified_date_and_time.hour} ? * {freq} *)"

            scheduler_dict = dict()
            scheduler_dict["SchedulerId"] = str(scheduler_config_id)
            scheduler_dict["LoanPackageId"] = str(loan_package[0]["value"])
            scheduler_dict["Iterator"] = {"Index": 0, "Continue": True}
            scheduler_dict["LoanPackageName"] = loan_package[0]["label"]

            rule_name = rule_name.replace(" ", "")
            rule_name = rule_name + "_" + client
            event_rule = event_bridge.put_rule(Name=rule_name, ScheduleExpression=cron, State=state)

            add_target = event_bridge.put_targets(Rule=rule_name,
                                                  Targets=[
                                                      {
                                                          "Arn": os.environ["STATE_MACHINE_ARN"],
                                                          "Input": json.dumps(scheduler_dict),
                                                          "RoleArn": os.environ["STATE_MACHINE_ROLE_ARN"],
                                                          "Id": "MyTargetID"
                                                      }
                                                  ]
                                                  )
            try:
                logdin_username = event["context"]["username"]
            except:
                logdin_username = ""
            log_document["ip"] = ip
            log_document["browser"] = browser
            log_document["username"] = logdin_username
            log_document["timestamp"] = cdt
            log_document["action"] = action
            log_document["resource"] = event['body'].get("name")
            log_document["log_source"] = "Scheduler"
            # Check if there is any change/update in below fields and if there is a change then log it else ignore
            if rulesets_updated is not None:
                log_document["scheduler_rulesets"] = rulesets_updated
            if frequency_updated is not None:
                log_document["scheduler_frequency"] = frequency_updated
            if name_updated is not None:
                log_document["scheduler_name"] = name_updated
            if runs_updated is not None:
                log_document["scheduler_runs_every"] = runs_updated
            if active_updated is not None:
                log_document["scheduler_active"] = active_updated

            insert_log = db.BussinessAuditLogs.insert_one(log_document)
            return {
                "statusCode": 200,
                "message": "Scheduler configured"
            }
        except Exception as e:
            return {"statusCode": 500, "error": str(e)}
    except Exception as err:
        return {'statusCode': 500, "Error": "event schedular failed"}

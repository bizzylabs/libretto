import csv
import datetime
import io
import json
import os
import re
from collections import OrderedDict
from string import punctuation

import boto3
import pandas as pd
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]
s3 = boto3.client("s3")


def getExceldata(data, file_name, keys):
    df = pd.DataFrame(data, columns=keys)
    with io.BytesIO() as output:
        with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
            df.to_excel(writer, index=False)
        data = output.getvalue()

    s3 = boto3.resource('s3')
    s3.Bucket(BUCKET_NAME).put_object(Key=file_name, Body=data)
    return {"status": 200, "body": "file uploaded."}


def get_data_keys(query):
    final_keys = None
    try:
        initial_cond = list(db.ResultSetDetail.aggregate([
            {"$match": {"$and": query}},
            {"$group": {
                "_id": None,
                "count": {"$max": {
                    "$sum": {
                        "$size": {"$objectToArray": "$LoanInfo"}
                    }
                }},
            }}
        ]))
        if len(initial_cond) > 0:
            count = initial_cond[0].get("count")
            final_cond = list(db.ResultSetDetail.aggregate([{"$match": {"$and": query}},
                                                            {"$group": {
                                                                "_id": "$LoanInfo",
                                                                "count": {"$max": {
                                                                    "$sum": {
                                                                        "$size": {"$objectToArray": "$LoanInfo"}
                                                                    }
                                                                }},
                                                            }},
                                                            {"$match": {"count": count}}

                                                            ]))
            if len(final_cond) > 0:
                keys = list(final_cond[0]["_id"].keys())
                final_keys = []
                for key in keys:
                    val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ',
                                 key).strip()  # adding space after camelcase
                    val1 = re.search("\d{1,}", val)  # search for number after lower case
                    if val1:
                        final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                    else:
                        final_key = val
                    final_keys.append(final_key)

                final_keys.extend(["ExceptionStatus", "CreatedOn"])
    except Exception as e:
        print(e)
    return final_keys


def exception_status(system_note):
    data_dict = {"Open Exceptions": "", "Complete": "C", "Snooze": "S", "Review": "R", "Ignore": "I"}

    exception_status_string = ""
    if len(system_note) == 0:
        return exception_status_string
    else:
        for item in system_note:
            exception_status_string = exception_status_string + data_dict[item]

    return exception_status_string


def remove_special_char(filename):
    special_char_list = list(punctuation)
    special_char_list.extend(['_ ', ' '])
    for char in special_char_list:
        filename = filename.replace(char, "_")
    return filename


def get_csv_data(data, keys):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post)
    if not keys:
        keys = data[0].keys()
    csv_out.writerow(keys)
    for row in data:
        csv_out.writerow(row.values())
    print("Done")
    return data_to_post.getvalue()


def create_presigned_url(bucket_name, object_name, expiration=300):
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None
    return response


def get_search_fields(search, field):
    search = search.rstrip("/")
    criteria = {}
    if search and field:
        if search.lower() == "null":
            criteria = {"$expr": {"$eq": [f"$LoanInfo.{field}", None]}}

        elif "date" in field.lower():
            m = search.split("/")
            # For complete date format.
            if len(m) == 3:
                dtformat = "%m/%d/%Y"
                criteria = {"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}}, f"{search}"]}}
            # For month, date and year only if one number is given. eg: 11
            elif len(m) == 1:
                if len(m[0]) <= 2:
                    mnth_format = "%m"
                    date_format = "%d"
                    criteria = {"$or": [{"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{mnth_format}", "date": f"$LoanInfo.{field}"}},
                                f"{search}"]}},
                        {"$expr": {"$eq": [{"$dateToString": {"format": f"{date_format}",
                                                              "date": f"$LoanInfo.{field}"}},
                                           f"{search}"]}}]}
                if len(m[0]) == 4:
                    dtformat = "%Y"
                    criteria = {"$expr": {
                        "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}},
                                f"{search}"]}}
            # For month with year or month with date if 2 numbers are given. eg: 11/06 or 11/2021
            elif len(m) == 2:
                year_flag = False
                for i in m:
                    if len(i) == 4:
                        year_flag = True
                        break
                if year_flag:
                    # date with year or month with year
                    if int(m[0]) <= 12:
                        dtformat = "%m/%Y"
                    else:
                        print("coming here")
                        dtformat = "%d/%Y"
                else:
                    dtformat = "%m/%d"
                criteria = {"$expr": {
                    "$eq": [{"$dateToString": {"format": f"{dtformat}", "date": f"$LoanInfo.{field}"}}, f"{search}"]}}
            else:
                return {"searchMsg": "Provide either full date along with month and year or only month or only year."}
            print(criteria)

        else:
            criteria = {"$expr":
                {
                    "$regexMatch": {
                        "input": {"$toString": f"$LoanInfo.{field}"},
                        "regex": f"{search}",
                        "options": "i"
                    }
                }
            }
    return criteria


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            content_type = "text/csv;charset=utf-8"
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data, ContentType=content_type)
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def export_detail_data(event, context):
    # TODO implement
    try:
        user_name = event["context"]["username"]
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        event = event["body"]
        d = str(datetime.datetime.now().timestamp()).split('.')[0]
        filename = ""
        data_list = []
        rule_id = event.get("RuleId", None)
        rule_set_id = event.get("RuleSetId", None)
        loan_ids = event.get("LoanIds", [])
        search_word = event.get("searchWord", None)
        search_item = event.get("searchItem", None)
        filter_key = event.get("filter_key", "all")
        run_id = event.get("run_id", None)
        loan_package_id = event.get("loan_package_id")
        workflow_filters = event.get("note_filter", ["Open Exceptions"])
        workflow_items_list = ["Open Exceptions", "Complete", "Snooze", "Review", "Ignore"]
        ignore_status = False

        if type(workflow_filters) is not list:
            return {"error": "Improper workflow payload"}
        for item in workflow_filters:
            if item not in workflow_items_list:
                return {"error": "Improper workflow payload"}
        if "Open Exceptions" in workflow_filters:
            workflow_filters.remove("Open Exceptions")
            workflow_filters.append([])

        if rule_id is None or rule_set_id is None or run_id is None:
            return {"error": "Required Fields are  Missing"}
        filters = event.get("filters", None)
        condition_filters = event.get("condition_filters", None)

        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters[key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters[key]}
        else:
            loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = db.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        filter_condition["FilterInfo." + key] = {"$in": filters["Filters"][loan_package_name][key]}
                    else:
                        filter_condition["FilterInfo." + key] = {"$nin": filters["Filters"][loan_package_name][key]}

        rulename = str(list(db.Rule.find({"_id": ObjectId(rule_id)}, {"Name": 1}))[0].get("Name"))
        match_criteria = get_search_fields(search_word, search_item)
        rulesetname = str(
            list(db.RuleSetMaster.find({"_id": ObjectId(rule_set_id)}, {"RuleSetName": 1}))[0].get("RuleSetName"))
        cdt = datetime.datetime.now()
        currentstmp = str(cdt.timestamp()).split('.')[0]
        filename = "{}_{}_{}".format(user_name, rulename, currentstmp)

        filename = remove_special_char(filename) + ".xlsx"
        if filter_key == "NewExceptions":
            query = [
                {"RuleId": ObjectId(rule_id)},
                {"LoanNumber": {"$ne": None}},
                filter_condition, match_criteria,
                {"RuleSetId": ObjectId(rule_set_id)},
                {"RunId": run_id},
                {"ExceptionStatus": True},
                {"SystemNote": {"$in": workflow_filters}},
                {"$expr": {
                    "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                            {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}}
            ]
        else:

            query = [
                {"RuleId": ObjectId(rule_id)},
                {"LoanNumber": {"$ne": None}},
                filter_condition, match_criteria,
                {"RuleSetId": ObjectId(rule_set_id)},
                {"RunId": run_id},
                {"ExceptionStatus": True},
                {"SystemNote": {"$in": workflow_filters}},
            ]
        # if "Ignore" not in workflow_filters:
        #     query.append({"Ignore": ignore_status})

        loans = []
        if loan_ids:
            query.append({"LoanNumber": {"$in": loan_ids}})
            loans = db.ResultSetDetail.find(
                {"$and": query}
            )
        else:
            loans = db.ResultSetDetail.find(
                {"$and": query}
            )

        total_loan = db.ResultSetDetail.find(
            {"$and": query}
        ).count()
        if loans.count() <= 0:
            return {
                'statusCode': 200,
                'result': data_list
            }

        # Todo: If filter needed on loan info fileds then loans[:10]
        # unique_loans = []
        for loan in loans:
            loan_detail = OrderedDict()
            # if loan["LoanNumber"] not in unique_loans:
            #     unique_loans.append(loan["LoanNumber"])
            for key, value in loan["LoanInfo"].items():
                if key == "_id":
                    value = str(value)
                if "Date" in key:
                    if type(value) != str and value != None:
                        value = value.strftime('%m/%d/%Y')
                val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', key).strip()  # adding space after camelcase
                val1 = re.search("\d{1,}", val)  # search for number after lower case
                if val1:
                    final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                else:
                    final_key = val
                loan_detail[final_key] = value
            # loan_detail["CreatedOn"] = datetime.datetime.today() - loan.get("CreatedOn")
            loan_detail["ExceptionStatus"] = exception_status(loan.get("SystemNote", []))
            loan_detail["CreatedOn"] = loan.get("CreatedOn").strftime('%m/%d/%Y')

            data_list.append(loan_detail)
        # print("*****************",loan_detail)
        keys = get_data_keys(query)
        response = getExceldata(data_list, filename, keys)
        if response.get("status") == 200:
            file_link = create_presigned_url(BUCKET_NAME, filename)
        else:
            file_link = ""
        response['file_path'] = file_link
        log_document = {"Ip": ip, "browser": browser, "Username": user_name, "CreatedOn": cdt, "RunId": run_id,
                        "action": "Export", "log_from": "Details", "file_name": filename, "RuleSetName": rulesetname,
                        "RuleName": rulename}
        insert_log = db.BussinessAuditLogs.insert_one(log_document)
        return response
    except Exception as err:
        return {'statusCode': 500, "Error": "export loan details failed"}


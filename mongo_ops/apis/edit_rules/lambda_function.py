import datetime
import json
import os
from base64 import b64decode
from urllib.parse import urlparse

import boto3
from bson.objectid import ObjectId
from log_queue import push_log
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


# minimum_required_group = 'SUPER-ADMIN'

def delete_mapping(rule_set_ids, rule_id):
    temp_dict = {}
    for ruleSetId in rule_set_ids:
        rule_set_data = list(db.RuleSetMaster.find({'_id': ruleSetId}))
        if rule_set_data:
            for ruleId in rule_set_data[0]['RuleIds']:
                if ruleId['RuleId'] == rule_id:
                    rule_set_data[0]['RuleIds'].remove(ruleId)
                    temp_dict[ruleSetId] = ruleId["IsActive"]
            db.RuleSetMaster.update({'_id': ObjectId(ruleSetId)}, {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})

    return temp_dict


def delete_mapping_from_draft(rule_set_ids, rule_id):
    for ruleSetId in rule_set_ids:
        rule_set_data = list(db.RuleSetMaster.find({'_id': ruleSetId}))
        if rule_set_data:
            for mapping in rule_set_data[0]['RuleIds']:
                if mapping['ChildId'] == rule_id:
                    rule_set_data[0]['RuleIds'].remove(mapping)
            db.RuleSetMaster.update({'_id': ObjectId(ruleSetId)}, {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})

def rule_in_DB(ruleId):
    return len(list(db.Rule.find({"_id": ObjectId(ruleId)}))) == 1


def uri_validator(x):
    try:
        result = urlparse(x)
        if result.scheme in ["https", "http", "www"] or "www" in result.netloc.title():
            return True
    except Exception as e:
        return False


def get_date_time(date_string):
    """
    :str date_string: date_string should be in Month/Date/Year(10/20/2018) Format.
    :return: datetime object.
    """
    if isinstance(date_string, str):
        date_time = date_string.strip() + " 00:00:00"
        date_obj = datetime.datetime.strptime(date_time, "%m/%d/%Y %H:%M:%S")
        return date_obj
    else:
        return None


def edit_rule(event, context):
    role = event["context"].get("role", "")
    rule_id = None
    try:
        user_name = event["context"].get("username", None)
        # Todo: Fetch user_id from user table if any for CreatorId
        # Todo: check role of
        current_ruleset_id = event["body"].get("rule_set_id", None)
        start_date = event["body"].get("start_date", None)
        end_date = event["body"].get("end_date", None)
        rule_id = event["body"].get("rule_id", None)
        rule_name = event["body"].get("rule_name", None)
        description = event["body"].get("description", None)
        rule_type = event["body"].get("rule_type", None)
        rule_sub_type = event["body"].get("rule_sub_type", None)
        compliance_type = event["body"].get("compliance_type", None)
        rule_set_ids = event["body"].get("departments", [])
        rule_set_names = event["body"].get("rule_set_names", None)
        criteria = event["body"].get("criteria", "{}")
        readable_criteria = event["body"].get("readable_criteria", None)
        display_fields = event["body"].get("display_fields", None)
        masking_fields = event["body"].get("masking_fields", None)
        is_published = event["body"].get("is_published", None)
        version = event["body"].get("version", None)
        json_criteria = event["body"].get("json_criteria", {})
        rule_logic_fields = event["body"].get("rule_logic_fields", [])
        loan_package = event["body"].get("loan_package_id", [{'label': 'No Label', 'value': ""}])
        severity = event["body"].get("severity", "Medium")
        source = event["body"].get("source", "")
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        hot_link_url = event["body"].get("HotLink", "")
        external_source = event["body"].get("ExternalSource", False)

        # convert string date into datetime obj.
        start_date = get_date_time(start_date)
        end_date = get_date_time(end_date)
        if len(rule_logic_fields) > 0:
            rule_logic_fields = [item for item in rule_logic_fields if "()" not in item]

        if len(rule_set_ids) > 0:
            rule_set_ids = list(set(rule_set_ids))

        is_ui_editable = True
        priority = 2
        priority_dict = {"Critical": 0,
                         "High": 1,
                         "Medium": 2,
                         "Low": 3,
                         "Informational": 4}
        for item in priority_dict.keys():
            if severity == item:
                priority = priority_dict[item]

        # Retrun true if hot link is valid
        if hot_link_url:
            res = uri_validator(hot_link_url)
            if not res:
                return {
                    "status": 500,
                    "Error": "Hot link is not a valid one"
                }
        if not rule_in_DB(rule_id):
            return {'error': 'Wrong parameters'}
        if json_criteria and external_source == False:
            try:
                validatejson = json.loads(json.dumps(json_criteria))
            except Exception as e:
                return {
                    "error": "json_criteria is not json serialize. Give a valid json"
                }
        log = {"action": "RuleSetAssign/Unassign", "username": user_name, "email": "", "ip": ip,
               "browser": browser, "timestamp": str(datetime.datetime.now()), "PrevRuleId": "",
               "PrevRuleSetId": "", "PrevRuleNameId": "",
               "PrevVersion": "", "PrevRuleName": "", "LatestRuleId": "",
               "LatestRuleSetId": "", "LatestRuleNameId": "",
               "LatestVersion": "", "LatestRuleName": ""}
        if is_published == 1 and external_source == False and None in [rule_name, criteria, severity, source, rule_type,
                                                                       rule_sub_type, compliance_type, display_fields,
                                                                       is_published,
                                                                       rule_logic_fields] and len(rule_set_ids) <= 0:
            return {
                "error": "Missing Required fields"
            }
        elif rule_id is None:
            return {
                "error": "Missing Required fields"
            }
        else:
            print(rule_id, rule_name, criteria, rule_set_ids, rule_type, rule_sub_type, version, is_published)

            rule_set_ids = [ObjectId(rule_set_id) for rule_set_id in rule_set_ids]
            rule_id = ObjectId(rule_id)
            # check in draft rule
            # draft_rule = db.RuleSetMaster.find({"RuleIds": {"$elemMatch": {"ChildId": rule_id}}})
            match_criteria = {"$match": {"RuleIds": {"$elemMatch": {"ChildId": rule_id}}}}
            group_query = {"$group": {"_id": None, "count": {"$sum": 1}}}
            ruleset_db = db.RuleSetMaster.aggregate([match_criteria, group_query])
            draft_rule = [data for data in ruleset_db]
            draft_rule_count = 0
            if draft_rule:
                draft_rule_count = draft_rule[0].get('count', 0)
            # update RuleSetMaster if published
            if rule_type is not "":
                rule_type = ObjectId(rule_type)
            if rule_sub_type is not "":
                rule_sub_type = ObjectId(rule_sub_type)
            if compliance_type is not "":
                compliance_type = ObjectId(compliance_type)
            criteria = b64decode(criteria).decode('utf-8')

            temp_rule = db.Rule.find_one({"_id": rule_id})
            rule_name_id = temp_rule["RuleNameId"]
            created_by = temp_rule.get("CreatedBy")
            created_on = temp_rule.get("CreatedOn")
            log["PrevRuleId"] = str(temp_rule["_id"])
            log["PrevRuleName"] = temp_rule["Name"]
            log["PrevRuleNameId"] = temp_rule["RuleNameId"]
            log["PrevVersion"] = temp_rule["Version"]
            delete_log = {"action": "RuleDeleted", "username": user_name, "email": "", "ip": ip,
                          "browser": browser, "timestamp": str(datetime.datetime.now()),
                          "RuleId": str(temp_rule["_id"]),
                          "RuleSetId": [], "RuleNameId": temp_rule["RuleNameId"],
                          "Version": temp_rule["Version"], "RuleName": temp_rule["Name"]}
            core_lib_flag = temp_rule["CoreLibFlag"]
            is_editable = temp_rule["IsEditable"]
            if not created_by:
                created_by = user_name

            if temp_rule["CoreLibFlag"] == 1:
                bizzy_department = temp_rule["BizzyDepartment"]

            if temp_rule["CoreLibFlag"] == 1 and temp_rule["IsEditable"] == 0:
                core_lib_flag = 1
                is_editable = 0
                criteria = temp_rule["Criteria"]
                try:
                    json_criteria = temp_rule["JsonCriteria"]
                except Exception as e:
                    pass
                rule_logic_fields = temp_rule["RuleLogicFields"]
                rule_name = temp_rule["Name"]
                description = temp_rule["Description"]
                rule_type = temp_rule["RuleType"]
                rule_sub_type = temp_rule["RuleSubType"]
                source = temp_rule.get("Source", "")

            if temp_rule.get("isUiEditable", True) == False:
                criteria = temp_rule["Criteria"]
                is_ui_editable = temp_rule.get("isUiEditable", True)
                try:
                    json_criteria = temp_rule["JsonCriteria"]
                except Exception as e:
                    pass
            rule_detail = {
                "AuthorName": user_name,
                "RuleType": rule_type,
                "RuleSubType": rule_sub_type,
                "ComplianceType": compliance_type,
                "MaskingFieldsCriteria": masking_fields,
                "CoreLibFlag": core_lib_flag,
                "IsEditable": is_editable,
                "isUiEditable": is_ui_editable,
                "IsActive": 1,
                "DeleteStatus": 0,
                "Departments": rule_set_ids,
                "Name": rule_name,
                "RuleNameId": rule_name_id,
                "Description": description,
                "ReadableCriteria": readable_criteria,
                "Criteria": criteria,
                "JsonCriteria": json_criteria,
                "DisplayFields": str(display_fields),
                "MaskingFields": str(masking_fields),
                "IsPublished": is_published,
                "Version": version,
                "CreatedOn": created_on,
                "UpdatedOn": datetime.datetime.now(),
                "RuleLogicFields": rule_logic_fields,
                "LoanPackage": loan_package,
                "Severity": severity,
                "Source": source,
                "CreatedBy": created_by,
                "Priority": priority,
                "UpdatedBy": user_name,
                "Slug": rule_name.lower(),
                "HotLink": hot_link_url,
                "ExternalSource": external_source,
                "StartDate": start_date,
                "EndDate": end_date
            }
            # check if adding rule from bizzy
            app_detail = db.ApplicationDetails.find_one({})
            if app_detail['Client'].lower() == "bizzy":
                rule_detail["CoreLibFlag"] = 1
                rule_detail["IsEditable"] = 1

            if rule_detail["CoreLibFlag"] == 1:
                rule_detail["BizzyDepartment"] = bizzy_department

            if is_published == 1 and draft_rule_count > 0:
                rule_set_mappings = {}
                try:
                    parentId = ""
                    returned_ruleset_id = str(rule_set_ids[0])
                    rule_mappings = list(db.RuleSetMaster.find({'_id': ObjectId("5f1af96897d568eae6cca2cf")}))[0][
                        "RuleIds"]
                    for mapping in rule_mappings:
                        if mapping["ChildId"] == rule_id:
                            parentId = mapping["ParentId"]
                    if parentId:
                        parentDepartments = list(db.Rule.find({'_id': parentId}))[0]['Departments']
                        rule_set_mappings = delete_mapping(parentDepartments, parentId)
                    old_rule_set_ids = []
                    if len(rule_set_mappings.keys()) > 0:
                        old_rule_set_ids = list(rule_set_mappings.keys())
                    if len(old_rule_set_ids) > 0:
                        rule_set_log = list(set(old_rule_set_ids) - set(rule_set_ids))
                        delete_log["RuleSetId"] = rule_set_log
                    db.Rule.update({"_id": rule_id}, {"$set": rule_detail})
                    for rule_set_id in rule_set_ids:
                        db.RuleSetMaster.update(
                            {"_id": rule_set_id},
                            {"$push":
                                 {"RuleIds":
                                      {"RuleId": rule_id, "Version": version,
                                       "IsActive": rule_set_mappings.get(rule_set_id, 1),
                                       "EndDate": end_date,
                                       "StartDate": start_date
                                       }
                                  }
                             }
                        )
                    if rule_detail["CoreLibFlag"] == 1 and rule_detail["IsEditable"] == 0:
                        pass
                    else:
                        db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    delete_mapping_from_draft([ObjectId("5f1af96897d568eae6cca2cf")], rule_id)
                    log["PrevRuleSetId"] = ["5f1af96897d568eae6cca2cf"]
                    log["LatestVersion"] = version
                    log["LatestRuleNameId"] = rule_name_id
                    log["LatestRuleId"] = str(rule_id)
                    log["LatestRuleSetId"] = [str(item) for item in rule_set_ids]
                    log["LatestRuleName"] = rule_name

                    # because user has published locked rule
                    # removing from lock table.
                    db.LockRules.delete_one({"RuleId": rule_id})
                    # Although parent will no longer exists because-Removed from mapping,
                    # delte for parent locking too.
                    db.LockRules.delete_one({"RuleId": parentId})
                    push_log.push(log)
                    if len(delete_log["RuleSetId"]) > 0:
                        db.RuleHistory.insert_one(delete_log)
                except Exception as e:
                    db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    return {"error": "Error while publishing from Draft - " + str(e)}

            elif is_published == 1 and draft_rule_count == 0:
                rule_set_mappings = {}
                try:
                    returned_ruleset_id = str(rule_set_ids[0])
                    old_departments = list(db.Rule.find({'_id': rule_id}))[0]['Departments']
                    rule_set_mappings = delete_mapping(old_departments, rule_id)
                    old_rule_set_ids = []
                    if len(rule_set_mappings.keys()) > 0:
                        old_rule_set_ids = list(rule_set_mappings.keys())
                    if len(old_rule_set_ids) > 0:
                        rule_set_log = list(set(old_rule_set_ids) - set(rule_set_ids))
                        delete_log["RuleSetId"] = rule_set_log
                    if rule_detail["CoreLibFlag"] == 1 and rule_detail["IsEditable"] == 0:
                        version = float("{:.3f}".format(version + 0.001))
                    else:
                        version = version + 1
                    rule_detail["Version"] = version
                    new_rule_id = db.Rule.insert_one(rule_detail).inserted_id
                    for rule_set_id in rule_set_ids:
                        db.RuleSetMaster.update(
                            {"_id": rule_set_id},
                            {"$push":
                                 {"RuleIds":
                                      {"RuleId": new_rule_id, "Version": version,
                                       "IsActive": rule_set_mappings.get(rule_set_id, 1),
                                       "EndDate": end_date,
                                       "StartDate": start_date}
                                  # Todo: replace with version
                                  }
                             }
                        )

                    if rule_detail["CoreLibFlag"] == 1 and rule_detail["IsEditable"] == 0:
                        pass
                    else:
                        db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})

                    log["PrevRuleSetId"] = [str(item) for item in old_departments]
                    log["LatestVersion"] = version
                    log["LatestRuleNameId"] = rule_name_id
                    log["LatestRuleId"] = str(new_rule_id)
                    log["LatestRuleSetId"] = [str(item) for item in rule_set_ids]
                    log["LatestRuleName"] = rule_name
                    push_log.push(log)
                    if len(delete_log["RuleSetId"]) > 0:
                        db.RuleHistory.insert_one(delete_log)
                except Exception as e:
                    db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    return {"error": "Error while publishing from ruleset - " + str(e)}

            elif is_published == 0 and draft_rule_count == 0:
                try:
                    if rule_detail["CoreLibFlag"] == 1 and rule_detail["IsEditable"] == 0:
                        version = float("{:.3f}".format(version + 0.001))
                    else:
                        version = version + 1
                    rule_detail["Version"] = version
                    new_rule_id = db.Rule.insert_one(rule_detail).inserted_id
                    db.RuleSetMaster.update(
                        {"RuleSetName": "Draft Rules"},
                        {"$push":
                             {"RuleIds":
                                  {"ParentId": rule_id, "ChildId": new_rule_id}
                              }
                         }
                    )
                    if rule_detail["CoreLibFlag"] == 1 and rule_detail["IsEditable"] == 0:
                        pass
                    else:
                        db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    # update with find_one()
                    returned_ruleset_id = str(
                        list(db.RuleSetMaster.find({"RuleSetName": "Draft Rules"}, {"_id": 1}))[0].get("_id"))
                except Exception as e:
                    db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    return {"error": "Error while saving from ruleset - " + str(e)}

            elif is_published == 0 and draft_rule_count > 0:
                try:
                    # rule_detail["ParentId"] = list(db.Rule.find({"_id": rule_id}))[0]["ParentId"]
                    db.Rule.update({'_id': rule_id}, {"$set": rule_detail})
                    # because user has again saved to edit in future
                    # removing from lock table. so other can edit.
                    db.LockRules.delete_one({"RuleId": rule_id})
                    returned_ruleset_id = str(
                        list(db.RuleSetMaster.find({"RuleSetName": "Draft Rules"}, {"_id": 1}))[0].get("_id"))
                except Exception as e:
                    db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
                    return {"error": "Error while saving from Draft - " + str(e)}

            return {
                "status": 200,
                "returned_ruleset_id": returned_ruleset_id,
                "message": "Updated successfully"
            }
    except Exception as err:
        db.Rule.update({'_id': rule_id}, {"$set": {'IsEditable': 1}})
        return {
            "error": "Unable to edit rule"
        }

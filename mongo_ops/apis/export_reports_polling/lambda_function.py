import json
import boto3
import os

client = boto3.client("secretsmanager")
mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
BUCKET_NAME = mongo_dict["export_bucket"]

s3 = boto3.client('s3', region_name="us-east-1")


def create_presigned_url(bucket_name, object_name, expiration=60):
    try:
        response = s3.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=expiration)
    except Exception as e:
        print(e)
        return None
    return response


def lambda_handler(event, context):
    try:
        file_name = event["body"]["file_name"]
        obj = s3.get_object(Bucket=BUCKET_NAME, Key=file_name)
        file_path = create_presigned_url(BUCKET_NAME, file_name)
        return {
            'statusCode': 200,
            'file_path': file_path}
    except Exception as e:
        print(e)
        return {'statusCode': 500, 'Error': 'File not present'}

import csv
import io
import json
import os
import re
from base64 import b64decode
from collections import OrderedDict
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]
BUCKET_NAME = secret_dict["export_bucket"]

null = None
true = True
false = False


def data_formatter(projection, data):
    non_displayable_field = [key for key in projection.keys() if not data.get(key, None)]
    projection.update(data)
    for item in non_displayable_field:
        projection[item] = None
    return projection


def get_csv_data(data):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post)
    if len(data) > 0:
        keys = data[0].keys()
        csv_out.writerow(keys)
        for row in data:
            csv_out.writerow(row.values())
        print("Done")
        return data_to_post.getvalue()
    else:
        csv_out.writerow("")
        return data_to_post.getvalue()


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            s3 = boto3.client("s3")
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data)
            s3 = boto3.resource('s3')
            object_acl = s3.ObjectAcl(bucket_name, file_name)
            response = object_acl.put(ACL='public-read')
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def lambda_handler(event, context):
    data_list = []
    loans = []
    condition_list = []
    try:
        user_name = event["context"].get("username")
        criteria = event["body"].get("criteria", "{}")
        display_fields = event["body"].get("display_fields", "{}")
        loan_package_id = event["body"].get("loan_package_id")
        file_name = event["body"].get("filename")
        loan_sort_key = event["body"].get("loan_sort_key", "LoanNumber")
        filters = event["body"].get("filter_query", "{'$and':[]}")
        asc_key = -1 if event["body"].get("asc", "N") == "N" else 1
        cfilters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        limit = event.get("limit", 100)
        offset = event.get("offset", 0)
        if not loan_package_id:
            return {"statusCode": 500, "Error": "Loan package id missing"}
        if not file_name:
            return {"statusCode": 500, "Error": "filename field is  missing"}

        if criteria:
            try:
                criteria = eval(b64decode(criteria).decode('utf-8'))
                temp = {"LoanNumber": 1}
                temp.update(eval(display_fields))
                display_fields = temp.copy()
            except Exception as err:
                print(err)
                return {"statusCode": 500, "Error": "Query not supported."}
        if cfilters and condition_filters:
            for key in cfilters:
                if len(cfilters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {key: {"$in": cfilters[key]}}
                        condition_list.append(cond)
                    else:
                        cond = {key: {"$nin": cfilters[key]}}
                        condition_list.append(cond)
        else:
            loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = db.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {key: {"$in": filters["Filters"][loan_package_name][key]}}
                        condition_list.append(cond)
                    else:
                        cond = {key: {"$nin": filters["Filters"][loan_package_name][key]}}
                        condition_list.append(cond)
        condition_list.append(criteria)
        loan_collection_name = db.RuleTestingLoanData.find_one({"LoanPackageId": ObjectId(loan_package_id), "IsActive": 1})
        if not loan_collection_name:
            return {"statusCode": 500, "Error": "Either data processing is going on or data is not present"}
        if asc_key == -1:
            loans = db[loan_collection_name["LoanDataCollectionName"]].find({'$and': condition_list}, display_fields).sort(
                [(loan_sort_key, -1)]).limit(limit).skip(offset)
        else:
            loans = db[loan_collection_name["LoanDataCollectionName"]].find({'$and': condition_list}, display_fields).limit(
                limit).skip(offset)
        count = db[loan_collection_name["LoanDataCollectionName"]].find({'$and': condition_list}, display_fields).count()
        for loan in loans:
            formatted_data = data_formatter(display_fields, loan)
            loan_detail = OrderedDict()
            for key, value in formatted_data.items():
                if key == "_id":
                    value = str(value)

                if "date" in key.lower():
                    if type(value) != str and value:
                        value = value.strftime('%m/%d/%Y')
                val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', key).strip()  # adding space after camelcase
                val1 = re.search("\d{1,}", val)  # search for number after lower case
                if val1:
                    final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                else:
                    final_key = val
                loan_detail[final_key] = value
            loan_detail["TotalCount"] = count
            data_list.append(loan_detail)
        if data_list and loan_sort_key:
            final_data = []
            non_key_data = []
            val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ',
                         loan_sort_key).strip()  # adding space after camelcase
            val1 = re.search("\d{1,}", val)  # search for number after lower case
            if val1:
                key_value = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
            else:
                key_value = val
            if "date" in key_value.lower():
                for item in data_list:
                    itm_exist = item.get(key_value, "")
                    if itm_exist:
                        d = item[key_value]
                        s = datetime.strptime(d, '%m/%d/%Y')
                        item["temp_date"] = s
                        final_data.append(item)
                    else:
                        non_key_data.append(item)

                data = sorted(final_data, key=lambda i: i["temp_date"])
                for doc in data:
                    del doc["temp_date"]
            else:
                for item in data_list:
                    itm_exist = item.get(key_value, "")
                    if itm_exist:
                        final_data.append(item)
                    else:
                        non_key_data.append(item)
                data = sorted(final_data, key=lambda i: i[key_value])
                # data = data_list

            data.extend(non_key_data)
            if asc_key == -1:
                data = data[::-1]

            csv_data = get_csv_data(data)
            response = save_data(BUCKET_NAME, csv_data, file_name)
            return {
                "statusCode": 200,
                "filename": file_name,
                "total_count": count
            }
        csv_data = get_csv_data(data_list)
        response = save_data(BUCKET_NAME, csv_data, file_name)
        return {
            "statusCode": 200,
            "filename": file_name,
            "total_count": count
        }
    except Exception as err:
        return {'statusCode': 500, "error": "test rule from rule editor failed"}
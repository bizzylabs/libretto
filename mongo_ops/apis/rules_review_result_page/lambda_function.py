import json
import os
from collections import OrderedDict
import datetime
import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient


def get_database():
    secret = boto3.client("secretsmanager", region_name="us-east-1")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


null = None
false = False
true = True

db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


database = get_initialized_db()


def formatter(value):
    return "{:,}".format(value)


def date_to_str(data):
    """datetime data: covert data into string format. (%d/%m/%Y)"""
    record = data.strftime('%m/%d/%Y') if isinstance(data, datetime.datetime) else ''
    return record


def get_zero_exception_count(database, rule_set_id, run_id, rule_count_condition, new_count_condition):
    count = 0
    if rule_set_id is not None:
        pipe = [
            {"$match": {"$and": [{
                "RunId": run_id,
                "RuleSetId": ObjectId(rule_set_id),
                "ExceptionStatus": True}
            ]}},
            {"$group": {
                "_id": {"rule_set_id": "$RuleSetId",
                        "rule_id": "$RuleId"
                        },
                "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                "priority": {"$first": "$Severity"},
                "sort_priority": {"$first": "$Priority"},
                "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}}
            }},
            {"$group": {
                "_id": "$_id.rule_set_id",
                "Rules": {
                    "$push": {
                        "RuleId": "$_id.rule_id",
                        "exceptions": "$rule_count",
                        "new": "$new_count",
                        "severity": "$priority",
                        "priority": "$sort_priority"
                    },
                }
            }},
            {"$unwind": "$Rules"},
            {"$match": {"Rules.exceptions": {"$eq": 0}}}
        ]
        result_set_detail_data = database.ResultSetDetail.aggregate(pipe)
        for item in result_set_detail_data:
            count += 1

    return count


def get_all_data_count(database, rule_set_id, data_filter, run_id, rule_count_condition, new_count_condition):
    count = 0
    if rule_set_id is not None:
        pipe = [
            {"$match": {"$and": [{
                "RunId": run_id,
                "ExceptionStatus": True,
                "RuleSetId": ObjectId(rule_set_id)}

            ]}},
            {"$group": {
                "_id": {"rule_set_id": "$RuleSetId",
                        "rule_id": "$RuleId"
                        },
                "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                "priority": {"$first": "$Severity"},
                "sort_priority": {"$first": "$Priority"},
                "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}}
            }},
            {"$group": {
                "_id": "$_id.rule_set_id",
                "Rules": {
                    "$push": {
                        "RuleId": "$_id.rule_id",
                        "exceptions": "$rule_count",
                        "new": "$new_count",
                        "severity": "$priority",
                        "priority": "$sort_priority"
                    },
                }
            }},
            {"$unwind": "$Rules"},
            {"$match": data_filter}
        ]
        result_set_detail_data = database.ResultSetDetail.aggregate(pipe)
        try:
            temp_data = [item["Rules"].get("RuleId") for item in result_set_detail_data if item.get("Rules", None)]
            count = len(temp_data)
        except Exception as e:
            pass
    return count


def get_filter_template(filter):
    if filter == "ZeroExceptions":
        return {"Rules.exceptions": {"$eq": 0}}
    elif filter == "WithExceptions":
        return {"Rules.exceptions": {"$gt": 0}}
    elif filter == "NewExceptions":
        return {"Rules.new": {"$gt": 0}}
    else:
        return {}


def valid_user(user_name, rule_set_id):
    rule_set = database.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id), "Users": {"$in": [user_name]}})
    if rule_set:
        return True


def get_rule_data(event, context):
    temp_out = []
    output = []
    final_output = []
    sorted_output = []
    try:
        user_name = event["context"]["username"]
        rule_detail = OrderedDict()
        loan_package_id = event["body"].get("loan_package_id")
        rule_set_id = event['body'].get("ruleSetId", None)
        rule_id = event['body'].get("ruleId", None)
        data_filter = get_filter_template(event['body'].get("filter", "all"))
        offset = event['body'].get("offset", 0)
        limit = event['body'].get("limit", 10)
        sort_key = event['body'].get("sortby", "Name")
        asc_key = -1 if event['body'].get("asc", "N") == "N" else 1
        exception_type = event["body"].get("exception_type", None)

        if not valid_user(user_name, rule_set_id):
            return {"result": output, "ZeroExceptionsFlag": 0, 'total_loan': 0, 'statusCode': 500}

        if sort_key == "Exception":
            sort_key = "Rules.exceptions"
        elif sort_key == "NewException":
            sort_key = "Rules.new"
        elif sort_key == "RuleType":
            sort_key = "RuleType.Name"
        elif sort_key == "RuleSubType":
            sort_key = "RuleSubType.Name"
        elif sort_key == "ComplianceType":
            sort_key = "ComplianceType.Name"
        elif sort_key == "Severity":
            sort_key = "Rules.priority"
        elif sort_key == "Name":
            sort_key = "RuleData.Slug"

        if exception_type == "open":
            exception_filter = {"$eq": ["$SystemNote", []]}
        else:
            exception_filter = {}

        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)

        rule_count_condition = [exception_filter, {"$eq": ["$ExceptionStatus", True]}, {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]},
                               {"$ne": ["$LoanNumber", None]}, {
                                   "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                           {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}]

        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)

                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)

        else:
            loan_package_name = database.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = database.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                        rule_count_condition.append(cond)
                        new_count_condition.append(cond)

        if rule_set_id is not None:
            run_id = event['body'].get("run_id")
            rule_set = database.RuleSetMaster.find_one({"_id": ObjectId(rule_set_id)},
                                                       {"_id": 0, "RuleSetName": 1, "RuleIds": 1})
            if limit == "all":
                pipe = [
                    {"$match": {"$and": [{
                        "RunId": run_id,
                        "RuleSetId": ObjectId(rule_set_id),
                        "ExceptionStatus": True
                    }
                    ]}},
                    {"$group": {
                        "_id": {"rule_set_id": "$RuleSetId",
                                "rule_id": "$RuleId"
                                },
                        "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                        "priority": {"$first": "$Severity"},
                        "sort_priority": {"$first": "$Priority"},
                        "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}}
                    }},
                    {"$group": {
                        "_id": "$_id.rule_set_id",
                        "Rules": {
                            "$push": {
                                "RuleId": "$_id.rule_id",
                                "exceptions": "$rule_count",
                                "new": "$new_count",
                                "severity": "$priority",
                                "priority": "$sort_priority"
                            },
                        }
                    }},
                    {"$unwind": "$Rules"},
                    {"$match": data_filter},
                    {"$lookup":
                        {
                            "from": "Rule",
                            "let": {"rule_id": "$Rules.RuleId"},
                            "pipeline": [
                                {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                            ],
                            "as": "RuleData"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "RuleType",
                            "localField": "RuleData.RuleType",
                            "foreignField": "_id",
                            "as": "RuleType"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "RuleSubType",
                            "localField": "RuleData.RuleSubType",
                            "foreignField": "_id",
                            "as": "RuleSubType"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "ComplianceType",
                            "localField": "RuleData.ComplianceType",
                            "foreignField": "_id",
                            "as": "ComplianceType"
                        }
                    },
                    {"$sort": {sort_key: asc_key}},
                ]
            else:
                pipe = [
                    {"$match": {"$and": [{
                        "RunId": run_id,
                        "RuleSetId": ObjectId(rule_set_id),
                        "ExceptionStatus": True}
                    ]}},
                    {"$group": {
                        "_id": {"rule_set_id": "$RuleSetId",
                                "rule_id": "$RuleId"
                                },
                        "rule_count": {"$sum": {"$cond": [{"$and": rule_count_condition}, 1, 0]}},
                        "priority": {"$first": "$Severity"},
                        "sort_priority": {"$first": "$Priority"},
                        "new_count": {"$sum": {"$cond": [{"$and": new_count_condition}, 1, 0]}}
                    }},
                    {"$group": {
                        "_id": "$_id.rule_set_id",
                        "Rules": {
                            "$push": {
                                "RuleId": "$_id.rule_id",
                                "exceptions": "$rule_count",
                                "new": "$new_count",
                                "severity": "$priority",
                                "priority": "$sort_priority"
                            },
                        }
                    }},
                    {"$unwind": "$Rules"},
                    {"$match": data_filter},
                    {"$lookup":
                        {
                            "from": "Rule",
                            "let": {"rule_id": "$Rules.RuleId"},
                            "pipeline": [
                                {"$match": {"$expr": {"$eq": ["$_id", "$$rule_id"]}}}
                            ],
                            "as": "RuleData"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "RuleType",
                            "localField": "RuleData.RuleType",
                            "foreignField": "_id",
                            "as": "RuleType"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "RuleSubType",
                            "localField": "RuleData.RuleSubType",
                            "foreignField": "_id",
                            "as": "RuleSubType"
                        }
                    },
                    {"$lookup":
                        {
                            "from": "ComplianceType",
                            "localField": "RuleData.ComplianceType",
                            "foreignField": "_id",
                            "as": "ComplianceType"
                        }
                    },
                    {"$sort": {sort_key: asc_key}},
                    {"$skip": offset},
                    {"$limit": limit}
                ]
            result_set_detail_data = database.ResultSetDetail.aggregate(pipe)
            # rule_status = {}
            # for rule in rule_set.get("RuleIds"):
            #     rule_status[rule["RuleId"]] = rule

            for rule in result_set_detail_data:
                rule_detail = OrderedDict()
                try:
                    rule_data = rule["RuleData"][0]
                except Exception as e:
                    rule_data = database.Rule.find_one({"_id": ObjectId(rule["Rules"]["RuleId"])})
                rule_detail["_id"] = str(rule["Rules"]["RuleId"])
                rule_detail["AuthorName"] = rule_data["AuthorName"]
                rule_detail["Name"] = rule_data["Name"]
                rule_detail["Slug"] = rule_data.get("Slug", rule_data["Name"].lower())
                rule_detail["Description"] = rule_data["Description"]
                rule_detail["RuleNameId"] = rule_data["RuleNameId"]

                try:
                    rule_detail["RuleType"] = rule["RuleType"][0]["Name"]
                except Exception as e:
                    rule_detail["RuleType"] = ""
                try:
                    rule_detail["RuleSubType"] = rule["RuleSubType"][0]["Name"]
                except Exception as e:
                    rule_detail["RuleSubType"] = ""
                try:
                    rule_detail["ComplianceType"] = rule["ComplianceType"][0]["Name"]
                except Exception as e:
                    rule_detail["ComplianceType"] = ""

                rule_detail["Criteria"] = eval(rule_data["Criteria"])
                rule_detail["DisplayFields"] = eval(rule_data["DisplayFields"])
                rule_detail["MaskingFieldsCriteria"] = rule_data["MaskingFieldsCriteria"]
                rule_detail["IsActive"] = rule_data["IsActive"]
                rule_detail["CoreLibFlag"] = rule_data["CoreLibFlag"]
                if rule_detail["CoreLibFlag"] == 0:
                    client = database.ApplicationDetails.find_one({})['Client']
                    rule_detail["ClientRule"] = client + ' Rule'
                rule_detail["IsEditable"] = rule_data["IsEditable"]
                rule_detail["IsPublished"] = rule_data["IsPublished"]
                rule_detail["Severity"] = rule["Rules"].get("severity")
                rule_detail["Priority"] = rule["Rules"].get("priority", 2)
                rule_detail["JsonCriteria"] = rule_data.get("JsonCriteria", "")
                rule_detail["Source"] = rule_data.get("Source", "")
                rule_detail["CreatedOn"] = rule_data["CreatedOn"].isoformat()
                rule_detail["UpdatedOn"] = rule_data["UpdatedOn"].isoformat()
                rule_detail["CreatedBy"] = rule_data.get("CreatedBy", "")
                rule_detail["UpdatedBy"] = rule_data.get("UpdatedBy", "")
                rule_detail["StartDate"] = date_to_str(rule_data.get("StartDate", ""))
                rule_detail["EndDate"] = date_to_str(rule_data.get("EndDate", ""))
                rule_detail["Version"] = rule_data["Version"]
                rule_detail["ExternalSource"] = rule_data.get("ExternalSource", False)
                rule_detail["ReadableCriteria"] = rule_data["ReadableCriteria"]
                rule_detail["Departments"] = [str(dept) for dept in rule_data["Departments"]]
                rule_detail["Exception"] = formatter(rule["Rules"]["exceptions"])
                rule_detail["NewException"] = formatter(rule["Rules"]["new"])
                rule_summary_data = database.RuleSummary.find_one(
                    {"RunId": run_id, "RuleSetId": ObjectId(rule_set_id),
                     "RuleId": ObjectId(str(rule["Rules"]["RuleId"]))},
                    {"ExecutionCompleted": 1}).get("ExecutionCompleted", False)
                if not rule_summary_data:
                    rule_detail["Exception"] = "N/A"
                    rule_detail["NewException"] = "N/A"
                output.append(rule_detail)
            try:
                count = get_zero_exception_count(database, rule_set_id, run_id, rule_count_condition,
                                                 new_count_condition)
                total_loan = get_all_data_count(database, rule_set_id, data_filter, run_id, rule_count_condition,
                                                new_count_condition)
                return {"result": output, "ZeroExceptionsFlag": count, 'total_loan': total_loan, 'statusCode': 200}
            except Exception as e:
                return {"result": e, "ZeroExceptionsFlag": 0, 'total_loan': 0, 'statusCode': 500}
    except Exception as err:
        return {'statusCode': 500, "Error": "rules review result page failed"}

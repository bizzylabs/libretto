import csv
import datetime
import io
import json
import os
import boto3
import botocore
from pymongo import MongoClient

lambda_invoke = boto3.client("lambda", region_name="us-east-1")
client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]
BUCKET_NAME = mongo_dict["export_bucket"]


def get_csv_data(data):
    data_to_post = io.StringIO()
    csv_out = csv.writer(data_to_post)
    keys = data[0].keys()
    csv_out.writerow(keys)
    for row in data:
        csv_out.writerow(row.values())
    print("Done")
    return data_to_post.getvalue()


def save_data(bucket_name, data, file_name):
    if data is None:
        raise S3Error("csv data can not be empty.")
    else:
        try:
            s3 = boto3.client("s3")
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=data)
            s3 = boto3.resource('s3')
            object_acl = s3.ObjectAcl(bucket_name, file_name)
            response = object_acl.put(ACL='public-read')
            return {"status": 200, "body": "file uploaded."}
        except Exception as e:
            print(e)
            return {"status": 400, "body": "file upload failed."}


def extract_date_details(date):
    date_string = (" ").join(date.split(" ")[1:-1])
    converted_datetime = datetime.datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
    return converted_datetime.replace(hour=0, minute=0, second=0, microsecond=0)


# def getUserCognitoGroups(username):
#     user_groups = []
#     try:
#         result = cognito.admin_list_groups_for_user(
#             Username=username,
#             UserPoolId=user_pool_id
#         )
#         for grp in result["Groups"]:
#             user_groups.append(grp["GroupName"])
#     except botocore.exceptions.ClientError as e:
#         print("username :" + username)
#         print(str(e))
#         pass
#     return user_groups


# def get_user_details(users_first_last_names, cognito_user):
#     """
#     :dict cognito_user: contains detail for user data like first name, last name, email.
#     """
#     for item in cognito_user['Attributes']:
#         users_first_last_names[cognito_user["Username"]][item['Name']] = item['Value']
#     return users_first_last_names


def generate_user_logout_history(event):
    """dict event: {"from_date": gmt_from_date, "to_date": gmt_to_date} """
    try:
        null = None
        false = False
        true = True

        current_time = datetime.datetime.now() + datetime.timedelta(
            hours=24)  # To add the logs during current day as well
        yesterdays_date = (datetime.datetime.now() - datetime.timedelta(hours=24))

        gmt_from_date = event.get("from_date", yesterdays_date)
        gmt_to_date = event.get("to_date", current_time)

        from_date = extract_date_details(gmt_from_date)
        to_date = extract_date_details(gmt_to_date)

        login_pipe = [{"$match": {"$expr": {
            "$and": [{"$eq": ["$action", "login"]}, {"$gte": ["$timestamp", from_date]},
                     {"$lte": ["$timestamp", to_date]}]}}}, {"$project": {"_id": 0}}]
        login_logs = db.BussinessAuditLogs.aggregate(login_pipe)

        login_count = 0
        user_logout_logger = []
        # Iterate each user's login log and check for logout within the interval else append a log to logout logger list
        for user_login_log in login_logs:
            login_count += 1

            # Get 24 hour interval from the login of the user
            username = user_login_log["username"]
            from_datetime = user_login_log["timestamp"]
            to_datetime = user_login_log["timestamp"] + datetime.timedelta(hours=24)
            pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$username", username]}, {"$gt": ["$timestamp", from_datetime]},
                         {"$lte": ["$timestamp", to_datetime]}]}}}]
            user_interval_logs = db.BussinessAuditLogs.aggregate(pipe)

            current_login_time = user_login_log["timestamp"]
            min_login_diff = datetime.timedelta(hours=24)
            min_logout_diff = datetime.timedelta(hours=24)
            min_timeout_diff = datetime.timedelta(hours=24)
            min_login_time = current_login_time + datetime.timedelta(seconds=2)
            for user_log in user_interval_logs:
                user_log_diff = user_log["timestamp"] - current_login_time
                if user_log["action"] == "login" and user_log_diff < min_login_diff:
                    min_login_diff = user_log_diff
                    min_login_time = user_log["timestamp"]
                elif user_log["action"] == "logout" and user_log_diff < min_logout_diff:
                    min_logout_diff = user_log_diff
                elif user_log["action"] == "timeout" and user_log_diff < min_timeout_diff:
                    min_timeout_diff = user_log_diff

            # if this min_logout or min_timeout is greater than min_login -> meaning there is no logout or timeout between two logins. If so, then
            if min_logout_diff > min_login_diff and min_timeout_diff > min_login_diff and min_login_time > current_login_time + datetime.timedelta(
                    seconds=2):
                # if the min_login is within 15 minutes from the current_login the create a logout with 1 minutes before the  min_login timestamp
                if min_login_diff <= datetime.timedelta(minutes=15):
                    new_logout_log = user_login_log
                    new_logout_log["action"] = "logout"
                    if min_login_diff <= datetime.timedelta(minutes=1):
                        new_logout_log["timestamp"] = min_login_time - datetime.timedelta(
                            seconds=1)  # logout 1 second before next login
                    else:
                        new_logout_log["timestamp"] = min_login_time - datetime.timedelta(
                            minutes=1)  # logout 1 minute before next login
                    user_logout_logger.append(new_logout_log)
                # if the min_login is not within 15 minutes from the current_login then create a logout with 15 minutes from current_login.
                elif min_login_diff > datetime.timedelta(minutes=15):
                    new_logout_log = user_login_log
                    new_logout_log["action"] = "logout"
                    new_logout_log["timestamp"] = current_login_time + datetime.timedelta(
                        minutes=15)  # logout 15 minutes after the current login
                    user_logout_logger.append(new_logout_log)

        # write logout_logger into the DB
        for log in user_logout_logger:
            db.BussinessAuditLogs.insert(log)

        return True

    except Exception as e:
        print({'statusCode': 500, "Error": "generate user logout history failed"})


def lambda_handler(event, context):
    try:
        null = None
        false = False
        true = True

        # Validate and send 500 return with error message as shown above inside save_data
        if event["body"].get("gmtFromTime", None):
            gmt_from_date = event["body"].get("gmtFromTime", None)
        else:
            return {"status": 500, "body": "Missing parameter value for gmtFromTime"}

        if event["body"].get("gmtToTime", None):
            gmt_to_date = event["body"].get("gmtToTime", None)
        else:
            return {"status": 500, "body": "Missing parameter value for gmtToTime"}

        if event["body"].get("usernames", []) or event["body"].get("usernames", []) == []:
            usernames = event["body"].get("usernames", [])
        else:
            return {"status": 500, "body": "Missing parameter values for usernames"}

        if event["body"].get("file_name", None):
            file_name = event["body"].get("file_name", None)
        else:
            return {"status": 500, "body": "Missing parameter value for file_name"}

        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        cdt = datetime.datetime.now()
        log_document = {}
        # Get list of all cognito users for their first name and last name
        # cognito_active_user_list = []
        # response = cognito.list_users(
        #     UserPoolId=user_pool_id,
        #     AttributesToGet=[
        #         'email',
        #         'custom:FirstName',
        #         'custom:LastName'
        #     ]
        # )
        # cognito_active_user_list.extend(response["Users"])
        #
        # while response.get("PaginationToken", None):
        #     response = cognito.list_users(
        #         PaginationToken=response["PaginationToken"],
        #         UserPoolId=user_pool_id,
        #         AttributesToGet=[
        #             'email',
        #             'custom:FirstName',
        #             'custom:LastName'
        #         ]
        #     )
        #     cognito_active_user_list.extend(response["Users"])

        from_date_format = extract_date_details(gmt_from_date)
        to_date_format = extract_date_details(gmt_to_date)

        get_users_list = []
        # If user list is empty, get all users from cognito with first name and last name
        if len(usernames) == 0:
            ## geting active users from BussinessAuditLogs if none username is selected.
            bussiness_user_obj = db.BussinessAuditLogs.aggregate([{"$match": {"$and":[
                                                                                   {'timestamp': {'$ne': None,
                                                                                                  "$gte": from_date_format,
                                                                                                  "$lte": to_date_format}}]
                                                                          }},
                                                              {"$group": {"_id": None,
                                                                          "uniqueValues": {"$addToSet": "$username"}}},
                                                              {"$project": {"_id": 0}}
                                                              ])

            bauditlog_user_objs = [data['uniqueValues'] for data in bussiness_user_obj][0]
            final_username_list = list(set(bauditlog_user_objs))
            usernames = []

            if "" in final_username_list:
                final_username_list.remove("")
            if None in final_username_list:
                final_username_list.remove(None)
            for user in final_username_list:
                if user:
                    if ".com" in user or ".tech" in user:
                        user = user.split("@")[0]
                    if user not in usernames:
                        usernames.append(user)

        # If user list has users, compare and get their first name and last name from cognito userlist

        users_first_last_names = {}
        get_users_list = usernames
        all_users = db.Users.find({})
        for db_user in all_users:
            if db_user["username"] in usernames:
                users_first_last_names[db_user["username"]] = {}
                users_first_last_names[db_user["username"]]["custom:FirstName"] = db_user["firstName"]
                users_first_last_names[db_user["username"]]["custom:LastName"] = db_user["LastName"]
                users_first_last_names[db_user["username"]]["email"] = db_user["email"]
                users_first_last_names[db_user["username"]]["roles"] = db_user["roles"]

        print(f'users list:: {get_users_list}')
        print(f'users info::  {users_first_last_names}')
        # invoking logout validation lambda
        payload = {
            "from_date": gmt_from_date,
            "to_date": gmt_to_date
        }

        # lambda_invoke.invoke(
        #     FunctionName=os.environ["GENERATE_LOGOUTS"],
        #     Payload=json.dumps(payload)
        # )
        generate_user_logout_history(payload)

        # ================================================================================================================================================
        # Iterate each user in logs and compute no of logins, no of days they logged in, duration and noted count
        all_users_details = []
        for username in get_users_list:

            # Compute no of logins
            pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$username", username]}, {"$gte": ["$timestamp", from_date_format]},
                         {"$lte": ["$timestamp", to_date_format]}]}}}]
            logs = db.BussinessAuditLogs.aggregate(pipe)
            no_of_logins = 0
            days_login = 0
            login_dates = set()
            for log in logs:
                if log["action"] == "login":
                    no_of_logins += 1
                    login_dates.add(log["timestamp"].strftime('%Y-%m-%d'))
            no_of_logins = no_of_logins
            days_login = len(login_dates)

            # ================================================================================================================================================
            # Compute user logged in duration
            login_pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$username", username]}, {"$eq": ["$action", "login"]},
                         {"$gte": ["$timestamp", from_date_format]}, {"$lte": ["$timestamp", to_date_format]}]}}}]
            logout_timeout_pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$username", username]}, {"$in": ["$action", ["logout", "timeout"]]},
                         {"$gte": ["$timestamp", from_date_format]}, {"$lte": ["$timestamp", to_date_format]}]}}}]
            log_in_list = db.BussinessAuditLogs.aggregate(login_pipe)
            log_out_time_out_list = db.BussinessAuditLogs.aggregate(logout_timeout_pipe)

            days = 0
            seconds = 0
            duration = "00:00"
            for log_in_entry in log_in_list:
                # Assuming logout exist within 30 minutes
                min_diff = datetime.timedelta(minutes=15)
                # checking if the logout/timeout is within 15 min from login time
                for log_out_time_out_entry in log_out_time_out_list:
                    if log_out_time_out_entry["timestamp"] > log_in_entry["timestamp"]:
                        diff = log_out_time_out_entry["timestamp"] - log_in_entry["timestamp"]
                        # getting the closest logout/timeout from the logout-timeout list
                        if diff <= min_diff:
                            min_diff = diff

                days += min_diff.days
                seconds += min_diff.seconds

            hours = (days * 24 + seconds // 3600)
            minutes = (seconds % 3600) // 60
            duration = str(hours) + ":" + str(minutes)

            user_details = {}
            # user_details["Username"] = username
            if users_first_last_names[username]["custom:FirstName"] and users_first_last_names[username][
                "custom:FirstName"] != "Firstname":
                first_name = users_first_last_names[username]["custom:FirstName"]
            else:
                first_name = ""
            if users_first_last_names[username]["custom:LastName"] and users_first_last_names[username][
                "custom:LastName"] != "Lastname":
                last_name = users_first_last_names[username]["custom:LastName"]
            else:
                last_name = ""

            email_id = users_first_last_names[username]["email"] if "email" in users_first_last_names[username] else ""

            user_roles = users_first_last_names[username]["roles"] if "roles" in users_first_last_names[username] else ""

            # user_details["Full Name"] = first_name + " " + last_name
            # user_details["Number of Logins"] = no_of_logins
            # user_details["Days Logged In"] = days_login
            # user_details["Amount of time in Libretto"] = duration
            # user_details["Exceptions Completed"] = ""
            # user_details["Exceptions Marked for Review"] = ""
            # user_details["Exceptions Snoozed"] = ""
            # user_details["Exceptions Ignored"] = ""
            # user_details["Notes Added"] = ""
            # all_users_details.append(user_details)

            # ================================================================================================================================================
            # Compute Notes Added for each ruleset from Notes Collection for each user

            user_ruleset_notes_count = {}
            # from_date_format = datetime.datetime.strptime(from_date, "%m-%d-%Y").replace(minute=0, hour=0, second=0)
            # to_date_format = datetime.datetime.strptime(to_date, "%m-%d-%Y").replace(minute=59, hour=23, second=59)
            pipe = [{"$match": {"$expr": {
                "$and": [{"$eq": ["$Note.Username", username]}, {"$gte": ["$Note.createdon", from_date_format]},
                         {"$lte": ["$Note.createdon", to_date_format]}]}}}]
            notes = db.Notes.aggregate(pipe)

            user_notes_count = {}
            user_notes_count["Notes Added"] = 0
            user_notes_count["Exceptions Completed"] = 0
            user_notes_count["Exceptions Snoozed"] = 0
            user_notes_count["Exceptions Review"] = 0
            user_notes_count["Exceptions Ignored"] = 0

            for note in notes:

                # rule_set = db.RuleSetMaster.find({"_id": note["RulesetId"]})
                # rule_set_name = rule_set[0]["RuleSetName"]
                # if rule_set_name in user_ruleset_notes_count:
                #     user_ruleset_notes_count[rule_set_name]["Notes Added"] += 1
                # else:
                #     user_ruleset_notes_count[rule_set_name] = {}
                #     user_ruleset_notes_count[rule_set_name]["Notes Added"] = 1
                #     user_ruleset_notes_count[rule_set_name]["Exceptions Completed"] = 0
                #     user_ruleset_notes_count[rule_set_name]["Exceptions Snoozed"] = 0
                #     user_ruleset_notes_count[rule_set_name]["Exceptions Review"] = 0
                #     user_ruleset_notes_count[rule_set_name]["Exceptions Ignored"] = 0

                # ================================================================================================================================================
                # Compute Exceptions Completed for each ruleset from Notes collection for each user
                # if note["NoteType"] == "SystemNote":
                #     if "marked as complete" in note["Note"]["Note"].lower():
                #         user_ruleset_notes_count[rule_set_name]["Exceptions Completed"] += 1
                #     elif "snoozed for" in note["Note"]["Note"].lower():
                #         user_ruleset_notes_count[rule_set_name]["Exceptions Snoozed"] += 1
                #     elif "marked for review" in note["Note"]["Note"].lower():
                #         user_ruleset_notes_count[rule_set_name]["Exceptions Review"] += 1
                #     elif "ignore" in note["Note"]["Note"].lower():
                #         user_ruleset_notes_count[rule_set_name]["Exceptions Ignored"] += 1

                if note["NoteType"] == "UserNote":
                    user_notes_count["Notes Added"] += 1
                if note["NoteType"] == "SystemNote" and note["Note"]["Action"] == "Add":
                    if "marked as complete" in note["Note"]["Note"].lower():
                        user_notes_count["Exceptions Completed"] += 1
                    elif "snoozed for" in note["Note"]["Note"].lower():
                        user_notes_count["Exceptions Snoozed"] += 1
                    elif "marked for review" in note["Note"]["Note"].lower():
                        user_notes_count["Exceptions Review"] += 1
                    elif "ignore" in note["Note"]["Note"].lower():
                        user_notes_count["Exceptions Ignored"] += 1

            # for key, value in user_ruleset_notes_count.items():
            #     user_details = {}
            #     # Don't change the order the data is appended
            #     user_details["Username"] = " ---" + key
            #     if first_name == "" and last_name == "":
            #         user_details["Full Name"] = username
            #     else:
            #         user_details["Full Name"] = first_name + " " + last_name
            #     user_details["Number of Logins"] = ""
            #     user_details["Days Logged In"] = ""
            #     user_details["Amount of time in Libretto"] = ""
            #     user_details["Exceptions Completed"] = value["Exceptions Completed"]
            #     user_details["Exceptions Marked for Review"] = value["Exceptions Review"]
            #     user_details["Exceptions Snoozed"] = value["Exceptions Snoozed"]
            #     user_details["Exceptions Ignored"] = value["Exceptions Ignored"]
            #     user_details["Notes Added"] = value["Notes Added"]
            #     all_users_details.append(user_details)

            user_details["Full Name"] = first_name + " " + last_name
            user_details["Completed"] = user_notes_count["Exceptions Completed"]
            user_details["Snoozed"] = user_notes_count["Exceptions Snoozed"]
            user_details["Mark/Review"] = user_notes_count["Exceptions Review"]
            user_details["Ignored"] = user_notes_count["Exceptions Ignored"]
            user_details["Notes Added"] = user_notes_count["Notes Added"]
            user_details["Username"] = username
            user_details["Number of Logins"] = no_of_logins
            user_details["Days Logged In"] = days_login
            user_details["Amount of time in Libretto"] = duration
            if len(user_roles) > 0:
                user_details["Role(s)"] = ', '.join(user_roles)
            else:
                user_details["Role(s)"] = "No active roles"
            user_details["Email"] = email_id
            all_users_details.append(user_details)

        csv_data = get_csv_data(all_users_details)
        response = save_data(BUCKET_NAME, csv_data, file_name)

        logdin_username = event["context"]["username"] if "username" in event["context"] else ""
        log_document["ip"] = ip
        log_document["browser"] = browser
        log_document["username"] = logdin_username
        log_document["timestamp"] = cdt
        log_document["file name"] = file_name
        log_document["action"] = "Reports"
        log_document["log_source"] = "Usage_report"
        insert_log = db.BussinessAuditLogs.insert_one(log_document)
        return True

    except Exception as e:
        return {'statusCode': 500, "Error": "generate usage report failed"}


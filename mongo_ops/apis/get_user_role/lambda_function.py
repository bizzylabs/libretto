import json
import os
import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    output = []
    try:
        db_role_data = db.UserRolePermissions.aggregate([{"$sort": {"RoleName": 1}}])
        if not db_role_data:
            return {"Error": "No role present", "statusCode": 500}
        else:
            for data in db_role_data:
                groupname = data.get("RoleName")
                response = cognito.list_users_in_group(
                    UserPoolId=user_pool_id,
                    GroupName=groupname,
                    Limit=60
                )
                data["NoOfUsers"] = 0
                if response:
                    data["NoOfUsers"] = len(response['Users'])
                data["_id"] = str(data["_id"])
                output.append(data)

        return {'statusCode': 200, 'result': output}
    except Exception as err:
        return {'statusCode': 500, "error": "get user role failed"}

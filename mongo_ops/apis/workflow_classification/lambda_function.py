import json
import os

import boto3
from pymongo import MongoClient

null = None
true = True
false = False


def get_database():
    secret = boto3.client("secretsmanager", region_name="us-east-1")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def lambda_handler(event, context):
    # TODO implement
    info = []
    result = {}
    try:
        workflow_action = event["body"].get("workflow_action", "")
        add_reason = event["body"].get("add_reason", "")
        default = event["body"].get("is_default", False)

        database = get_initialized_db()

        if not workflow_action:
            return {"ErrorMessage": "workflow action is required."}
        workflow_action = workflow_action.lower()

        if add_reason:
            doc = {
                "reason": add_reason,
                "workflowAction": workflow_action
            }

            database.WorkflowClassification.insert_one(doc)
            return {
                'statusCode': 200,
                'body': json.dumps('Action has been added successfully.')
            }
        else:

            workflow_action_info = database.WorkflowClassification.find({"workflowAction": workflow_action})
            if not workflow_action_info:
                return {"ErrorMessage": "Database operation fail. Try again after sometimes."}
            for item in workflow_action_info:
                info.append(item["reason"])
            info.sort()
            result[workflow_action] = info

            return {
                'statusCode': 200,
                'body': result
            }
    except Exception as err:
        return {'statusCode': 500, "error": "workflow classification failed"}
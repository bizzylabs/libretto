import json
import os

import boto3
from bson import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def lambda_handler(event, context):
    # TODO implement
    try:
        loan_package_id = ObjectId(event["body"].get("loanPackageId"))
        Username = event["context"].get("username", None)
        if not loan_package_id:
            return {"status": 500, "Error": "loanPackageId is mendatory "}
        filterobjs = db.UserFilters.find_one({"UserName": Username})
        loanpkgname = db.LoanPackage.find_one({"_id": loan_package_id})["Name"]
        if filterobjs:
            data = filterobjs["Filters"][loanpkgname]
            conditions_filter = filterobjs["ConditionsFilter"][loanpkgname]
            options_filter = filterobjs["OptionsFilter"][loanpkgname]
        else:
            user_access_filter_pbj = db.UserAccessFilters.find_one({})["Filters"]
            pkgitems = user_access_filter_pbj[loanpkgname]
            data = {}
            conditions_filter = {}
            options_filter = {}
            for item in pkgitems:
                data[item] = []
                conditions_filter[item] = {"Include": []}
                options_filter[item] = [
                    {
                        "label": "Include",
                        "value": "Include"
                    }
                ]
        return {
            'status': 200,
            'result': data,
            'conditions_filter': conditions_filter,
            'options_filter': options_filter
        }
    except Exception as err:
        return {'statusCode': 500, "error": "get filters failed"}


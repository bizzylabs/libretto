import json
import os
import re
from collections import OrderedDict

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

null = None
true = True
false = False


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
            "host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])
    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def lambda_handler(event, context):
    try:
        rule_info = []
        rule_id = event["body"].get("rule_id", None)
        rule_set_id = event["body"].get("rule_set_id", None)
        run_id = event["body"].get("run_id", None)

        if not rule_id and not rule_set_id and not run_id:
            return {"ErrorMessage": "rule_id, rule_set_id and run_id are required."}

        database = get_initialized_db()

        if rule_id:

            try:
                rule_info = database.ResultSetDetail.find_one(
                    {"$and": [{"RuleId": ObjectId(rule_id), "RuleSetId": ObjectId(rule_set_id), "LoanNumber": {"$ne": None},
                               "RunId": run_id,
                               "ExceptionStatus": True}]}, {"LoanInfo": 1, "_id": 0})
            except Exception as err:
                print(err)
                return {"ErrorMessage": "Database operation fail. Try again after sometimes."}

        if rule_info:
            display_fields = OrderedDict()
            try:
                rule_keys = list(rule_info.get("LoanInfo").keys())

                for value in rule_keys:
                    val = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ',
                                 value).strip()  # adding space after camelcase
                    val1 = re.search("\d{1,}", val)  # search for number after lower case
                    if val1:
                        final_key = val.replace(val1.group(), ' {}'.format(val1.group())).strip()  # replacing value
                    else:
                        final_key = val
                    display_fields[value] = final_key
                display_fields.move_to_end('LoanNumber', False)
            except Exception as err:
                return {"ErrorMessage": str(err)}

            return {
                'statusCode': 200,
                'body': display_fields
            }
        return {
            'statusCode': 200,
            'body': []
        }
    except Exception as err:
        return {'statusCode': 500, "error": "get display fields failed"}


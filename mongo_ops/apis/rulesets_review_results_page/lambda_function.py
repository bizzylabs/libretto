import json
import os
from datetime import datetime

import boto3
from bson.objectid import ObjectId
from pymongo import MongoClient

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def formatter(value):
    return "{:,}".format(value)


def latest_available(rule_set_id, run_id, created_date, loan_package):
    try:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S.%f')
    except:
        created_on = datetime.strptime(str(created_date), '%Y-%m-%d %H:%M:%S')
    run_ids = [item["RunId"] for item in db.SchedulerHistory.find(
        {"LoanPackage": ObjectId(loan_package), "RunId": {"$gt": run_id}, "IsActive": 1}) if item]
    result = db.ResultSetSummary.find_one({"$and": [{"RunId": {"$in": run_ids}},
                                                    {"RuleSetId": ObjectId(rule_set_id), "RunId": {"$gt": run_id},
                                                     "CreatedOn": {"$gte": created_on}}]})
    if result:
        val = db.SchedulerHistory.find_one({"RunId": result["RunId"], "LoanPackage": ObjectId(loan_package)},
                                           {"_id": 0, "IsActive": 1})
        if val and val["IsActive"] == 1:
            return True
    return False


def rule_set_handler(event, context):
    output = []
    all_rulesets = []
    try:
        user_name = event["context"]["username"]
        loan_package_id = event["body"].get("loan_package_id")
        filters = event["body"].get("filters", None)
        condition_filters = event["body"].get("condition_filters", None)
        exception_type = event["body"].get("exception_type", None)
        if exception_type == "open":
            exception_filter = {"$eq": ["$SystemNote", []]}
        else:
            exception_filter = {}
        rule_count_condition = [exception_filter, {"$ne": ["$Severity", "Informational"]},
                                {"$eq": ["$ExceptionStatus", True]},
                                {"$ne": ["$LoanNumber", None]}, {"$eq": ["$Ignore", False]}]
        new_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$ne": ["$Severity", "Informational"]},
                               {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$ExceptionStatus", True]}, {
                                   "$eq": [{"$dateToString": {"format": "%Y-%m-%d", "date": "$InExistence"}},
                                           {"$dateToString": {"format": "%Y-%m-%d", "date": "$CreatedOn"}}]}]
        cleared_count_condition = [exception_filter, {"$eq": ["$Ignore", False]},
                                   {"$ne": ["$Severity", "Informational"]}, {"$ne": ["$LoanNumber", None]},
                                   {"$eq": ["$ExceptionStatus", False]}]
        high_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]},
                                {"$ne": ["$LoanNumber", None]},
                                {"$eq": ["$Severity", "High"]}]
        low_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]},
                               {"$ne": ["$LoanNumber", None]},
                               {"$eq": ["$Severity", "Low"]}]
        medium_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]},
                                  {"$ne": ["$LoanNumber", None]},
                                  {"$eq": ["$Severity", "Medium"]}]
        critical_count_condition = [exception_filter, {"$eq": ["$Ignore", False]}, {"$eq": ["$ExceptionStatus", True]},
                                    {"$ne": ["$LoanNumber", None]},
                                    {"$eq": ["$Severity", "Critical"]}]
        filter_condition = dict()
        if filters and condition_filters:
            for key in filters:
                if len(filters[key]) > 0:
                    if "Include" in condition_filters[key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters[key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters[key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)
        else:
            loan_package_name = db.LoanPackage.find_one({"_id": ObjectId(loan_package_id)})["Name"]
            filters = db.UserFilters.find_one({"UserName": user_name})
            for key in filters["Filters"][loan_package_name]:
                if len(filters["Filters"][loan_package_name][key]) > 0:
                    if "Include" in filters["ConditionsFilter"][loan_package_name][key].keys():
                        cond = {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}
                    else:
                        cond = {"$not": {"$in": ["$FilterInfo." + key, filters["Filters"][loan_package_name][key]]}}
                    rule_count_condition.append(cond)
                    new_count_condition.append(cond)
                    cleared_count_condition.append(cond)
                    high_count_condition.append(cond)
                    low_count_condition.append(cond)
                    medium_count_condition.append(cond)
                    critical_count_condition.append(cond)

        scheduler_config_detail = list(db.SchedulerConfig.find({"LoanPackageId": ObjectId(loan_package_id)}))
        for scheduler_config in scheduler_config_detail:
            scheduler_config_id = scheduler_config.get("_id")
            # if not run_id:
            scheduler_history = list(db.SchedulerHistory.find({"IsActive": 1, "LoanPackage": ObjectId(loan_package_id),
                                                               "SchedulerConfigId": ObjectId(
                                                                   scheduler_config_id)}).sort(
                [("RunId", -1)]).limit(1))
            if len(scheduler_history) > 0:
                scheduler_history = scheduler_history[0]
                run_id = scheduler_history.get("RunId")
                pipe = [
                    {"$match": {"$and": [{
                        "RunId": run_id,
                        "ExceptionStatus": True}
                    ]}},
                    {"$group": {
                        "_id": {"rule_set_id": "$RuleSetId",
                                "rule_id": "$RuleId"
                                },
                        "rule_count": {"$sum": {"$cond": [
                            {"$and": rule_count_condition}, 1,
                            0]}},
                        "priority": {"$first": "$Severity"},
                        "new_count": {"$sum": {"$cond": [
                            {"$and": new_count_condition}, 1, 0]}},
                        "cleared_count": {"$sum": {"$cond": [
                            {"$and": cleared_count_condition}, 1,
                            0]}},
                        "High_count": {"$sum": {
                            "$cond": [{"$and": high_count_condition}, 1,
                                      0]}},
                        "Low_count": {"$sum": {
                            "$cond": [{"$and": low_count_condition}, 1,
                                      0]}},
                        "Medium_count": {"$sum": {
                            "$cond": [{"$and": medium_count_condition}, 1,
                                      0]}},
                        "Critical_count": {"$sum": {
                            "$cond": [{"$and": critical_count_condition},
                                      1, 0]}},

                    }},
                    {"$group": {
                        "_id": "$_id.rule_set_id",
                        "Rules": {
                            "$push": {
                                "Rule": "$_id.rule_id",
                                "exceptions": "$rule_count",
                                "new": "$new_count",
                                "severity": "$priority"
                            },
                        },
                        "total_exception": {"$sum": "$rule_count"},
                        "new_exceptions": {"$sum": "$new_count"},
                        "cleared_exceptions": {"$sum": "$cleared_count"},
                        "high_severity": {"$sum": "$High_count"},
                        "low_severity": {"$sum": "$Low_count"},
                        "medium_severity": {"$sum": "$Medium_count"},
                        "critical_severity": {"$sum": "$Critical_count"},
                    }},
                    {"$sort": {"total_exception": -1}}
                ]

                for rule_set in db.ResultSetDetail.aggregate(pipe):
                    if rule_set["_id"] not in all_rulesets:
                        rule_set_master = db.RuleSetMaster.find_one({"_id": ObjectId(rule_set["_id"])})
                        rule_set_detail = dict()
                        rule_set_detail["TotalNoOfExceptions"] = formatter(rule_set["total_exception"])
                        rule_set_detail["NoOfLoans"] = ""
                        rule_set_detail["NoOfRules"] = len(rule_set.get("Rules"))
                        rule_set_detail["NoOfNewException"] = rule_set["new_exceptions"]
                        rule_set_detail["Severity"] = ""
                        rule_set_detail["RunId"] = run_id
                        rule_set_detail["rulecount"] = len(rule_set.get("Rules"))
                        exceptions = db.ResultSetSummary.find_one(
                            {"RuleSetId": ObjectId(rule_set["_id"]), "RunId": run_id})
                        if exceptions:
                            rule_set_detail["UpdatedOn"] = exceptions.get("UpdatedOn").isoformat()
                            created_on_temp = exceptions.get("CreatedOn")

                        # adding for filtering exceptions
                        rule_set_detail["_id"] = str(rule_set.get("_id"))
                        rule_set_detail["RuleSetName"] = rule_set_master.get("RuleSetName")
                        rule_set_detail["ShowStatusFlag"] = 0
                        if user_name in rule_set_master["Users"]:
                            rule_set_detail["ShowStatusFlag"] = 1
                            rule_set_detail["RuleSetVersion"] = rule_set_master.get("RuleSetVersion")
                            rule_set_detail["RuleIds"] = [{key: str(value) for (key, value) in rule.items()} for rule in
                                                          rule_set_master.get("RuleIds")]
                            rule_set_detail["ChildRuleSetIds"] = [str(rule) for rule in
                                                                  rule_set_master.get("ChildRuleSetIds")]
                        if not latest_available(rule_set_detail["_id"], rule_set_detail["RunId"], created_on_temp,
                                                loan_package_id):
                            all_rulesets.append(rule_set["_id"])
                            total_loans_count = list(db.ResultSetSummary.aggregate([
                                {"$match": {"RunId": run_id}},
                                {"$group": {
                                    "_id": "$RuleSetId",
                                    "total_loans": {"$max": "$NoOfLoans"}
                                }},
                                {"$limit": 1}
                            ]))
                            if len(total_loans_count) > 0:
                                rule_set_detail["NoOfLoans"] = total_loans_count[0]["total_loans"]
                            rule_set_detail["Severity"] = {"TotalException": {"Critical": rule_set["critical_severity"],
                                                                              "High": rule_set["high_severity"],
                                                                              "Medium": rule_set["medium_severity"],
                                                                              "Low": rule_set["low_severity"]}}
                            output.append(rule_set_detail)
        output = sorted(output, key=lambda i: i['RunId'], reverse=True)
        client.close()
        return {"result": sorted(output, key=lambda k: (-k['ShowStatusFlag'], k['RuleSetName']))}
    except Exception as err:
        return {'statusCode': 500, "Error": "ruleset review result page failed"}

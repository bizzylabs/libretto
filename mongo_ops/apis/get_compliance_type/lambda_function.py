import json
import os

import boto3
from pymongo import MongoClient

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]


def get_compliance_type(event, context):
    try:
        compliance_type = db.ComplianceType.aggregate([{"$match": {"Name": {"$ne": ""}}}, {"$sort": {"Name": 1}}])
        output = []
        for data in compliance_type:
            compliance_type_data = dict()
            compliance_type_data["_id"] = str(data["_id"])
            compliance_type_data["Name"] = data["Name"]
            output.append(compliance_type_data)
        client.close()
        return {
            "result": output
        }
    except Exception as err:
        return {'statusCode': 500, "error": "get compliance type failed"}
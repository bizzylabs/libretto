import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
from datetime import datetime
import os
from log_queue import push_log


def get_database():
    secret = boto3.client("secretsmanager")
    secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
    secret_dict = json.loads(secret_response["SecretString"])
    client = MongoClient(
        secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict["host"] + "/" +
        secret_dict["db_name"] + secret_dict["mongo_tag"])

    return client[secret_dict["db_name"]]


db = None


def get_initialized_db():
    global db
    if db is None:
        db = get_database()
    return db


def change_status(event, context):
    try:
        try:
            contexts = event["context"]
            event = event["body"]
        except Exception as e:
            print(e)
            event = event
        rule_set_ids = event.get("RuleSetIds")
        rule_id = event.get("RuleId")
        rule_set_ids_log = event.get("RuleSetIds")
        rule_id_log = event.get("RuleId")
        status = event.get("status")
        database = get_initialized_db()
        ip = contexts.get("source-ip", None)
        browser = contexts.get("user-agent", None)
        username = contexts.get("username", None)
        email = contexts.get("email", None)
        unique_rule_id = database.Rule.find_one({"_id": ObjectId(rule_id_log)}).get("RuleNameId", "")
        log = {"action": "RuleDeactivated", "username": username, "email": email, "ip": ip,
               "browser": browser, "timestamp": str(datetime.now()), "RuleId": rule_id_log, "RuleName": "",
               "RuleSetId": rule_set_ids_log, "RuleNameId": unique_rule_id}
        # deactivate rule
        if status == 0:
            if len(rule_set_ids) == 0:
                rule = list(database.Rule.find({'_id': ObjectId(rule_id)}))
                if rule:
                    rule_set_ids = [ObjectId(id) for id in rule[0]['Departments']]

            else:
                rule_set_ids = [ObjectId(id) for id in rule_set_ids]

            # update in ruleSetMaster
            try:
                for ruleSetId in rule_set_ids:
                    rule_set_data = list(database.RuleSetMaster.find({'_id': ruleSetId}))
                    if rule_set_data:
                        for ruleId in rule_set_data[0]['RuleIds']:
                            if ruleId['RuleId'] == ObjectId(rule_id):
                                ruleId['IsActive'] = 0
                        database.RuleSetMaster.update({'_id': ObjectId(ruleSetId)},
                                                      {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})
            except:
                return {"error": "Rule status could not be updated."}

            # update in rule table
            try:
                # database.Rule.update({'_id': ObjectId(rule_id)}, {'$pull': {'Departments': {'$in': rule_set_ids}}})
                rule = list(database.Rule.find({'_id': ObjectId(rule_id)}))
                if rule:
                    rule_name = rule[0]["Name"]
                    if len(rule[0]['Departments']) == 0:
                        database.Rule.update({'_id': ObjectId(rule_id)}, {"$set": {'IsActive': 0}})
            except Exception as e:
                return {"error": "Rule status could not be updated."}
            try:
                # push logs to database
                log["action"] = "RuleDeactivated"
                log["RuleName"] = rule_name
                push_log.push(log)
            except Exception as e:
                print(e)
                pass
            return {"result": "Rule deactivated."}

        else:
            rule_set_ids = [ObjectId(id) for id in event.get("RuleSetIds")]
            try:
                # update ruleId status in ruleMaster table
                for ruleSetId in rule_set_ids:
                    rule_set_data = list(database.RuleSetMaster.find({'_id': ruleSetId}))
                    if rule_set_data:
                        for ruleId in rule_set_data[0]['RuleIds']:
                            if ruleId['RuleId'] == ObjectId(rule_id):
                                ruleId['IsActive'] = 1
                        database.RuleSetMaster.update({'_id': ObjectId(ruleSetId)},
                                                      {"$set": {'RuleIds': rule_set_data[0]['RuleIds']}})

                # update ruleId status and append the ruleSetId in departments in rule table,
                database.Rule.update({'_id': ObjectId(rule_id)}, {'$set': {'IsActive': 1}})
                database.Rule.update({'_id': ObjectId(rule_id)}, {'$push': {'Departments': {'$each': rule_set_ids}}})
                rule_data = database.Rule.find_one({'_id': ObjectId(rule_id)})

                rule_name = rule_data.get("Name", "")
                depart = list(set(rule_data.get("Departments", [])))

                database.Rule.update({'_id': ObjectId(rule_id)}, {"$set": {"Departments": depart}})

            except:
                return {"error": "Rule status could not be updated."}
            try:
                # push. logs to database
                log["action"] = "RuleActivated"
                log["RuleName"] = rule_name
                push_log.push(log)
            except Exception as e:
                print(e)
                pass

            return {"result": "Rule activated."}

    except Exception as e:
        return {'statusCode': 500, "error": "update rule active status failed"}
variable "env" {
    type = string
}

variable "customers" {
    type = list
}

variable "cloudfront_users" {
  type = map
}

data "aws_canonical_user_id" "current_user" {}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "libretto" {
  for_each = toset(var.customers)
  bucket = "libretto-${each.key}-files-${var.env}"
  acl    = "private"

  tags = {
    client      = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket" "libretto-bre" {
  for_each = toset(var.customers)
  bucket = "libretto-${each.key}-bre-files-${var.env}"
  acl    = "private"

  tags = {
    client      = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket" "libretto-bulk-action" {
  for_each = toset(var.customers)
  bucket = "libretto-${each.key}-bulk-action-bucket"
  acl = "private"
  
  tags = {
    client = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket" "libretto-csv-export" {
  for_each = toset(var.customers)
  bucket = "libretto-${each.key}-csv-export-data-${var.env}"
  acl    = "private"

  tags = {
    client      = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket" "libretto-ui" {
  for_each = toset(var.customers)
  bucket = "libretto-${each.key}-bizzy-${var.env}"
  acl    = "private"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    client      = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket_policy" "allow-only-from-cloudfront" {
  for_each = toset(var.customers)
  bucket = aws_s3_bucket.libretto-ui[each.key].bucket
  policy = data.aws_iam_policy_document.allow-only-from-cloudfront[each.key].json
}

data "aws_iam_policy_document" "allow-only-from-cloudfront" {
  for_each = toset(var.customers)
  statement {
    principals {
      type        = "AWS"
      identifiers = [var.cloudfront_users[each.key], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/SVC-${each.key}"]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      aws_s3_bucket.libretto-ui[each.key].arn,
      "${aws_s3_bucket.libretto-ui[each.key].arn}/*",
    ]
  }
}

resource "aws_s3_bucket" "logs" {
  bucket = "bizzy-${var.env}-logs"
  acl    = "log-delivery-write"

  tags = {
    Environment = "${var.env}"
  }
}

output "logs-bucket" {
    value = aws_s3_bucket.logs.bucket_domain_name
}
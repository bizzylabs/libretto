variable "env" {
  type = string
}

locals {
     functions = [ "activate_execution", "activate_execution_status", "add_rules", "admin_create_user_sign_up", "admin_list_users", "admin_reset_mfa", "admin_set_user_password", "admin_update_mfa_setting", "apis", "apis", "auth_change_password", "auth_confirm_forgot_password", "auth_forgot_password", "authorizer", "auth_sign_in", "auth_sign_out", "auth_verify_mfa", "bre_insert_data", "bre_rule_check_cured", "bre_status_notification", "cognito_user_role_permission", "data_archival", "delete_cognito_user", "delete_rule", "delete_user_role", "display_fields_loan_package", "edit_role_permissions", "edit_rules", "edit_user", "enable_disable_cognito_user", "event_scheduler", "exception_data_from_archive", "execute_external_rule", "execute_rule", "execute_rule_prev_data", "export_exception_data_from_archive", "export_exception_data_from_archive_polling", "export_exception_summary", "export_loan_details", "export_reports_polling", "export_test_rule_from_rule_editor", "export_test_rule_polling", "export_usage_report_polling", "external_rule_processor", "fetch_userpool_mfa_status", "functions", "functions", "generate_external_rules", "generate_usage_report", "generate_user_export_logs", "generate_user_logout_history", "get_compliance_type", "get_display_fields", "get_filters", "get_filter_values_add_user_page", "get_loan_package_fields", "get_mfa_secret", "get_refresh_token", "get_result_set_summary", "get_rule_loan_info", "get_rules_data", "get_rule_subtype", "get_rule_type", "get_schedulers", "get_user_role", "global_hotlink", "home_page_exceptions_overview", "home_page_results", "home_page_severe_exception", "is_editable", "loan_centric_view", "loan_data_columns", "loan_package_details", "log_writer", "long_polling", "okta_permission", "populate_data_from_files", "populate_loan_data", "populate_unique_column_values", "pre_signup_trigger", "result_set_detail", "rule_set_executor", "rulesets_on_add_rule", "rulesets_review_results_page", "rulesets_rule_library_page", "rules_review_result_page", "rule_testing_polling", "run_scheduler", "s3_cloudfront", "save_loan_package_columns", "save_user_access_filters", "save_values_user_access_filters", "scheduler_end_date_validation", "scheduler_status", "sftp", "sso_config", "state_machine_event", "state_machine_status", "stepfunctions_check_file_in_bucket", "stepfunctions_get_config", "system_notes", "test_rule_from_rule_editor", "unsnooze_system_note", "update_rule_active_status", "user_list_for_export_page_dropdown", "user_notes", "verify_mfa_secret", "workflow_classification", "sign_in_iframe", "get_role_permission", "save_external_rule_columns", "get_columns_width", "notes_config", "workflow_reasons_report"]
     layers = [ "log_queue", "permission" ]
}

variable "mutabilityMapping" {
  description = "mapping for domain names"
  default = {
    "dev" = "MUTABLE",
    # "uat" = "MUTABLE"
    "uat" = "IMMUTABLE"
  }
}


data "aws_caller_identity" "current" {}

resource "aws_ecr_repository" "functions" {
  for_each = toset(local.functions)
  name                 = each.key
  image_tag_mutability = "${lookup(var.mutabilityMapping, var.env)}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "layers" {
  for_each = toset(local.layers)
  name                 = "layers/${each.key}"
  image_tag_mutability = "${lookup(var.mutabilityMapping, var.env)}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository_policy" "crossAccountFunctions" {
  for_each = toset(local.functions)
  repository = aws_ecr_repository.functions[each.key].name

  policy = <<EOF
{
   "Version":"2008-10-17",
   "Statement":[
      {
         "Sid":"AllowPullFromProd",
         "Effect":"Allow",
         "Principal":{
            "AWS":"arn:aws:iam::583794896384:root"
         },
         "Action":[
            "ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:GetRepositoryPolicy",
            "ecr:DescribeRepositories",
            "ecr:ListImages",
            "ecr:DescribeImages",
            "ecr:BatchGetImage",
            "ecr:GetLifecyclePolicy",
            "ecr:GetLifecyclePolicyPreview",
            "ecr:ListTagsForResource",
            "ecr:DescribeImageScanFindings"
         ]
      },
      {
         "Sid":"LambdaECRImageRetrievalPolicy",
         "Condition":{
            "StringLike":{
               "aws:sourceArn":"arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:*"
            }
         },
         "Effect":"Allow",
         "Principal":{
            "Service":"lambda.amazonaws.com"
         },
         "Action":[
            "ecr:BatchGetImage",
            "ecr:GetDownloadUrlForLayer",
            "ecr:SetRepositoryPolicy",
            "ecr:DeleteRepositoryPolicy",
            "ecr:GetRepositoryPolicy"
         ]
      }
   ]
}
EOF
}

resource "aws_ecr_lifecycle_policy" "functions" {
  for_each = toset(local.functions)
  repository = aws_ecr_repository.functions[each.key].name

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire untagged images older than 1 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        },
        {
            "rulePriority": 2,
            "description": "Keep last 10 images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["v"],
                "countType": "imageCountMoreThan",
                "countNumber": 10
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}

resource "aws_ecr_repository_policy" "crossAccountLayers" {
  for_each = toset(local.layers)
  repository = aws_ecr_repository.layers[each.key].name

  policy = <<EOF
{
   "Version":"2008-10-17",
   "Statement":[
      {
         "Sid":"AllowPullFromProd",
         "Effect":"Allow",
         "Principal":{
            "AWS":"arn:aws:iam::583794896384:root"
         },
         "Action":[
            "ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:GetRepositoryPolicy",
            "ecr:DescribeRepositories",
            "ecr:ListImages",
            "ecr:DescribeImages",
            "ecr:BatchGetImage",
            "ecr:GetLifecyclePolicy",
            "ecr:GetLifecyclePolicyPreview",
            "ecr:ListTagsForResource",
            "ecr:DescribeImageScanFindings"
         ]
      },
      {
         "Sid":"LambdaECRImageRetrievalPolicy",
         "Condition":{
            "StringLike":{
               "aws:sourceArn":"arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:*"
            }
         },
         "Effect":"Allow",
         "Principal":{
            "Service":"lambda.amazonaws.com"
         },
         "Action":[
            "ecr:BatchGetImage",
            "ecr:GetDownloadUrlForLayer",
            "ecr:SetRepositoryPolicy",
            "ecr:DeleteRepositoryPolicy",
            "ecr:GetRepositoryPolicy"
         ]
      }
   ]
}
EOF
}

resource "aws_ecr_lifecycle_policy" "layers" {
  for_each = toset(local.layers)
  repository = aws_ecr_repository.layers[each.key].name

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire untagged images older than 1 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        },
        {
            "rulePriority": 2,
            "description": "Keep last 10 images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["v"],
                "countType": "imageCountMoreThan",
                "countNumber": 10
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}

variable logs-bucket {
  type = string
}

resource "aws_wafv2_web_acl" "ui" {
  name        = "frontend-waf"
  description = "waf for frontend app"
  scope       = "CLOUDFRONT"

  default_action {
    allow {}
  }

  rule {
    name     = "AWS-AWSManagedRulesAmazonIpReputationList"
    priority = 1

    override_action {
      count {}
    }

    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesAmazonIpReputationList"
        vendor_name = "AWS"
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWS-AWSManagedRulesAmazonIpReputationList"
      sampled_requests_enabled   = true
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "AWS-AWSManagedRulesAmazonIpReputationList"
    sampled_requests_enabled   = true
  }
}

resource "aws_cloudwatch_log_group" "waf" {
  name = "aws-waf-logs-ui"
}

resource "aws_wafv2_web_acl_logging_configuration" "ui" {
  log_destination_configs = [aws_cloudwatch_log_group.waf.arn]
  resource_arn            = aws_wafv2_web_acl.ui.arn
  redacted_fields {
    single_header {
      name = "user-agent"
    }
  }
}

output "waf-id" {
    value = aws_wafv2_web_acl.ui.arn
}
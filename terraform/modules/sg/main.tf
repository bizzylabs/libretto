variable "customers" {
    type = list
}

data "aws_vpcs" "client-vpcs" {
  filter {
    name = "tag:client"
    values = var.customers
  }
}

data "aws_vpc" "client-vpcs" {
  count = length(data.aws_vpcs.client-vpcs.ids)
  id    = tolist(data.aws_vpcs.client-vpcs.ids)[count.index]
}

resource "aws_security_group" "mongo-egress" {
  count = length(data.aws_vpcs.client-vpcs.ids)
  name        = "allow_mongo_egress"
  description = "Allows Lambda functions to contact mongdb instances"
  vpc_id      = data.aws_vpc.client-vpcs[count.index].id

  egress = [
    {
      from_port        = 27017
      to_port          = 27017
      protocol         = "TCP"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      self = null
      description = "allow mongo egress"
      prefix_list_ids  = null
      security_groups  = null      
    }
  ]

  tags = {
    Name = "allow_mongo_egress"
    client = data.aws_vpc.client-vpcs[count.index].tags["client"]
  }
}
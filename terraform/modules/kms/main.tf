variable "customers" {
    type = list
}

resource "aws_kms_key" "key" {
  for_each = toset(var.customers)
  description             = "${each.key}"
  deletion_window_in_days = 10
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_kms_alias" "key" {
  for_each = toset(var.customers)
  name          = "alias/${each.key}"
  target_key_id = aws_kms_key.key[each.key].key_id

}
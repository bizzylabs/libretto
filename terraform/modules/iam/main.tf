variable "env" {
    type = string
}

variable "customers" {
    type = list
}

#variable "sqs" {
#  type = map
#}

resource "aws_iam_role" "customer_role" {
  for_each = toset(var.customers)
  assume_role_policy    = jsonencode(
    {
      "Version": "2012-10-17",
      "Statement": [
          {
            "Effect": "Allow",
            "Principal": {
                "Service": [
                  "lambda.amazonaws.com",
                  "edgelambda.amazonaws.com",
                  "states.amazonaws.com",
                  "events.amazonaws.com"
                ]
            },
            "Action": "sts:AssumeRole"
          }
      ]
    }
  )
  description           = "Allows Lambda functions to call AWS services on your behalf."
  name                  = "SVC-${each.key}"
  tags = {
    client        = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_iam_role_policy_attachment" "customer_attach1" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_role[each.key].name
  policy_arn = aws_iam_policy.customer_policy[each.key].arn
}

resource "aws_iam_role_policy_attachment" "customer_attach2" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_role[each.key].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "customer_attach3" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_role[each.key].name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

resource "aws_iam_role_policy_attachment" "customer_attach4" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_role[each.key].name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess"
}

resource "aws_iam_role_policy_attachment" "customer_attach5" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_role[each.key].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}


resource "aws_iam_policy" "customer_policy" {
  for_each = toset(var.customers)
  description = "allows ${each.key} to access necessary services"
  name        = "${each.key}-policy"
  path        = "/"
  policy      = jsonencode(
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
            "cognito-identity:Describe*",
            "cognito-identity:Get*",
            "cognito-identity:List*",
            "cognito-idp:*",
            "cognito-sync:Describe*",
            "cognito-sync:Get*",
            "cognito-sync:List*",
            "iam:ListOpenIdConnectProviders",
            "iam:ListRoles",
            "sns:ListPlatformApplications",
            "states:StartExecution",
            "lambda:ListFunctions",
            "lambda:InvokeFunction",
            "lambda:ListVersionsByFunction",
            "lambda:GetLayerVersion",
            "lambda:GetEventSourceMapping",
            "lambda:GetFunction",
            "lambda:ListAliases",
            "lambda:GetAccountSettings",
            "lambda:InvokeAsync",
            "lambda:GetFunctionConfiguration",
            "lambda:AddLayerVersionPermission",
            "lambda:GetLayerVersionPolicy",
            "lambda:RemoveLayerVersionPermission",
            "lambda:EnableReplication",
            "lambda:AddPermission",
            "lambda:ListTags",
            "lambda:ListEventSourceMappings",
            "lambda:ListLayerVersions",
            "lambda:ListLayers",
            "lambda:GetAlias",
            "lambda:RemovePermission",
            "lambda:GetPolicy",
            "kms:Decrypt",
            "kms:GenerateDataKey",
            "s3:*", 
            "sqs:*"
        ],
        "Resource": "*",
        "Condition": {
          "StringEquals": {"aws:ResourceTag/client":"${each.key}"}
        }
      },
      {
        "Effect": "Allow",
        "Action": [
            "cognito-identity:Get*",
            "cognito-identity:List*",
            "cognito-idp:AdminList*",
            "cognito-idp:AdminGet*",
            "cognito-idp:Get*",
            "cognito-idp:List*",
            "cognito-sync:Get*",
            "cognito-sync:List*",
            "iam:ListOpenIdConnectProviders",
            "iam:ListRoles",
            "kms:Decrypt",
            "kms:Encrypt",
            "lambda:GetAccountSettings",
            "lambda:GetAlias",
            "lambda:GetEventSourceMapping",
            "lambda:GetFunction",
            "lambda:GetFunctionConfiguration",
            "lambda:GetLayerVersion",
            "lambda:GetLayerVersionPolicy",
            "lambda:GetPolicy",
            "lambda:ListAliases",
            "lambda:ListEventSourceMappings",
            "lambda:ListFunctions",
            "lambda:ListLayerVersions",
            "lambda:ListLayers",
            "lambda:ListTags",
            "lambda:ListVersionsByFunction",
            "lambda:InvokeFunction",
            "lambda:InvokeAsync",
            "secretsmanager:Describe*",
            "secretsmanager:Get*",
            "secretsmanager:List*",
            "sns:ListPlatformApplications",
        ],
        "Resource": "*"
      },
      {
        "Effect": "Allow",
        "Action": [
            "iam:ListOpenIdConnectProviders",
            "iam:ListRoles",
            "sns:ListPlatformApplications",
            "states:StartExecution",
            "lambda:ListFunctions",
            "lambda:InvokeFunction",
            "lambda:ListVersionsByFunction",
            "lambda:GetLayerVersion",
            "lambda:GetEventSourceMapping",
            "lambda:GetFunction",
            "lambda:ListAliases",
            "lambda:GetAccountSettings",
            "lambda:InvokeAsync",
            "lambda:GetFunctionConfiguration",
            "lambda:AddLayerVersionPermission",
            "lambda:GetLayerVersionPolicy",
            "lambda:RemoveLayerVersionPermission",
            "lambda:EnableReplication",
            "lambda:AddPermission",
            "lambda:ListTags",
            "lambda:ListEventSourceMappings",
            "lambda:ListLayerVersions",
            "lambda:ListLayers",
            "lambda:GetAlias",
            "lambda:RemovePermission",
            "lambda:GetPolicy",
            "kms:Decrypt",
            "kms:GenerateDataKey",
            "s3:*", 
            "sqs:*"
        ],
        "Resource":  ["arn:aws:sqs:*:*:${each.key}*",
        "arn:aws:s3:::${each.key}*",
        "arn:aws:s3:::libretto-${each.key}*",
        "arn:aws:kms:*:*:${each.key}*",
        "arn:aws:lambda:*:*:${each.key}*",
        "arn:aws:sns:*:*:${each.key}*",
        "arn:aws:secretsmanager:*:*:${each.key}*",
        "arn:aws:states:*:*:stateMachine:${each.key}*"]
      },
      {
        Effect   = "Allow",
        Action   = [
            "ec2:RunInstances",
            "ec2:CreateTags",
            "ec2:DescribeInstances",
        ],
        Resource = "*"
      },
      {
        Effect    = "Allow",
        Action    = ["ec2:*"],
        Resource  = "arn:aws:ec2:*:*:instance/*",
        Condition = {
            StringEquals = {
                "ec2:ResourceTag/Owner" = "Lambda-Temp-Mongodb",
                "aws:ResourceTag/client":"${each.key}"
            }
        }
      }
    ]
  }
  )
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_iam_role" "customer_sfn_role" {
  for_each = toset(var.customers)
  assume_role_policy    = jsonencode(
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "states.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  )
  description           = "Allows sfn to call AWS services on your behalf."
  name                  = "SFN-${each.key}"
  tags = {
    client        = "${each.key}"
    Environment = "${var.env}"
  }
}

resource "aws_iam_role_policy_attachment" "customer_sfn_attach1" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_sfn_role[each.key].name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

resource "aws_iam_role_policy_attachment" "customer_sfn_attach2" {
  for_each = toset(var.customers)
  role      = aws_iam_role.customer_sfn_role[each.key].name
  policy_arn = aws_iam_policy.customer_policy[each.key].arn
}

resource "aws_iam_role" "apigateway" {
    assume_role_policy    = jsonencode(
      {
          "Version": "2012-10-17",
          "Statement": [
              {
                  "Sid": "",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "apigateway.amazonaws.com"
                  },
                  "Action": "sts:AssumeRole"
              }
          ]
      }
    )
    description           = "Allows Lambda functions to call AWS services on your behalf."
    name                  = "apigw-cw"
}

resource "aws_iam_role_policy_attachment" "apig_attach1" {
  role      = aws_iam_role.apigateway.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}
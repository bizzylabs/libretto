variable "env" {
    type = string
}

variable "customers" {
    type = list
}

data "aws_caller_identity" "current" {}

data "aws_vpcs" "control-plane" {
  tags = {
    Name = "control-plane"
  }
}

data "aws_vpc" "control-plane" {
  count = length(data.aws_vpcs.control-plane.ids)
  id    = tolist(data.aws_vpcs.control-plane.ids)[count.index]
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.control-plane[0].id

  filter {
    name   = "tag:aws:cloudformation:logical-id"
    values = ["AppSubnet*"]
  }
}

data "aws_subnet" "private" {
  for_each = data.aws_subnet_ids.private.ids
  id       = each.value
}

data "aws_security_groups" "egress-only" {
  filter {
    name   = "group-name"
    values = ["egress-only"]
  }

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.control-plane[0].id]
  }
}

data "aws_security_group" "egress-only" {
  count = length(data.aws_security_groups.egress-only.ids)
  id    = tolist(data.aws_security_groups.egress-only.ids)[count.index]
}

resource "aws_codebuild_project" "infra" {
  for_each = toset(var.customers)
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  encryption_key         = "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:alias/aws/s3"

  environment {
    compute_type            = "BUILD_GENERAL1_SMALL"
    image                       = "${data.aws_caller_identity.current.account_id}.dkr.ecr.us-east-1.amazonaws.com/serverless-deployer:latest"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "false"
    type                        = "LINUX_CONTAINER"
    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }

    environment_variable {
      name  = "DD_KEY"
      type  = "SECRETS_MANAGER"
      value = "Datadog:DD_KEY"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_${each.key}_INFRA_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-infra.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/libretto.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
  tags = {
    client        = "${each.key}"
  }
}


resource "aws_codebuild_project" "serverless" {
  for_each = toset(var.customers)
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  encryption_key         = "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:alias/aws/s3"

  environment {
    compute_type            = "BUILD_GENERAL1_SMALL"
    image                       = "${data.aws_caller_identity.current.account_id}.dkr.ecr.us-east-1.amazonaws.com/serverless-deployer:latest"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "false"
    type                        = "LINUX_CONTAINER"
    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }

    environment_variable {
      name  = "DD_KEY"
      type  = "SECRETS_MANAGER"
      value = "Datadog:DD_KEY"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_${each.key}_Serverless_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-serverless.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/libretto.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_codebuild_project" "terraform" {
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  encryption_key         = "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:alias/aws/s3"

  environment {
    compute_type            = "BUILD_GENERAL1_SMALL"
    image                       = "${data.aws_caller_identity.current.account_id}.dkr.ecr.us-east-1.amazonaws.com/serverless-deployer:latest"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "false"
    type                        = "LINUX_CONTAINER"
    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_Terraform_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-terraform.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/libretto.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
}

resource "aws_codebuild_project" "frontend-deploy" {
  for_each = toset(var.customers)
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  description            = "For ${each.key} UI in ${var.env} env depending on libretto-${each.key}-bizzy-${var.env} bucket in S3"
  encryption_key         = "arn:aws:kms:us-east-1:337920175745:alias/aws/s3"

  environment {
    compute_type                = "BUILD_GENERAL1_LARGE"
    image                       = "aws/codebuild/amazonlinux2-aarch64-standard:1.0"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "false"
    type                        = "ARM_CONTAINER"
    environment_variable {
      name  = "CLIENT_NAME"
      value = "${each.key}"
    }
    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }

    environment_variable {
      name  = "VERSION"
      value = "latest"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "${each.key}_Frontend_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-deploy.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/bsi-frontend.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_codebuild_project" "frontend-build" {
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  description            = "Builds UI artifact and stores in s3"
  encryption_key         = "arn:aws:kms:us-east-1:337920175745:alias/aws/s3"

  environment {
    compute_type                = "BUILD_GENERAL1_LARGE"
    image                       = "aws/codebuild/amazonlinux2-aarch64-standard:1.0"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "false"
    type                        = "ARM_CONTAINER"

    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_Frontend_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/bsi-frontend.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
}

resource "aws_codebuild_project" "buildall" {
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  build_batch_config {
    combine_artifacts = "false"

    restrictions {
      compute_types_allowed  = ["BUILD_GENERAL1_LARGE"]
      maximum_builds_allowed = "99"
    }

    service_role    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"
    timeout_in_mins = "480"
  }

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "100"
  encryption_key         = "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:alias/aws/s3"

  environment {
    compute_type                = "BUILD_GENERAL1_LARGE"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"

    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }
  }

  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_Build_All_Containers_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-buildall.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/libretto.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
}

resource "aws_codebuild_project" "buildspecific" {
  for_each = toset(var.customers)
  artifacts {
    encryption_disabled    = "false"
    override_artifact_name = "false"
    type                   = "NO_ARTIFACTS"
  }

  badge_enabled = "false"
  build_timeout = "60"

  cache {
    type = "NO_CACHE"
  }

  concurrent_build_limit = "1"
  description            = "Builds and deploys specified functions. FUNCTIONS is a comma separated list of functions to deploy"
  encryption_key         = "arn:aws:kms:us-east-1:337920175745:alias/aws/s3"

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "${data.aws_caller_identity.current.account_id}.dkr.ecr.us-east-1.amazonaws.com/serverless-deployer:latest"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"

    environment_variable {
      name  = "STAGE"
      value = "${var.env}"
    }
    environment_variable {
      name  = "FUNCTIONS"
      value = "add_rules,admin_set_user_password"
    }
    environment_variable {
      name  = "DD_KEY"
      type  = "SECRETS_MANAGER"
      value = "Datadog:DD_KEY"
    }
  }
  
  vpc_config {
    vpc_id = data.aws_vpc.control-plane[0].id

    subnets = data.aws_subnet_ids.private.ids

    security_group_ids = [data.aws_security_group.egress-only[0].id]
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }

  name           = "Libretto_${each.key}_Build_and_Deploy_Specific_Functions_${var.env}"
  queued_timeout = "480"
  service_role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Serverless-Deploy"

  source {
    buildspec       = "buildspec-buildspecific.yaml"
    git_clone_depth = "1"

    git_submodules_config {
      fetch_submodules = "false"
    }

    insecure_ssl        = "false"
    location            = "https://bitbucket.org/bizzylabs/libretto.git"
    report_build_status = "false"
    type                = "BITBUCKET"
  }

  source_version = "refs/heads/dev"
}
resource "aws_ses_email_identity" "help" {
  email = "help@bizzylabs.tech"
}


resource "aws_ses_domain_identity" "bizzy" {
  domain = "bizzylabs.tech"
}

resource "aws_ses_configuration_set" "bizzy" {
  name                       = "SendingStats"
  reputation_metrics_enabled = "false"
  sending_enabled            = "true"
}

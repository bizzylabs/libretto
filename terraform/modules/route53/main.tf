variable "uis" {
    type = list
}


resource "aws_route53_health_check" "ui" {
  for_each = toset(var.uis)
  fqdn              = each.key
  port              = 443
  type              = "HTTPS"
  resource_path     = "/"
  failure_threshold = "5"
  request_interval  = "30"

  tags = {
    Name = each.key
  }
}
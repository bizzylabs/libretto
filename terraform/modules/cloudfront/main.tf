variable "env" {
    type = string
}

variable "customers" {
    type = list
}

variable "waf" {
    type = string
}

variable "logs-bucket" {
    type = string
}

variable "cert_mapping" {
  description = "mapping for domain names"
  default = {
    "dev" = "incedolabs.com",
    "uat" = "bizzylabs.tech"
  }
}

variable "prefix" {
  description = "mapping for api prefix"
  default = {
    "dev" = "dev-",
    "uat" = "uat-"
    "prod" = ""
  }
}

variable "s3_cloudfront_version_mapping" { 
  description = "mapping for api prefix"
  default = { 
    "dev" = { "tc1" = "12" }, #Version of s3 for each customer
    "uat" = { "dc4" = "9" }, #Version of s3 for each customer
    "prod" = {}, #Version of s3 for each customer in prod
  }
}

data "aws_acm_certificate" "cert" {
  domain = "*.${lookup(var.cert_mapping, var.env)}"
}

data "aws_caller_identity" "current" {}

resource "aws_cloudfront_distribution" "frontend" {
  for_each = toset(var.customers)
  aliases = ["${lookup(var.prefix, var.env)}${each.key}.${lookup(var.cert_mapping, var.env)}"]
  comment = "For ${each.key}"

  custom_error_response {
    error_caching_min_ttl = "60"
    error_code            = "403"
    response_code         = "200"
    response_page_path    = "/"
  }

  default_cache_behavior {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["GET", "HEAD"]
    compress        = "false"
    default_ttl     = "0"

    forwarded_values {
      cookies {
        forward = "none"
      }

      query_string = "false"
    }

    max_ttl                = "0"
    min_ttl                = "0"
    smooth_streaming       = "false"
    target_origin_id       = "libretto-${each.key}-bizzy-${var.env}"
    viewer_protocol_policy = "redirect-to-https"

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_s3_cloudfront:${lookup(lookup(var.s3_cloudfront_version_mapping, var.env), each.key)}"
    }
  }

  default_root_object = "index.html"
  enabled             = "true"
  http_version        = "http2"
  is_ipv6_enabled     = "true"

  origin {
    connection_attempts = "3"
    connection_timeout  = "10"
    domain_name         = "libretto-${each.key}-bizzy-${var.env}.s3.amazonaws.com"
    origin_id           = "libretto-${each.key}-bizzy-${var.env}"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.client[each.key].cloudfront_access_identity_path
    }
  }

  price_class = "PriceClass_All"
  
  logging_config {
    include_cookies = false
    bucket          = var.logs-bucket
    prefix          = "cloudfront/${each.key}"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  retain_on_delete = "false"

  viewer_certificate {
    acm_certificate_arn            = data.aws_acm_certificate.cert.arn
    cloudfront_default_certificate = "false"
    minimum_protocol_version       = "TLSv1.2_2019"
    ssl_support_method             = "sni-only"
  }

  web_acl_id = var.waf
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_cloudfront_origin_access_identity" "client" {
  for_each = toset(var.customers)
  comment = each.key
}

output "cloudfront_users" {
  value = {
    for a, b in aws_cloudfront_origin_access_identity.client : a => b.iam_arn
  }
}

output "ui_urls" {
  value = toset([
    for ui in aws_cloudfront_distribution.frontend: one(ui.aliases)
  ])
}

variable "env" {
    type = string
}

variable "customers" {
    type = list
}

data "aws_caller_identity" "current" {}

resource "aws_sfn_state_machine" "StateMachine" {
  for_each = toset(var.customers)
  definition = jsonencode(
{
  "Comment": "BRE state machine.",
  "StartAt": "CheckEndDate",
  "States": {
    "CheckEndDate": {
    "Type": "Task",
    "Parameters": {
        "scheduler_id.$": "$.SchedulerId"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_scheduler_end_date_validation",
    "ResultPath": "$.ValidatedEndDate",
    "Next": "ValidateEndDate"
    },
    "ValidateEndDate": {
    "Type": "Choice",
    "Choices": [
        {
        "Variable": "$.ValidatedEndDate",
        "BooleanEquals": true,
        "Next": "SFTPDataFetch"
        },
        {
        "Variable": "$.ValidatedEndDate",
        "BooleanEquals": false,
        "Next": "InvalidDate"
        }
    ]
    },
    "SFTPDataFetch": {
    "Type": "Task",
    "Parameters": {
        "name": "${each.key}",
        "bucket": "libretto-${each.key}-files-${var.env}/config",
        "key": "sftp-config.json"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_sftp",
    "Next": "GetConfig",
    "ResultPath": "$.SFTP"
    },
    "GetConfig": {
    "Type": "Task",
    "Parameters": {
        "loan_package_id.$": "$.LoanPackageId"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_stepfunctions_get_config",
    "Next": "CheckFileInSFTPBucket",
    "ResultPath": "$.Config"
    },
    "CheckFileInSFTPBucket": {
    "Type": "Task",
    "Parameters": {
        "bucket_path.$": "$.Config.BucketPaths.SFTPInboundPath",
        "package_files.$": "$.Config.Files"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_stepfunctions_check_file_in_bucket",
    "ResultPath": "$.FileAvailbleInSFTP",
    "Comment": "check if file-available in SFTP Bucket",
    "Next": "IsFileAvailableInSFTPBucket"
    },
    "IsFileAvailableInSFTPBucket": {
    "Type": "Choice",
    "Choices": [
        {
        "Variable": "$.FileAvailbleInSFTP",
        "BooleanEquals": true,
        "Next": "PreProcessing"
        },
        {
        "Variable": "$.FileAvailbleInSFTP",
        "BooleanEquals": false,
        "Next": "FailState"
        }
    ]
    },
    "PreProcessing": {
    "Type": "Pass",
    "Comment": "Here we can implement a pre-processing lambda, Glue one!",
    "Next": "PopulateDataFromFiles"
    },
    "PopulateDataFromFiles": {
    "Type": "Task",
    "Parameters": {
        "bucket_path.$": "$.Config.BucketPaths.SFTPInboundPath",
        "package_files.$": "$.Config.Files",
        "scheduler_id.$": "$.SchedulerId",
        "loan_package_id.$": "$.LoanPackageId",
        "iterator.$": "$.Iterator"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_populate_data_from_files",
    "ResultPath": "$.Iterator",
    "TimeoutSeconds": 900,
    "Next": "IterateRecords"
    },
    "IterateRecords": {
    "Type": "Choice",
    "Choices": [
        {
        "Variable": "$.Iterator.Continue",
        "BooleanEquals": true,
        "Next": "PopulateDataFromFiles"
        }
    ],
    "Default": "PopulateLoanDataCollection"
    },
    "PopulateLoanDataCollection": {
    "Type": "Task",
    "Parameters": {
        "collection_details.$": "$.Config.Files",
        "scheduler_id.$": "$.SchedulerId",
        "loan_package_id.$": "$.LoanPackageId"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_populate_loan_data",
    "ResultPath": "$.LoanDataPKey",
    "TimeoutSeconds": 900,
    "Next": "BREExecutionEngine"
    },
    "BREExecutionEngine": {
    "Type": "Task",
    "Parameters": {
        "scheduler_id.$": "$.SchedulerId",
        "loan_package_id.$": "$.LoanPackageId",
        "loan_data_prime_key.$": "$.LoanDataPKey",
        "loan_package_name.$": "$.LoanPackageName"
    },
    "Resource": "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:${each.key}_run_scheduler",
    "ResultPath": "$.Started_BRE",
    "TimeoutSeconds": 900,
    "End": true
    },
    "FailState": {
    "Type": "Fail",
    "Cause": "Invalid response.",
    "Error": "Error"
    },
    "InvalidDate": {
    "Type": "Pass",
    "Result": {
        "cause": "Invalid EndDate",
        "isError": true
    },
    "End": true
    }
  }
})

  logging_configuration {
    include_execution_data = "true"
    level                  = "ALL"
    log_destination        = "${aws_cloudwatch_log_group.states[each.key].arn}:*"
  }

  name     = "${each.key}StateMachine"
  role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/SVC-${each.key}"

  tracing_configuration {
    enabled = "false"
  }

  type = "STANDARD"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_cloudwatch_log_group" "states" {
  for_each = toset(var.customers)
  name = "states/${each.key}/StateMachine-Logs"
}

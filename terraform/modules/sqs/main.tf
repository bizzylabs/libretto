variable "customers" {
    type = list
}

resource "aws_sqs_queue" "long_polling" {
  for_each = toset(var.customers)
  content_based_deduplication       = "true"
  deduplication_scope               = "queue"
  delay_seconds                     = "15"
  fifo_queue                        = "true"
  fifo_throughput_limit             = "perQueue"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_LONG_POLLING_QUEUE.fifo"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "900"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "ruleset" {
  for_each = toset(var.customers)
  content_based_deduplication       = "true"
  deduplication_scope               = "queue"
  delay_seconds                     = "0"
  fifo_queue                        = "true"
  fifo_throughput_limit             = "perQueue"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_RULESET_QUEUE.fifo"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "900"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "rule" {
  for_each = toset(var.customers)
  content_based_deduplication       = "true"
  deduplication_scope               = "queue"
  delay_seconds                     = "0"
  fifo_queue                        = "true"
  fifo_throughput_limit             = "perQueue"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_RULE_QUEUE.fifo"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "900"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "external_rule_processing" {
  for_each = toset(var.customers)
  content_based_deduplication       = "true"
  deduplication_scope               = "messageGroup"
  delay_seconds                     = "0"
  fifo_queue                        = "true"
  fifo_throughput_limit             = "perMessageGroupId"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_EXTERNAL_RULE_PROCESSING_QUEUE.fifo"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "1200"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "logs" {
  for_each = toset(var.customers)
  content_based_deduplication       = "true"
  deduplication_scope               = "queue"
  delay_seconds                     = "0"
  fifo_queue                        = "true"
  fifo_throughput_limit             = "perQueue"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_LOGS_QUEUE.fifo"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "900"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "rule_set_detail" {
  for_each = toset(var.customers)
  content_based_deduplication       = "false"
  delay_seconds                     = "0"
  fifo_queue                        = "false"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "345600"
  name                              = "${each.key}_RESULT_SET_DETAIL_QUEUE"

  receive_wait_time_seconds  = "0"
  visibility_timeout_seconds = "1800"
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_sqs_queue" "sftp" {
  for_each = toset(var.customers)
  content_based_deduplication       = "false"
  delay_seconds                     = "0"
  fifo_queue                        = "false"
  kms_data_key_reuse_period_seconds = "300"
  max_message_size                  = "262144"
  message_retention_seconds         = "1209600"
  name                              = "${each.key}_SFTP"

  receive_wait_time_seconds         = "0"
  visibility_timeout_seconds        = "60"
  tags = {
    client        = "${each.key}"
  }
}

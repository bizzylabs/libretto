variable "customers" {
    type = list
}

data "aws_caller_identity" "current" {}

resource "aws_cognito_user_pool_client" "user_pool" {
  for_each = toset(var.customers)
  access_token_validity                = "60"
  allowed_oauth_flows_user_pool_client = "false"
  enable_token_revocation              = "false"
  explicit_auth_flows                  = ["ALLOW_USER_SRP_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
  id_token_validity                    = "5"
  name                                 = "Libretto-${each.key}"
  prevent_user_existence_errors        = "ENABLED"
  read_attributes                      = ["birthdate", "email", "phone_number", "locale", "updated_at", "website", "nickname", "profile", "picture", "address", "email_verified", "phone_number_verified", "preferred_username", "name", "family_name", "zoneinfo", "gender", "given_name", "middle_name"]
  refresh_token_validity               = "60"

  token_validity_units {
    access_token  = "minutes"
    id_token      = "minutes"
    refresh_token = "minutes"
  }

  user_pool_id     = aws_cognito_user_pool.user-pool[each.key].id
  write_attributes = ["address", "preferred_username", "picture", "profile", "zoneinfo", "family_name", "middle_name", "locale", "updated_at", "nickname", "given_name", "website", "name", "birthdate", "email", "gender", "phone_number"]
}


resource "aws_cognito_user_pool" "user-pool" {
  for_each = toset(var.customers)
  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_phone_number"
      priority = "2"
    }

    recovery_mechanism {
      name     = "verified_email"
      priority = "1"
    }
  }

  admin_create_user_config {
    allow_admin_create_user_only = "false"

    invite_message_template {
      email_message = "Your username is {username} and temporary password is {####}. "
      email_subject = "Your temporary password"
      sms_message   = "Your username is {username} and temporary password is {####}. "
    }
  }

  alias_attributes         = ["email"]
  auto_verified_attributes = ["email"]

  email_configuration {
    email_sending_account = "DEVELOPER"
    from_email_address    = "Bizzy Labs Setup <help@bizzylabs.tech>"
    source_arn            = "arn:aws:ses:us-east-1:${data.aws_caller_identity.current.account_id}:identity/help@bizzylabs.tech"
  }

  mfa_configuration = "OFF"
  name              = "${each.key}UserPool"

  password_policy {
    minimum_length                   = "8"
    require_lowercase                = "true"
    require_numbers                  = "true"
    require_symbols                  = "true"
    require_uppercase                = "true"
    temporary_password_validity_days = "7"
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = "false"
    mutable                  = "true"
    name                     = "FirstName"
    required                 = "false"

    string_attribute_constraints {
      max_length = "256"
      min_length = "1"
    }
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = "false"
    mutable                  = "true"
    name                     = "LastName"
    required                 = "false"

    string_attribute_constraints {
      max_length = "256"
      min_length = "1"
    }
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = "false"
    mutable                  = "true"
    name                     = "email"
    required                 = "true"

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  sms_authentication_message = "Your authentication code is {####}. "

  username_configuration {
    case_sensitive = "false"
  }

  verification_message_template {
    default_email_option  = "CONFIRM_WITH_LINK"
    email_message         = "<html>\n<body>\nHi,<br><br>\nWe have received a request to reset your Libretto account password. Your verification code is {####}. \n<br><br>\nIf you did not make this request, please contact us ASAP @  help@bizzylabs.tech.\n<br><br>\nThanks,<br>\nTeam Libretto\n</body>\n</html>"
    email_message_by_link = "Hello,<br><br>\nWelcome to Libretto! Please click {##here##} to verify your email. This link is valid for 24 hours.\n<br><br>\nAfter verification, you can click <a href=\"https://democlient3.incedolabs.com/\">here</a> to log in.\n<br><br>\nIf you did not register for Libretto, please let us know at help@bizzylabs.tech.\n<br><br>\nThank you,\n<br>\nThe Libretto Team @ Bizzy Labs"
    email_subject         = "Libretto - Password Reset Request"
    email_subject_by_link = "Libretto Verification Link"
    sms_message           = "Your verification code is {####}. "
  }
  tags = {
    client        = "${each.key}"
  }
}

resource "aws_cognito_user_pool_domain" "domain" {
  for_each = toset(var.customers)    
  domain       = "libretto-${each.key}-bizzy-labs"
  user_pool_id = aws_cognito_user_pool.user-pool[each.key].id
}
terraform {
  backend "s3" {
    bucket = "bizzylabs-terraform-dev"
    key    = "Libretto/dev/"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.57.0"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region  = "us-east-1"
}

locals {
  env = "dev"
  customers = ["tc1"]
}

module "s3" {
  source = "../modules/s3"
  env = local.env
  customers = local.customers
  cloudfront_users = module.cloudfront.cloudfront_users
}

module "ecr" {
  env = local.env
  source = "../modules/ecr"
}

module "sqs" {
  source = "../modules/sqs"
  customers = local.customers
}

module "sfn" {
  source = "../modules/sfn"
  env = local.env
  customers = local.customers
}

module "iam" {
  source = "../modules/iam"
  env = local.env
  customers = local.customers
}

module "codebuild" {
  source = "../modules/codebuild"
  env = local.env
  customers = local.customers
}

module "waf" {
  source = "../modules/waf"
  logs-bucket =  module.s3.logs-bucket
}

module "cloudfront" {
  source = "../modules/cloudfront"
  env = local.env
  customers = local.customers
  waf = module.waf.waf-id
  logs-bucket =  module.s3.logs-bucket
}

module "kms" {
  source = "../modules/kms"
  customers = local.customers
}

module "cloudwatch" {
  source = "../modules/cloudwatch"
  customers = local.customers
}

module "cognito" {
  source = "../modules/cognito"
  customers = local.customers
}

module "ses" {
  source = "../modules/ses"
}

module "sg" {
  source = "../modules/sg"
  customers = local.customers
}

module "route53" {
  source = "../modules/route53"
  uis = module.cloudfront.ui_urls
}

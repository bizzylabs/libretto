The terraform directory has all the terraform scripts for libretto backend and frontend.

Environment directories (dev/uat/prod) contain the environment specific configurations and variables

the module directories contains the actual terraform code for creating the aws resources.
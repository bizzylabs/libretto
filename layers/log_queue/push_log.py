import json
import boto3

sqs = boto3.client('sqs')
secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId="Queues")
secret_dict = json.loads(secret_response["SecretString"])


def push(payload):
    payload = {k: v for k, v in payload.items() if v is not None}
    if not {'username', 'action', 'timestamp'}.issubset(payload.keys()):
        raise Exception("Information not enough to log, required - Action, Username and Timestamp.")
    try:
        response = sqs.send_message(
            QueueUrl=secret_dict['AuditLogQueueDemoClient4'],
            MessageBody=json.dumps(payload),
            DelaySeconds=0,
            MessageGroupId=payload.get("username")
        )
    except Exception as e:
        try:
            response = sqs.send_message(
                QueueUrl=secret_dict['AuditLogQueueDemoClient4'],
                MessageBody=json.dumps(payload),
                DelaySeconds=0,
                MessageGroupId=payload.get("username")
            )
        except Exception as e:
            raise Exception(e)
    return response

role_precedence= {'SUPER-ADMIN':3,
                  'BSI':2,
                  'MEMBER':1}

def is_permitted(user_group= "", required_group= ""):
    user_precedence = role_precedence.get(user_group, 0)
    required_group_precedence = role_precedence.get(required_group, 0)
    if user_precedence >= required_group_precedence:
        return True
    return False
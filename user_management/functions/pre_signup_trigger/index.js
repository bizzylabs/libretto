const aws = require("aws-sdk");
const CognitoIdentityServiceProvider = aws.CognitoIdentityServiceProvider;
const cognitoIdp = new CognitoIdentityServiceProvider()
const getUserByEmail = async (userPoolId, email) => {
 const params = {
   UserPoolId: userPoolId,
   Filter: `email = "${email}"`
 }
 return cognitoIdp.listUsers(params).promise()
}
 
const linkProviderToUser = async (username, userPoolId, providerName, providerUserId) => {
 const params = {
   DestinationUser: {
     ProviderAttributeValue: username, 
     ProviderName: 'Cognito'
   },
   SourceUser: {
     ProviderAttributeName: 'Cognito_Subject',
     ProviderAttributeValue: providerUserId, 
     ProviderName: providerName 
   }, 
   UserPoolId: userPoolId
 }
 console.log(params);
 const result = await (new Promise((resolve, reject) => {
   cognitoIdp.adminLinkProviderForUser(params, (err, data) => {
     if (err) {
       console.log(err);
       reject(err)
       return
     }
     console.log(data);
     resolve(data)
   })
 }))
 return result
}

exports.handler = async (event, context, callback) => {
 console.log(event);
 if (event.triggerSource === 'PreSignUp_ExternalProvider') {
   const userRs = await getUserByEmail(event.userPoolId, event.request.userAttributes.email)
   console.log(userRs)
   console.log(userRs["Users"][0]["Attributes"])
   if (userRs && userRs.Users.length > 0 && userRs["Users"][0]["Attributes"].length > 0) {
     // const [ providerName, providerUserId ] = event.userName.split('_') // event userName example: "Facebook_12324325436"
     const providerName = event.userName.split('_')[0]; 
     var providerUserIdEmail =  event.userName.split('_')[1]
     userRs["Users"][0]["Attributes"].forEach(element => {
      if (element["Name"] == "email"){
        providerUserIdEmail = element["Value"];
        console.log(providerUserIdEmail)
      }
     });
     const providerUserId = providerUserIdEmail
     await linkProviderToUser( userRs.Users[0].Username, event.userPoolId, providerName, providerUserId) ;
     throw "User Linked";
   } else {
     console.log('user not found, skip.')
     // abort the signup
     throw "User not found";
   }
 }
 return callback(null, event)
}
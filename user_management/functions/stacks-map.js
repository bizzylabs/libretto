module.exports = {
   'AWS::ApiGateway::Resource': false,
};

module.exports = (resource, logicalId) => {
  if (logicalId.startsWith('ApiGatewayMethodA')) {
    return { destination: 'GatewayPostMethodsA', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodB')) {
    return { destination: 'GatewayPostMethodsB', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodC')) {
    return { destination: 'GatewayPostMethodsC', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodD')) {
    return { destination: 'GatewayPostMethodsD', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodE')) {
    return { destination: 'GatewayPostMethodsE', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodF')) {
    return { destination: 'GatewayPostMethodsF', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodG')) {
    return { destination: 'GatewayPostMethodsG', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodH')) {
    return { destination: 'GatewayPostMethodsH', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodI')) {
    return { destination: 'GatewayPostMethodsI', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodJ')) {
    return { destination: 'GatewayPostMethodsJ', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodK')) {
    return { destination: 'GatewayPostMethodsK', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodL')) {
    return { destination: 'GatewayPostMethodsL', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodM')) {
    return { destination: 'GatewayPostMethodsM', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodN')) {
    return { destination: 'GatewayPostMethodsN', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodO')) {
    return { destination: 'GatewayPostMethodsO', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodP')) {
    return { destination: 'GatewayPostMethodsP', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodQ')) {
    return { destination: 'GatewayPostMethodsQ', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodR')) {
    return { destination: 'GatewayPostMethodsR', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodS')) {
    return { destination: 'GatewayPostMethodsS', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodT')) {
    return { destination: 'GatewayPostMethodsT', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodU')) {
    return { destination: 'GatewayPostMethodsU', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodV')) {
    return { destination: 'GatewayPostMethodsV', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodW')) {
    return { destination: 'GatewayPostMethodsW', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodX')) {
    return { destination: 'GatewayPostMethodsX', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodY')) {
    return { destination: 'GatewayPostMethodsY', allowSuffix: true,  force: true};
  }
  if (logicalId.startsWith('ApiGatewayMethodZ')) {
    return { destination: 'GatewayPostMethodsZ', allowSuffix: true,  force: true};
  }
};

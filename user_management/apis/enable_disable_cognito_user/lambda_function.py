import datetime
import json
import os

import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        Username = event["body"].get("Username", None)
        action = event["body"].get("Action", "Deactivate")
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        if not Username and not action:
            return {"Error": "User name and action is required", "statusCode": 500}
        cdt = datetime.datetime.now()
        log_document = {}
        try:
            if action == "Deactivate":
                response = cognito.admin_disable_user(
                    UserPoolId=user_pool_id,
                    Username=Username
                )
                try:
                    logdin_username = event["context"]["username"]
                except:
                    logdin_username = ""
                log_document["ip"] = ip
                log_document["browser"] = browser
                log_document["username"] = logdin_username
                log_document["timestamp"] = cdt
                log_document["action"] = "Deactivated"
                log_document["resource"] = Username
                log_document["log_source"] = "Users"
            elif action == "Activate":
                response = cognito.admin_enable_user(
                    UserPoolId=user_pool_id,
                    Username=Username
                )
                try:
                    logdin_username = event["context"]["username"]
                except:
                    logdin_username = ""
                log_document["ip"] = ip
                log_document["browser"] = browser
                log_document["username"] = logdin_username
                log_document["timestamp"] = cdt
                log_document["action"] = "Activated"
                log_document["resource"] = Username
                log_document["log_source"] = "Users"
            else:
                return {"Error": "Invalid Action", "statusCode": 500}
            insert_log = db.BussinessAuditLogs.insert_one(log_document)

            post = {
                'isActive': True if action == 'Activate' else False,
                'UpdatedOn': cdt
            }
            users_update_query = db.Users.update_one({'username': Username}, {"$set": post}, upsert=False)
            # print(users_update_query.raw_result)
        except Exception as err:
            return {
                'statusCode': 500,
                'body': {"Error": str(err)}
            }

        return {
            'statusCode': 200,
            'body': {"Success": f"{Username} has been {action.lower()}d."}
        }

    except Exception as err:
        return {'statusCode': 500, "Error": "enable disable cognito user failed"}

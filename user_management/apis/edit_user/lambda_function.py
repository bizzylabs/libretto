import json
import os
import datetime
import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]
source_email = secret["source_email"]
address = secret["address"]

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

cognito = boto3.client("cognito-idp", region_name="us-east-1")
lambda_invoke = boto3.client("lambda", region_name="us-east-1")


def lambda_handler(event, context):
    try:
        # TODO implement
        username = event["body"].get("username")
        groups = event["body"].get("groups", [])
        access_filters = event["body"].get("access_filters", [])
        group_result = cognito.admin_list_groups_for_user(Username=username, UserPoolId=USER_POOL_ID)
        group = group_result.get("Groups", "")
        firstname = event["body"].get("Firstname", "Firstname")
        lastname = event["body"].get("Lastname", "Lastname")
        enable_filter = event["body"].get("EnableFilter", False)
        condition_filter = event["body"].get("condition_filters", {})
        options_filter = event["body"].get("options_filter", {})
        user_groups = []
        if not username and not groups:
            return {"Error": "username and  groups are required", "statusCode": 500}

        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)

        cdt = datetime.datetime.now()
        log_document = {}

        try:
            if group:
                user_groups = [item["GroupName"] for item in group]
                for groupname in user_groups:
                    response = cognito.admin_remove_user_from_group(
                        UserPoolId=USER_POOL_ID,
                        Username=username,
                        GroupName=groupname
                    )
                    delete_users = db.RuleSetMaster.update(
                        {},
                        {"$pull": {"Users": {"$in": [username]}}}, upsert=False, multi=True
                    )

            ruleset_names = set()

            for group in groups:
                response = cognito.admin_add_user_to_group(
                    UserPoolId=USER_POOL_ID,
                    Username=username,
                    GroupName=group
                )
            ruleobjs = db.UserRolePermissions.find({"RoleName": {"$in": groups}},
                                                   {"_id": False, "ReviewResults.RuleSetNames": 1})
            if ruleobjs:
                for item in ruleobjs:
                    ruleset_names.update(item["ReviewResults"]["RuleSetNames"])
            if len(ruleset_names) > 0:
                rulesets = db.RuleSetMaster.find({"RuleSetName": {"$in": list(ruleset_names)}}, {"Users": 1})
                for ruleset in rulesets:
                    user = ruleset["Users"]
                    if username not in user:
                        user.append(username)
                    db.RuleSetMaster.update({"_id": ruleset["_id"]}, {"$set": {"Users": user}})

            filterobj = db.UserFilters.update({"UserName": username},
                                              {"$set": {"Filters": access_filters, "EnableFilter": enable_filter,
                                                        "ConditionsFilter": condition_filter,
                                                        "OptionsFilter": options_filter}})
            user_attributes = []
            if firstname:
                user_attributes.append({
                    'Name': 'custom:FirstName',
                    'Value': firstname
                })
            if lastname:
                user_attributes.append({
                    'Name': 'custom:LastName',
                    'Value': lastname
                })

            res = cognito.admin_update_user_attributes(
                UserPoolId=USER_POOL_ID,
                Username=username,
                UserAttributes=user_attributes
            )

            user_edit_logs = {}
            mismatch_count = 0
            for grp in user_groups:
                if grp not in groups:
                    mismatch_count+= 1
            if mismatch_count > 0 or len(groups) != len(user_groups):
                user_edit_logs["user_groups_old"] = user_groups
                user_edit_logs["user_groups_new"] = groups
            if firstname:
                user_edit_logs["user_firstname"] = firstname
            if lastname:
                user_edit_logs["user_lastname"] = lastname

            user_filters = {"Filters": access_filters, "EnableFilter": enable_filter, "ConditionsFilter": condition_filter, "OptionsFilter": options_filter}
            user_edit_logs["user_filters"] = user_filters

            try:
                # This is the logedin
                logdin_username = event["context"]["username"]
            except:
                logdin_username = ""
            log_document["ip"] = ip
            log_document["browser"] = browser
            log_document["username"] = logdin_username
            log_document["timestamp"] = cdt
            log_document["action"] = "Edited"
            log_document["resource"] = username
            log_document["log_source"] = "Users"
            log_document["edit_details"] = user_edit_logs

            insert_log = db.BussinessAuditLogs.insert_one(log_document)
            post = {
                'firstName': firstname,
                'LastName': lastname,
                'roles': groups,
                'UpdatedOn': cdt
            }
            users_update_query = db.Users.update_one({'username': username}, {"$set": post}, upsert=False)
            # print(users_update_query.raw_result)
            return {
                'statusCode': 200,
                'body': 'User has been updated successfully.'
            }
        except Exception as err:
            return {"Error": str(err), "statusCode": 500}

    except Exception as err:
        return {'statusCode': 500, "Error": "edit user failed"}
import boto3
import json
from datetime import datetime
from pymongo import MongoClient
import os
from base64 import b64decode, b64encode

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

cognito_idp = boto3.client("cognito-idp", region_name="us-east-1")


def lambda_handler(event, context):
    try:
        try:
            event["body"] = eval(b64decode(event["body"]["result"]).decode('utf-8'))
        except Exception as e:
            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Unknown Error"}

        access_token = event["body"]["AccessToken"]
        previous_password = event["body"]["OldPassword"]
        new_password = event["body"]["NewPassword"]

        username = event["context"].get("username", None)
        email = event["context"].get("email", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)

        if len(previous_password) < 8 or len(new_password) < 8:
            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Password must be minimum 8 characters in length and must contain uppercase letters, lowercase letters, numbers and special characters."}

        if db.BussinessAuditLogs.find({'$and': [{'action': 'change-password'}, {'username': username}, {
            '$where': 'daysBetween(this.timestamp, new Date() ) <= 1 '}]}).count() == 3:
            return {"error": True, "data": None, "success": False,
                    "message": "Maximum limit reached, you can send maximum 3 requests in a day"}
        try:
            response = cognito_idp.change_password(
                PreviousPassword=previous_password,
                ProposedPassword=new_password,
                AccessToken=access_token)

            db.BussinessAuditLogs.insert_one({"action": "change-password", "username": username, "email": email, "ip": ip,
                                              "browser": browser, "timestamp": datetime.now()})

            return {
                "error": False,
                "success": True,
                "message": "Password changed successfully"

            }

        except cognito_idp.exceptions.UserNotFoundException:

            return {"error": True,
                    "data": None,
                    "success": False,
                    "message": "Username doesnt exists"}

        except cognito_idp.exceptions.InvalidParameterException:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": f"User <{username}> is not confirmed yet"}

        except cognito_idp.exceptions.InvalidPasswordException:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Password must be minimum 8 characters in length and must contain uppercase letters, lowercase letters, numbers and special characters."}

        except cognito_idp.exceptions.NotAuthorizedException:
            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Invalid Old Password."}

        except Exception as e:
            print(e)

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Unknown Error"}

    except Exception as err:
        return {'statusCode': 500, "Error": "admin change password failed"}
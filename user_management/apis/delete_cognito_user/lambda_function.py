import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import boto3
import os
import datetime

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
user_pool_id = secret["user_pool_id"]
cognito = boto3.client("cognito-idp", region_name="us-east-1")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        Username = event["body"].get("Username", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)

        cdt = datetime.datetime.now()
        log_document = {}

        if not Username:
            return {"Error": "User name is required", "statusCode": 500}
        try:
            response = cognito.admin_delete_user(
                UserPoolId=user_pool_id,
                Username=Username
            )

            delete_users = db.RuleSetMaster.update(
                {},
                {"$pull": {"Users": {"$in": [Username]}}}, upsert=False, multi=True
            )

            remove0bj = db.UserFilters.remove({"UserName": Username})
            try:
                logdin_username = event["context"]["username"]
            except:
                logdin_username = ""
            log_document["ip"] = ip
            log_document["browser"] = browser
            log_document["username"] = logdin_username
            log_document["timestamp"] = cdt
            log_document["action"] = "Deleted"
            log_document["resource"] = Username
            log_document["log_source"] = "Users"

            insert_log = db.BussinessAuditLogs.insert_one(log_document)

            post = {
                'isDeleted': True,
                'isActive': False,
                'UpdatedOn': cdt
            }
            users_update_query = db.Users.update_one({'username': Username}, {"$set": post}, upsert=False)
            # print(users_update_query.raw_result)
        except Exception as err:
            return {
                'statusCode': 500,
                'body': {"Error": str(err)}
            }
        return {
            'statusCode': 200,
            'body': {"Success": "User deleted successfully."}
        }

    except Exception as err:
        return {'statusCode': 500, "Error": "delete cognito user failed"}


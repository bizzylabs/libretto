import json
import os
from datetime import datetime
from botocore.exceptions import ClientError
import boto3
from log_queue import push_log
from pymongo import MongoClient
from base64 import b64decode, b64encode

client = boto3.client("secretsmanager")

secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret.get("USER_POOL_ID", secret["user_pool_id"])
CLIENT_ID = secret.get("CLIENT_ID", secret["client_id"])

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict[
        "host"] + "/" + mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def verify_auth_mfa(cognito_client, challenge_name, session, username, user_code):
    try:
        response = cognito_client.respond_to_auth_challenge(
            ClientId=CLIENT_ID,
            ChallengeName=challenge_name,
            Session=session,
            ChallengeResponses={
                'USERNAME': username,
                'SOFTWARE_TOKEN_MFA_CODE': user_code
            }
        )
        return response, None
    except cognito_client.exceptions.CodeMismatchException:
        return None, "Code mismatch, please enter correct code"
    except cognito_client.exceptions.EnableSoftwareTokenMFAException:
        return None, "Code mismatch, please enter correct code"
    except cognito_client.exceptions.SoftwareTokenMFANotFoundException:
        return None, "Code does not match what server was expecting"
    except cognito_client.exceptions.InvalidUserPoolConfigurationException:
        return None, "User pool configuration invalid"
    except cognito_client.exceptions.InvalidParameterException:
        return None, "Invalid input entered, please enter correct MFA code"
    except cognito_client.exceptions.NotAuthorizedException:
        return None, "Invalid session ID, please log-in again"
    except ClientError as e:
        error_msg = e.__str__()
        if e.response and 'Error' in e.response and 'Message' in e.response['Error']:
            error_msg = e.response['Error']['Message']
        return None, error_msg
    except Exception as e:
        return None, e.__str__()


def lambda_handler(event, context):
    try:
        """
        This lambda is invoked after MFA has been configured by user
    
        :param event: username, challenge_name, session and user_code
        :param context:
        :return:refresh_token,access_token,token_type,group_name,permissions,username,first_name,last_name,Environment
        """

        for field in ["username", "challenge_name", "session", "user_code"]:
            if event["body"].get(field) is None:
                return {"statusCode": 400, "error": f"{field} is required"}

        username = event["body"].get("username", None)
        challenge_name = event["body"].get("challenge_name", 'SOFTWARE_TOKEN_MFA')
        session = event["body"].get("session", None)
        user_code = event["body"].get("user_code", None)
        cognito_client = boto3.client('cognito-idp')

        response, error_message = verify_auth_mfa(cognito_client, challenge_name, session, username, user_code)
        if error_message is not None:
            return {
                "statusCode": 400,
                'message': error_message,
            }
        if response.get("AuthenticationResult"):
            email, first_name, last_name = get_user_attributes(cognito_client, username)
            group_name, permissions_dict = get_group_with_permissions(cognito_client, username)
            # Logging events
            log_user_events(email, event, username)
            env_data = db.ApplicationDetails.find_one({})
            if env_data:
                env = env_data["Environment"]
            else:
                env = "uat"

            result = {
                "statusCode": 200,
                "result": {
                    "refresh_token": response["AuthenticationResult"]["RefreshToken"],
                    "access_token": response["AuthenticationResult"]["AccessToken"],
                    "token_type": response["AuthenticationResult"]["TokenType"],
                    "group": group_name,
                    "permissions": permissions_dict,
                    "username": username,
                    "first_name": first_name,
                    "last_name": last_name,
                    "Environment": env
                }
            }
            encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            return {"result": encoded_string}
        else:  # Unknown error, ideally code should not reach this else condition
            return {
                "statusCode": 503,
                "error": "Unknown state, should never reach here"
            }
    except Exception as err:
        return {'statusCode': 500, "Error": "auth verify mfa failed"}


def log_user_events(email, event, username):
    ip = event["context"].get("source-ip", None)
    browser = event["context"].get("user-agent", None)
    log = {"action": "login", "username": username, "email": email, "ip": ip,
           "browser": browser, "timestamp": str(datetime.now())}
    push_log.push(log)


def get_user_attributes(cognito_client, username):
    user_data = cognito_client.admin_get_user(
        UserPoolId=USER_POOL_ID,
        Username=username
    )
    attributes_list = user_data["UserAttributes"]
    email, first_name, last_name = "", "", ""
    for attribute in attributes_list:
        attr_name = attribute["Name"]
        attr_val = attribute["Value"]
        if attr_name == "email":
            email = attr_val
        elif attr_name == "custom:LastName" and attr_val != "Lastname":
            last_name = attr_val
        elif attr_name == "custom:FirstName" and attr_val != "Firstname":
            first_name = attr_val
    return email, first_name, last_name


def get_group_with_permissions(cognito_client, username):
    permissions_dict = {
        "ReviewResults": {
            "RuleSetNames": [],
            "View": False,
            "Export": False,
            "Create/Edit": False,
            "GrantAccessFalg": False,
        },
        "RuleLibrary": {"Create/Edit": False},
        "Scheduler": False,
        "UserManagement": False,
    }
    group_result = cognito_client.admin_list_groups_for_user(Username=username,
                                                             UserPoolId=USER_POOL_ID)
    groups = group_result.get("Groups", "")
    groups_name = ["MEMBER"]
    if groups:
        groups_name = [item["GroupName"] for item in groups]
        temp_lis = []
        for role_name in groups_name:
            access = db.UserRolePermissions.find_one({"RoleName": role_name})
            access_review_result = access["ReviewResults"]
            temp_lis.extend(access_review_result["RuleSetNames"])
            if access_review_result["View"]:
                permissions_dict["ReviewResults"]["View"] = True
            if access_review_result["View"]:
                permissions_dict["ReviewResults"]["View"] = True
            if access_review_result["Export"]:
                permissions_dict["ReviewResults"]["Export"] = True
            if access_review_result["GrantAccessFalg"]:
                permissions_dict["ReviewResults"]["GrantAccessFalg"] = True
            # Defaulting to True for ReviewResults["Create/Edit"] to handle existing roles in DB
            permissions_dict["ReviewResults"]["Create/Edit"] = access_review_result.get("Create/Edit", True)
            if access["RuleLibrary"]["Create/Edit"]:
                permissions_dict["RuleLibrary"]["Create/Edit"] = True
            if access["Scheduler"]:
                permissions_dict["Scheduler"] = True
            if access["UserManagement"]:
                permissions_dict["UserManagement"] = True
        permissions_dict["ReviewResults"]["RuleSetNames"] = list(set(temp_lis))
    enable_filter = db.UserFilters.find_one({"UserName": username})
    if enable_filter:
        enable_filter_value = enable_filter["EnableFilter"]
    else:
        enable_filter_value = False
    permissions_dict["EnableFilter"] = enable_filter_value
    return groups_name, permissions_dict

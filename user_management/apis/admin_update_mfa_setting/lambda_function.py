import json
import os

import boto3

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])

secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]


def lambda_handler(event, context):
    cognito_client = boto3.client('cognito-idp')
    status = event.get('body', {}).get("status", True)

    try:
        if status:
            cognito_client.set_user_pool_mfa_config(
                UserPoolId=USER_POOL_ID,
                SoftwareTokenMfaConfiguration={
                    'Enabled': status
                },
                MfaConfiguration='OPTIONAL'
            )
            return {
                "statusCode": 200,
                "message": "Successfully enabled for all users"}
        elif not status:
            cognito_client.set_user_pool_mfa_config(
                UserPoolId=USER_POOL_ID,
                MfaConfiguration='OFF'
            )
            return {
                "statusCode": 200,
                "message": "Successfully disabled for all users"}
    except Exception as err:
        return {'statusCode': 500, "Error": "admin update mfa setting failed"}
import json
import os
from datetime import datetime

import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
CLIENT_ID = secret["client_id"]
USER_POOL_ID = secret["user_pool_id"]

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

cognito_idp = boto3.client('cognito-idp', region_name='us-east-1')


def forgot_password(event, context):
    try:
        try:
            event = event["body"]
        except Exception as e:
            event = event
        try:
            username = event['username']
            auth = cognito_idp.admin_get_user(
                UserPoolId=USER_POOL_ID,
                Username=username
            )
            if db.BussinessAuditLogs.find({'$and': [{'action': 'forgot-password'}, {'username': username}, {
                '$where': 'function(){var dt1 = new Date(); var diff = ((dt1.getTime() - this.timestamp.getTime()) / 1000/60); out = Math.abs(Math.round(diff)); return out < 15}'}]}).count() == 3:
                return {"error": True, "data": None, "success": False,
                        "message": "Maximum limit reached, you can send maximum 3 requests per 15 minutes"}

            response = cognito_idp.forgot_password(
                ClientId=CLIENT_ID,
                Username=username,

            )
            db.BussinessAuditLogs.insert_one(
                {"action": "forgot-password", "username": username, "timestamp": datetime.now()})
            return {
                "error": False,
                "success": True,
                "message": f"Please check your Registered email id for validation code",
                "data": response}

        except cognito_idp.exceptions.UserNotFoundException:

            return {"error": True,
                    "data": None,
                    "success": True,
                    "message": "Please check your Registered email id for validation code"}

        except cognito_idp.exceptions.InvalidParameterException:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": f"User <{username}> is not confirmed yet....."}

        except Exception as e:
            print(e)
            return {"error": True,
                    "success": True,
                    "data": None,
                    "message": "Unknown Error"}
    except Exception as err:
        return {'statusCode': 500, "Error": "auth forgot password failed"}

import json
import os
from datetime import datetime
import boto3
from pymongo import MongoClient

# getting cognito details
client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]

# getting user password details
secret_pass_response = client.get_secret_value(SecretId=os.environ["SECRET_PASS"])
secret_pass = json.loads(secret_pass_response["SecretString"])
secret_password = secret_pass["password"]

# getting mongo details
mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

client = boto3.client("cognito-idp")
lambda_invoke = boto3.client("lambda")


def filter_gen(filter_val):
    try:
        packages_name = [item["Name"] for item in db.LoanPackage.find({})]
        # check if both are identical having same packages name and count
        packages_name.sort()
        keys = list(filter_val.keys())
        keys.sort()
        if packages_name != keys:
            return {"error": True,
                    "message": "Wrong access filters"}

        # check if all values present in filter are correct
        user_access_filter = db.UserAccessFilters.find_one({})
        for package_data in packages_name:
            temp_key = list(filter_val[package_data].keys())
            temp_key2 = user_access_filter["Filters"][package_data]
            if temp_key != temp_key2:
                return {"error": True,
                        "message": "Wrong access filters"}
        acc_filter = {}
        con_filter = {}
        opt_filter = {}
        for package_data in packages_name:
            acc_filter[package_data] = {}
            con_filter[package_data] = {}
            opt_filter[package_data] = {}
            for filter_key in user_access_filter["Filters"][package_data]:
                key = list(filter_val[package_data][filter_key].keys())[0]
                if key == "Include":
                    acc_filter[package_data][filter_key] = filter_val[package_data][filter_key][key]
                    con_filter[package_data][filter_key] = {"Include": filter_val[package_data][filter_key][key]}
                    opt_filter[package_data][filter_key] = [{"label": "Include", "value": "Include"}]
                else:
                    acc_filter[package_data][filter_key] = filter_val[package_data][filter_key][key]
                    con_filter[package_data][filter_key] = {"NotInclude": filter_val[package_data][filter_key][key]}
                    opt_filter[package_data][filter_key] = [{"label": "Exclude", "value": "Exclude"}]

        return {"error": False,
                "acc_filter": acc_filter,
                "con_filter": con_filter,
                "opt_filter": opt_filter
                }
    except Exception as e:
        print(e)
        return {"error": True,
                "message": "Error while creating access filter."}


def default_filter_gen():
    user_access_filter = db.UserAccessFilters.find_one({})
    acc_filter = {}
    con_filter = {}
    opt_filter = {}
    for package_data in user_access_filter["Filters"]:
        acc_filter[package_data] = {}
        con_filter[package_data] = {}
        opt_filter[package_data] = {}
        for filter_key in user_access_filter["Filters"][package_data]:
            acc_filter[package_data][filter_key] = []
            con_filter[package_data][filter_key] = {"Include": []}
            opt_filter[package_data][filter_key] = [{"label": "Include", "value": "Include"}]

    return acc_filter, con_filter, opt_filter


def should_prompt_mfa_setup(cognito_client):
    user_pool_resp = cognito_client.get_user_pool_mfa_config(UserPoolId=USER_POOL_ID)
    should_prompt_mfa = False
    if user_pool_resp \
            and user_pool_resp.get("MfaConfiguration") == 'OPTIONAL' \
            and user_pool_resp["SoftwareTokenMfaConfiguration"].get("Enabled", False) is True:
        should_prompt_mfa = True
    return should_prompt_mfa


def initiate_auth(username, password):
    try:
        resp = client.initiate_auth(
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': username,
                'PASSWORD': password
            },
            ClientId=CLIENT_ID
        )
    except client.exceptions.NotAuthorizedException:
        return None, "The username or password is incorrect"
    except client.exceptions.UserNotConfirmedException:
        return None, "User is not confirmed"
    except Exception as e:
        return None, str(e)
    return resp, None


def create_user(event, username, email, first_name, last_name, groups, enable_filter, access_filters, condition_filter,
                options_filter, password):
    try:
        # create user
        response = client.admin_create_user(
            UserPoolId=USER_POOL_ID,
            Username=username,
            UserAttributes=[
                {
                    'Name': 'email',
                    'Value': email
                },
                {
                    'Name': 'email_verified',
                    'Value': 'true'
                },
                {
                    'Name': 'custom:FirstName',
                    'Value': first_name
                },
                {
                    'Name': 'custom:LastName',
                    'Value': last_name
                }
            ],
            ValidationData=[
                {
                    'Name': 'email',
                    'Value': email
                },
            ],
            MessageAction='SUPPRESS',
            DesiredDeliveryMediums=[
                'EMAIL',
            ]
        )

    except client.exceptions.UsernameExistsException:
        return {"error": True,
                "message": "User with same username/email id already exists"}
    except Exception as e:
        print(e)
        return {"error": True,
                "message": str(e), }

    try:
        # create password
        response = client.admin_set_user_password(
            UserPoolId=USER_POOL_ID,
            Username=username,
            Password=password,
            Permanent=True
        )
    except client.exceptions.InvalidPasswordException as e:
        print(e)
        return {"error": True,
                "message": "Password must be minimum 8 characters in length and must contain uppercase letters, "
                           "lowercase letters, numbers and special characters."}
    except client.exceptions.UserNotFoundException as e:
        print(e)
        return {"error": True,
                "message": "User not found"}
    except Exception as e:
        print(e)
        return {"error": True,
                "message": str(e)}

    try:
        # add user in the group
        ruleset_names = set()
        for group in groups:
            response = client.admin_add_user_to_group(
                UserPoolId=USER_POOL_ID,
                Username=username,
                GroupName=group
            )
        ruleobjs = db.UserRolePermissions.find({"RoleName": {"$in": groups}},
                                               {"_id": False, "ReviewResults.RuleSetNames": 1})
        if ruleobjs:
            for item in ruleobjs:
                ruleset_names.update(item["ReviewResults"]["RuleSetNames"])
        if len(ruleset_names) > 0:
            rulesets = db.RuleSetMaster.find({"RuleSetName": {"$in": list(ruleset_names)}}, {"Users": 1})
            for ruleset in rulesets:
                user = ruleset["Users"]
                if username not in user:
                    user.append(username)
                db.RuleSetMaster.update({"_id": ruleset["_id"]}, {"$set": {"Users": user}})
    except Exception as e:
        print(e)
        return {"error": True,
                "message": str(e)}

    # add some custom filter
    document = {"UserName": username, "EnableFilter": enable_filter, "Filters": access_filters,
                "ConditionsFilter": condition_filter, "OptionsFilter": options_filter}
    filterobj = db.UserFilters.insert_one(document)

    log_inserter(event, username)

    return {"error": False, 'message': "User Created Successfully"}


def log_inserter(event, username, log=None):
    cdt = datetime.now()
    ip = event["context"].get("source-ip", None)
    browser = event["context"].get("user-agent", None)
    logged_in_username = event["context"].get("username", "")
    log_document = {"ip": ip, "browser": browser, "username": logged_in_username, "timestamp": cdt, "action": "Added",
                    "message": "added through iframe", "resource": username, "log_source": "Users"}
    if log:
        log_document = log
        log_document["ip"] = ip
        log_document["browser"] = browser
    try:
        insert_log = db.BussinessAuditLogs.insert_one(log_document)
    except Exception as err:
        return {
            'statusCode': 500,
            'body': {"Error": str(err)}
        }


def lambda_handler(event, context):
    try:
        for field in ["email", "username"]:
            if not event["body"].get(field):
                return {"error": True, "success": False, 'message': f"{field} is not present", "data": None}
        username = event["body"].get("username", None).lower()
        email = event["body"]["email"]
        groups = event["body"].get("roles", [])
        first_name = event["body"].get("firstname", "Firstname")
        last_name = event["body"].get("lastname", "Lastname")
        access_filters_value = event["body"].get("access_filters", None)
        enable_filter = event["body"].get("enable_filter", False)
        if not first_name:
            first_name = "Firstname"
        if not last_name:
            last_name = "Lastname"
        if type(groups) != list:
            return {"statusCode": 500, "error": "Roles should be in array format"}
        if len(groups) == 0:
            groups = ["MEMBER"]

        if access_filters_value:
            data = filter_gen(access_filters_value)
            if data["error"]:
                return {'statusCode': 500, "error": data["message"]}
            else:
                access_filters = data["acc_filter"]
                condition_filter = data["con_filter"]
                options_filter = data["opt_filter"]
        else:
            access_filters, condition_filter, options_filter = default_filter_gen()

        for item in groups:
            try:
                response = client.get_group(
                    GroupName=item,
                    UserPoolId=USER_POOL_ID
                )
            except client.exceptions.ResourceNotFoundException:
                return {'statusCode': 500, "error": f"Role name - {item}  doesn't exist."}
            except Exception as e:
                print(e)
                return {'statusCode': 500, "error": "Sign in failed"}

        # check if user is present in system
        try:
            res = client.admin_get_user(
                UserPoolId=USER_POOL_ID,
                Username=username
            )
        except client.exceptions.UserNotFoundException:
            # create user in the system
            response = create_user(event, username, email, first_name, last_name, groups, enable_filter, access_filters,
                                   condition_filter,
                                   options_filter, secret_password)
            if response["error"]:
                return {'statusCode': 500, "error": response["message"]}
        except Exception as e:
            print(e)
            return {'statusCode': 500, "error": "Sign in failed"}

        resp, msg = initiate_auth(username, secret_password)
        if msg:
            print("here")
            return {'error': msg,
                    'statusCode': 500}
        if resp.get("AuthenticationResult"):

            log = {"action": "login", "username": username, "email": email, "timestamp": str(datetime.now())}
            log_inserter(event, username, log)
            should_prompt_mfa = should_prompt_mfa_setup(client)
            result = {
                'statusCode': 200,
                "refresh_token": resp["AuthenticationResult"]["RefreshToken"],
                "access_token": resp["AuthenticationResult"]["AccessToken"],
                "mfa_flag": should_prompt_mfa,
                "role": groups
            }
            # encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            # return {"result": encoded_string}
            return result
        # this code block is relevant only when MFA is enabled
        elif 'Session' in resp:
            result = {"statusCode": 201,
                      "data": {"session": resp['Session'], "challenge_name": resp['ChallengeName'],
                               "mfa_flag": False},
                      }
            # encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            # return {"result": encoded_string}
            return result
        else:
            return {'statusCode': 500,
                    "error": "Sign in failed"}
    except Exception as err:
        print(err)
        return {'statusCode': 500, "error": "Sign in failed"}

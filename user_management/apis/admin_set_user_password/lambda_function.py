import boto3
import json
import os
from base64 import b64decode, b64encode

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret_dict = json.loads(secret_response["SecretString"])
CLIENT_ID = secret_dict["client_id"]
USER_POOL_ID = secret_dict["user_pool_id"]

client = boto3.client("cognito-idp")


def lambda_handler(event, context):
    try:
        try:
            event = event["body"]
        except Exception as e:
            event = event
        try:
            event = eval(b64decode(event["result"]).decode('utf-8'))
        except Exception as e:
            return {"error": True, "success": False, 'message': "Data is not present", "data": None}
        for field in ["username", "password"]:
            if not event.get(field):
                return {"error": True, "success": False, 'message': f"{field} is not present", "data": None}
        username = event["username"]
        password = event["password"]
        try:

            response = client.admin_set_user_password(
                UserPoolId=USER_POOL_ID,
                Username=username,
                Password=password,
                Permanent=True
            )
            # encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            return {"error": False,
                    "success": True,
                    "message": "Password created  successfully.",
                    "data": None
                    }
        except client.exceptions.InvalidPasswordException as e:
            print("here")
            return {"error": True,
                    "success": False,
                    "message": "Password must be minimum 8 characters in length and must contain uppercase letters, lowercase letters, numbers and special characters.",
                    "data": None}
        except client.exceptions.UserNotFoundException as e:
            return {"error": True,
                    "success": False,
                    "message": "User not found",
                    "data": None
                    }

        except Exception as e:
            # print("here2")
            return {"error": True,
                    "success": False,
                    "message": str(e),
                    "data": None
                    }
    except Exception as err:
        return {'statusCode': 500, "Error": "admin set user password failed. Please try again"}

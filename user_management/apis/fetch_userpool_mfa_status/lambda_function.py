import json
import os

import boto3

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])


secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]


def lambda_handler(event, context):
    try:
        cognito_client = boto3.client('cognito-idp')
        try:
            is_mfa_enabled = is_userpool_mfa_enabled(cognito_client)

            return {
                "statusCode": 200,
                "is_mfa_configured": is_mfa_enabled
            }
        except Exception as e:
            return {
                "statusCode": 500,
                "message": e.__str__()
            }

    except Exception as err:
        return {'statusCode': 500, "Error": "fetch Userpool mfa status failed"}

def is_userpool_mfa_enabled(cognito_client):
    user_pool_resp = cognito_client.get_user_pool_mfa_config(UserPoolId=USER_POOL_ID)
    is_enabled = False
    if user_pool_resp \
            and user_pool_resp.get("MfaConfiguration") == 'OPTIONAL' \
            and user_pool_resp["SoftwareTokenMfaConfiguration"].get("Enabled", False) is True:
        is_enabled = True
    return is_enabled
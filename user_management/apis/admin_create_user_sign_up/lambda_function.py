import json
import os
from datetime import datetime
import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]
source_email = secret["source_email"]
address = secret["address"]
lform = ""
if "Lform" in secret.keys():
    lform = secret["Lform"]


mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

client = boto3.client("cognito-idp", region_name="us-east-1")
lambda_invoke = boto3.client("lambda", region_name="us-east-1")


def lambda_handler(event, context):
    try:
        for field in ["username", "email"]:
            if not event["body"].get(field):
                return {"error": True, "success": False, 'message': f"{field} is not present", "data": None}
        username = event["body"]["username"].lower().strip()
        email = event["body"]["email"]
        groups = event["body"].get("groups", [])
        first_name = event["body"].get("Firstname", "Firstname")
        last_name = event["body"].get("Lastname", "Lastname")
        access_filters = event["body"].get("access_filters", [])
        # rule_sets = event["body"].get("rulesets", [])
        action = event["body"].get("resend", False)
        enable_filter = event["body"].get("EnableFilter", False)
        condition_filter = event["body"].get("condition_filters", {})
        options_filter = event["body"].get("options_filter", {})

        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)

        cdt = datetime.now()
        log_document = {}

        if action:
            payload = {
                "username": username,
                "email": email,
                "address": address,
                "source_email": source_email,
                "lform":  lform
            }
            lambda_invoke.invoke(
                FunctionName=os.environ["EMAIL_SENDER"],
                InvocationType='Event',
                Payload=json.dumps(payload)
            )
            db.BussinessAuditLogs.insert(
                {"action": "verificationMail", "username": username, "email": email, "timestamp": datetime.now()})
            return {"error": False, "success": True, 'message': "Verification mail sent.", "data": None}
        if len(groups) == 0:
            return {"error": True, "success": False, 'message': "Group is not present", "data": None}

        # flag username exists in db#
        ## email and username should unique.
        user_find_flag = False
        find_user = db.Users.find_one({"username": username, "email": email})
        if find_user:
            user_find_flag = True

        try:
            response = client.admin_create_user(
                UserPoolId=USER_POOL_ID,
                Username=username,
                UserAttributes=[
                    {
                        'Name': 'email',
                        'Value': email
                    },
                    {
                        'Name': 'email_verified',
                        'Value': 'true'
                    },
                    {
                        'Name': 'custom:FirstName',
                        'Value': first_name
                    },
                    {
                        'Name': 'custom:LastName',
                        'Value': last_name
                    }
                ],
                ValidationData=[
                    {
                        'Name': 'email',
                        'Value': email
                    },
                ],
                MessageAction='SUPPRESS',
                DesiredDeliveryMediums=[
                    'EMAIL',
                ]
            )

        except client.exceptions.UsernameExistsException:
            return {"error": True,
                    "data": None,
                    "success": False,
                    "message": "User with same username/email id already exists"}
        except client.exceptions.InvalidParameterException:
            return {"error": True,
                    "data": None,
                    "success": False,
                    "message": "Invalid email address."}
        except Exception as e:
            return {"error": True, "success": False, 'message': str(e), "data": None}
        try:
            ruleset_names = set()
            for group in groups:
                response = client.admin_add_user_to_group(
                    UserPoolId=USER_POOL_ID,
                    Username=username,
                    GroupName=group
                )
            ruleobjs = db.UserRolePermissions.find({"RoleName": {"$in": groups}},
                                                   {"_id": False, "ReviewResults.RuleSetNames": 1})
            if ruleobjs:
                for item in ruleobjs:
                    ruleset_names.update(item["ReviewResults"]["RuleSetNames"])
            if len(ruleset_names) > 0:
                rulesets = db.RuleSetMaster.find({"RuleSetName": {"$in": list(ruleset_names)}}, {"Users": 1})
                for ruleset in rulesets:
                    user = ruleset["Users"]
                    if username not in user:
                        user.append(username)
                    db.RuleSetMaster.update({"_id": ruleset["_id"]}, {"$set": {"Users": user}})

        except Exception as e:
            return {"error": True, "success": False, 'message': str(e), "data": None}
        document = {"UserName": username, "EnableFilter": enable_filter, "Filters": access_filters,
                    "ConditionsFilter": condition_filter, "OptionsFilter": options_filter}
        filterobj = db.UserFilters.insert_one(document)

        payload = {
            "username": username,
            "email": email,
            "address": address,
            "source_email": source_email,
            "lform":  lform
        }
        lambda_invoke.invoke(
            FunctionName=os.environ["EMAIL_SENDER"],
            InvocationType='Event',
            Payload=json.dumps(payload)
        )
        db.BussinessAuditLogs.insert(
            {"action": "verificationMail", "username": username, "email": email, "timestamp": datetime.now()})
        try:
            logdin_username = event["context"]["username"]
        except:
            logdin_username = ""
        log_document["ip"] = ip
        log_document["browser"] = browser
        log_document["username"] = logdin_username
        log_document["timestamp"] = cdt
        log_document["action"] = "Added"
        log_document["resource"] = username
        log_document["log_source"] = "Users"

        insert_log = db.BussinessAuditLogs.insert_one(log_document)
        if user_find_flag:
            post = {
                'firstName': first_name,
                'LastName': last_name,
                'roles': groups,
                'UpdatedOn': cdt,
                'isActive': True,
                'isDeleted': False,
            }
            users_update_query = db.Users.update_one({'username': username}, {"$set": post}, upsert=False)
        else:
            data = {'username': username, 'isActive': True, 'isDeleted': False, 'email': email,
                    'firstName': first_name, 'LastName': last_name, 'roles': groups, 'createdOn': cdt,
                    'UpdatedOn': cdt}
            db.Users.insert_one(data)

        return {"error": False, "success": True, 'message': "User Created Successfully", "data": None}
    except Exception as err:
        return {'statusCode': 500, "error": "admin create user signup failed"}

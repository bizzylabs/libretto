import boto3
import json
import os

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId = os.environ["SECRET_COGN_ID"])
secret_dict = json.loads(secret_response["SecretString"])
CLIENT_ID = secret_dict["client_id"]
USER_POOL_ID = secret_dict['user_pool_id']


def lambda_handler(event, context):
    try:
        try:
            event = event["body"]
        except Exception as e:
            event = event
        client = boto3.client("cognito-idp", region_name="us-east-1")
        print(event)
        refresh_token = event["RefreshToken"]
        try:
            response = client.admin_initiate_auth(
                UserPoolId=USER_POOL_ID,
                ClientId=CLIENT_ID,
                AuthFlow='REFRESH_TOKEN',
                AuthParameters={
                    'REFRESH_TOKEN': refresh_token
                }
            )
            print("------------")
            print(response)
        except client.exceptions.UserNotFoundException as e:
            return {"error": "User Does not exists"}
        except client.exceptions.NotAuthorizedException as e:
            return {"error": "User is not authorized"}
        except Exception as e:
            return {"error": str(e)}
        return {
            "IdToken" :  response["AuthenticationResult"].get("AccessToken"),
            "AccessToken" : response["AuthenticationResult"].get("AccessToken"),
            "TokenType" : response["AuthenticationResult"].get("TokenType")
        }

    except Exception as e:
        return {'statusCode': 500, "Error": "get refresh token failed"}
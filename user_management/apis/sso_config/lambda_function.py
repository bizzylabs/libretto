import json
import os
from base64 import b64encode as enc

import boto3

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
data = json.loads(secret_response["SecretString"])


def lambda_handler(event, context):
    try:
        if not data.get("IsActive", None):
            return [{"isSsoEnabled": False}]
        authorize_url = data["AuthorizeUrl"].format(data["CognitoDomain"], data["address"], data["client_id"])
        token_url = data["TokenUrl"].format(data["CognitoDomain"])
        output = {
            "label1": authorize_url,
            "_id": data["client_id"],
            "value": data["Label"],
            "isSsoEnabled": True,
            "label2": token_url,
            "Redirect": data["address"]
        }
        if data.get("Lform", None):
            output["Lform"] = False
        else:
            output["Lform"] = True

        encoded_string = enc(json.dumps(output).encode('latin1')).decode("latin1")

        return {
            "statusCode": 200,
            "result": encoded_string
        }
    except Exception as e:
        print(e)
        return {
            "statusCode": 500,
            "Error": "Unknown Error "
        }

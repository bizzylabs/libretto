import json
import os
from datetime import datetime
from base64 import b64decode, b64encode
import boto3
from pymongo import MongoClient
import traceback
import time
from datadog_lambda.metric import lambda_metric

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def initiate_auth(client, username, password):
    try:
        resp = client.initiate_auth(
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': username,
                'PASSWORD': password
            },
            ClientId=CLIENT_ID
        )
    except client.exceptions.NotAuthorizedException:
        return None, "The username or password is incorrect"
    except client.exceptions.UserNotConfirmedException:
        return None, "User is not confirmed"
    except Exception as e:
        traceback.print_exc()
        return None, e.__str__()
    return resp, None


def sign_in(event, context):
    try:
        permissions_dict = {
            "ReviewResults": {
                "RuleSetNames": [],
                "View": False,
                "Export": False,
                "Create/Edit": False,
                "GrantAccessFalg": False,
            },
            "RuleLibrary": {"Create/Edit": False},
            "Scheduler": False,
            "UserManagement": False,
            "LoanHistory": {
                "View": False,
                "Export": False
            }
        }
        client = boto3.client('cognito-idp', region_name="us-east-1")
        group = ["MEMBER"]

        if event["body"].get("result") is None:
            return {"error": True, "success": False, "message": f"Data missing", "data": None}
        else:
            try:
                event["body"] = eval(b64decode(event["body"]["result"]).decode('utf-8'))
            except Exception as e:
                return {"error": True, "success": False, "message": f"Data missing", "data": None}

        for field in ["username", "password"]:
            if event["body"].get(field) is None:
                return {"error": True, "success": False, "message": f"{field} is required", "data": None}
        username = event["body"]["username"]
        password = event["body"]["password"]

        if not str(username).strip() or not str(password).strip():
            return {"error": True, "success": False, "message": "The username or password is incorrect", "data": None}

        sign_in.resp, msg = initiate_auth(client, username, password)
        if msg != None:
            return {'message': msg,
                    "error": True, "success": False, "data": None}
        if sign_in.resp.get("AuthenticationResult"):
            group_result = client.admin_list_groups_for_user(Username=event["body"]["username"],
                                                             UserPoolId=USER_POOL_ID)
            groups = group_result.get("Groups", "")
            data = client.admin_get_user(
                UserPoolId=USER_POOL_ID,
                Username=event["body"]["username"]
            )
            username = data["Username"]
            for details in data["UserAttributes"]:
                if details["Name"] == "custom:FirstName":
                    first_name = details["Value"]
                if details["Name"] == "custom:LastName":
                    last_name = details["Value"]

            if last_name == "Lastname":
                last_name = ""
            if first_name == "Firstname":
                first_name = ""
            if groups:
                group = [item["GroupName"] for item in groups]
                # print(group)
                temp_lis = []
                for item in group:
                    access = db.UserRolePermissions.find_one({"RoleName": item})
                    # print(access)
                    access_review_result = access["ReviewResults"]
                    access_loan_history_result = access["LoanHistory"]
                    temp_lis.extend(access_review_result["RuleSetNames"])
                    if access_review_result["View"]:
                        permissions_dict["ReviewResults"]["View"] = True
                    if access_review_result["Export"]:
                        permissions_dict["ReviewResults"]["Export"] = True
                    if access_review_result["GrantAccessFalg"]:
                        permissions_dict["ReviewResults"]["GrantAccessFalg"] = True
                    # Defaulting to True for ReviewResults["Create/Edit"] to handle existing roles in DB
                    permissions_dict["ReviewResults"]["Create/Edit"] = access_review_result.get("Create/Edit", True)
                    if access["RuleLibrary"]["Create/Edit"]:
                        permissions_dict["RuleLibrary"]["Create/Edit"] = True
                    if access["Scheduler"]:
                        permissions_dict["Scheduler"] = True
                    if access["UserManagement"]:
                        permissions_dict["UserManagement"] = True
                    if access_loan_history_result["View"]:
                        permissions_dict["LoanHistory"]["View"] = True
                    if access_loan_history_result["Export"]:
                        permissions_dict["LoanHistory"]["Export"] = True

                permissions_dict["ReviewResults"]["RuleSetNames"] = list(set(temp_lis))

            email = ""
            for attribute in client.get_user(AccessToken=sign_in.resp["AuthenticationResult"]["AccessToken"])[
                'UserAttributes']:
                if attribute["Name"] == "email":
                    email = attribute["Value"]
            ip = event["context"].get("source-ip", None)
            browser = event["context"].get("user-agent", None)
            log = {"action": "login", "username": username, "email": email, "ip": ip,
                   "browser": browser, "timestamp": datetime.now()}
            db.BussinessAuditLogs.insert_one(log)
            lambda_metric(
                metric_name='bizzy.users',
                value=1,
                timestamp=int(time.time()),  # optional, must be within last 20 mins
                tags=["action:login", "username:" + username]
            )
            username = log["username"]
            request_type = log["action"]

            enable_filter = db.UserFilters.find_one({"UserName": username})
            if enable_filter:
                enable_filter_value = enable_filter["EnableFilter"]
            else:
                enable_filter_value = False
            permissions_dict["EnableFilter"] = enable_filter_value
            should_prompt_mfa = should_prompt_mfa_setup(client)
            env_data = db.ApplicationDetails.find_one({})
            if env_data:
                env = env_data["Environment"]
            else:
                env = "uat"
            result = {'message': "success",
                      "error": False,
                      "success": True,
                      "data": {
                          "refresh_token": sign_in.resp["AuthenticationResult"]["RefreshToken"],
                          "access_token": sign_in.resp["AuthenticationResult"]["AccessToken"],
                          "token_type": sign_in.resp["AuthenticationResult"]["TokenType"],
                          "mfa_flag": should_prompt_mfa
                      },
                      "group": group,
                      "permissions": permissions_dict,
                      "username": username,
                      "first_name": first_name,
                      "last_name": last_name,
                      "Environment": env
                      }
            sign_in.encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            return {"result": sign_in.encoded_string}
        # this code block is relevant only when MFA is enabled
        elif 'Session' in sign_in.resp:
            result = {"error": False,
                      "success": True,
                      "data": {"session": sign_in.resp['Session'], "challenge_name": sign_in.resp['ChallengeName'],
                               "mfa_flag": False},
                      "message": "prompt user to verify MFA"}
            encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            return {"result": encoded_string}
        else:
            return {"error": True,
                    "success": False,
                    "data": {
                    },
                    "message": None}
    except Exception as err:
        return {'statusCode': 500, "Error": "auth sign_in failed"}


def should_prompt_mfa_setup(cognito_client):
    user_pool_resp = cognito_client.get_user_pool_mfa_config(UserPoolId=USER_POOL_ID)
    should_prompt_mfa = False
    if user_pool_resp \
            and user_pool_resp.get("MfaConfiguration") == 'OPTIONAL' \
            and user_pool_resp["SoftwareTokenMfaConfiguration"].get("Enabled", False) is True:
        should_prompt_mfa = True
    return should_prompt_mfa

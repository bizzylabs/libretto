import json
import boto3
from pymongo import MongoClient
from base64 import b64encode
import os

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]

secret = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_ID"])
secret_dict = json.loads(secret_response["SecretString"])
client = MongoClient(
    secret_dict["mongo_str"] + secret_dict["user_name"] + ":" + secret_dict["password"] + "@" + secret_dict[
        "host"] + "/" +
    secret_dict["db_name"] + secret_dict["mongo_tag"])
db = client[secret_dict["db_name"]]

client = boto3.client('cognito-idp', region_name="us-east-1")


def lambda_handler(event, context):
    try:
        user_roles = event["body"].get("role", [])
        access_token = event["body"].get("access_token", None)
        permissions_dict = {
            "ReviewResults": {
                "RuleSetNames": [],
                "View": False,
                "Export": False,
                "Create/Edit": False,
                "GrantAccessFalg": False,
            },
            "RuleLibrary": {"Create/Edit": False},
            "Scheduler": False,
            "UserManagement": False,
            "LoanHistory": {
                "View": False,
                "Export": False
            }
        }

        if not (user_roles or access_token):
            return {"status": 500, "body": "Missing parameter"}
        data = client.get_user(AccessToken=access_token)
        username = data["Username"]
        email = ""
        last_name = ""
        first_name = ""
        for details in data["UserAttributes"]:
            if details["Name"] == "custom:FirstName":
                first_name = details["Value"]
            if details["Name"] == "custom:LastName":
                last_name = details["Value"]
            if details["Name"] == "email":
                email = details["Value"]

        if last_name == "Lastname":
            last_name = ""
        if first_name == "Firstname":
            first_name = ""

        temp_lis = []
        for item in user_roles:
            access = db.UserRolePermissions.find_one({"RoleName": item})
            # print(access)
            access_review_result = access["ReviewResults"]
            access_loan_history_result = access["LoanHistory"]
            temp_lis.extend(access_review_result["RuleSetNames"])
            if access_review_result["View"]:
                permissions_dict["ReviewResults"]["View"] = True
            if access_review_result["Export"]:
                permissions_dict["ReviewResults"]["Export"] = True
            if access_review_result["GrantAccessFalg"]:
                permissions_dict["ReviewResults"]["GrantAccessFalg"] = True
            permissions_dict["ReviewResults"]["Create/Edit"] = access_review_result.get("Create/Edit", True)
            if access["RuleLibrary"]["Create/Edit"]:
                permissions_dict["RuleLibrary"]["Create/Edit"] = True
            if access["Scheduler"]:
                permissions_dict["Scheduler"] = True
            if access["UserManagement"]:
                permissions_dict["UserManagement"] = True
            if access_loan_history_result["View"]:
                permissions_dict["LoanHistory"]["View"] = True
            if access_loan_history_result["Export"]:
                permissions_dict["LoanHistory"]["Export"] = True

        permissions_dict["ReviewResults"]["RuleSetNames"] = sorted(list(set(temp_lis)))
        enable_filter = db.UserFilters.find_one({"UserName": username})
        if enable_filter:
            enable_filter_value = enable_filter["EnableFilter"]
        else:
            enable_filter_value = False
        permissions_dict["EnableFilter"] = enable_filter_value
        env_data = db.ApplicationDetails.find_one({})
        if env_data:
            env = env_data["Environment"]
        else:
            env = "uat"

        result = {"statusCode": 200,
                  "permissions": permissions_dict,
                  "username": username,
                  "first_name": first_name,
                  "last_name": last_name,
                  "Environment": env
                  }
        encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
        return {"result": encoded_string}
    except Exception as err:
        print(err)
        return {'statusCode': 500, "error": "Getting user permission using user role failed:"}

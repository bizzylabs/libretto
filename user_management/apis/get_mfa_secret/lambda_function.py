import json
import os

import boto3
from pymongo import MongoClient

client = boto3.client("secretsmanager")

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
        mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict[
            "host"] + "/" + mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]


def lambda_handler(event, context):
    try:
        """
        :param event: access_token (Auth token generated in UI once user has signed-in)
        :param context:
        :return: secret_code to set-up QR code in TOTP app
        """
        access_token = event["body"].get("access_token", None)
        if access_token is None:
            return {"statusCode": 400, "error": "access_token is missing"}
        cognito_client = boto3.client('cognito-idp')
        try:
            response = cognito_client.associate_software_token(AccessToken=access_token)
            secret_code = response.get("SecretCode")

            application_config = db.ApplicationDetails.find_one({})
            client_name = application_config["Client"] + "-" + application_config["Environment"]
            qr_code = f"otpauth://totp/{client_name}?secret={secret_code}"
            user_name = event.get("context", {}).get("username", None)
            if user_name is not None:
                qr_code = f"otpauth://totp/{user_name}%40{client_name}?secret={secret_code}"

            return {
                "statusCode": 200,
                "result":
                    {
                        "secret_code": qr_code
                    }
            }
        except cognito_client.exceptions.SoftwareTokenMFANotFoundException as e:
            return {
                "statusCode": 400,
                "message": "MFA is not enabled for the client/user pool"
            }
        except cognito_client.exceptions.NotAuthorizedException as e:
            return {
                "statusCode": 400,
                "message": "User is not authorized to access the requested resource"
            }
        except Exception as e:
            return {
                "statusCode": 400,
                "message": e.__str__()
            }

    except Exception as err:
        return {'statusCode': 500, "Error": "get mfa secret failed"}
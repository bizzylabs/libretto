import json
import os
from datetime import datetime
from base64 import b64decode, b64encode

import boto3
# from log_queue import push_log
from pymongo import MongoClient

client = boto3.client("secretsmanager", region_name="us-east-1")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]

mongo = client.get_secret_value(SecretId=os.environ["SECRET_ID"])
mongo_dict = json.loads(mongo["SecretString"])
mongo_client = MongoClient(
    mongo_dict["mongo_str"] + mongo_dict["user_name"] + ":" + mongo_dict["password"] + "@" + mongo_dict["host"] + "/" +
    mongo_dict["db_name"] + mongo_dict["mongo_tag"])
db = mongo_client[mongo_dict["db_name"]]

client = boto3.client('cognito-idp', region_name="us-east-1")


def sign_in(event, context):
    try:
        if event["body"].get("result") is None:
            return {"error": True, "success": False, "message": f"Data missing", "data": None}
        else:
            try:
                event["body"] = eval(b64decode(event["body"]["result"]).decode('utf-8'))
            except Exception as e:
                return {"error": True, "success": False, "message": f"Data missing", "data": None}

        access_token = event["body"].get("AccessToken", None)
        if not access_token:
            return {"error": True,
                    "success": False,
                    "message": "Auth Missing"
                    }
        permissions_dict = {
            "ReviewResults": {
                "RuleSetNames": [],
                "View": False,
                "Export": False,
                "Create/Edit": False,
                "GrantAccessFalg": False,
            },
            "RuleLibrary": {"Create/Edit": False},
            "Scheduler": False,
            "UserManagement": False,
            "LoanHistory": {
                "View": False,
                "Export": False
            }
        }

        email = ""
        username = ""
        last_name = ""
        first_name = ""
        user_prop = client.get_user(AccessToken=access_token)
        username = user_prop.get("Username", "")
        for attribute in user_prop['UserAttributes']:
            if attribute["Name"] == "email":
                email = attribute["Value"]
            if attribute["Name"] == "custom:LastName":
                last_name = attribute["Value"]
            if attribute["Name"] == "custom:FirstName":
                first_name = attribute["Value"]

        if last_name == "Lastname":
            last_name = ""
        if first_name == "Firstname":
            first_name = ""
        group_result = client.admin_list_groups_for_user(Username=event["context"]["username"], UserPoolId=USER_POOL_ID)
        groups = group_result.get("Groups", "")
        if groups:
            group = [item["GroupName"] for item in groups]
            temp_lis = []
            for item in group:
                access = db.UserRolePermissions.find_one({"RoleName": item})
                access_review_result = access["ReviewResults"]
                access_loan_history_result = access["LoanHistory"]
                temp_lis.extend(access_review_result["RuleSetNames"])
                if access_review_result["View"]:
                    permissions_dict["ReviewResults"]["View"] = True
                if access_review_result["Export"]:
                    permissions_dict["ReviewResults"]["Export"] = True
                if access_review_result["GrantAccessFalg"]:
                    permissions_dict["ReviewResults"]["GrantAccessFalg"] = True
                if access_review_result["Create/Edit"]:
                    permissions_dict["ReviewResults"]["Create/Edit"] = True
                if access["RuleLibrary"]["Create/Edit"]:
                    permissions_dict["RuleLibrary"]["Create/Edit"] = True
                if access["Scheduler"]:
                    permissions_dict["Scheduler"] = True
                if access["UserManagement"]:
                    permissions_dict["UserManagement"] = True
                if access_loan_history_result["View"]:
                    permissions_dict["LoanHistory"]["View"] = True
                if access_loan_history_result["Export"]:
                    permissions_dict["LoanHistory"]["Export"] = True

            permissions_dict["ReviewResults"]["RuleSetNames"] = list(set(temp_lis))
            ip = event["context"].get("source-ip", None)
            browser = event["context"].get("user-agent", None)
            log = {"action": "login", "username": username, "email": email, "ip": ip,
                   "browser": browser, "timestamp": datetime.now()}
            # push_log.push(log)
            db.BussinessAuditLogs.insert_one(log)
            username = log["username"]
            request_type = log["action"]

            enable_filter = db.UserFilters.find_one({"UserName": username})
            if enable_filter:
                enable_filter_value = enable_filter["EnableFilter"]
            else:
                enable_filter_value = False
            permissions_dict["EnableFilter"] = enable_filter_value
            env_data = db.ApplicationDetails.find_one({})

            if env_data:
                env = env_data["Environment"]
            else:
                env = "uat"
            result = {"message": "success",
                      "error": False,
                      "success": True,
                      "group": group,
                      "permissions": permissions_dict,
                      "username": username,
                      "first_name": first_name,
                      "last_name": last_name,
                      "Environment": env
                      }
            sign_in.encoded_string = b64encode(json.dumps(result).encode('latin1')).decode("latin1")
            return {"result": sign_in.encoded_string}
    except Exception as e:
        print(e)
        return {
            "message": "Unknown Error",
            "error": True,
            "success": False
        }

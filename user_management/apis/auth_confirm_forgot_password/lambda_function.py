import json
import boto3
import os

secret = boto3.client("secretsmanager")
secret_response = secret.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])
secret_dict = json.loads(secret_response["SecretString"])
CLIENT_ID = secret_dict["client_id"]


def confirm_password(event, context):
    try:
        client = boto3.client("cognito-idp", region_name="us-east-1")
        try:
            event = event["body"]
        except Exception as e:
            event = event
        try:
            username = event["username"]
            password = event["password"]
            code = event["code"]
            res = client.confirm_forgot_password(
                ClientId=CLIENT_ID,
                Username=username,
                ConfirmationCode=code,
                Password=password,
            )
        except client.exceptions.UserNotFoundException as e:
            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Username doesnt exists"}

        except client.exceptions.CodeMismatchException as e:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "Invalid Verification code"}

        except client.exceptions.NotAuthorizedException as e:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": "User is already confirmed"}

        except Exception as e:

            return {"error": True,
                    "success": False,
                    "data": None,
                    "message": f"Password must be minimum 8 characters in length and must contain uppercase letters, lowercase letters, numbers and special characters."}
        return {"error": False,
                "success": True,
                "message": f"Password has been changed successfully",
                "data": res}
    except Exception as err:
        return {'statusCode': 500, "Error": "auth confirm forgot password failed"}

import json
import os

import boto3

client = boto3.client("secretsmanager")
secret_response = client.get_secret_value(SecretId=os.environ["SECRET_COGN_ID"])

secret = json.loads(secret_response["SecretString"])
USER_POOL_ID = secret["user_pool_id"]
CLIENT_ID = secret["client_id"]


def lambda_handler(event, context):
    try:
        for field in ["username"]:
            if event["body"].get(field) is None:
                return {"statusCode": 400, "message": f"{field} is required"}

        username = event["body"].get("username", None)
        cognito_client = boto3.client('cognito-idp')
        try:
            cognito_client.admin_set_user_mfa_preference(

                SoftwareTokenMfaSettings={
                    'Enabled': False,
                    'PreferredMfa': False
                },
                Username=username,
                UserPoolId=USER_POOL_ID
            )
            cognito_client.admin_user_global_sign_out(UserPoolId=USER_POOL_ID,
                                                      Username=username)
            return {
                "statusCode": 200,
                "message": "MFA successfully reset"
            }
        except cognito_client.exceptions.UserNotFoundException:
            return {
                "StatusCode": 400,
                "message": "User does not exist, please enter correct details"
            }
    except Exception as err:
        return {'statusCode': 500, "Error": "admin reset mfa failed"}
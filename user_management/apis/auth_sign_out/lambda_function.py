import boto3
from datetime import datetime
from log_queue import push_log
import traceback
import time
from datadog_lambda.metric import lambda_metric
client = boto3.client("cognito-idp", region_name="us-east-1")


def make_current_refresh_token_invalid(access_token):
    try:
        client.global_sign_out(AccessToken=access_token)
    except client.exceptions.UserNotFoundException as e:
        return {"error": "User Does not exists"}
    except client.exceptions.NotAuthorizedException as e:
        return {"error": "Access Token has already been revoked"}
    except Exception as e:
        return {"error": ""}


def lambda_handler(event, context):
    try:
        access_token = ""
        try:
            access_token = event["body"]["AccessToken"]
        except:
            return {"result": "Not enough parameters for request.", "status": 400}

        username = event["context"].get("username", None)
        email = event["context"].get("email", None)
        ip = event["context"].get("source-ip", None)
        browser = event["context"].get("user-agent", None)
        request_type = event["body"].get("RequestType", "")
        # make current refresh token invalid
        make_current_refresh_token_invalid(access_token)
        if request_type not in ["timeout", "logout"]:
            request_type = "timeout/logout"
        log = {"action": request_type, "username": username, "email": email, "ip": ip,
               "browser": browser, "timestamp": str(datetime.now())}

        try:
            push_log.push(log)
            lambda_metric(
                metric_name='bizzy.users',
                value= 1,
                timestamp=int(time.time()), # optional, must be within last 20 mins
                tags=["action:logout", "username:" + username]
                )
        except Exception as e:
            traceback.print_exc()
            return {"error": str(e)}
        return {"result": "User signed out", "status": 200}
        # return {"result": "User signed out", "status": 200, "another": client.get_user(AccessToken= access_token)}

    except Exception as err:
        return {'statusCode': 500, "Error": "auth sign out failed"}

import boto3
from botocore.exceptions import ClientError


def verify_mfa_setup(cognito_client, access_token, user_code):
    try:
        response = cognito_client.verify_software_token(
            AccessToken=access_token,
            UserCode=user_code
        )
        if response.get("Status") == "SUCCESS":
            enable_mfa_response_ign = cognito_client.set_user_mfa_preference(
                SoftwareTokenMfaSettings={
                    'Enabled': True,
                    'PreferredMfa': True
                },
                AccessToken=access_token
            )
        return response, None
    except cognito_client.exceptions.CodeMismatchException:
        return None, "User code mismatch, please enter correct code"
    except cognito_client.exceptions.EnableSoftwareTokenMFAException:
        return None, "Code mismatch, please enter correct code"
    except cognito_client.exceptions.SoftwareTokenMFANotFoundException:
        return None, "Code does not match what server was expecting"
    except cognito_client.exceptions.InvalidUserPoolConfigurationException:
        return None, "User pool configuration invalid"
    except cognito_client.exceptions.InvalidParameterException:
        return None, "Invalid input entered, please enter valid code"
    except ClientError as e:
        error_msg = e.__str__()
        if e.response and 'Error' in e.response and 'Message' in e.response['Error']:
            error_msg = e.response['Error']['Message']
        return None, error_msg
    except Exception as e:
        return None, e.__str__()


def lambda_handler(event, context):
    """
    :param event: access_token & user_code(once user has scanned QR code in TOTP app)
    :param context:
    :return: status=SUCCESS or ERROR (SUCCESS if user_code entered by user was correct)
    """

    access_token = event["body"].get("access_token", None)
    user_code = event["body"].get("user_code", None)

    if access_token is None or user_code is None:
        return {
            "statusCode": 404,
            "message": "Access token or user code cannot be empty"
        }

    cognito_client = boto3.client('cognito-idp')
    response, error_message = verify_mfa_setup(cognito_client, access_token, user_code)
    if error_message is not None:
        return {
            "statusCode": 400,
            'message': error_message,
        }

    return {
        "statusCode": 200,
        "result":
            {
                "status": response.get("Status")
            }
    }
